//
//  RiskAppraiseNextController.h
//  华康通
//
//  Created by 雷雨 on 16/5/17.
//  Copyright © 2016年 华康集团. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RiskAppraiseNextController : UITableViewController

//记录问题的题号
@property (nonatomic, unsafe_unretained) NSInteger count;
//答案
@property (nonatomic, strong) NSString* selection;

@end
