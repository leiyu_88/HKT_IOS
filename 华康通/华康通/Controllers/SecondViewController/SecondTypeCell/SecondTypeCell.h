//
//  SecondTypeCell.h
//  华康通
//
//  Created by leiyu on 16/10/25.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecondTypeCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIButton *leftButton;
@property (strong, nonatomic) IBOutlet UIButton *rightButton;

@end
