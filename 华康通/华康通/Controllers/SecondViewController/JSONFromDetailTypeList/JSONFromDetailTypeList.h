//
//  JSONFromDetailTypeList.h
//  华康通
//
//  Created by leiyu on 16/10/26.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSONFromDetailTypeList : NSObject

//产品类型id
@property (nonatomic,strong) NSString* product_type_id;
@property (nonatomic,strong) NSString* product_type_name;
@property (nonatomic,strong) NSString* product_type_logo;
@property (nonatomic,strong) NSArray* detailTypeObjectList;

+(id)detailTypeListWithJSON:(NSDictionary*)json;

@end
