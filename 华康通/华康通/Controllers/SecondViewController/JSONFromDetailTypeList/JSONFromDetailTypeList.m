//
//  JSONFromDetailTypeList.m
//  华康通
//
//  Created by leiyu on 16/10/26.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import "JSONFromDetailTypeList.h"

@implementation JSONFromDetailTypeList

-(id)initWithJSON:(NSDictionary*)json
{
    if (self = [super init])
    {
        if ([json[@"product_type_name"] isKindOfClass:[NSNull class]] || !json[@"product_type_name"])
        {
            self.product_type_name=@"";
        }
        else
        {
            self.product_type_name=json[@"product_type_name"];
        }
        
        if ([json[@"product_type_logo"] isKindOfClass:[NSNull class]] || !json[@"product_type_logo"])
        {
            self.product_type_logo=@"";
        }
        else
        {
            self.product_type_logo=json[@"product_type_logo"];
        }
        
        if ([json[@"product_type_id"] isKindOfClass:[NSNull class]] || !json[@"product_type_id"])
        {
            self.product_type_id=@"";
        }
        else
        {
            self.product_type_id=json[@"product_type_id"];
        }
        
        if ([json[@"detailTypeObjectList"] isKindOfClass:[NSNull class]] || !json[@"detailTypeObjectList"])
        {
            self.detailTypeObjectList=@[];
        }
        else
        {
            self.detailTypeObjectList=json[@"detailTypeObjectList"];
        }
    }
    return self;
}

+(id)detailTypeListWithJSON:(NSDictionary*)json
{
    return [[self alloc]initWithJSON:json];
}

@end
