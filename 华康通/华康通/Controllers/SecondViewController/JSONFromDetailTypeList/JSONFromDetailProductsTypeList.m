//
//  JSONFromDetailProductsTypeList.m
//  华康通
//
//  Created by leiyu on 16/11/1.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import "JSONFromDetailProductsTypeList.h"

@implementation JSONFromDetailProductsTypeList

-(id)initWithJSON:(NSDictionary*)json
{
    if (self = [super init])
    {
        if ([json[@"product_detailtype_id"] isKindOfClass:[NSNull class]] || !json[@"product_detailtype_id"])
        {
            self.product_detailtype_id=@"";
        }
        else
        {
            self.product_detailtype_id=json[@"product_detailtype_id"];
        }
        
        if ([json[@"product_detailtype_name"] isKindOfClass:[NSNull class]] || !json[@"product_detailtype_name"])
        {
            self.product_detailtype_name=@"";
        }
        else
        {
            self.product_detailtype_name=json[@"product_detailtype_name"];
        }
        
        if ([json[@"product_type_id"] isKindOfClass:[NSNull class]] || !json[@"product_type_id"])
        {
            self.product_type_id=@"";
        }
        else
        {
            self.product_type_id=json[@"product_type_id"];
        }

    }
    return self;
}

+(id)detailTypeListWithJSON:(NSDictionary*)json
{
    return [[self alloc]initWithJSON:json];
}

@end
