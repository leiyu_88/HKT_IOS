//
//  JSONFromBrandList.h
//  华康通
//
//  Created by leiyu on 16/11/7.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSONFromBrandList : NSObject


@property (nonatomic,strong) NSString* brand_id;
@property (nonatomic,strong) NSString* brand_name;
@property (nonatomic,strong) NSString* brand_logo;

+(id)brandWithJSON:(NSDictionary*)json;

@end
