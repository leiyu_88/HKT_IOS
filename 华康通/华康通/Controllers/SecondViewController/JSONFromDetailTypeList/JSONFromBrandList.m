//
//  JSONFromBrandList.m
//  华康通
//
//  Created by leiyu on 16/11/7.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import "JSONFromBrandList.h"

@implementation JSONFromBrandList

-(id)initWithJSON:(NSDictionary*)json
{
    if (self = [super init])
    {
        if ([json[@"brand_id"] isKindOfClass:[NSNull class]] || !json[@"brand_id"])
        {
            self.brand_id=@"";
        }
        else
        {
            self.brand_id=json[@"brand_id"];
        }
        
        if ([json[@"brand_name"] isKindOfClass:[NSNull class]] || !json[@"brand_name"])
        {
            self.brand_name=@"";
        }
        else
        {
            self.brand_name=json[@"brand_name"];
        }
        
        if ([json[@"brand_logo"] isKindOfClass:[NSNull class]] || !json[@"brand_logo"])
        {
            self.brand_logo=@"";
        }
        else
        {
            self.brand_logo=json[@"brand_logo"];
        }
    }
    return self;
}

+(id)brandWithJSON:(NSDictionary*)json
{
    return [[self alloc]initWithJSON:json];
}

@end
