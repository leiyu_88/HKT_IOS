//
//  JSONFromDetailProductsTypeList.h
//  华康通
//
//  Created by leiyu on 16/11/1.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSONFromDetailProductsTypeList : NSObject

//产品类型id
@property (nonatomic,strong) NSString* product_detailtype_id;
@property (nonatomic,strong) NSString* product_detailtype_name;
@property (nonatomic,strong) NSString* product_type_id;

+(id)detailTypeListWithJSON:(NSDictionary*)json;

@end
