//
//  TRTypeArray.h
//  华康通
//
//  Created by leiyu on 16/10/26.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TRTypeArray : NSObject

//处理产品分类，重组数组
+ (NSArray*)getNewArrayWithArray:(NSArray*)arr;

@end
