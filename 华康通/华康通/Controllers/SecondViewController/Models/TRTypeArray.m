//
//  TRTypeArray.m
//  华康通
//
//  Created by leiyu on 16/10/26.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import "TRTypeArray.h"
#import "JSONFromDetailTypeList.h"

@implementation TRTypeArray

//处理产品分类，重组数组
+ (NSArray*)getNewArrayWithArray:(NSArray*)arr
{
    NSMutableArray *dateMutablearray = [NSMutableArray array];
    NSMutableArray *array = [NSMutableArray arrayWithArray:arr];
    for (int i = 0; i < array.count; i ++)
    {
        JSONFromDetailTypeList* list = array[i];
        NSString* string = [NSString stringWithFormat:@"%@",list.product_type_id];
        NSMutableArray *tempArray = [NSMutableArray array];
        [tempArray addObject:list];
        for (int j = i+1; j < array.count; j ++)
        {
            JSONFromDetailTypeList* list1 = array[j];
            NSString *jstring = [NSString stringWithFormat:@"%@",list1.product_type_id];
            if([jstring isEqualToString:string])
            {
                [tempArray addObject:list1];
//                [array removeObjectAtIndex:j]; //内循环做删除行为 导致总对比次数不完整 此动作应在循环外处理
            }
        }
        if ([tempArray count] > 0)
        {
            JSONFromDetailTypeList* myList = tempArray[0];
            NSDictionary* dic = @{[NSString stringWithFormat:@"%@",myList.product_type_id]:tempArray};
            [dateMutablearray addObject:dic];
            [array removeObjectsInArray:tempArray];
            i -= 1;    //去除重复数据 新数组开始遍历位置不变
        }
    }
    return [dateMutablearray copy];
}

@end
