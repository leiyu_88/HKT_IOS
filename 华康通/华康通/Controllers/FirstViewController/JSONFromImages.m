//
//  JSONFromImages.m
//  华康通
//
//  Created by  雷雨 on 2017/1/5.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import "JSONFromImages.h"

@implementation JSONFromImages

-(id)initWithJSON:(NSDictionary*)json
{
    if (self = [super init])
    {
        if ([json[@"target"] isKindOfClass:[NSNull class]] || !json[@"target"])
        {
            self.target=@"";
        }
        else
        {
            self.target=json[@"target"];
        }
        
        if ([json[@"gotoUrl"] isKindOfClass:[NSNull class]] || !json[@"gotoUrl"])
        {
            self.gotoUrl=@"";
        }
        else
        {
            self.gotoUrl=json[@"gotoUrl"];
        }
        
        if ([json[@"prictureId"] isKindOfClass:[NSNull class]] || !json[@"prictureId"])
        {
            self.prictureId=@"";
        }
        else
        {
            self.prictureId=json[@"prictureId"];
        }
        
        if ([json[@"prictureTitle"] isKindOfClass:[NSNull class]] || !json[@"prictureTitle"])
        {
            self.prictureTitle=@"";
        }
        else
        {
            self.prictureTitle=json[@"prictureTitle"];
        }
        
        if ([json[@"prictureDesc"] isKindOfClass:[NSNull class]] || !json[@"prictureDesc"])
        {
            self.prictureDesc=@"";
        }
        else
        {
            self.prictureDesc=json[@"prictureDesc"];
        }
        
        if ([json[@"prictureUrl"] isKindOfClass:[NSNull class]] || !json[@"prictureUrl"])
        {
            self.prictureUrl=@"";
        }
        else
        {
            self.prictureUrl=json[@"prictureUrl"];
        }
        
        if ([json[@"prictrueType"] isKindOfClass:[NSNull class]] || !json[@"prictrueType"])
        {
            self.prictrueType=@"";
            
        }
        else
        {
            self.prictrueType=json[@"prictrueType"];
        }
        
        if ([json[@"product_features"] isKindOfClass:[NSNull class]] || !json[@"product_features"])
        {
            self.product_features = @"";
            
        }
        else
        {
            self.product_features = json[@"product_features"];
        }
        
        if ([json[@"small_picture"] isKindOfClass:[NSNull class]] || !json[@"small_picture"])
        {
            self.small_picture = @"";
            
        }
        else
        {
            self.small_picture = json[@"small_picture"];
        }
        
    }
    return self;
}

+(id)getPicturesWithJSON:(NSDictionary*)json
{
    return [[self alloc]initWithJSON:json];
}

@end
