//
//  JSONFromImages.h
//  华康通
//
//  Created by  雷雨 on 2017/1/5.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSONFromImages : NSObject

@property (nonatomic,strong) NSString* target;
@property (nonatomic,strong) NSString* gotoUrl;
@property (nonatomic,strong) NSString* prictureId;
@property (nonatomic,strong) NSString* prictureTitle;
@property (nonatomic,strong) NSString* prictureDesc;
@property (nonatomic,strong) NSString* prictureUrl;
@property (nonatomic,strong) NSString* prictrueType;
@property (nonatomic,strong) NSString* product_features;
@property (nonatomic,strong) NSString* small_picture;


+(id)getPicturesWithJSON:(NSDictionary*)json;

@end
