//
//  BackOverburden.m
//  华康通
//
//  Created by  雷雨 on 2019/1/23.
//  Copyright © 2019年 com.huakang. All rights reserved.
//

#import "BackOverburden.h"

@interface BackOverburden()

@property (nonatomic, strong) UIView* backView;

@end

@implementation BackOverburden

- (UIView*)backView
{
    if (!_backView)
    {
        _backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, UISCREENWEITH, UISCREENHEIGHT)];
        _backView.backgroundColor = [UIColor blackColor];
        _backView.alpha = 0.5;
    }
    return _backView;
}

- (void)showView
{
    [[UIApplication sharedApplication].keyWindow addSubview:self.backView];
}

- (void)removeView
{
    [self.backView removeFromSuperview];
    self.backView = nil;
}

@end
