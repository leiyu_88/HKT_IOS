//
//  BackOverburden.h
//  华康通
//
//  Created by  雷雨 on 2019/1/23.
//  Copyright © 2019年 com.huakang. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BackOverburden : NSObject


- (void)showView;
- (void)removeView;


@end

NS_ASSUME_NONNULL_END
