//
//  PostImageView.h
//  华康通
//
//  Created by  雷雨 on 2017/6/5.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import <Foundation/Foundation.h>

//回传上传图片成功后的data数据
typedef void(^ReturnJSONBlock)(NSData* data);
//回传失败返回err
typedef void(^ReturnJSONFailBlock)(NSError* error);

//下面俩个方法都是设置请求参数
static NSString *boundaryStr = @"--";
static NSString *uploadID = @"image";

@interface PostImageView : NSObject

@property (nonatomic, strong) NSString* randomIDStr;
@property (nonatomic, strong) NSString* pictureType;
@property (nonatomic, strong) NSString* customerId;
@property (nonatomic, strong) NSString* companyCode;
@property (nonatomic, strong) NSString* transType;
@property (nonatomic, strong) NSString* fileSuffix;

//网络传图到服务器
- (void)saveWithImage:(UIImage *)image withImageName:(NSString*)imageName withURL:(NSString*)urlStr withType:(NSString*)type success:(ReturnJSONBlock)dataBlock failure:(ReturnJSONFailBlock)errBlock;

@end
