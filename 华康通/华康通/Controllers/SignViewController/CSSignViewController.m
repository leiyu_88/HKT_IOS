

#import "CSSignViewController.h"
#import "SignViewController.h"

@interface CSSignViewController ()<UINavigationControllerDelegate,UIImagePickerControllerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *signImageView;
@property (weak, nonatomic) IBOutlet UIButton *signButton;

//拍照和选择相册传图
@property (strong, nonatomic) UIImagePickerController* pickerController;

@end

@implementation CSSignViewController

- (UIImagePickerController*)pickerController
{
    if (!_pickerController)
    {
        _pickerController = [[UIImagePickerController alloc]init];
    }
    return _pickerController;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"电子签名";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:LoadImage(@"back1") style:UIBarButtonItemStylePlain target:self action:@selector(popRootVC)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCamera target:self action:@selector(photo)];
    self.signImageView.layer.borderColor = KMainColor.CGColor;
    self.signImageView.layer.borderWidth = 1.0;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"CSSignViewController"];//("PageOne"为页面名称，可自定义)
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"CSSignViewController"];
}
//拍照
- (void)photo
{
    [SRActionSheet sr_showActionSheetViewWithTitle:@"请选择上传方式"
                                 cancelButtonTitle:@"取消"
                            destructiveButtonTitle:nil
                                 otherButtonTitles:@[@"拍照", @"相册"]
                                  selectSheetBlock:^(SRActionSheet *actionSheetView, NSInteger actionIndex) {
                                      switch (actionIndex)
                                      {
                                          case -1:
                                              return;
                                              break;
                                          case 1:
                                          {
                                              self.pickerController.view.backgroundColor = FFFFFFColor;
                                              self.pickerController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
                                              self.pickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                              self.pickerController.allowsEditing = YES;
                                              self.pickerController.delegate = self;
                                              [self presentViewController:self.pickerController animated:YES completion:nil];
                                          }
                                              break;
                                          case 0:
                                          {
                                              //自定义相机拍照
                                              SKFCamera *homec = [[SKFCamera alloc]init];
                                              __weak typeof(self)myself = self;
                                              homec.fininshcapture = ^(UIImage *ss){
                                                  if (ss)
                                                  {
                                                      //在这里获取裁剪后的照片
                                                      myself.signImageView.image = ss;
                                                  }
                                              } ;
                                              [self presentViewController:homec animated:NO completion:^{}];
                                          }
                                              break;
                                          default:
                                              break;
                                      }
                                  }];
}

#pragma mark - 拍照和选择相册的代理方法

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    //当用户选取完成后调用;
    UIImage* editedImage = info[UIImagePickerControllerEditedImage];
    self.signImageView.image = editedImage;
    self.pickerController.delegate = nil;
//    //下面是上传照片到后台（图片上传功能）
//    PostImageView* postIV = [[PostImageView alloc]init];
//    [postIV saveWithImage:editedImage withImageName:@"liudehua" withURL:POSTIMAGE_URL success:^(NSData *data) {
//        id result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [SVProgressHUD showSuccessWithStatus:@"上传成功..." maskType:SVProgressHUDMaskTypeNone];
//            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
//        });
//        [self.pickerController dismissViewControllerAnimated:YES completion:nil];
//        NSLog(@"上传图片后返回来的结果result = %@",result);
//    } failure:^(NSError *error) {
//        
//    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    //但用户取消选取时调用；
    self.pickerController.delegate = nil;
    [self.pickerController dismissViewControllerAnimated:YES completion:nil];
}

- (void)popRootVC
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)sign:(id)sender
{
    SignViewController *signVC = [[SignViewController alloc] init];
    signVC.signLineColorDic = @{@"red":@(101/255.0),@"green":@(102/255.0),@"blue":@(23/255.0),@"alpha":@1.0};
    [self.navigationController pushViewController:signVC animated:YES];
    [signVC signResultWithBlock:^(UIImage *signImage) {
        self.signImageView.image = signImage;
    }];
}

@end
