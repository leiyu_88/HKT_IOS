

#import "SignViewController.h"
#import "UIView+Helper.h"
#import "LYDrawingView.h"

@interface SignViewController ()
//@property(nonatomic, strong) UIImageView *signImageView;
@property (nonatomic, strong) LYDrawingView* signImageView;
@property (nonatomic, strong) UIView* backView;
//得到签名图片
@property (nonatomic, strong) UIImage* currectImage;
@property (nonatomic, copy) SignResult result;


@end

@implementation SignViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:LoadImage(@"back1") style:UIBarButtonItemStylePlain target:self action:@selector(popRootVC)];
    if (self.type)
    {
        if ([self.type isEqualToString:@"0"])
        {
            self.title = @"投保人签名";
        }
        else if ([self.type isEqualToString:@"1"])
        {
            self.title = @"被保人签名";
        }
        else if ([self.type isEqualToString:@"2"])
        {
            self.title = @"业务员签名";
        }
        else
        {
            self.title = @"代理人签名";
        }
    }
    else
    {
        self.title = @"电子签名";
    }
}

- (void)popRootVC
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"SignViewController"];//("PageOne"为页面名称，可自定义)
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"SignViewController"];
}

- (void)viewWillLayoutSubviews
{
    [self layoutSubviews];
}

- (NSDictionary *)RGBDictionaryByColor:(UIColor *)color
{
    CGFloat red = 0, green = 0, blue = 0, alpha = 0;
    if ([self respondsToSelector:@selector(getRed:green:blue:alpha:)])
    {
        [color getRed:&red green:&green blue:&blue alpha:&alpha];
    } else
    {
        const CGFloat *compoments = CGColorGetComponents(color.CGColor);
        red = compoments[0];
        green = compoments[1];
        blue = compoments[2];
        alpha = compoments[3];
    }
    return @{@"red":@(red), @"green":@(green), @"blue":@(blue), @"alpha":@(alpha)};
}

- (void)layoutSubviews
{
    self.backView = [[UIView alloc]initWithFrame:CGRectMake(0, ((UISCREENHEIGHT - 64 - 50 - (self.view.width * 3/4))/2) + 64, self.view.width, self.view.width * 3/4)];
    self.backView.backgroundColor = FFFFFFColor;
    [self.view addSubview:self.backView];
    
    //
    self.signImageView = [[LYDrawingView alloc]init];
    self.signImageView.x = 0;
    self.signImageView.y = ((UISCREENHEIGHT - 64 - 50 - (self.view.width * 3/4))/2) + 64;
    self.signImageView.width = self.view.width;
    self.signImageView.height = self.view.width * 3/4;
    [self.signImageView initDrawingView];
    self.signImageView.backgroundColor = ClEARColor;
    [self.view addSubview:self.signImageView];
    //
    UIButton *clearBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    clearBtn.frame = CGRectMake(0, self.view.frame.size.height - 50, UISCREENWEITH/2, 50);
    clearBtn.layer.borderColor = KMainColor.CGColor;
    clearBtn.layer.borderWidth = 1.0;
    NSString *title = @"清除";
    [clearBtn setTitle:title forState:UIControlStateNormal];
    [clearBtn setTitleColor:FFFFFFColor forState:UIControlStateNormal];
    [clearBtn addTarget:self action:@selector(clearSignAction:) forControlEvents:UIControlEventTouchUpInside];
    clearBtn.backgroundColor = KMainColor;
    [self.view addSubview:clearBtn];
    
    UIButton *signDone = [UIButton buttonWithType:UIButtonTypeCustom];
    signDone.frame = CGRectMake(UISCREENWEITH/2, self.view.frame.size.height - 50, UISCREENWEITH/2, 50);
    signDone.layer.borderColor = KMainColor.CGColor;
    signDone.layer.borderWidth = 1.0;
    [signDone setTitle:@"完成" forState:UIControlStateNormal];
    [signDone setTitleColor:KMainColor forState:UIControlStateNormal];
    [signDone addTarget:self action:@selector(signDoneAction:) forControlEvents:UIControlEventTouchUpInside];
    signDone.backgroundColor = FFFFFFColor;
    [self.view addSubview:signDone];
}

//判断弹出框
- (void)judeLoginWithMessage:(NSString*)message
{
    CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
    TextStatusView *customView  = [TextStatusView view];
    customView.frame = CGRectMake(10, 10, UISCREENWEITH - 80, ALERTVIEW_HEIGHT_B);
    customView.bottomLabel.text = message;
    [alertView setContainerView:customView];
    [alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"好的", nil]];
    [alertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex)
    {
        switch (buttonIndex)
        {
            case 0:
                return ;
                break;
            default:
                break;
        }
    }];
    [alertView show];
}

#pragma mark getter && setter

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)signResultWithBlock:(SignResult)result
{
    self.result = result;
}

- (void)signDoneAction:(UIButton *)sender
{
    [self.signImageView saveImage:^(UIImage *image ,BOOL isImage)
    {
        self.currectImage = image;
        if (isImage)
        {
            if (self.result)
            {
                NSLog(@"self.currectImage = %@",self.currectImage);
                self.result(self.currectImage);
            }
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            [self judeLoginWithMessage:@"您还没有进行签名，请先点击签名区域签名吧！"];
        }
    }];
}

- (void)clearSignAction:(UIButton *)sender
{
    [self.signImageView doBack];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


@end
