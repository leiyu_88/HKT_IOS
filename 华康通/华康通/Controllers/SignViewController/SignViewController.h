//
//  SignViewController.h
//  华康通
//
//  Created by  雷雨 on 2017/6/5.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef void(^SignResult)(UIImage *signImage);

@interface SignViewController : UIViewController

/**
 已签名的照片,跳转传入
 */
@property (nonatomic, strong) UIImage *signImage;

/**
 签名笔划颜色
 */
@property (nonatomic, strong) UIColor *signLineColor;
/**
 签名笔颜色各值参数字典
 */
@property (nonatomic, strong) NSDictionary *signLineColorDic;
/**
 签名笔划宽度
 */
@property (nonatomic, assign) CGFloat signLineWidth;

/**
 无签名时占位文字
 */
@property (nonatomic, copy) NSString *signPlaceHoalder;

/**
 电子签名种类
 */
@property (nonatomic, copy) NSString *type;


/**
 签名完成后的回调Block,里面有完成的签名图片
 
 @param result block
 */
- (void)signResultWithBlock:(SignResult)result;

@end
