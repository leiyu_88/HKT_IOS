//
//  LYDrawingView.h
//  华康通
//
//  Created by  雷雨 on 2017/7/19.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^SaveSuccessBlock)(UIImage* image,BOOL isImage);

@interface LYDrawingView : UIView

// 用来设置线条的颜色
@property (nonatomic, strong) UIColor *color;
// 用来设置线条的宽度
@property (nonatomic, assign) CGFloat lineWidth;
// 用来记录已有线条
@property (nonatomic, strong) NSMutableArray *allLines;
@property (nonatomic, strong) UILabel *placeHoalderLabel;
// 初始化相关参数
- (void)initDrawingView;
// back操作
- (void)doBack;
// 保存Image
- (void)saveImage:(SaveSuccessBlock)saveSuccessBlock;

@end
