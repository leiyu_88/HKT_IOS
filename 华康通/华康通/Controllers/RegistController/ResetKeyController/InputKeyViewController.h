//
//  InputKeyViewController.h
//  华康通
//
//  Created by leiyu on 16/9/13.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InputKeyViewController : UIViewController

//验证码
@property (nonatomic, strong) NSString* verificationCode;
//用户手机号码
@property (nonatomic, strong) NSString* mobile;

@end
