//
//  PhoneRegistController.h
//  华康通
//
//  Created by leiyu on 16/9/12.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger
{
    PushRegistVC,//注册
    PushResetKeyVC,//重设密码
} pushType;

@interface PhoneRegistController : UIViewController

@property (nonatomic, unsafe_unretained) pushType myPushType;
//手机号码
@property (nonatomic, strong) NSString* userPhone;

@end
