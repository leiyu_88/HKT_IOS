//
//  PhoneRegistController.m
//  华康通
//
//  Created by leiyu on 16/9/12.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import "PhoneRegistController.h"
#import "InputKeyViewController.h"
//#import "ResetKeyController.h"
#import "RecommendPhoneVC.h"
#import "ShowProvisionViewController.h"
#import "UMMobClick/MobClick.h"

static NSString* myStr;

@interface PhoneRegistController ()
@property (strong, nonatomic) IBOutlet UITextField *topTextField;
@property (strong, nonatomic) IBOutlet UITextField *bottomTextField;
@property (strong, nonatomic) IBOutlet UIButton *codeButton;
@property (strong, nonatomic) IBOutlet UILabel *coreLabel;
@property (strong, nonatomic) IBOutlet UILabel *showLoginLabel;
@property (strong, nonatomic) IBOutlet UIButton *nextButton;


//我已同意并阅读
@property (strong, nonatomic) IBOutlet UILabel *iComeToAgreementLabel;
@property (strong, nonatomic) IBOutlet UIButton *showAgreementVCButton;

//语音接收验证码
@property (strong, nonatomic) IBOutlet UIButton *vocieButton;
//验证码收不到label
@property (strong, nonatomic) IBOutlet UILabel *voiceLabel;

//放一个时间记录器 来记录 验证码的时间
@property (nonatomic, strong) NSTimer* timer;
//记录时间
@property (nonatomic, unsafe_unretained) NSInteger count;
//延迟请求后台数据的bool值
@property (nonatomic, unsafe_unretained) BOOL isSelected;

@property (nonatomic, strong) ZTVertyStatus* vertyStatus;
//后台返回回来的验证码
@property (nonatomic, strong) NSString* verificationCode;
//保存输入的手机号码
@property (nonatomic, strong) NSString* myUserPhone;

@end

@implementation PhoneRegistController


- (ZTVertyStatus*)vertyStatus
{
    if (!_vertyStatus)
    {
        _vertyStatus = [[ZTVertyStatus alloc]init];
    }
    return _vertyStatus;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    if (self.myPushType == PushRegistVC)
    {
        //注册
        [self.showAgreementVCButton setHidden:NO];
        [self.iComeToAgreementLabel setHidden:NO];
    }
    else if (self.myPushType == PushResetKeyVC)
    {
        //重置密码
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:LoadImage(@"back1") style:UIBarButtonItemStylePlain target:self action:@selector(popRootVC)];
        [self.showAgreementVCButton setHidden:YES];
        [self.iComeToAgreementLabel setHidden:YES];
        self.topTextField.text = self.userPhone;
        [self postPhoneCode];
        
    }
    self.isSelected = NO;
    self.showLoginLabel.layer.cornerRadius = 6.0;
    self.showLoginLabel.layer.masksToBounds = YES;
}

- (void)popRootVC
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.count = 90;
    [MobClick beginLogPageView:@"PhoneRegistController"];//("PageOne"为页面名称，可自定义)
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [MobClick endLogPageView:@"PhoneRegistController"];
}

- (void)viewDidDisappear:(BOOL)animated
{
    //停止计时器
    [self.timer invalidate];
    self.timer = nil;
    self.coreLabel.text = @"获取验证码";
    self.coreLabel.textColor = KMainColor;
    //重新获取按钮可以用
    self.codeButton.enabled = YES;
    [self.bottomTextField setText:nil];
    //隐藏语音接收验证码
    [self hideVoiceButton];
}

- (void)showAlertViewWithMsg:(NSString*)msg
{
    CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
    TextStatusView *customView  = [TextStatusView view];
    customView.frame = CGRectMake(10, 10, UISCREENWEITH - 80, ALERTVIEW_HEIGHT_B);
    customView.bottomLabel.text = msg;
    //    customView.layer.cornerRadius = 7.0;
    //    customView.layer.masksToBounds = YES;
    [alertView setContainerView:customView];
    [alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"确认", nil]];
    [alertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
        switch (buttonIndex)
        {
            case 0:
                [self.bottomTextField setText:nil];
                [self.bottomTextField becomeFirstResponder];
                break;
            default:
                return ;
                break;
        }
    }];
    [alertView show];
}
#pragma mark  //用户手机或工号是否注册验证
- (void)postPrama
{
    NSDictionary* json = @{@"platformType":@"1",
                           @"orderType":@"01",
                           @"requestService":@"checkRegister",
                           @"requestObject":@{@"customerName":self.topTextField.text,
                                              }
                           };
    //字符串转字符串
    NSString* str = [JSONToString dictionaryToJson:json];
    //转md5
    MyAdditions* md5 = [[MyAdditions alloc]init];
    md5.str = [NSString stringWithFormat:@"%@%@",KEY_POST,str];
    NSString* md5_Str = [md5 md5];
    NSString* url = [NSString stringWithFormat:@"%@&sign=%@",SERVER_URL,md5_Str];
    [self getUserPhoneWithURL:url withParam:json];
}

- (void)getUserPhoneWithURL:(NSString*)url withParam:(NSDictionary*)param
{
    
    HKTShowNetworkView* networking = [HKTShowNetworkView shareInstance];
    [networking showErrorWithSuccess:^{
        
        [XHNetworking POST:url parameters:param success:^(NSData *responseObject) {
            id json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
            NSLog(@"验证手机码是否已经注册json = %@",json);
            if ([json isKindOfClass:[NSDictionary class]] && json)
            {
                if ([json[@"resultCode"] isEqualToString:@"0"])
                {
                    
                    dispatch_async(dispatch_get_global_queue(0, 0), ^{
                        if (![json[@"responseObject"][@"bindMobile"] isEqualToString:@""])
                        {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [self showAlertViewWithMsg:@"您输入的手机号码已经注册!"];
                            });
                        }
                        else
                        {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                XYCustomStatusBar* statusBar = [[XYCustomStatusBar alloc]init];
                                [statusBar showStatusMessage:@"可以注册"];
                                //可以进入下一步
                                UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
                                RecommendPhoneVC* phoneAndKeyVC = [storyboard instantiateViewControllerWithIdentifier:@"RecommendPhoneVC"];
                                phoneAndKeyVC.verificationCode = self.verificationCode;
                                phoneAndKeyVC.mobile = self.topTextField.text;
                                [self.navigationController pushViewController:phoneAndKeyVC animated:YES];
                            });
                        }
                    });
                    
                    
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self showAlertViewWithMsg:@"未知错误!"];
                    });
                }
            }
        } failure:^(NSError *error) {
            NSLog(@"error.code = %@",error.userInfo);
            if (error.code == -1001)
            {
                NSLog(@"网络请求超时.....................");
            }
        }];
        
        
    } failure:^{
        //展示广告图
        NSLog(@"检测到设备无网络连接！！！");
    }];
    
}


#pragma mark  //发送验证码
- (void)postVerificationCodeWithURL:(NSString*)url withParam:(NSDictionary*)param
{
    
    HKTShowNetworkView* networking = [HKTShowNetworkView shareInstance];
    [networking showErrorWithSuccess:^{
        
        [XHNetworking POST:url parameters:param success:^(NSData *responseObject) {
            id json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
            NSLog(@"发送验证码json = %@",json);
            if ([json isKindOfClass:[NSDictionary class]] && json)
            {
                if ([json[@"responseObject"][@"result"] isEqualToString:@"0"])
                {
                    dispatch_async(dispatch_get_global_queue(0, 0), ^{
                        self.verificationCode = json[@"responseObject"][@"verificationCode"];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
                            TextStatusView *customView  = [TextStatusView view];
                            customView.frame = CGRectMake(10, 10, UISCREENWEITH - 80, ALERTVIEW_HEIGHT_B);
                            customView.bottomLabel.text = @"验证码已发送";
                            [alertView setContainerView:customView];
                            [alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"确认", nil]];
                            [alertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
                                switch (buttonIndex)
                                {
                                    case 0:
                                        [self.bottomTextField becomeFirstResponder];
                                        return ;
                                        break;
                                    default:
                                        return ;
                                        break;
                                }
                            }];
                            [alertView show];
                        });
                    });
                    
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self showAlertViewWithMsg:[NSString stringWithFormat:@"%@",json[@"responseObject"][@"msg"]]];
                    });
                }
            }
        } failure:^(NSError *error) {
            NSLog(@"error.code = %@",error.userInfo);
            if (error.code == -1001)
            {
                NSLog(@"网络请求超时.....................");
            }
        }];
        //每次获取验证码，保存当前输入的手机号码
        self.myUserPhone = self.topTextField.text;
        
    } failure:^{
        //展示广告图
        NSLog(@"检测到设备无网络连接！！！");
    }];
    
    
}

//使用语音接收验证码
- (IBAction)voiceVerify:(UIButton *)sender
{
    [self.topTextField resignFirstResponder];
    [self.bottomTextField resignFirstResponder];
    if (self.verificationCode)
    {
        NSString* operateType;
        if (self.myPushType == PushRegistVC)
        {
            operateType = @"1";
        }
        else if (self.myPushType == PushResetKeyVC)
        {
            operateType = @"3";
        }
        NSDictionary* json = @{@"platformType":@"1",
                               @"orderType":@"01",
                               @"requestService":@"ttsVerifyCodeCall",
                               @"requestObject":@{@"operateType":operateType,
                                                  @"operateCode":self.topTextField.text ,
                                                  @"validateCode":self.verificationCode,
                                                  @"mobile":self.topTextField.text
                                                  }
                               };
        //字符串转字符串
        NSString* str = [JSONToString dictionaryToJson:json];
        //转md5
        MyAdditions* md5 = [[MyAdditions alloc]init];
        md5.str = [NSString stringWithFormat:@"%@%@",KEY_POST,str];
        NSString* md5_Str = [md5 md5];
        NSLog(@"转md5之前时的字符串:%@",[NSString stringWithFormat:@"%@%@",KEY_POST,str]);
        NSString* url = [NSString stringWithFormat:@"%@&sign=%@",SERVER_URL,md5_Str];
        NSLog(@"语音接收验证码url = %@  json字典 ＝ %@",url,json);
        
        HKTShowNetworkView* networking = [HKTShowNetworkView shareInstance];
        [networking showErrorWithSuccess:^{
            
            [XHNetworking POST:url parameters:json success:^(NSData *responseObject) {
                id json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
                NSLog(@"语音接收验证码json = %@",json);
                if ([json isKindOfClass:[NSDictionary class]] && json)
                {
                    
                    dispatch_async(dispatch_get_global_queue(0, 0), ^{
                        if ([json[@"responseObject"][@"result"] isEqualToString:@"0"])
                        {
                            NSLog(@"语音接收验证码成功");
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [self showAlertViewWithMsg:[NSString stringWithFormat:@"语音发送验证码成功，请留意接听"]];
                            });
                        }
                        else
                        {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [self showAlertViewWithMsg:[NSString stringWithFormat:@"%@",json[@"errorMessage"]]];
                            });
                        }
                    });
                    
                    
                }
            } failure:^(NSError *error) {
                NSLog(@"error.code = %@",error.userInfo);
                if (error.code == -1001)
                {
                    NSLog(@"网络请求超时.....................");
                }
            }];
        } failure:^{
            //展示广告图
            NSLog(@"检测到设备无网络连接！！！");
            [self showAlertViewWithMsg:[NSString stringWithFormat:@"检测到您的设备没有连接网络!"]];
        }];
    }
    else
    {
        [self showAlertViewWithMsg:[NSString stringWithFormat:@"服务端错误"]];
    }
}

//当点击获取验证码时显示语音接收验证码
- (void)showVoiceButton
{
    self.voiceLabel.hidden = NO;
    self.vocieButton.hidden = NO;
}
//隐藏语音接收验证码
- (void)hideVoiceButton
{
    self.voiceLabel.hidden = YES;
    self.vocieButton.hidden = YES;
}

//关闭键盘
- (IBAction)closeKeyboard:(UITextField *)sender
{
    [sender resignFirstResponder];
    //点next 进入密码输入
    [self.bottomTextField becomeFirstResponder];
    //执行
    [self getVertitionCode:self.codeButton];
}
- (IBAction)closeBottomKeyboard:(UITextField *)sender
{
    [self.topTextField resignFirstResponder];
    [self.bottomTextField resignFirstResponder];
     //点击密码键盘上的 go时  就会后台请求
    [self regist:self.nextButton];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

//时间纪录
- (void)recordTime:(NSTimer*)timer
{
    self.count--;
    self.coreLabel.text = [NSString stringWithFormat:@"重新发送(%ldS)",(long)self.count];
    self.coreLabel.textColor = LLCColor;
    if (self.count <= 0)
    {
        self.coreLabel.textColor = KMainColor;
        self.coreLabel.text = @"重新获取";
        self.count = 90;
        //停止计时器
        [self.timer invalidate];
        //重新获取按钮可以用
        self.codeButton.enabled = YES;
    }
    else
    {
        //重新获取按钮不可以用
        self.codeButton.enabled = NO;
    }
}

//重启计时器，并获取验证码
- (void)postPhoneCode
{
    self.codeButton.enabled = NO;
    //重启计时器
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(recordTime:) userInfo:nil repeats:YES];
//    [self.bottomTextField becomeFirstResponder];
    NSString* operateType;
    if (self.myPushType == PushRegistVC)
    {
        operateType = @"1";
    }
    else if (self.myPushType == PushResetKeyVC)
    {
        operateType = @"3";
    }
    NSDictionary* json = @{@"platformType":@"1",
                           @"orderType":@"01",
                           @"requestService":@"sendSmsVerifyCode",
                           @"requestObject":@{@"operateType":operateType,
                                              @"operateCode":self.topTextField.text ,
                                              @"recommendCode":@"nAduW",
                                              @"mobile":self.topTextField.text
                                              }
                           };
    //字符串转字符串
    NSString* str = [JSONToString dictionaryToJson:json];
    //转md5
    MyAdditions* md5 = [[MyAdditions alloc]init];
    md5.str = [NSString stringWithFormat:@"%@%@",KEY_POST,str];
    NSString* md5_Str = [md5 md5];
    NSLog(@"转md5之前时的字符串:%@",[NSString stringWithFormat:@"%@%@",KEY_POST,str]);
    NSString* url = [NSString stringWithFormat:@"%@&sign=%@",SERVER_URL,md5_Str];
    NSLog(@"发送验证码url = %@  json字典 ＝ %@",url,json);
    [self postVerificationCodeWithURL:url withParam:json];
}

//获取验证码
- (IBAction)getVertitionCode:(UIButton *)sender
{
    [self.topTextField resignFirstResponder];
    [self.bottomTextField resignFirstResponder];
    
    [self showVoiceButton];
    //如果有输入手机号码
    if (![self.topTextField.text isEqualToString:@""] && self.topTextField.text)
    {
        //验证手机号码的有效性
        if ([ExamineTool verityPhoneWithString:self.topTextField.text])
        {
            NSLog(@"手机号码填写正确!");
            [self postPhoneCode];
        }
        else
        {
//            [self presentAlerControllerWithMessage:@"您输入的手机号码有误" withTextField:self.topTextField];
            
            ShowAlertView(@"您输入的手机号码有误",self.topTextField);
        }
    }
    else
    {
//        [self presentAlerControllerWithMessage:@"您还未输入手机号码！" withTextField:self.topTextField];
        
        ShowAlertView(@"您还未输入手机号码！",self.topTextField);
    }
}

//按下去就变色
- (IBAction)showLabelColorChange:(UIButton *)sender
{
    if (![self.topTextField.text isEqualToString:@""] && ![self.bottomTextField.text isEqualToString:@""])
    {
        
        self.showLoginLabel.backgroundColor = [UIColor colorWithRed:236.0/255.0 green:22.0/255.0 blue:0.0 alpha:1.0];
        dispatch_time_t delay = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.2 * NSEC_PER_SEC));
        dispatch_after(delay, dispatch_get_main_queue(), ^{
            self.showLoginLabel.backgroundColor = [UIColor colorWithPatternImage:LoadImage(@"login_btn.png")];
        });
    }
    else
    {
        self.showLoginLabel.backgroundColor = [UIColor colorWithRed:236.0/255.0 green:22.0/255.0 blue:0.0 alpha:0.1];
    }
}

//系统提示框
- (void)presentAlerControllerWithMessage:(NSString*)message withTextField:(UITextField*)textField
{
    NSLog(@"%@",message);
    [self.view endEditing:YES];
    if (self.myPushType == PushRegistVC)
    {
        //如果是手机注册页面
        CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
        VertyStatusView *customView  = [VertyStatusView view];
        customView.frame = CGRectMake(10, 10, UISCREENWEITH - 80, ALERTVIEW_HEIGHT_A);
        customView.otherLabel.text = message;
        //    customView.layer.cornerRadius = 7.0;
        //    customView.layer.masksToBounds = YES;
        [alertView setContainerView:customView];
        [alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"放弃注册", @"重新输入", nil]];
        [alertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
            switch (buttonIndex)
            {
                case 0:
//                [textField becomeFirstResponder];
                break;
                default:
                [textField becomeFirstResponder];
                break;
            }
        }];
        [alertView show];
    }
    else if (self.myPushType == PushResetKeyVC)
    {
        //如果是密码重置页面
        CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
        VertyStatusView *customView  = [VertyStatusView view];
        customView.frame = CGRectMake(10, 10, UISCREENWEITH - 80, ALERTVIEW_HEIGHT_A);
        customView.otherLabel.text = message;
        [alertView setContainerView:customView];
        [alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"重新输入", @"取消", nil]];
        [alertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
            switch (buttonIndex)
            {
                case 0:
                [textField becomeFirstResponder];
                break;
                default:
                NSLog(@"取消操作" );
                break;
            }
        }];
        [alertView show];
    }
    
}

//注册或者重置密码
- (IBAction)regist:(UIButton *)sender
{
    NSLog(@"注册。。。。");
    if (![self.topTextField.text isEqualToString:@""] && ![self.bottomTextField.text isEqualToString:@""])
    {
        [self.view endEditing:YES];
        //验证码 要等与6位并为纯数字
        if (self.bottomTextField.text.length == 4 && [ExamineTool isPureNumandCharacters:self.bottomTextField.text])
        {
            if (self.isSelected)
            {
                return;
            }
            self.isSelected = YES;
            [self performSelector:@selector(timeEnough) withObject:nil afterDelay:1.2];
        }
        //非纯数字或不等于6位时
        else
        {
            __weak  PhoneRegistController *wself = self;
//            [self presentAlerControllerWithMessage:@"验证码输入位数不正确!" withTextField:wself.bottomTextField];
            ShowAlertView(@"验证码输入位数不正确!",wself.bottomTextField);
        }
    }
}
//查看<华康通用户协议>
- (IBAction)showAgreementVC:(UIButton *)sender
{
    ShowProvisionViewController* showProvisitonVC = [[ShowProvisionViewController alloc]initWithNibName:@"ShowProvisionViewController" bundle:nil];
    showProvisitonVC.provisionStr = @"huakangtongagreement";
    showProvisitonVC.titleStr = @"注册协议";
    [self.navigationController pushViewController:showProvisitonVC animated:YES];
}

//判断按钮背景的颜色
- (void)judeColor
{
    if (![self.topTextField.text isEqualToString:@""] && ![self.bottomTextField.text isEqualToString:@""])
    {
        self.showLoginLabel.backgroundColor = [UIColor colorWithPatternImage:LoadImage(@"login_btn.png")];
    }
    else
    {
        self.showLoginLabel.backgroundColor = [UIColor colorWithRed:236.0/255.0 green:22.0/255.0 blue:0.0 alpha:0.1];
    }
}

- (void)timeEnough
{
    if ([self.topTextField.text isEqualToString:self.myUserPhone])
    {
        if (self.verificationCode)
        {
            //如果填写的验证码和系统返回的验证码一样
            if ([self.bottomTextField.text isEqualToString:self.verificationCode])
            {
                if (self.myPushType == PushRegistVC)
                {
                    //注册
                    [self postPrama];
                }
                else if (self.myPushType == PushResetKeyVC)
                {
                    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
                    InputKeyViewController* resetVC = [storyboard instantiateViewControllerWithIdentifier:@"InputKeyVC"];
                    resetVC.verificationCode = self.verificationCode;
                    resetVC.mobile = self.topTextField.text;
                    [self.navigationController pushViewController:resetVC animated:YES];
                }
            }
            else
            {
                [self showAlertViewWithMsg:@"验证码输入有误，请重新输入"];
            }
        }
        else
        {
            [self showAlertViewWithMsg:[NSString stringWithFormat:@"请先获取验证码！"]];
            self.coreLabel.textColor = KMainColor;
            self.coreLabel.text = @"重新获取";
            self.count = 90;
            //停止计时器
            [self.timer invalidate];
            //重新获取按钮可以用
            self.codeButton.enabled = YES;
        }
    }
    else
    {
        [self showAlertViewWithMsg:[NSString stringWithFormat:@"验证码错误，请重新获取！"]];
        self.coreLabel.textColor = KMainColor;
        self.coreLabel.text = @"获取验证码";
        self.count = 90;
        
        //停止计时器
        [self.timer invalidate];
        //重新获取按钮可以用
        self.codeButton.enabled = YES;
    }
        //正式填写与后台的交互....
    NSLog(@"注册。。。。正式填写与后台的交互....");
    //    UIButton *btn = (UIButton *)[self.view viewWithTag:100];
    self.isSelected = NO;
}

#pragma mark - UITextFieldDelegate

//输入框文字改变的时候
- (IBAction)topTextChange:(UITextField *)sender
{
    if (sender.text.length == 11)
    {
        myStr = sender.text;
    }
    else if (sender.text.length > 11)
    {
        sender.text = myStr;
    }
    NSLog(@"最上面文字发生改变了");
    
    [self judeColor];
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if (self.myPushType == PushResetKeyVC)
    {
        if (textField == self.topTextField)
        {
            return NO;
        }
        else
        {
            return YES;
        }
    }
    else
    return YES;
}
//输入框文字改变的时候
- (IBAction)bottomTextChange:(UITextField *)sender
{

    if (sender.text.length == 4)
    {
        myStr = sender.text;
    }
    else if (sender.text.length > 4)
    {
        sender.text = myStr;
    }
    NSLog(@"密码输入文字发生改变了");
    [self judeColor];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    NSLog(@"textFieldDidBeginEditing");
}
//如果输入完
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}


//测试一下Gitlab；

@end
