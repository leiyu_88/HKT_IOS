//
//  FourViewController.m
//  华康通
//
//  Created by leiyu on 16/9/23.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import "FourViewController.h"
#import "SetSelfInformationController.h"
#import "MainInsuranceController.h"
#import "TeamDetailController.h"
#import "UMMobClick/MobClick.h"
#import "MainBrokerageController.h"
#import "MoreViewController.h"
#import "UnderLinePolicyController.h"
#import "FourCell.h"

#ifndef iPhone5

#define iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ?  CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)

#endif


@interface FourViewController ()

@property (strong, nonatomic) IBOutlet UIImageView *backImageView;
@property (strong, nonatomic) IBOutlet UIButton *loginButton;
@property (strong, nonatomic) IBOutlet UIImageView *myImageView;
@property (strong, nonatomic) NSDictionary* images;
@property (strong, nonatomic) NSDictionary* titles;
@property (strong, nonatomic) IBOutlet UILabel *codeLabel;
@property (strong, nonatomic) IBOutlet UIButton *showButton;
//登陆体验更多的功能
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;



//@property (strong, nonatomic) UIView* bottomView;

@end

@implementation FourViewController

//- (UIView*)bottomView
//{
//    if (!_bottomView)
//    {
//        _bottomView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, UISCREENWEITH, 50)];
//        _bottomView.backgroundColor = LIGHTGREYColor;
//    }
//    return _bottomView;
//}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
//    [self.tabBarController.tabBar setTintColor:KMainColor];
    [self.tableView registerNib:[UINib nibWithNibName:@"FourCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    CGPoint point = self.tableView.contentOffset;
    NSLog(@"point.y = %f",point.y);
//    self.tableView.tableFooterView = self.bottomView;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (NSDictionary*)images
{
    if (!_images)
    {
        _images = @{@"0":@[@"my_insurance",@"my_listpay",@"my_bdsherch",@"my_payfor"],@"1":@[@"my_more",@"my_set"]};
//        _images = @{@"0":@[@"dingfdan1",@"dingfdan2",@"yongjin"],@"1":@[@"more",@"settings"]};
    }
    return _images;
}

- (NSDictionary*)titles
{
    if (!_titles)
    {
        _titles = @{@"0":@[@"寿险订单",@"短期险订单",@"保单查询",@"佣金查询"],@"1":@[@"更多",@"设置"]};
//        _titles = @{@"0":@[@"寿险订单",@"短期险订单",@"佣金查询（试运行）"],@"1":@[@"更多",@"设置"]};
    }
    return _titles;
}

//如果用户已经登录隐藏登录按钮
- (void)hideLoginButton
{
    if ([TRUserAgenCode isLogin])
    {
        [self.loginButton setHidden:YES];
        [self.codeLabel setHidden:NO];
        self.codeLabel.text = [TRUserAgenCode getUserName];
        self.detailLabel.text = [TRUserAgenCode getUserAgenCode];
    }
    else
    {
        [self.loginButton setHidden:YES];
        [self.codeLabel setHidden:NO];
        self.codeLabel.text = @"请登录";
        self.detailLabel.text = @"登录体验更多功能";
    }
}

//设置状态栏背景颜色
//- (void)setStatusBarBackgroundColor:(UIColor *)color
//{
//    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
//    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
//        statusBar.backgroundColor = color;
//    }
//}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSLog(@"导航栏的高度 = %f",NavigationBarHeight);
    
    self.navigationController.navigationBar.hidden = YES;
    ShowTabbar;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self setNeedsStatusBarAppearanceUpdate];
    //如果用户已经登录隐藏登录按钮
    [self hideLoginButton];
//    [self.navigationController.navigationBar setBackgroundImage:LoadImage(@"my_top_bg") forBarMetrics:UIBarMetricsDefault];
//    //判断iphone5及以下尺寸 替换导航栏图
//    NSLog(@"UISCREENWEITH = %.0lf",UISCREENWEITH);
//    if (UISCREENWEITH <= 320)
//    {
//        [self.navigationController.navigationBar setBackgroundImage:LoadImage(@"my_top_bg10") forBarMetrics:UIBarMetricsDefault];
//        self.backImageView.image = LoadImage(@"my_top_bg101");
//    }
//    if (UISCREENHEIGHT >= 812)
//    {
//        NSLog(@"UISCREENHEIGHT屏幕高 = %.0lf",UISCREENHEIGHT);
//        [self.navigationController.navigationBar setBackgroundImage:LoadImage(@"my_top_bg11") forBarMetrics:UIBarMetricsDefault];
//        self.backImageView.image = LoadImage(@"my_top_bg111.png");
//    }
    [MobClick beginLogPageView:@"FourViewController"];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    HiddenTabbar;
    self.navigationController.navigationBar.hidden = NO;
    [MobClick endLogPageView:@"FourViewController"];
}

//展示登陆用户个人信息
- (IBAction)showSelfInformation:(UIButton *)sender
{
    if ([TRUserAgenCode isLogin])
    {
        TeamDetailController* teamDetailVC=[[TeamDetailController alloc]initWithNibName:@"TeamDetailController" bundle:nil];
        teamDetailVC.isFirst = YES;
        teamDetailVC.hidesBottomBarWhenPushed = YES;
        
        [self.navigationController pushViewController:teamDetailVC animated:YES];
    }
    else
    {
        ShowLogin(self.navigationController);
    }
}

- (void)judgeNetworkWithTitle:(NSString*)title
{
    dispatch_async(dispatch_get_main_queue(), ^{
        ShowAlertViewWithYes(title);
    });
}

//系统提示框
- (void)presentAlerControllerWithMessage:(NSString*)message
{
    CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
    TextStatusView *customView  = [TextStatusView view];
    customView.frame = CGRectMake(10, 10, UISCREENWEITH - 80, ALERTVIEW_HEIGHT_B);
    customView.bottomLabel.text = message;
    [alertView setContainerView:customView];
    [alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"好的", nil]];
    [alertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
        switch (buttonIndex)
        {
            case 0:
                return ;
                break;
            default:
                return;
                break;
        }
    }];
    [alertView show];
}

//登录
- (IBAction)login:(UIButton *)sender
{
//    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
//    UserLoginController* navc = [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
//    navc.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:navc animated:YES];
}

#pragma mark - 滚动的代理方法

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGPoint point = self.tableView.contentOffset;
    if (point.y <= 0)
    {
        point.y =  0;
        self.tableView.contentOffset = point;
    }self.tableView.contentMode = UIViewContentModeScaleAspectFill;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.images[[NSString stringWithFormat:@"%ld",(long)section]] count];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FourCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    NSString* str = self.images[[NSString stringWithFormat:@"%ld",(long)indexPath.section]][indexPath.row];
    cell.myImageView.image = LoadImage(str);
    cell.titleLabel.text = self.titles[[NSString stringWithFormat:@"%ld",(long)indexPath.section]][indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 0;
    }
    else
    {
        return 12.0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView* view;
    if (section == 1)
    {
        view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, UISCREENWEITH, 12.0)];
        view.backgroundColor = LIGHTGREYColor;
    }
    else
    {
         view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, UISCREENWEITH, 0.0)];
    }
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([TRUserAgenCode isLogin])
    {
        if (indexPath.section == 0)
        {
            if (indexPath.row == 0)
            {
                MainInsuranceController* allInsurancePolicyVC = [[MainInsuranceController alloc]init];
                allInsurancePolicyVC.isNavi = NO;
                allInsurancePolicyVC.titles = @[@"全部",@"未提交",@"待支付",@"已承保"];
                allInsurancePolicyVC.title = @"寿险订单列表";
                allInsurancePolicyVC.isSort = NO;
                allInsurancePolicyVC.hidesBottomBarWhenPushed = YES;
                self.navigationController.navigationBar.hidden = NO;
                [self.navigationController pushViewController:allInsurancePolicyVC animated:YES];
            }
            else if (indexPath.row == 1)
            {
                MainInsuranceController* allInsurancePolicyVC = [[MainInsuranceController alloc]init];
                allInsurancePolicyVC.isNavi = NO;
                allInsurancePolicyVC.titles = @[@"全部",@"待支付",@"已承保",@"已失效",@"待续保"];
                allInsurancePolicyVC.title = @"短期险订单列表";
                allInsurancePolicyVC.isSort = YES;
                allInsurancePolicyVC.hidesBottomBarWhenPushed = YES;
                self.navigationController.navigationBar.hidden = NO;
                [self.navigationController pushViewController:allInsurancePolicyVC animated:YES];
            }
            else if (indexPath.row == 2)
            {
                UnderLinePolicyController* underLineVC = [[UnderLinePolicyController alloc]init];
                underLineVC.hidesBottomBarWhenPushed = YES;
                self.navigationController.navigationBar.hidden = NO;
                [self.navigationController pushViewController:underLineVC animated:YES];
            }
            else if (indexPath.row == 3)
            {
                MainBrokerageController* mainBrokerVC = [[MainBrokerageController alloc]initWithNibName:@"MainBrokerageController" bundle:nil];
                mainBrokerVC.hidesBottomBarWhenPushed = YES;
                self.navigationController.navigationBar.hidden = NO;
                [self.navigationController pushViewController:mainBrokerVC animated:YES];
            }
        }
        else if (indexPath.section == 1)
        {
            if (indexPath.row == 1)
            {
                SetSelfInformationController* setSelfVC = [[SetSelfInformationController alloc]initWithNibName:@"SetSelfInformationController" bundle:nil];
                setSelfVC.hidesBottomBarWhenPushed = YES;
                self.navigationController.navigationBar.hidden = NO;
                [self.navigationController pushViewController:setSelfVC animated:YES];
            }
            else
            {
                MoreViewController* moreVC = [[MoreViewController alloc]initWithNibName:@"MoreViewController" bundle:nil];
                moreVC.hidesBottomBarWhenPushed = YES;
                self.navigationController.navigationBar.hidden = NO;
                [self.navigationController pushViewController:moreVC animated:YES];
            }
        }
        else
        {
            [self showNoPush];
        }
    }
    else
    {
        if (indexPath.section == 1 && indexPath.row == 1)
        {
            MoreViewController* moreVC = [[MoreViewController alloc]initWithNibName:@"MoreViewController" bundle:nil];
            moreVC.hidesBottomBarWhenPushed = YES;
            self.navigationController.navigationBar.hidden = NO;
            [self.navigationController pushViewController:moreVC animated:YES];
        }
        else
        {
            [self judeLoginWithMessage:@"您还没登录，请先登录!"];
        }
    }
}

//弹出提示功能正在开发
- (void)showNoPush
{
    CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
    TextStatusView *customView  = [TextStatusView view];
    customView.frame = CGRectMake(10, 10, UISCREENWEITH - 80, ALERTVIEW_HEIGHT_B);
    customView.bottomLabel.text = @"该功能正在开发中，敬请期待！";
    [alertView setContainerView:customView];
    [alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"确定", nil]];
    [alertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
        switch (buttonIndex)
        {
            case 0:
                return ;
                break;
            default:
                return;
                break;
        }
    }];
    [alertView show];
}

//判断并且登录
- (void)judeLoginWithMessage:(NSString*)message
{
    ShowLogin(self.navigationController);
}

@end
