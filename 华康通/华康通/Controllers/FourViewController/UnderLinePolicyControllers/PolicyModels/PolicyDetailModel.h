//
//********************保单基本信息 （contract）********************
//********************保单基本信息 （contract）********************
//********************保单基本信息 （contract）********************
//********************保单基本信息 （contract）********************
//********************保单基本信息 （contract）********************
//********************保单基本信息 （contract）********************
//

#import <Foundation/Foundation.h>

@interface PolicyDetailModel : NSObject

//合同号
@property (nonatomic, strong) NSString* insContId;
//保险公司名称
@property (nonatomic, strong) NSString* companyName;
//产品名称
@property (nonatomic, strong) NSString* productkName;
//投保单号
@property (nonatomic, strong) NSString* orderNo;
//保单号
@property (nonatomic, strong) NSString* contNo;
//投保日期
@property (nonatomic, strong) NSString* applyDate;
//生效日期
@property (nonatomic, strong) NSString* startDate;
//交回执日期
@property (nonatomic, strong) NSString* receiptDate;
//受理日期
@property (nonatomic, strong) NSString* acceptDate;
//保费合计
@property (nonatomic, strong) NSString* premium;
//保单状态
@property (nonatomic, strong) NSString* status;

+ (id)policyDetailWithJSON:(NSDictionary*)json;

@end
