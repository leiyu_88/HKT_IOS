//
//  PolicyDetailModel.m
//  华康通
//
//  Created by  雷雨 on 2017/6/23.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import "PolicyDetailModel.h"

@implementation PolicyDetailModel

- (id)initWithJSON:(NSDictionary*)json
{
    if (self = [super init])
    {
        if ([json[@"insContId"] isKindOfClass:[NSNull class]] || !json[@"insContId"])
        {
            self.insContId = @"-";
        }
        else
        {
            self.insContId = json[@"insContId"];
        }
        if ([json[@"companyName"] isKindOfClass:[NSNull class]] || !json[@"companyName"])
        {
            self.companyName = @"-";
        }
        else
        {
            self.companyName = json[@"companyName"];
        }
        if ([json[@"productkName"] isKindOfClass:[NSNull class]] || !json[@"productkName"])
        {
            self.productkName = @"-";
        }
        else
        {
            self.productkName = json[@"productkName"];
        }
        if ([json[@"orderNo"] isKindOfClass:[NSNull class]] || !json[@"orderNo"])
        {
            self.orderNo = @"-";
        }
        else
        {
            self.orderNo = json[@"orderNo"];
        }
        if ([json[@"contNo"] isKindOfClass:[NSNull class]] || !json[@"contNo"])
        {
            self.contNo = @"-";
        }
        else
        {
            self.contNo = json[@"contNo"];
        }
        if ([json[@"applyDate"] isKindOfClass:[NSNull class]] || !json[@"applyDate"])
        {
            self.applyDate = @"-";
        }
        else
        {
            self.applyDate = json[@"applyDate"];
        }
        if ([json[@"startDate"] isKindOfClass:[NSNull class]] || !json[@"startDate"])
        {
            self.startDate = @"-";
        }
        else
        {
            self.startDate = json[@"startDate"];
        }
        if ([json[@"receiptDate"] isKindOfClass:[NSNull class]] || !json[@"receiptDate"])
        {
            self.receiptDate = @"-";
        }
        else
        {
            self.receiptDate = json[@"receiptDate"];
        }
        if ([json[@"premium"] isKindOfClass:[NSNull class]] || !json[@"premium"])
        {
            self.premium = @"-";
        }
        else
        {
            self.premium = [NSString stringWithFormat:@"%@元",json[@"premium"]];
        }
        if ([json[@"status"] isKindOfClass:[NSNull class]] || !json[@"status"])
        {
            self.status = @"-";
        }
        else
        {
            self.status = json[@"status"];
        }
        if ([json[@"acceptDate"] isKindOfClass:[NSNull class]] || !json[@"acceptDate"])
        {
            self.acceptDate = @"-";
        }
        else
        {
            self.acceptDate = json[@"acceptDate"];
        }
    }
    return self;
}

+ (id)policyDetailWithJSON:(NSDictionary*)json
{
    return [[self alloc]initWithJSON:json];
}

@end
