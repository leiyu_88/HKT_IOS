//
//  RecordListModel.m
//  华康通
//
//  Created by  雷雨 on 2017/6/23.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import "RecordListModel.h"

@implementation RecordListModel

- (id)initWithJSON:(NSDictionary*)json
{
    if (self = [super init])
    {
        if ([json[@"insContId"] isKindOfClass:[NSNull class]] || !json[@"insContId"])
        {
            self.insContId = @"-";
        }
        else
        {
            self.insContId = json[@"insContId"];
        }
        if ([json[@"payActualAmont"] isKindOfClass:[NSNull class]] || !json[@"payActualAmont"])
        {
            self.payActualAmont = @"-";
        }
        else
        {
            self.payActualAmont = [NSString stringWithFormat:@"%@",json[@"payActualAmont"]];
        }
        if ([json[@"payActualDate"] isKindOfClass:[NSNull class]] || !json[@"payActualDate"])
        {
            self.payActualDate = @"-";
        }
        else
        {
            self.payActualDate = json[@"payActualDate"];
        }
    }
    return self;
}

+ (id)recordListWithJSON:(NSDictionary *)json
{
    return [[self alloc]initWithJSON:json];
}

@end
