//
//  XZDetailModel.m
//  华康通
//
//  Created by  雷雨 on 2017/6/23.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import "XZDetailModel.h"

@implementation XZDetailModel

- (id)initWithJSON:(NSDictionary*)json
{
    if (self = [super init])
    {
        if ([json[@"riskName"] isKindOfClass:[NSNull class]] || !json[@"riskName"])
        {
            self.riskName = @"-";
        }
        else
        {
            self.riskName = json[@"riskName"];
        }
        if ([json[@"payMethod"] isKindOfClass:[NSNull class]] || !json[@"payMethod"])
        {
            self.payMethod = @"-";
        }
        else
        {
            self.payMethod = json[@"payMethod"];
        }
        if ([json[@"insPeriod"] isKindOfClass:[NSNull class]] || !json[@"insPeriod"])
        {
            self.insPeriod = @"-";
        }
        else
        {
            self.insPeriod = json[@"insPeriod"];
        }
        if ([json[@"payPeriod"] isKindOfClass:[NSNull class]] || !json[@"payPeriod"])
        {
            self.payPeriod = @"-";
        }
        else
        {
            self.payPeriod = json[@"payPeriod"];
        }
        if ([json[@"amount"] isKindOfClass:[NSNull class]] || !json[@"amount"])
        {
            self.amount = @"-";
        }
        else
        {
            self.amount = [NSString stringWithFormat:@"%@元",json[@"amount"]];
        }
        if ([json[@"premium"] isKindOfClass:[NSNull class]] || !json[@"premium"])
        {
            self.premium = @"-";
        }
        else
        {
            self.premium = [NSString stringWithFormat:@"%@元",json[@"premium"]];
        }
        
        if ([json[@"isRenewal"] isKindOfClass:[NSNull class]] || !json[@"isRenewal"])
        {
            self.isRenewal = @"-";
        }
        else
        {
            self.isRenewal = json[@"isRenewal"];
        }
        
        if ([json[@"isMainRisk"] isKindOfClass:[NSNull class]] || !json[@"isMainRisk"])
        {
            self.isMainRisk = @"-";
        }
        else
        {
            self.isMainRisk = json[@"isMainRisk"];
        }
    }
    return self;
}

+ (id)policyDetailWithJSON:(NSDictionary*)json
{
    return [[self alloc]initWithJSON:json];
}

@end
