//
//  PolicyListModel.m
//  华康通
//
//  Created by  雷雨 on 2017/6/23.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import "PolicyListModel.h"

@implementation PolicyListModel

- (id)initWithJSON:(NSDictionary*)json
{
    if (self = [super init])
    {
        if ([json[@"insContId"] isKindOfClass:[NSNull class]] || !json[@"insContId"])
        {
            self.insContId = @"-";
        }
        else
        {
            self.insContId = json[@"insContId"];
        }
        if ([json[@"companyName"] isKindOfClass:[NSNull class]] || !json[@"companyName"])
        {
            self.companyName = @"";
        }
        else
        {
            self.companyName = json[@"companyName"];
        }
        
        if ([json[@"companyLogo"] isKindOfClass:[NSNull class]] || !json[@"companyLogo"])
        {
            self.companyLogo = @"-";
        }
        else
        {
            self.companyLogo = json[@"companyLogo"];
        }
        
        if ([json[@"productCode"] isKindOfClass:[NSNull class]] || !json[@"productCode"])
        {
            self.productCode = @"-";
        }
        else
        {
            self.productCode = json[@"productCode"];
        }
        
        if ([json[@"productName"] isKindOfClass:[NSNull class]] || !json[@"productName"])
        {
            self.productName = @"-";
        }
        else
        {
            self.productName = json[@"productName"];
        }
        
        
        if ([json[@"orderNo"] isKindOfClass:[NSNull class]] || !json[@"orderNo"])
        {
            self.orderNo = @"-";
        }
        else
        {
            self.orderNo = json[@"orderNo"];
        }
        if ([json[@"applyDate"] isKindOfClass:[NSNull class]] || !json[@"applyDate"])
        {
            self.applyDate = @"-";
        }
        else
        {
            self.applyDate = json[@"applyDate"];
        }
        if ([json[@"contNo"] isKindOfClass:[NSNull class]] || !json[@"contNo"])
        {
            self.contNo = @"-";
        }
        else
        {
            self.contNo = json[@"contNo"];
        }
        if ([json[@"premium"] isKindOfClass:[NSNull class]] || !json[@"premium"])
        {
            self.premium = @"-";
        }
        else
        {
            self.premium = [NSString stringWithFormat:@"%@",json[@"premium"]];
        }
        if ([json[@"status"] isKindOfClass:[NSNull class]] || !json[@"status"])
        {
            self.status = @"-";
        }
        else
        {
            self.status = json[@"status"];
        }
        
        if ([json[@"appntName"] isKindOfClass:[NSNull class]] || !json[@"appntName"])
        {
            self.appntName = @"-";
        }
        else
        {
            self.appntName = json[@"appntName"];
        }
        if ([json[@"inseredName"] isKindOfClass:[NSNull class]] || !json[@"inseredName"])
        {
            self.inseredName = @"-";
        }
        else
        {
            self.inseredName = json[@"inseredName"];
        }
        if ([json[@"acceptDate"] isKindOfClass:[NSNull class]] || !json[@"acceptDate"])
        {
            self.acceptDate = @"-";
        }
        else
        {
            self.acceptDate = json[@"acceptDate"];
        }
    }
    return self;
}

+ (id)policyListWithJSON:(NSDictionary*)json
{
    return [[self alloc]initWithJSON:json];
}

@end
