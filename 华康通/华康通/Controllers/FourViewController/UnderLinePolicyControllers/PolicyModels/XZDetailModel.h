//
//********************险种信息 （plan）********************
//********************险种信息 （plan）********************
//********************险种信息 （plan）********************
//********************险种信息 （plan）********************
//********************险种信息 （plan）********************
//********************险种信息 （plan）********************
//

#import <Foundation/Foundation.h>

@interface XZDetailModel : NSObject

//附加险名称
@property (nonatomic, strong) NSString* riskName;
//缴费方式
@property (nonatomic, strong) NSString* payMethod;
//保障期间
@property (nonatomic, strong) NSString* insPeriod;
//缴费期间
@property (nonatomic, strong) NSString* payPeriod;
//保额
@property (nonatomic, strong) NSString* amount;
//保费
@property (nonatomic, strong) NSString* premium;
//是否续期(续订)
@property (nonatomic, strong) NSString* isRenewal;
//判断是否为主险或者附险
@property (nonatomic, strong) NSString* isMainRisk;


+ (id)policyDetailWithJSON:(NSDictionary*)json;

@end
