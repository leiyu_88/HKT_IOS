//
//********************fast保单列表信息********************
//********************fast保单列表信息********************
//********************fast保单列表信息********************
//********************fast保单列表信息********************
//********************fast保单列表信息********************
//********************fast保单列表信息********************
//


#import <Foundation/Foundation.h>

@interface PolicyListModel : NSObject

//合同号
@property (nonatomic, strong) NSString* insContId;
//保险公司名称
@property (nonatomic, strong) NSString* companyName;
//保单在线logo
@property (nonatomic, strong) NSString* companyLogo;
//保单编号
@property (nonatomic, strong) NSString* productCode;
//保单名称
@property (nonatomic, strong) NSString* productName;
//订单号
@property (nonatomic, strong) NSString* orderNo;
//投保时间
@property (nonatomic, strong) NSString* applyDate;
//受理日期
@property (nonatomic, strong) NSString* acceptDate;
//
@property (nonatomic, strong) NSString* contNo;
//保费
@property (nonatomic, strong) NSString* premium;
//保单状态
@property (nonatomic, strong) NSString* status;

//投保人
@property (nonatomic, strong) NSString* appntName;
//被保人
@property (nonatomic, strong) NSString* inseredName;

+ (id)policyListWithJSON:(NSDictionary*)json;

@end
