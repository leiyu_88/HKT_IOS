//
//********************缴费历史记录信息********************
//********************缴费历史记录信息********************
//********************缴费历史记录信息********************
//********************缴费历史记录信息********************
//********************缴费历史记录信息********************
//********************缴费历史记录信息********************
//

#import <Foundation/Foundation.h>

@interface RecordListModel : NSObject

//参数
@property (nonatomic, strong) NSString* insContId;
//金额
@property (nonatomic, strong) NSString* payActualAmont;
//日期
@property (nonatomic, strong) NSString* payActualDate;

+ (id)recordListWithJSON:(NSDictionary*)json;


@end
