//
//  UserDetailModel.m
//  华康通
//
//  Created by  雷雨 on 2017/6/23.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import "UserDetailModel.h"

@implementation UserDetailModel

- (id)initWithJSON:(NSDictionary*)json
{
    if (self = [super init])
    {
        if ([json[@"name"] isKindOfClass:[NSNull class]] || !json[@"name"])
        {
            self.name = @"-";
        }
        else
        {
            self.name = json[@"name"];
        }
        if ([json[@"sex"] isKindOfClass:[NSNull class]] || !json[@"sex"])
        {
            self.sex = @"-";
        }
        else
        {
            self.sex = json[@"sex"];
        }
        if ([json[@"idType"] isKindOfClass:[NSNull class]] || !json[@"idType"])
        {
            self.idType = @"-";
        }
        else
        {
            self.idType = [NSString stringWithFormat:@"%@",json[@"idType"]];
        }
        if ([json[@"idNo"] isKindOfClass:[NSNull class]] || !json[@"idNo"])
        {
            self.idNo = @"-";
        }
        else
        {
            NSInteger startLocation = 3;
            NSString* str = [NSString stringWithFormat:@"%@",json[@"idNo"]];
            //下面是身份证号码加*号
            if ([self.idType isEqualToString:@"身份证"])
            {
                if (str.length > 7)
                {
                    for (NSInteger i = 0; i < str.length - 7; i++)
                    {
                        NSRange range = NSMakeRange(startLocation, 1);
                        str = [str stringByReplacingCharactersInRange:range withString:@"*"];
                        startLocation++;
                    }
                }
            }
            self.idNo = str;
        }
        if ([json[@"mobileNo"] isKindOfClass:[NSNull class]] || !json[@"mobileNo"])
        {
            self.mobileNo = @"-";
        }
        else
        {
            self.mobileNo = json[@"mobileNo"];
        }
        if ([json[@"email"] isKindOfClass:[NSNull class]] || !json[@"email"])
        {
            self.email = @"-";
        }
        else
        {
            self.email = json[@"email"];
        }
        if ([json[@"provice"] isKindOfClass:[NSNull class]] || !json[@"provice"])
        {
            self.provice = @"-";
        }
        else
        {
            self.provice = json[@"provice"];
        }
        if ([json[@"city"] isKindOfClass:[NSNull class]] || !json[@"city"])
        {
            self.city = @"-";
        }
        else
        {
            self.city = json[@"city"];
        }
        if ([json[@"address"] isKindOfClass:[NSNull class]] || !json[@"address"])
        {
            self.address = @"-";
        }
        else
        {
            self.address = json[@"address"];
        }
        if ([json[@"userType"] isKindOfClass:[NSNull class]] || !json[@"userType"])
        {
            self.userType = @"-";
        }
        else
        {
            self.userType = json[@"userType"];
        }
        
        
        if ([json[@"occupationName"] isKindOfClass:[NSNull class]] || !json[@"occupationName"])
        {
            self.occupationName = @"-";
        }
        else
        {
            self.occupationName = json[@"occupationName"];
        }
        if ([json[@"bnfRate"] isKindOfClass:[NSNull class]] || !json[@"bnfRate"])
        {
            self.bnfRate = @"-";
        }
        else
        {
            self.bnfRate = json[@"bnfRate"];
        }
        if ([json[@"appInsType"] isKindOfClass:[NSNull class]] || !json[@"appInsType"])
        {
            self.appInsType = @"-";
        }
        else
        {
            self.appInsType = json[@"appInsType"];
        }
        if ([json[@"insBnfType"] isKindOfClass:[NSNull class]] || !json[@"insBnfType"])
        {
            self.insBnfType = @"-";
        }
        else
        {
            self.insBnfType = json[@"insBnfType"];
        }
        if ([json[@"bnfType"] isKindOfClass:[NSNull class]] || !json[@"bnfType"])
        {
            self.bnfType = @"-";
        }
        else
        {
            self.bnfType = json[@"bnfType"];
        }
        
        if ([json[@"birthDate"] isKindOfClass:[NSNull class]] || !json[@"birthDate"])
        {
            self.birthDate = @"-";
        }
        else
        {
            self.birthDate = json[@"birthDate"];
        }
    }
    return self;
}

+ (id)policyDetailWithJSON:(NSDictionary*)json
{
    return [[self alloc]initWithJSON:json];
}

@end
