//
//********************投保人或被保人信息 （user）********************
//********************投保人或被保人信息 （user）********************
//********************投保人或被保人信息 （user）********************
//********************投保人或被保人信息 （user）********************
//********************投保人或被保人信息 （user）********************
//********************投保人或被保人信息 （user）********************
//

#import <Foundation/Foundation.h>

@interface UserDetailModel : NSObject

//姓名
@property (nonatomic, strong) NSString* name;
//性别
@property (nonatomic, strong) NSString* sex;
//证件类型
@property (nonatomic, strong) NSString* idType;
//证件号码
@property (nonatomic, strong) NSString* idNo;
//手机号
@property (nonatomic, strong) NSString* mobileNo;
//电子邮箱
@property (nonatomic, strong) NSString* email;
//所在省
@property (nonatomic, strong) NSString* provice;
//所在市
@property (nonatomic, strong) NSString* city;
//详细地址
@property (nonatomic, strong) NSString* address;
//判断是否为 投保人1、被保人2、受益人3
@property (nonatomic, strong) NSString* userType;
//职业（名称）
@property (nonatomic, strong) NSString* occupationName;
//受益比例
@property (nonatomic, strong) NSString* bnfRate;
//出生日期
@property (nonatomic, strong) NSString* birthDate;


//受益人类型
@property (nonatomic, strong) NSString* bnfType;
//与投保人关系
@property (nonatomic, strong) NSString* appInsType;
//与被保人关系
@property (nonatomic, strong) NSString* insBnfType;

+ (id)policyDetailWithJSON:(NSDictionary*)json;

@end
