//
//  PaymentCell.h
//  华康通
//
//  Created by  雷雨 on 2017/5/15.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PaymentCell : UITableViewCell



//下次缴费日期
@property (weak, nonatomic) IBOutlet UILabel *label1;
//下次缴费金额
@property (weak, nonatomic) IBOutlet UILabel *label2;
//开户银行
@property (weak, nonatomic) IBOutlet UILabel *label3;
//持卡人
@property (weak, nonatomic) IBOutlet UILabel *label4;
//银行卡号
@property (weak, nonatomic) IBOutlet UILabel *label5;
//缴费时间
@property (weak, nonatomic) IBOutlet UILabel *label6;
//缴费金额
@property (weak, nonatomic) IBOutlet UILabel *label7;
//查看更多按钮
@property (weak, nonatomic) IBOutlet UIButton *showMoreButton;

@end
