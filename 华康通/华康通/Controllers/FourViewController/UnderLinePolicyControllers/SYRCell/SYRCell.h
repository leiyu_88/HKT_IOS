//
//  SYRCell.h
//  华康通
//
//  Created by  雷雨 on 2017/5/15.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SYRCell : UITableViewCell
//受益人标题
@property (weak, nonatomic) IBOutlet UILabel *label1;
//与被保险人关系
@property (weak, nonatomic) IBOutlet UILabel *label2;
//姓名
@property (weak, nonatomic) IBOutlet UILabel *label3;
//证件类型
@property (weak, nonatomic) IBOutlet UILabel *label4;
//证件号码
@property (weak, nonatomic) IBOutlet UILabel *label5;
//收益比例
@property (weak, nonatomic) IBOutlet UILabel *label6;

@end
