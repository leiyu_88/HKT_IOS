
//  “线下保单” 点击 查看更多 页面的单元格设计

#import <UIKit/UIKit.h>

@interface PaymentHistoryCell : UITableViewCell
//缴费时间
@property (weak, nonatomic) IBOutlet UILabel *topLabel;
//缴费金额
@property (weak, nonatomic) IBOutlet UILabel *bottomLabel;

@end
