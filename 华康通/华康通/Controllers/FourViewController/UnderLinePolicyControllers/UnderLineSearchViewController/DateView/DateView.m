//
//  DateView.m
//  华康通
//
//  Created by  雷雨 on 2017/7/10.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import "DateView.h"

@implementation DateView

+ (id)view
{
    return [[[NSBundle mainBundle]loadNibNamed:@"DateView" owner:nil options:nil]lastObject];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self=[super initWithCoder:aDecoder])
    {
        self.autoresizingMask = UIViewAutoresizingNone;
    }
    return self;
}

- (IBAction)changeDate:(UIDatePicker *)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LYDateView" object:self userInfo:@{@"Date":sender.date}];
}

@end
