//
//  DateView.h
//  华康通
//
//  Created by  雷雨 on 2017/7/10.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DateView : UIView
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *determineButton;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

+ (id)view;

@end
