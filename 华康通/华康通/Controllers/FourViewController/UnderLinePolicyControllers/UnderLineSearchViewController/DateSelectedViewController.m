//
//  DateSelectedViewController.m
//  华康通
//
//  Created by  雷雨 on 2017/7/7.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import "DateSelectedViewController.h"
#import "DateView.h"

@interface DateSelectedViewController ()
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UILabel *startDateLabel;
@property (weak, nonatomic) IBOutlet UIView *startDateView;
@property (weak, nonatomic) IBOutlet UIButton *startDateButton;
@property (weak, nonatomic) IBOutlet UILabel *endDateLabel;
@property (weak, nonatomic) IBOutlet UIView *endDateView;
@property (weak, nonatomic) IBOutlet UIButton *endDateButton;

@property (nonatomic, strong) UIView* backView;
@property (nonatomic, strong) DateView* dateView;
//用一个int值记录是选择了哪个按钮
@property (unsafe_unretained, nonatomic) NSInteger isStart;
//记录开始日期
@property (nonatomic, strong) NSDate* startDate;
//记录结束日期
@property (nonatomic, strong) NSDate* endDate;
//临时用一个日期记录选择好的日期
@property (nonatomic, strong) NSDate* myDate;

@end

@implementation DateSelectedViewController

- (IBAction)goBackWithMessage:(id)sender
{
    [self done];
}
- (IBAction)closeKeyboard:(UITextField*)sender
{
    [sender resignFirstResponder];
}

- (UIView*)backView
{
    if (!_backView)
    {
        _backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        _backView.backgroundColor = YYColor;
        UITapGestureRecognizer* tapGR = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tagGR:)];
        tapGR.numberOfTapsRequired = 1;
        tapGR.numberOfTouchesRequired = 1;
        [_backView addGestureRecognizer:tapGR];
        _backView.alpha = 0.7;
    }
    return _backView;
}

- (DateView*)dateView
{
    if (!_dateView)
    {
        _dateView = [DateView view];
        _dateView.frame = CGRectMake(0, self.view.frame.size.height - 255, UISCREENWEITH, 255);
        _dateView.datePicker.maximumDate = [[NSDate alloc]init];
        [_dateView.cancelButton addTarget:self action:@selector(cancelSelect) forControlEvents:UIControlEventTouchUpInside];
        [_dateView.determineButton addTarget:self action:@selector(determineSelect) forControlEvents:UIControlEventTouchUpInside];
    }
    return _dateView;
}

- (void)tagGR:(UITapGestureRecognizer*)gr
{
    [self removeDateView];
}

- (void)cancelSelect
{
    [self removeDateView];
}

- (void)determineSelect
{
    NSDate* date = [[NSDate alloc]init];
    if (self.isStart == 1)
    {
        self.startDateLabel.text = [self returnDateFormatterForStringWithDate:date];
        self.startDate = date;
        //选择日期显示在 开始日期那里
        if (self.myDate)
        {
            self.startDateLabel.text = [self returnDateFormatterForStringWithDate:self.myDate];
            self.startDate = self.myDate;
        }
    }
    else if (self.isStart == 2)
    {
        self.endDateLabel.text = [self returnDateFormatterForStringWithDate:date];
        self.endDate = date;
        if (self.myDate)
        {
            //选择日期显示在 结束日期那里
            self.endDateLabel.text = [self returnDateFormatterForStringWithDate:self.myDate];
            self.endDate = self.myDate;
        }
    }
    [self removeDateView];
}
//弹出视图
- (void)newOneDateView
{
    [self.view addSubview:self.backView];
    [self.view addSubview:self.dateView];
}
//隐藏去掉弹出视图部分
- (void)removeDateView
{
    [self.dateView removeFromSuperview];
    self.dateView = nil;
    [self.backView removeFromSuperview];
    self.backView = nil;
    self.myDate = nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"高级查询";
    self.searchButton.layer.cornerRadius = 5.0;
    self.searchButton.layer.masksToBounds = YES;
    self.isStart = 0;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:LoadImage(@"back1") style:UIBarButtonItemStylePlain target:self action:@selector(popRootVC)];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"LYDateView" object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"DateSelectedViewController"];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"DateSelectedViewController"];//("PageOne"为页面名称，可自定义)
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(selectedDate:) name:@"LYDateView" object:nil];
}

- (void)returnDateWithBlock:(returnMyDateBlock)block
{
    self.block = block;
}

//选择日期
- (void)selectedDate:(NSNotification*)notification
{
    //完成选择日期
    NSDate* date = notification.userInfo[@"Date"];
    self.myDate = date;
}

//比较两个日期大小
- (BOOL)getIsShowAlertViewWithStartDate:(NSDate*)startDate WithEndDate:(NSDate*)endDate
{
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* cmps = [calendar components:NSCalendarUnitDay fromDate:startDate toDate:endDate options:NSCalendarMatchStrictly];
    NSLog(@"开始日期：%@，结束日期：%@",startDate,endDate);
    NSLog(@"差的秒数 = %@",cmps);
    if (cmps.day > 0)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

//完成选择时间
- (void)done
{
    NSLog(@"完成选择日期......");
    NSString* text = self.textField.text;
    if (!text)
    {
        text = @"";
    }
    if (self.startDate && self.endDate)
    {
        if ([self getIsShowAlertViewWithStartDate:self.startDate WithEndDate:self.endDate])
        {
            NSLog(@"可以进行下一步......");
            if (self.block)
            {
                self.block([self returnDateFormatterForStringWithDate:self.startDate], [self returnDateFormatterForStringWithDate:self.endDate],text);
            }
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            NSLog(@"不可以进行下一步 nonono");
            [self judeLoginWithMessage:@"结束日期应当晚于开始日期才可查询!"];
        }
    }
    else
    {
        [self judeLoginWithMessage:@"尝试先选择完开始日期和结束日期后，再进一步操作！"];
    }
}

//判断弹出框
- (void)judeLoginWithMessage:(NSString*)message
{
    CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
    TextStatusView *customView  = [TextStatusView view];
    customView.frame = CGRectMake(10, 10, UISCREENWEITH - 80, ALERTVIEW_HEIGHT_B);
    customView.bottomLabel.text = message;
    [alertView setContainerView:customView];
    [alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"确定", nil]];
    [alertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
        switch (buttonIndex)
        {
            case 0:
                return ;
                break;
            default:
                break;
        }
    }];
    [alertView show];
}

//选中开始时间选择
- (void)selectStartDate
{
    self.isStart = 1;
    self.startDateLabel.textColor = KMainColor;
    self.startDateView.backgroundColor = KMainColor;
    self.endDateView.backgroundColor = B4B4B4Color;
    self.endDateLabel.textColor = B4B4B4Color;
    [self newOneDateView];
}

//选中结束时间选择
- (void)selectEndDate
{
    self.isStart = 2;
    self.startDateLabel.textColor = B4B4B4Color;
    self.startDateView.backgroundColor = B4B4B4Color;
    self.endDateView.backgroundColor = KMainColor;
    self.endDateLabel.textColor = KMainColor;
    [self newOneDateView];
}

//返回标准日期格式
- (NSString*)returnDateFormatterForStringWithDate:(NSDate*)date
{
    //实例化一个NSDateFormatter对象
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //设定时间格式,这里可以设置成自己需要的格式
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    //用[NSDate date]可以获取系统当前时间
    NSString *currentDateStr = [dateFormatter stringFromDate:date];
    return currentDateStr;
}

- (IBAction)startDateSelect:(id)sender
{
    [self selectStartDate];
}

- (IBAction)endStartDateSelect:(id)sender
{
    [self selectEndDate];
}

- (void)popRootVC
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}


@end
