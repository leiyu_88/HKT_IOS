//
//  UnderLineSearchViewController.m
//  华康通
//
//  Created by  雷雨 on 2017/7/6.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import "UnderLineSearchViewController.h"
#import "SearchView.h"
#import "GJSearchView.h"
#import "PolicyCell.h"
#import "PolicyListModel.h"
#import "DateSelectedViewController.h"
#import "DetailPolicyController.h"

@interface UnderLineSearchViewController ()<UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource>

//创建一个高级搜索按钮
@property (strong, nonatomic) UIButton* searchButton;
//创建导航栏搜索视图
@property (strong, nonatomic) UIView* titleView;
//搜索bar
@property (strong, nonatomic) UISearchBar* mySearchBar;
//固定高级搜索视图
@property (strong, nonatomic) SearchView* topView;

//背景视图
@property (strong, nonatomic) UIView* backView;
//搜索出来的 线下保单列表
@property (nonatomic, strong) UITableView* tableView;
//如果没有搜索结果的时候展示出来的提示视图
@property (nonatomic, strong) ShowNoNetworkView* workView;

//产品列表数据
@property (nonatomic, strong) NSMutableArray* products;
//记录高级搜索 按钮的 状态
@property (nonatomic, unsafe_unretained) BOOL isGJ;
//一个变量记录受理日期快速搜索的值
@property (nonatomic, unsafe_unretained) NSInteger buttonTag;
//记录开始日期；
@property (nonatomic, strong) NSString* startDate;
//记录结束日期;
@property (nonatomic, strong) NSString* endDate;
//记录被保人或者投保人的姓名
@property (nonatomic, strong) NSString* name;
//记录保单号
@property (nonatomic, strong) NSString* contNo;
//创建一个返回按钮
@property (nonatomic, strong) UIButton* backButton;


//用一个数来记录用户上拉加载的次数
@property (nonatomic, unsafe_unretained) NSInteger slCount;
@property (nonatomic, strong) MJRefreshAutoGifFooter *footer;
@property (nonatomic, strong) MJRefreshGifHeader *header;

@end

@implementation UnderLineSearchViewController

- (NSMutableArray*)products
{
    if (!_products)
    {
        _products = [NSMutableArray array];
    }
    return _products;
}

- (UITableView*)tableView
{
    if (!_tableView)
    {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, NavigationBarHeight + StatusBarHeight + 20, UISCREENWEITH,UISCREENHEIGHT - NavigationBarHeight - StatusBarHeight - 20) style:UITableViewStyleGrouped];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = F3F3F3Color;
        self.header = [MJRefreshGifHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
        self.header.lastUpdatedTimeLabel.hidden = YES;
        self.tableView.header = self.header;
        self.footer = [MJRefreshAutoGifFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
        // Set footer
        self.tableView.footer = self.footer;
        [self.footer setHidden:YES];
    }
    return _tableView;
}

- (UIButton*)backButton
{
    if (!_backButton)
    {
        _backButton = [[UIButton alloc]init];
        _backButton.frame = CGRectMake(16, NavigationBarHeight + StatusBarHeight + 20 - 28 - 13, 28, 28);
        _backButton.backgroundColor = ClEARColor;
        [_backButton setImage:LoadImage(@"back1") forState:UIControlStateNormal];
        [_backButton addTarget:self action:@selector(popRootVC) forControlEvents:UIControlEventTouchUpInside];
    }
    return _backButton;
}

- (UIButton*)searchButton
{
    if (!_searchButton)
    {
        _searchButton = [[UIButton alloc]init];
        _searchButton.frame = CGRectMake(UISCREENWEITH - 60 - 16, NavigationBarHeight + StatusBarHeight + 20 - 10 - 34, 60, 34);
        _searchButton.backgroundColor = ClEARColor;
        [_searchButton setTitle:@"高级查询" forState:UIControlStateNormal];
        _searchButton.tag = 10;
        [_searchButton setTitleColor:FFFFFFColor forState:UIControlStateNormal];
        _searchButton.titleLabel.font = SetFont(15.0);
        [_searchButton addTarget:self action:@selector(searchForGJ:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _searchButton;
}

- (UISearchBar*)mySearchBar
{
    if (!_mySearchBar)
    {
        _mySearchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(16 + 28 , NavigationBarHeight + StatusBarHeight + 20 - 10 - 34, UISCREENWEITH - 16 - 28 - 16 - 16 - 60, 34)];
        _mySearchBar.searchBarStyle = UISearchBarStyleDefault;
        _mySearchBar.delegate = self;
        _mySearchBar.backgroundColor = FFFFFFColor;
        _mySearchBar.barTintColor = FFFFFFColor;
        _mySearchBar.layer.cornerRadius = _mySearchBar.frame.size.height/2;
        _mySearchBar.layer.masksToBounds = YES;
        UITextField* searchTextField = (UITextField*)[[[_mySearchBar.subviews firstObject] subviews] lastObject];
        searchTextField.font = SetFont(12);
        searchTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        searchTextField.textColor = SHOWCOLOR(@"a9a9a9");
        searchTextField.backgroundColor = ClEARColor;
        //设置placeholder颜色
        [searchTextField setValue:SHOWCOLOR(@"a9a9a9") forKeyPath:@"_placeholderLabel.textColor"];
        [searchTextField setValue:SetFont(14) forKeyPath:@"_placeholderLabel.font"];
        [_mySearchBar setReturnKeyType:UIReturnKeySearch];
        //设置不自动匹配到字符
        _mySearchBar.autocorrectionType = UITextAutocorrectionTypeNo;
        //设置不自动首字母大写
        _mySearchBar.autocapitalizationType = UITextAutocapitalizationTypeNone;
        //把搜索框的最里面的背景视图去除
        [[[_mySearchBar.subviews objectAtIndex:0].subviews objectAtIndex:0]removeFromSuperview];
        _mySearchBar.barStyle = UIBarStyleBlackTranslucent;
        _mySearchBar.keyboardType = UIKeyboardTypeDefault;
        _mySearchBar.placeholder = @"姓名／保单号";
        //鼠标点击出现光标的颜色
        [_mySearchBar setTintColor:KMainColor];
        
    }
    return _mySearchBar;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.isGJ = YES;
    self.slCount = 1;
    self.view.backgroundColor = F3F3F3Color;

    //创建搜索视图
    [self createSearchView];
    //创建固定高级搜索视图部分
    [self newTopView];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"SearchViewNT" object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [MobClick endLogPageView:@"UnderLineSearchViewController"];
    self.navigationController.navigationBar.hidden = NO;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"UnderLineSearchViewController"];//("PageOne"为页面名称，可自定义)
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(changeSearch:) name:@"SearchViewNT" object:nil];
    self.navigationController.navigationBar.hidden = YES;
}
#pragma mark- 计算日历的相隔天数
- (NSString*)getOldDateWithCurrentDate:(NSDate*)date withDay:(NSInteger)day withMonth:(NSInteger)month
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSCalendar *gregorian = [NSCalendar currentCalendar];
    NSDateComponents *compt4 = [[NSDateComponents alloc] init];
    [compt4 setDay:day];
    [compt4 setMonth:month];
    NSDate* date1 = [gregorian dateByAddingComponents:compt4 toDate:date options:0];
    NSLog(@"cmt.date = %@",[dateFormatter stringFromDate:date1]);
    return [dateFormatter stringFromDate:date1];
                             
}
#pragma mark- 所有执行搜索的方法

//固定高级搜索按钮的方法
- (void)changeSearch:(NSNotification*)notification
{
    self.buttonTag = [notification.userInfo[@"buttonIndex"] integerValue];
    NSLog(@"点击了第%ld个按钮----------",(long)self.buttonTag + 1);
    //执行搜索
    //.........................
    self.name = @"";
    [self.mySearchBar setText:nil];
    NSDate* date = [[NSDate alloc]init];
    self.endDate = [self returnDateFormatterForStringWithDate:date];
    switch (self.buttonTag)
    {
        case 0:
            self.endDate = @"";
            self.startDate = [self getOldDateWithCurrentDate:date withDay:0 withMonth:0];
            break;
        case 1:
            self.startDate = [self getOldDateWithCurrentDate:date withDay:-7 withMonth:0];
            break;
        case 2:
            self.startDate = [self getOldDateWithCurrentDate:date withDay:-30 withMonth:0];
            break;
        case 3:
            self.startDate = [self getOldDateWithCurrentDate:date withDay:0 withMonth:-3];
            break;
        default:
            break;
    }
    [self startSearch];
}

//返回标准日期格式
- (NSString*)returnDateFormatterForStringWithDate:(NSDate*)date
{
    //实例化一个NSDateFormatter对象
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //设定时间格式,这里可以设置成自己需要的格式
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    //用[NSDate date]可以获取系统当前时间
    NSString *currentDateStr = [dateFormatter stringFromDate:date];
    return currentDateStr;
}

//查看更长时间（弹出高级搜索窗口）
- (void)searchForGJ:(UIButton*)sender
{
    //高级搜索
    //...............
    if (sender.tag == 10)
    {
        if (!self.isGJ)
        {
            //执行搜索
            //.........................
            self.startDate = @"";
            self.endDate = @"";
            [self startSearch];
        }
        else
        {
            //弹出视图
            [self showSearchAlertView];
        }
    }
    else
    {
        //弹出视图
        [self showSearchAlertView];
    }
}

//执行搜索
- (void)startSearch
{
    [self.products removeAllObjects];
    NSLog(@"执行了搜索+++++++++++++++");
    //如果搜索框文字没有包含汉字，那就把搜索框的文字赋值给self.contNo；
    if (![self hasChinese:self.name])
    {
        self.contNo = self.name;
        self.name = @"";
    }
    else
    {
        self.contNo = @"";
    }
    //设置post参数不为null
    
    [self setCSNoNull];
    [self.titleView endEditing:YES];
    [self.view endEditing:YES];
    [self showGJSearch];
    //隐藏去掉固定高级搜索视图部分
    [self removeTopView];
    //把表视图加到视图
    [self newTableView];
    [self postFastBDListWithPageSize:self.slCount withPages:10 withType:@"0"];
}

//push高级搜索页面
- (void)showSearchAlertView
{
    NSLog(@"弹出高级搜索视图+++++++++++++++");
    self.name = @"";
    self.startDate = @"";
    self.endDate = @"";
    [self.titleView endEditing:YES];
    [self.view endEditing:YES];
    //push高级搜索页面
    [self pushDateSelectVC];
}

//弹出高级搜索视图中点击开始时间的方法
- (void)showStartTime
{
    NSLog(@"弹出高级搜索视图中点击开始时间的方法++++++");
    [self pushDateSelectVC];
}

//弹出高级搜索视图中点击结束时间的方法
- (void)showEndTime
{
    NSLog(@"弹出高级搜索视图中点击结束时间的方法------");
    [self pushDateSelectVC];
}


//设置post参数不为null
- (void)setCSNoNull
{
    if (!self.name)
    {
        self.name = @"";
    }
    if (!self.startDate)
    {
        self.startDate = @"";
    }
    if (!self.endDate)
    {
        self.endDate = @"";
    }
}


//推出时间选择(高级视图)页面
- (void)pushDateSelectVC
{
    DateSelectedViewController* dateVC = [[DateSelectedViewController alloc]initWithNibName:@"DateSelectedViewController" bundle:nil];
    [dateVC returnDateWithBlock:^(NSString *startDate, NSString *endDate ,NSString* text) {
        self.startDate = startDate;
        self.endDate = endDate;
        self.name = text;
        NSLog(@"self.startDate = %@\nself.endDate = %@\nself.name = %@\n",self.startDate,self.endDate,self.name);
        //开始执行搜索
        [self startSearch];
    }];
    [self.navigationController pushViewController:dateVC animated:YES];
}

//返回上一页面
- (void)popRootVC
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark- 所有创建视图的方法
//变成高级搜索按钮
- (void)showGJSearch
{
    [self.searchButton setTitle:@"高级查询" forState:UIControlStateNormal];
    self.isGJ = YES;
}

//变成普通搜索按钮
- (void)showPTSearch
{
    [self.searchButton setTitle:@"搜索" forState:UIControlStateNormal];
    self.isGJ = NO;
}

//创建搜索视图
- (void)createSearchView
{
    self.titleView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, UISCREENWEITH, StatusBarHeight + NavigationBarHeight + 20)];
    self.titleView.backgroundColor = KMainColor;
    [self.titleView addSubview:self.backButton];
    [self.titleView addSubview:self.mySearchBar];
    [self.titleView addSubview:self.searchButton];
    [self.view addSubview:self.titleView];
}

//创建固定高级搜索视图部分
- (void)newTopView
{
    if (!self.topView)
    {
        self.topView = [SearchView view];
        self.topView.frame = CGRectMake(0, StatusBarHeight + NavigationBarHeight + 20, UISCREENWEITH, 70);
        [self setAttrabitionForTopView];
        [self.view addSubview:self.topView];
    }
}

//修改固定搜索试图中按钮的属性
- (void)setAttrabitionForTopView
{
    for (UIButton* button in self.topView.buttons)
    {
        button.layer.cornerRadius = 15.0;
        button.layer.masksToBounds = YES;
    }
}

//隐藏去掉固定高级搜索视图部分
- (void)removeTopView
{
    [self.topView removeFromSuperview];
    self.topView = nil;
}

//创建tableview
- (void)newTableView
{
    [self.view addSubview:self.tableView];
}

//清除tableview
- (void)removeTableView
{
    [self.tableView removeFromSuperview];
    self.tableView = nil;
}

#pragma mark - UISearchBarDelegate

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    return YES;
}

//点击搜索框时调用
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    NSLog(@"点击搜索框");
    self.name = searchBar.text;
    [self showPTSearch];
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    return YES;
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    
}

//判断字符串中是否包含汉字
- (BOOL)hasChinese:(NSString *)str
{
    for(int i=0; i< [str length];i++)
    {
        int a = [str characterAtIndex:i];
        if( a > 0x4e00 && a < 0x9fff)
        {
            return YES;
        }
    }
    return NO;
}

//输入文本实时更新时调用
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchBar.text.length == 0)
    {
        [self showGJSearch];
    }
    else
    {
        [self showPTSearch];
    }
    self.name = searchBar.text;
    NSLog(@"输入文字 searchText = %@",searchText);
}

- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    return YES;
}

//点击键盘上的search按钮时调用
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    //执行搜索
    //.........................
    self.startDate = @"";
    self.endDate = @"";
    [self startSearch];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
    [self.titleView endEditing:YES];
    [self showGJSearch];
}

//下拉刷新
- (void)loadNewData
{
    NSLog(@"下拉刷新");
    self.slCount = 1;
    [self.products removeAllObjects];
    [self.tableView.header beginRefreshing];
    [self.tableView.footer resetNoMoreData];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self removeWorkView];
        dispatch_time_t delay = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC));
        dispatch_after(delay, dispatch_get_main_queue(), ^{
            //请求数据
            [self postFastBDListWithPageSize:self.slCount withPages:10 withType:@"0"];
        });
    });
}

//上拉加载
- (void)loadMoreData
{
    self.slCount++;
    //self.myDistance = self.tableView.contentOffset.y;
    NSLog(@"self.slCount = %ld",(long)self.slCount);
    NSLog(@"上拉加载");
    [self removeWorkView];
    dispatch_time_t delay = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC));
    dispatch_after(delay, dispatch_get_main_queue(), ^{
        //请求数据
        [self postFastBDListWithPageSize:self.slCount withPages:10 withType:@"0"];
    });
}

#pragma mark  //获取fast保单数据列表
- (void)postFastBDListWithPageSize:(NSInteger)pageSize withPages:(NSInteger)pages withType:(NSString*)type
{
    NSDictionary* json = @{@"startDate":self.startDate,
                           @"endDate":self.endDate,
                           @"name":self.name,
                           @"contNo":self.contNo,
                           @"agentCode":[TRUserAgenCode getUserAgenCode],
                           @"pageParams":@{
                                   @"pageNum":[NSNumber numberWithInteger:pageSize],
                                   @"pageSize":[NSNumber numberWithInteger:pages]
                                   },
                           @"status":type
                           };
    NSString* str = [JSONToString dictionaryToJson:json];
    MyAdditions* md5 = [[MyAdditions alloc]init];
    NSString* SStr = [NSString stringWithFormat:@"%@%@",KEY_POST,str];
    md5.str = SStr;
    NSString* md5_Str = [md5 md5];
    NSString* url = [NSString stringWithFormat:@"%@%@?sign=%@",SERVER_URL1,@"/qryInsOrderList",md5_Str];
    NSLog(@"MD5 = %@。转换前的md5 = %@",md5_Str,str);
    NSLog(@"url = %@",url);
    [self getFastBDListWithURL:url withParam:json];
}

- (void)getFastBDListWithURL:(NSString*)url withParam:(NSDictionary*)param
{
    HKTShowNetworkView* networking = [HKTShowNetworkView shareInstanceWithTitle:@"检测到您的设备没有连接网络!" withImageStr:@"no_record" withType:0 withFrame:CGRectMake(0, NavigationBarHeight + StatusBarHeight + 20, UISCREENWEITH,UISCREENHEIGHT - NavigationBarHeight - StatusBarHeight - 20) withVC:self];
    [networking showNetworkViewWithSuccess:^{
        self.tableView.scrollEnabled = YES;
        [self.header setHidden:NO];
        
        [XHNetworking POST:url parameters:param success:^(NSData *responseObject) {
            //网络读取数据
            [self readFastBDListWithData:responseObject];
        } failure:^(NSError *error) {
            NSLog(@"error.code 线下保单列表 = %@",error.userInfo);
            dispatch_async(dispatch_get_main_queue(), ^{
                [self createWorkView];
                [self newTopView];
                [self.footer setHidden:YES];
            });
        }];
    } failure:^{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            self.tableView.scrollEnabled = NO;
            [self.footer setHidden:YES];
            [self.header setHidden:YES];
        });
    } click:^(NSInteger type) {
        NSLog(@"点击了什么类型的按钮%ld!",(long)type);
        if (type == 0)
        {
            //弹出网络提示
            
        }
    }];
}

- (void)createWorkView
{
    if (self.workView == nil)
    {
        self.workView = [[ShowNoNetworkView alloc]initWithFrame:CGRectMake(0, NavigationBarHeight + StatusBarHeight + 20, UISCREENWEITH,UISCREENHEIGHT - NavigationBarHeight - StatusBarHeight - 20)];
        self.workView.labelText = @"无";
        self.workView.imageName = @"no_record";
        self.workView.myType = MyTypeOfViewForShowError;
        [self.workView createAllSubView];
        [self.view addSubview:self.workView];
    }
}

- (void)removeWorkView
{
    self.workView.frame = CGRectZero;
    [self.workView removeFromSuperview];
    self.workView = nil;
}

- (void)readFastBDListWithData:(NSData*)data
{
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSString* str = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"线下保单列表json = %@  线下保单列表字符串 = %@",json,str);
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        if ([json isKindOfClass:[NSDictionary class]] && json)
        {
            if ([json[@"resultCode"] isEqualToString:@"0"])
            {
                if (![json[@"responseObject"][@"insOrderList"] isKindOfClass:[NSNull class]] && json[@"responseObject"][@"insOrderList"])
                {
                    for (NSDictionary* dict in json[@"responseObject"][@"insOrderList"])
                    {
                        PolicyListModel* product = [PolicyListModel policyListWithJSON:dict];
                        [self.products addObject:product];
                    }
                    if ([json[@"responseObject"][@"page"] isKindOfClass:[NSDictionary class]] && json[@"responseObject"][@"page"])
                    {
                        //总条数
                        NSInteger i1 = [json[@"responseObject"][@"page"][@"pages"] integerValue];
                        //当前页
                        NSInteger i2 = [json[@"responseObject"][@"page"][@"pageNum"] integerValue];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.footer setHidden:NO];
                            //当刚好数据为10条、总页数只有一页时
                            if (i1 == i2)
                            {
                                [self.tableView.footer noticeNoMoreData];
                            }
                            if ([json[@"responseObject"][@"insOrderList"] count] == 0)
                            {
                                [self.tableView.footer noticeNoMoreData];
                            }
                        });
                    }
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.footer setHidden:YES];
                        
                    });
                }
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.footer setHidden:YES];
                    
                });
            }
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.footer setHidden:YES];
                
            });
        }
        //通知主线程刷新
        dispatch_async(dispatch_get_main_queue(), ^{
            [self endRefresh];
            if (self.products.count == 0)
            {
                [self removeTableView];
                [self createWorkView];
                [self newTopView];
            }
            else
            {
                [self removeWorkView];
                [self removeTopView];
                [self newTableView];
            }
            [self.tableView reloadData];
        });
    });
}

/**
 *  停止刷新
 */
-(void)endRefresh
{
    if ([self.tableView.header isRefreshing])
    {
        [self.tableView.header endRefreshing];
    }
    if ([self.tableView.footer isRefreshing])
    {
        [self.tableView.footer endRefreshing];
    }
}

#pragma mark - 列表代理方法

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.products.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.bounds.size.height;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView* view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, UISCREENWEITH, 10)];
    view.backgroundColor = F3F3F3Color;
    return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PolicyCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == nil)
    {
        //直接加载 xib
        cell = [[[NSBundle mainBundle]loadNibNamed:@"PolicyCell" owner:self options:nil] lastObject];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    if (self.products.count > 0)
    {
        PolicyListModel* model = self.products[indexPath.row];
        [cell setMode:model];
        [cell setNeedsDisplay];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DetailPolicyController* detailPolicyVC = [[DetailPolicyController alloc]initWithNibName:@"DetailPolicyController" bundle:nil];
    PolicyListModel* model = self.products[indexPath.row];
    detailPolicyVC.insContId = model.insContId;
    [self.navigationController pushViewController:detailPolicyVC animated:YES];
}


@end
