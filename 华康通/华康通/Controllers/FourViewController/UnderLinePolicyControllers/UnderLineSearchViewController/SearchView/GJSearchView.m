//
//  GJSearchView.m
//  华康通
//
//  Created by  雷雨 on 2017/7/7.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import "GJSearchView.h"

@implementation GJSearchView

+ (id)view
{
    return [[[NSBundle mainBundle]loadNibNamed:@"GJSearchView" owner:nil options:nil]lastObject];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self=[super initWithCoder:aDecoder])
    {
        self.autoresizingMask = UIViewAutoresizingNone;
    }
    return self;
}

- (IBAction)closeBoardkey:(UITextField *)sender
{
    [sender resignFirstResponder];
}

@end
