//
//  SearchView.m
//  华康通
//
//  Created by  雷雨 on 2017/7/6.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import "SearchView.h"

@implementation SearchView

+ (id)view
{
    return [[[NSBundle mainBundle]loadNibNamed:@"SearchView" owner:nil options:nil]lastObject];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self=[super initWithCoder:aDecoder])
    {
        self.autoresizingMask = UIViewAutoresizingNone;
    }
    return self;
}

- (IBAction)search:(UIButton *)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SearchViewNT" object:self userInfo:@{@"buttonIndex":[NSString stringWithFormat:@"%ld",(long)sender.tag]}];
}

@end
