//
//  DateSelectedViewController.h
//  华康通
//
//  Created by  雷雨 on 2017/7/7.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^returnMyDateBlock)(NSString* startDate,NSString* endDate,NSString* text);

@interface DateSelectedViewController : UIViewController

@property (nonatomic, strong) returnMyDateBlock block;

- (void)returnDateWithBlock:(returnMyDateBlock)block;

@end
