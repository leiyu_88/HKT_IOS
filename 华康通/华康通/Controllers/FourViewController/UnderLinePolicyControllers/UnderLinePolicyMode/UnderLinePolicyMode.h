//
//  UnderLinePolicyMode.h
//  华康通
//
//  Created by  雷雨 on 2017/5/17.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UnderLinePolicyMode : NSObject

//投保时间
@property (nonatomic, strong) NSString* dateStr;
//保险标题
@property (nonatomic, strong) NSString* titleStr;
//投保人
@property (nonatomic, strong) NSString* tbPeopleStr;
//被保人
@property (nonatomic, strong) NSString* bbPeopleStr;
//保费
@property (nonatomic, strong) NSString* priceStr;
//保单状态
@property (nonatomic, strong) NSString* statusStr;
//保险logo
@property (nonatomic, strong) NSString* logo;


+ (NSArray*)allMode;


@end
