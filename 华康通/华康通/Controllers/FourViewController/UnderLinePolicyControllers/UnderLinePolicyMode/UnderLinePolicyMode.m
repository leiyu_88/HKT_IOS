//
//  UnderLinePolicyMode.m
//  华康通
//
//  Created by  雷雨 on 2017/5/17.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import "UnderLinePolicyMode.h"

@implementation UnderLinePolicyMode

+ (NSArray*)allMode
{
    NSMutableArray* arr = [NSMutableArray array];
    UnderLinePolicyMode* mode = nil;
    
    mode = [[UnderLinePolicyMode alloc]init];
    mode.logo = @"1ppt4.jpg";
    mode.dateStr = @"20170503 9:30";
    mode.titleStr = @"国华康运一生重大疾病A险";
    mode.tbPeopleStr = @"雷雨";
    mode.bbPeopleStr = @"雷崇文";
    mode.priceStr = @"7900000元";
    mode.statusStr = @"有效";
    [arr addObject:mode];
    
    mode = [[UnderLinePolicyMode alloc]init];
    mode.logo = @"1ppt4.jpg";
    mode.dateStr = @"20170503 9:30";
    mode.titleStr = @"国华康运一生重大疾病A险国华康运一生重大疾病A险国华重大疾病A险";
    mode.tbPeopleStr = @"雷雨";
    mode.bbPeopleStr = @"吴雄花";
    mode.priceStr = @"7900000元";
    mode.statusStr = @"有效";
    [arr addObject:mode];
    
    mode = [[UnderLinePolicyMode alloc]init];
    mode.logo = @"1ppt4.jpg";
    mode.dateStr = @"20170503 9:30";
    mode.titleStr = @"国华康运一生重大疾病A险";
    mode.tbPeopleStr = @"雷雨";
    mode.bbPeopleStr = @"雷崇文";
    mode.priceStr = @"7900000元";
    mode.statusStr = @"有效";
    [arr addObject:mode];
    
    mode = [[UnderLinePolicyMode alloc]init];
    mode.logo = @"1ppt4.jpg";
    mode.dateStr = @"20170503 9:30";
    mode.titleStr = @"国华康运一生重大疾病A险国华康运一生重大疾病A险";
    mode.tbPeopleStr = @"雷雨";
    mode.bbPeopleStr = @"雷崇文";
    mode.priceStr = @"7900000元";
    mode.statusStr = @"有效";
    [arr addObject:mode];
    
    
    mode = [[UnderLinePolicyMode alloc]init];
    mode.logo = @"1ppt4.jpg";
    mode.dateStr = @"20170503 9:30";
    mode.titleStr = @"国华康运一生重大疾病A险国华康运一生重大疾病A险国华康运一生重大疾病A险国华康运一生重大疾病A险国华康运一生重大疾病A险";
    mode.tbPeopleStr = @"雷雨";
    mode.bbPeopleStr = @"雷崇文";
    mode.priceStr = @"7900000元";
    mode.statusStr = @"有效";
    [arr addObject:mode];
    
    mode = [[UnderLinePolicyMode alloc]init];
    mode.logo = @"1ppt4.jpg";
    mode.dateStr = @"20170503 9:30";
    mode.titleStr = @"国华康运一生重大疾病A险国华康运一生重大疾病A险";
    mode.tbPeopleStr = @"雷雨";
    mode.bbPeopleStr = @"雷崇文";
    mode.priceStr = @"7900000元";
    mode.statusStr = @"有效";
    [arr addObject:mode];
    
    mode = [[UnderLinePolicyMode alloc]init];
    mode.logo = @"1ppt4.jpg";
    mode.dateStr = @"20170503 9:30";
    mode.titleStr = @"国华康运一生重大疾病A险国华康运一生重大疾病A险国华康运一生重大疾病A险";
    mode.tbPeopleStr = @"雷雨";
    mode.bbPeopleStr = @"雷崇文";
    mode.priceStr = @"7900000元";
    mode.statusStr = @"有效";
    [arr addObject:mode];
    
    
    return  [arr copy];
}

@end
