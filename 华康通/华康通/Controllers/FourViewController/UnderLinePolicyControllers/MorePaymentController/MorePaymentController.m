
#import "MorePaymentController.h"
#import "PaymentHistoryCell.h"
#import "CSSignViewController.h"
#import "RecordListModel.h"

@interface MorePaymentController ()

@property (nonatomic, strong) NSArray* cells;
@property (nonatomic, strong) ShowNoNetworkView* workView;

@end

@implementation MorePaymentController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"缴费历史记录";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:LoadImage(@"back1") style:UIBarButtonItemStylePlain target:self action:@selector(popRootVC)];
    [self.tableView registerNib:[UINib nibWithNibName:@"PaymentHistoryCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    //请求数据
    [self postRecordList];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [MobClick endLogPageView:@"MorePaymentController"];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"MorePaymentController"];//("PageOne"为页面名称，可自定义)
}

- (void)popRootVC
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark  //获取缴费记录数据列表
- (void)postRecordList
{
    NSDictionary* json = @{@"insContId":self.insContId};
    NSLog(@"json = %@",json);
    //字符串转字符串
    NSString* str = [JSONToString dictionaryToJson:json];
    //转md5
    MyAdditions* md5 = [[MyAdditions alloc]init];
    md5.str = [NSString stringWithFormat:@"%@%@",KEY_POST,str];
    NSString* md5_Str = [md5 md5];
    NSString* url = [NSString stringWithFormat:@"%@%@?sign=%@",SERVER_URL1,@"/qryPaymentList",md5_Str];
    [self getRecordListWithURL:url withParam:json];
}

- (void)getRecordListWithURL:(NSString*)url withParam:(NSDictionary*)param
{
    HKTShowNetworkView* networking = [HKTShowNetworkView shareInstanceWithTitle:@"检测到您的设备没有连接网络!" withImageStr:@"no_record" withType:0 withFrame:self.view.frame withVC:self];
    [networking showNetworkViewWithSuccess:^{
        self.tableView.scrollEnabled = YES;
        [XHNetworking POST:url parameters:param  success:^(NSData *responseObject) {
            //网络读取数据
            [self readRecordListWithData:responseObject];
        } failure:^(NSError *error) {
            NSLog(@"error.code = %@",error.userInfo);
            dispatch_async(dispatch_get_main_queue(), ^{
                [self createWorkView];
            });
        }];
        
    } failure:^{
        self.tableView.scrollEnabled = NO;
    } click:^(NSInteger type) {
        NSLog(@"点击了什么类型的按钮%ld!",(long)type);
        if (type == 0)
        {
            //弹出网络提示
            
        }
    }];
    
}

- (void)createWorkView
{
    if (self.workView == nil)
    {
        self.workView = [[ShowNoNetworkView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        self.workView.labelText = @"数据加载超时,点击刷新按钮重新加载!";
        self.workView.imageName = @"no_record";
        self.workView.myType = MyTypeOfViewForShowNoLists;
        [self.workView createAllSubView];
        __weak MorePaymentController* mySelf = self;
        [self.workView addActionWithBlock:^{
            [mySelf postRecordList];
        }];
        [self.tableView addSubview:self.workView];
    }
    
}

- (void)readRecordListWithData:(NSData*)data
{
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSLog(@"缴费记录数据列表json = %@",json);
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        if ([json isKindOfClass:[NSDictionary class]] && json)
        {
            if ([json[@"resultCode"] isEqualToString:@"0"])
            {
                if (![json[@"responseObject"] isKindOfClass:[NSNull class]] && json[@"responseObject"])
                {
                    // 处理耗时操作的代码块...
                    NSMutableArray* array = [NSMutableArray array];
                    for (NSDictionary* dict in json[@"responseObject"])
                    {
                        RecordListModel* list = [RecordListModel recordListWithJSON:dict];
                        [array addObject:list];
                    }
                    self.cells = [array copy];
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                    });
                }
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                });
            }
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
            });
        }
        //通知主线程刷新
        dispatch_async(dispatch_get_main_queue(), ^{
            //回调或者说是通知主线程刷新，
            if (self.cells.count == 0)
            {
                [self createWorkView];
            }
            else
            {
                self.workView.frame = CGRectZero;
                [self.workView removeFromSuperview];
                self.workView = nil;
            }
            [self.tableView reloadData];
        });
    });
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    return 10;
    return self.cells.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 75.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PaymentHistoryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    if (self.cells.count > 0)
    {
        RecordListModel* model = self.cells[indexPath.row];
        cell.topLabel.text = model.payActualDate;
        cell.bottomLabel.text = [NSString stringWithFormat:@"%@元",model.payActualAmont];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    CSSignViewController* cssSignVC = [[CSSignViewController alloc]initWithNibName:@"CSSignViewController" bundle:nil];
//    [self.navigationController pushViewController:cssSignVC animated:YES];
}

@end
