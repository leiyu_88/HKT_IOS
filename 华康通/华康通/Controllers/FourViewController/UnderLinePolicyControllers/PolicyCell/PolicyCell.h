//
//  PolicyCell.h
//  华康通
//
//  Created by  雷雨 on 2017/5/11.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UnderLinePolicyMode.h"
#import "PolicyListModel.h"


@interface PolicyCell : UITableViewCell

//@property (nonatomic, strong) UnderLinePolicyMode* mode;

@property (nonatomic, strong) PolicyListModel* mode;


//投保时间
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
//购买的保险公司logo
//@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
//保险标题
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
//投保人
@property (weak, nonatomic) IBOutlet UILabel *peopleLabel1;
//被保人
@property (weak, nonatomic) IBOutlet UILabel *peopleLabel2;
//保费
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
//保单状态
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UIView *lineView;


@end
