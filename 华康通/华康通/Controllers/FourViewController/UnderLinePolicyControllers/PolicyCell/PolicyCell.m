
#import "PolicyCell.h"

//#define originX (16 + 45 + 10)
#define originX (16)
#define H self.titleLabel.frame.size.height
//#define E (UISCREENWEITH - 45 - 32 - 10)
#define E (UISCREENWEITH - 32)
#define I (12 + 5)

@implementation PolicyCell

//- (void)setMode:(UnderLinePolicyMode *)mode
//{
//    _mode = mode;
//    self.logoImageView.frame = CGRectMake(16, 20, 45, 45);
//    self.logoImageView.image = LoadImage(mode.logo);
//    self.logoImageView.layer.borderWidth = 1.0;
//    self.logoImageView.layer.borderColor = F3F3F3Color.CGColor;
//    
//    self.titleLabel.numberOfLines = 0;
//    self.titleLabel.text = mode.titleStr;
//    //定位发布的消息label
//    CGRect rectOfText = CGRectMake(0, 0, E, 9999);
//    rectOfText = [self.titleLabel textRectForBounds:rectOfText limitedToNumberOfLines:0];
//    CGRect frameOfLabel = CGRectZero;
//    frameOfLabel.origin.x = originX;
//    frameOfLabel.origin.y = 15;
//    frameOfLabel.size = rectOfText.size;
//    self.titleLabel.frame = frameOfLabel;
//    
//    self.peopleLabel1.text = [NSString stringWithFormat:@"投保人：%@",mode.tbPeopleStr];
//    self.peopleLabel1.frame = CGRectMake(originX, H + 10 + 15, E/2, 12);
//    
//    self.peopleLabel2.text = [NSString stringWithFormat:@"被保人：%@",mode.bbPeopleStr];
//    self.peopleLabel2.frame = CGRectMake(originX + (E/2), H + 10 + 15, E/2, 12);
//    
//    self.priceLabel.text = [NSString stringWithFormat:@"保费：%@",mode.priceStr];
//    self.priceLabel.frame = CGRectMake(originX, H + 10 + 15 + I, E, 12);
//    
//    self.dateLabel.text = [NSString stringWithFormat:@"投保时间：%@",mode.dateStr];
//    self.dateLabel.frame = CGRectMake(originX, H + 10 + 15 + (2*I) , E, 12);
//    
//    self.statusLabel.text = [NSString stringWithFormat:@"投保状态：%@",mode.statusStr];
//    self.statusLabel.frame = CGRectMake(originX, H + 10 + 15 + (3*I) , E, 12);
//    
//    CGRect bounds = self.bounds;
//    bounds.size.height = H + 10 + 15 + (3*I) + 12 + 20;
//    self.bounds = bounds;
//    
//    self.lineView.frame = CGRectMake(0, H + 10 + 15 + (3*I) + 12 + 18, UISCREENWEITH, 2);
//}


- (void)setMode:(PolicyListModel *)mode
{
    _mode = mode;
    self.titleLabel.numberOfLines = 0;
    self.titleLabel.text = [NSString stringWithFormat:@"%@%@",mode.companyName,mode.productName];
    //定位发布的消息label
    CGRect rectOfText = CGRectMake(0, 0, E, 9999);
    rectOfText = [self.titleLabel textRectForBounds:rectOfText limitedToNumberOfLines:0];
    CGRect frameOfLabel = CGRectZero;
    frameOfLabel.origin.x = originX;
    frameOfLabel.origin.y = 15;
    frameOfLabel.size = rectOfText.size;
    self.titleLabel.frame = frameOfLabel;
    
    self.peopleLabel1.text = [NSString stringWithFormat:@"投保人：%@",mode.appntName];
    self.peopleLabel1.frame = CGRectMake(originX, H + 10 + 15, E/2, 12);
    
    self.peopleLabel2.text = [NSString stringWithFormat:@"被保人：%@",mode.inseredName];
    self.peopleLabel2.frame = CGRectMake(originX + (E/2), H + 10 + 15, E/2, 12);
    
    self.priceLabel.text = [NSString stringWithFormat:@"保费：%@元",mode.premium];
    self.priceLabel.frame = CGRectMake(originX, H + 10 + 15 + I, E, 12);
    
    self.dateLabel.text = [NSString stringWithFormat:@"受理日期：%@",mode.acceptDate];
    self.dateLabel.frame = CGRectMake(originX, H + 10 + 15 + (2*I) , E, 12);
    
    self.statusLabel.text = [NSString stringWithFormat:@"投保状态：%@",mode.status];
    self.statusLabel.frame = CGRectMake(originX, H + 10 + 15 + (3*I) , E, 12);
    
    CGRect bounds = self.bounds;
    bounds.size.height = H + 10 + 15 + (3*I) + 12 + 20;
    self.bounds = bounds;
    
    self.lineView.frame = CGRectMake(0, H + 10 + 15 + (3*I) + 12 + 18, UISCREENWEITH, 2);
    
    
    
    
//    self.logoImageView.frame = CGRectMake(16, 20, 45, 45);
//    //异步加载图片
//    [self.logoImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",mode.companyLogo]]
//                          placeholderImage:LoadImage(@"loading_figure")];
//    self.logoImageView.layer.borderWidth = 1.0;
//    self.logoImageView.layer.borderColor = F3F3F3Color.CGColor;
//    
//    self.titleLabel.numberOfLines = 0;
//    self.titleLabel.text = [NSString stringWithFormat:@"%@%@",mode.companyName,mode.productName];
//    //定位发布的消息label
//    CGRect rectOfText = CGRectMake(0, 0, E, 9999);
//    rectOfText = [self.titleLabel textRectForBounds:rectOfText limitedToNumberOfLines:0];
//    CGRect frameOfLabel = CGRectZero;
//    frameOfLabel.origin.x = originX;
//    frameOfLabel.origin.y = 15;
//    frameOfLabel.size = rectOfText.size;
//    self.titleLabel.frame = frameOfLabel;
//    
//    self.peopleLabel1.text = [NSString stringWithFormat:@"投保人：%@",mode.appntName];
//    self.peopleLabel1.frame = CGRectMake(originX, H + 10 + 15, E/2, 12);
//    
//    self.peopleLabel2.text = [NSString stringWithFormat:@"被保人：%@",mode.inseredName];
//    self.peopleLabel2.frame = CGRectMake(originX + (E/2), H + 10 + 15, E/2, 12);
//    
//    self.priceLabel.text = [NSString stringWithFormat:@"保费：%@元",mode.premium];
//    self.priceLabel.frame = CGRectMake(originX, H + 10 + 15 + I, E, 12);
//    
//    self.dateLabel.text = [NSString stringWithFormat:@"投保时间：%@",mode.applyDate];
//    self.dateLabel.frame = CGRectMake(originX, H + 10 + 15 + (2*I) , E, 12);
//    
//    self.statusLabel.text = [NSString stringWithFormat:@"投保状态：%@",mode.status];
//    self.statusLabel.frame = CGRectMake(originX, H + 10 + 15 + (3*I) , E, 12);
//    
//    CGRect bounds = self.bounds;
//    bounds.size.height = H + 10 + 15 + (3*I) + 12 + 20;
//    self.bounds = bounds;
//    
//    self.lineView.frame = CGRectMake(0, H + 10 + 15 + (3*I) + 12 + 18, UISCREENWEITH, 2);
    
}

@end
