//
//  TopButtonsView.h
//  华康通
//
//  Created by  雷雨 on 2017/5/10.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TopButtonsView : UIView

@property (nonatomic, strong) UIScrollView* myScrollView;
@property (nonatomic, strong) NSArray* buttonTitles;
@property (nonatomic, strong) NSMutableArray* allButtons;
@property (nonatomic, strong) UIView* bottomView;

- (void)scrollView;
- (void)removeScrollView;

@end
