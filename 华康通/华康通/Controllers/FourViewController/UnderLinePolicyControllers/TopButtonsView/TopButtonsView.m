//
//  TopButtonsView.m
//  华康通
//
//  Created by  雷雨 on 2017/5/10.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import "TopButtonsView.h"

@implementation TopButtonsView

- (NSMutableArray*)allButtons
{
    if (!_allButtons)
    {
        _allButtons = [NSMutableArray array];
    }
    return _allButtons;
}

//更新表格
- (void)changeData:(UIButton*)sender
{
    for (UIButton* button in self.allButtons)
    {
        if (button.tag == sender.tag)
        {
            [button setTitleColor:KMainColor forState:UIControlStateNormal];
        }
        else
        {
            [button setTitleColor:B4B4BColor forState:UIControlStateNormal];
        }
    }
    [self animationForViewWithButton:sender withTag:sender.tag];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"TopButtonsView" object:self userInfo:@{@"buttonTag":[NSString stringWithFormat:@"%ld",(long)sender.tag],@"buttonArray":self.allButtons}];
}

- (void)scrollView
{
    self.myScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, UISCREENWEITH, self.frame.size.height)];
    self.myScrollView.contentSize = CGSizeMake(self.buttonTitles.count * UISCREENWEITH / self.buttonTitles.count, self.frame.size.height);
    self.myScrollView.userInteractionEnabled = YES;
    self.myScrollView.bounces = YES;
    self.myScrollView.scrollEnabled = NO;
    self.myScrollView.pagingEnabled = NO;
    self.myScrollView.showsHorizontalScrollIndicator = NO;
    self.myScrollView.showsVerticalScrollIndicator = NO;
    for (int i = 0; i<self.buttonTitles.count; i++)
    {
        UIButton* button = [UIButton buttonWithType:UIButtonTypeSystem];
        button.titleLabel.font = SetFont(16.0);
        [button setTitle:self.buttonTitles[i] forState:UIControlStateNormal];
        [button setTitleColor:B4B4BColor forState:UIControlStateNormal];
        button.frame = CGRectMake(UISCREENWEITH/self.buttonTitles.count * i, 0, UISCREENWEITH/self.buttonTitles.count, self.myScrollView.frame.size.height);
        //去掉高亮
        button.adjustsImageWhenHighlighted = NO;
        [button addTarget:self action:@selector(changeData:) forControlEvents:UIControlEventTouchUpInside];
        button.tag = i;
        if (i == 0)
        {
            //得到文字的宽度
            CGFloat textWidth = [self calculateRowWidth:button.titleLabel.text];
            //根据文字的宽度 确定线条的长度
            CGFloat lineWidth = textWidth + 8;
            self.bottomView = [[UIView alloc]initWithFrame:CGRectMake((button.frame.size.width - lineWidth)/2, self.myScrollView.frame.size.height - 4, lineWidth, 2)];
            self.bottomView.backgroundColor = KMainColor;
            [button addSubview:self.bottomView];
            [button setTitleColor:KMainColor forState:UIControlStateNormal];
        }
        [self.allButtons addObject:button];
        [self.myScrollView addSubview:button];
    }
    UIView* lineView = [[UIView alloc]initWithFrame:CGRectMake(0, self.myScrollView.frame.size.height - 2, self.frame.size.width, 2)];
    lineView.backgroundColor = [UIColor colorWithRed:246.0/255.0 green:246.0/255.0 blue:246.0/255.0 alpha:1.0];
    [self addSubview:self.myScrollView];
    [self addSubview:lineView];
}

//根据内容计算宽度
- (CGFloat)calculateRowWidth:(NSString *)string
{
    NSDictionary *dic = @{NSFontAttributeName:SetFont(16.0)};//指定字号
    CGRect rect = [string boundingRectWithSize:CGSizeMake(0, 30)/*计算宽度时要确定高度*/ options:NSStringDrawingUsesLineFragmentOrigin |
                   NSStringDrawingUsesFontLeading attributes:dic context:nil];
    return rect.size.width;
}

- (void)animationForViewWithButton:(UIButton*)button withTag:(NSInteger)tag
{
    //得到文字的宽度
    CGFloat textWidth = [self calculateRowWidth:button.titleLabel.text];
    //根据文字的宽度 确定线条的长度
    CGFloat lineWidth = textWidth + 8;
    [UIView animateWithDuration:0.3 animations:^{
        self.bottomView.frame = CGRectMake((lineWidth * tag) + (((button.frame.size.width - lineWidth)/2)*(2*tag+1)), self.myScrollView.frame.size.height - 4, lineWidth, 2);
    }];
}

- (void)removeScrollView
{
    [self.myScrollView removeFromSuperview];
}

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        self.backgroundColor = FFFFFFColor;
    }
    
    return self;
}


@end
