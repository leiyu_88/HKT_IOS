//
//  DetailPolicyCell.h
//  华康通
//
//  Created by  雷雨 on 2017/5/12.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailPolicyModel.h"

@interface DetailPolicyCell : UITableViewCell
@property (nonatomic, strong) DetailPolicyModel* model;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

@end
