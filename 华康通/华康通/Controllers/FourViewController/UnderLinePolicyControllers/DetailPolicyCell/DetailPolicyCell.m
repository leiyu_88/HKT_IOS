//
//  DetailPolicyCell.m
//  华康通
//
//  Created by  雷雨 on 2017/5/12.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import "DetailPolicyCell.h"

@implementation DetailPolicyCell

- (void)setModel:(DetailPolicyModel *)model
{
    _model = model;
    self.contentLabel.text = model.title;
    
    //设置行距
    
    [self setHJWithLabel:self.contentLabel];
    
    CGRect textFrame = CGRectMake(0, 0, UISCREENWEITH - 32, 9999);
    textFrame = [self.contentLabel textRectForBounds:textFrame limitedToNumberOfLines:0];
    CGRect frameOfLabel = CGRectZero;
    frameOfLabel.size = textFrame.size;
    frameOfLabel.origin.x = 16;
    frameOfLabel.origin.y = 4;
    self.contentLabel.frame = frameOfLabel;
    
    CGRect bounds = self.bounds;
    bounds.size.height = 8 + self.contentLabel.frame.size.height;
    self.bounds = bounds;
}

//设置行距；
- (void)setHJWithLabel:(UILabel*)label
{
    //创建NSMutableAttributedString实例，并将text传入
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc]initWithString:label.text];
    //创建NSMutableParagraphStyle实例
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc]init];
    //设置行距
    [style setLineSpacing:8.0f];
    //根据给定长度与style设置attStr式样
    [attStr addAttribute:NSParagraphStyleAttributeName value:style range:NSMakeRange(0, attStr.length)];
    //Label获取attStr式样
    label.attributedText = attStr;
}

@end
