//
//  DetailPolicyController.m
//  华康通
//
//  Created by  雷雨 on 2017/5/11.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import "DetailPolicyController.h"
#import "DetailPolicyCell.h"
#import "SYRCell.h"
#import "PaymentCell.h"
#import "MorePaymentController.h"
#import "DetailPolicyModel.h"
#import "PolicyDetailModel.h"
#import "XZDetailModel.h"
#import "UserDetailModel.h"
#import "XQDetailModel.h"


@interface DetailPolicyController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView* tableView;

//
//
//********* 以下是测试数据 **********
//
//


//保单信息行数组
@property (nonatomic, strong) NSArray* BDXXArray;
//主险数组
@property (nonatomic, strong) NSArray* ZXArray;
//附加险数组
@property (nonatomic, strong) NSArray* FJXArray;

//投保人数组
@property (nonatomic, strong) NSArray* TBArray;
//被保人数组
@property (nonatomic, strong) NSArray* BBArray;
//受益人数组
@property (nonatomic, strong) NSArray* SYRArray;

//所有的数据写入一个字典
@property (nonatomic, strong) NSDictionary* allDic;

@property (nonatomic, strong) ShowNoNetworkView* workView;

//保单信息行字典
@property (nonatomic, strong) NSDictionary* BDXXDic;
////主险数组
//@property (nonatomic, strong) NSDictionary* ZXDic;
////附加险数组
//@property (nonatomic, strong) NSDictionary* FJXDic;
//
////投保人数组
//@property (nonatomic, strong) NSDictionary* TBDic;
////被保人数组
//@property (nonatomic, strong) NSDictionary* BBDic;
////受益人数组
//@property (nonatomic, strong) NSDictionary* SYRDic;

//续期缴费所有信息
@property (nonatomic, strong) XQDetailModel* XQModel;

//保存附加险的数量
@property (nonatomic, unsafe_unretained) NSInteger FJXCount;

//保存投保人的数量
@property (nonatomic, unsafe_unretained) NSInteger TBRCount;

//受益人

//
//
//********* 以上是测试数据 **********
//
//



@end

@implementation DetailPolicyController


- (UITableView*)tableView
{
    if (!_tableView)
    {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, UISCREENWEITH, UISCREENHEIGHT - NavigationBarHeight - StatusBarHeight - 50) style:UITableViewStylePlain];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = F3F3F3Color;
    }
    return _tableView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view addSubview:self.tableView];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:LoadImage(@"back1") style:UIBarButtonItemStylePlain target:self action:@selector(popRootVC)];
    self.title = @"保单详情";
    [self.tableView registerNib:[UINib nibWithNibName:@"SYRCell" bundle:nil] forCellReuseIdentifier:@"SYRCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"PaymentCell" bundle:nil] forCellReuseIdentifier:@"PaymentCell"];
    self.TBRCount = 0;
    self.FJXCount = 0;
    [self postDetailBD];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [MobClick endLogPageView:@"DetailPolicyController"];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"DetailPolicyController"];//("PageOne"为页面名称，可自定义)
}

- (void)popRootVC
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark  //获取保单明细数据
- (void)postDetailBD
{
    NSDictionary* json = @{@"insContId":self.insContId};
    NSLog(@"json = %@",json);
    NSString* str = [JSONToString dictionaryToJson:json];
    MyAdditions* md5 = [[MyAdditions alloc]init];
    md5.str = [NSString stringWithFormat:@"%@%@",KEY_POST,str];
    NSString* md5_Str = [md5 md5];
    NSString* url = [NSString stringWithFormat:@"%@%@?sign=%@",SERVER_URL1,@"/qryInsOrderDetail.do",md5_Str];
    [self getDetailBDWithURL:url withParam:json];
//    NSString* jsonStr = [NSString stringWithFormat:@"{\"%@\":\"%@\"}",@"insContId",self.insContId];
//    MyAdditions* md5 = [[MyAdditions alloc]init];
//    md5.str = [NSString stringWithFormat:@"%@%@",KEY_POST,jsonStr];
//    NSString* md5_Str = [md5 md5];
//    NSString* url = [NSString stringWithFormat:@"%@%@?sign=%@",SERVER_URL1,@"/qryInsOrderDetail",md5_Str];
//    [self getDetailBDWithURL:url withParam:jsonStr withCacheStr:cacheStr];
}

- (void)getDetailBDWithURL:(NSString*)url withParam:(NSDictionary*)param
{
    HKTShowNetworkView* networking = [HKTShowNetworkView shareInstanceWithTitle:@"检测到您的设备没有连接网络!" withImageStr:@"no_record" withType:0 withFrame:self.view.frame withVC:self];
    [networking showNetworkViewWithSuccess:^{
        self.tableView.scrollEnabled = YES;
        [XHNetworking POST:url parameters:param  success:^(NSData *responseObject) {
            //网络读取数据
            [self readDetailBDWithData:responseObject];
        } failure:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self createWorkView];
            });
            NSLog(@"error.code = %@",error.userInfo);
        }];
        
    } failure:^{
        self.tableView.scrollEnabled = NO;
    } click:^(NSInteger type) {
        NSLog(@"点击了什么类型的按钮%ld!",(long)type);
        if (type == 0)
        {
            //弹出网络提示
            
        }
    }];
    

}

- (void)createWorkView
{
    if (self.workView == nil)
    {
        self.workView = [[ShowNoNetworkView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        self.workView.labelText = @"数据加载超时,点击刷新按钮重新加载!";
        self.workView.imageName = @"no_record";
        self.workView.myType = MyTypeOfViewForShowNoLists;
        [self.workView createAllSubView];
        __weak DetailPolicyController* mySelf = self;
        [self.workView addActionWithBlock:^{
            [mySelf postDetailBD];
        }];
        [self.tableView addSubview:self.workView];
    }
    
}

- (void)readDetailBDWithData:(NSData*)data
{
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSLog(@"保单明细数据json = %@",json);
    NSMutableDictionary* allDic = [NSMutableDictionary dictionary];
    if ([json isKindOfClass:[NSDictionary class]] && json)
    {
        if ([json[@"resultCode"] isEqualToString:@"0"])
        {
            if (json[@"responseObject"][@"contract"] && ![json[@"responseObject"][@"contract"] isKindOfClass:[NSNull class]] && [json[@"responseObject"][@"contract"] isKindOfClass:[NSDictionary class]])
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    PolicyDetailModel* model = [PolicyDetailModel policyDetailWithJSON:json[@"responseObject"][@"contract"]];
                    //拿到保单基本信息行内容
                    NSString* sectionTitle = [NSString stringWithFormat:@"%@",model.productkName];
                    self.BDXXDic = @{@"section":@"保单基本信息",
                                     @"sectionTitle":sectionTitle,
                                     @"cellArray":[self getBBXXArrayWithModel:model]};
                });
            }
            if (json[@"responseObject"][@"plan"] && ![json[@"responseObject"][@"plan"] isKindOfClass:[NSNull class]] && [json[@"responseObject"][@"plan"] isKindOfClass:[NSArray class]])
            {
                //主险数组
                NSMutableArray* array1 = [NSMutableArray array];
                //附加险数组
                NSMutableArray* array2 = [NSMutableArray array];
                dispatch_async(dispatch_get_main_queue(), ^{
                    for (NSDictionary* dic in json[@"responseObject"][@"plan"])
                    {
                        XZDetailModel* model = [XZDetailModel policyDetailWithJSON:dic];
                        //拿到主险数组
                        if ([model.isMainRisk isEqualToString:@"主约"])
                        {
                            NSString* sectionTitle = [NSString stringWithFormat:@"%@",model.riskName];
                            NSDictionary* ZXDic = @{@"section":@"险种信息",
                                                    @"sectionTitle":sectionTitle,
                                                    @"cellArray":[self getXZXXArrayWithModel:model]};
                            [array1 addObject:ZXDic];
                            self.ZXArray = [array1 copy];
                        }
                        //拿到附加险数组
                        else
                        {
                            NSString* sectionTitle = [NSString stringWithFormat:@"%@",model.riskName];
                            NSString* str = [NSString stringWithFormat:@"附加险%@",[self arabicNumeralsToChinese:self.FJXCount withType:@"0"]];
                            NSDictionary* FXDic = @{@"section":str,
                                                    @"sectionTitle":sectionTitle,
                                                    @"cellArray":[self getXZXXArrayWithModel:model]};
                            [array2 addObject:FXDic];
                            self.FJXCount += 1;
                            self.FJXArray = [array2 copy];
                        }
                    }
                });
            }
            if (json[@"responseObject"][@"user"] && ![json[@"responseObject"][@"user"] isKindOfClass:[NSNull class]] && [json[@"responseObject"][@"user"] isKindOfClass:[NSArray class]])
            {
                //投保人数组
                NSMutableArray* array1 = [NSMutableArray array];
                //被投保人数组
                NSMutableArray* array2 = [NSMutableArray array];
                //受益人数组
                NSMutableArray* array3 = [NSMutableArray array];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    for (NSDictionary* dic in json[@"responseObject"][@"user"])
                    {
                        UserDetailModel* model = [UserDetailModel policyDetailWithJSON:dic];
                        //拿到投保人数组
                        if ([model.userType isEqualToString:@"1"])
                        {
                            NSString* sectionTitle = [NSString stringWithFormat:@"姓名：%@",model.name];
                            NSDictionary* ZXDic = @{@"section":@"投保人信息",
                                                    @"sectionTitle":sectionTitle,
                                                    @"cellArray":[self getUserArrayWithModel:model withType:model.userType]};
                            [array1 addObject:ZXDic];
                            self.TBArray = [array1 copy];
                        }
                        //拿到被保人数组
                        else if ([model.userType isEqualToString:@"2"])
                        {
                            NSString* sectionTitle = [NSString stringWithFormat:@"与投保人关系：%@",model.appInsType];
                            NSString* str = [NSString stringWithFormat:@"被保人信息%@",[self arabicNumeralsToChinese:self.TBRCount withType:@"0"]];
                            NSDictionary* BBDic = @{@"section":str,
                                                    @"sectionTitle":sectionTitle,
                                                    @"cellArray":[self getUserArrayWithModel:model withType:model.userType]};
                            [array2 addObject:BBDic];
                            self.TBRCount += 1;
                            self.BBArray = [array2 copy];
                        }
                        //拿到受益人数组
                        else
                        {
                            NSString* sectionTitle = [NSString stringWithFormat:@"受益人类型：%@",model.bnfType];
                            NSDictionary* SYRDic = @{@"section":@"受益人信息",
                                                    @"sectionTitle":sectionTitle,
                                                    @"cellArray":model};
                            [array3 addObject:SYRDic];
                            self.SYRArray = [array3 copy];
                        }
                    }
                });
            }
            if (json[@"responseObject"][@"payment"] && ![json[@"responseObject"][@"payment"] isKindOfClass:[NSNull class]] && [json[@"responseObject"][@"payment"] isKindOfClass:[NSDictionary class]])
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    XQDetailModel* model = [XQDetailModel policyDetailWithJSON:json[@"responseObject"][@"payment"]];
                    //拿到续期缴费信息所有字段
                    self.XQModel = model;
                });
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                //把所有数组 变成健值对
                [allDic setValue:self.BDXXDic forKey:@"0"];
                
                for (NSInteger i = 0; i < self.ZXArray.count; i++)
                {
                    NSDictionary* dict = self.ZXArray[i];
                    [allDic setValue:dict forKey:[NSString stringWithFormat:@"%ld",(long)(i + 1)]];
                }
                for (NSInteger j = 0; j < self.FJXArray.count; j++)
                {
                    NSDictionary* dict = self.FJXArray[j];
                    [allDic setValue:dict forKey:[NSString stringWithFormat:@"%ld",(long)(self.ZXArray.count + 1 + j)]];
                }
                for (NSInteger i = 0; i < self.TBArray.count; i++)
                {
                    NSDictionary* dict = self.TBArray[i];
                    [allDic setValue:dict forKey:[NSString stringWithFormat:@"%ld",(long)(self.ZXArray.count + self.FJXArray.count + 1 + i)]];
                }
                for (NSInteger i = 0; i < self.BBArray.count; i++)
                {
                    NSDictionary* dict = self.BBArray[i];
                    [allDic setValue:dict forKey:[NSString stringWithFormat:@"%ld",(long)(i + self.ZXArray.count + self.FJXArray.count + 1 + self.TBArray.count)]];
                }
                if (self.SYRArray.count > 0)
                {
                    [allDic setValue:self.SYRArray forKey:[NSString stringWithFormat:@"%ld",(long)(self.ZXArray.count + self.FJXArray.count + 1 + self.TBArray.count +  self.BBArray.count)]];
                    if (self.XQModel)
                    {
                        [allDic setValue:self.XQModel forKey:[NSString stringWithFormat:@"%ld",(long)(self.ZXArray.count + self.FJXArray.count + 1 + self.TBArray.count +  self.BBArray.count + 1)]];
                    }
                }
                else
                {
                    if (self.XQModel)
                    {
                        [allDic setValue:self.XQModel forKey:[NSString stringWithFormat:@"%ld",(long)(self.ZXArray.count + self.FJXArray.count + 1 + self.TBArray.count +  self.BBArray.count)]];
                    }
                }
                self.allDic = [allDic copy];
                if (self.allDic)
                {
                    self.workView.frame = CGRectZero;
                    [self.workView removeFromSuperview];
                    self.workView = nil;
                }
                else
                {
                    [self createWorkView];
                }
                [self.tableView reloadData];
            });
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self createWorkView];
            });
        }
    }
}

//返回主险的数量
- (NSInteger)getCountWithArr:(NSArray*)arr withType:(NSString*)type withCS:(NSString*)cs
{
    NSMutableArray* array = [NSMutableArray array];
    for (NSDictionary* dic in arr)
    {
        if ([dic[cs] isEqualToString:type])
        {
            [array addObject:dic];
        }
    }
    return [[array copy] count];
}

//阿拉伯数字转成汉字
-(NSString *)arabicNumeralsToChinese:(NSInteger)number withType:(NSString*)type
{
    switch (number) {
        case 0:
        {
            if ([type isEqualToString:@"syr"])
            {
                return @"一";
            }
            else
            {
                return @"";
            }
        }
            break;
        case 1:
            return @"二";
            break;
        case 2:
            return @"三";
            break;
        case 3:
            return @"四";
            break;
        case 4:
            return @"五";
            break;
        case 5:
            return @"六";
            break;
        case 6:
            return @"七";
            break;
        case 7:
            return @"八";
            break;
        case 8:
            return @"九";
            break;
        case 9:
            return @"十";
            break;
        case 10:
            return @"十一";
            break;
        case 11:
            return @"十二";
            break;
        case 12:
            return @"十三";
            break;
        case 13:
            return @"十四";
            break;
        case 14:
            return @"十五";
            break;
        case 15:
            return @"十六";
            break;
        case 16:
            return @"十七";
            break;
        case 17:
            return @"十八";
            break;
        case 18:
            return @"十九";
            break;
        case 19:
            return @"二十";
            break;
        case 20:
            return @"二十一";
            break;
        default:
            return @"";
            break;
    }
}
    
//处理保单基本信息展示
- (NSArray*)getBBXXArrayWithModel:(PolicyDetailModel*)model
{
    NSMutableArray* arr = [NSMutableArray array];
    if (![model.companyName isEqualToString:@"-"])
    {
        [arr addObject:[NSString stringWithFormat:@"保险公司名称：%@",model.companyName]];
    }
    if (![model.orderNo isEqualToString:@"-"])
    {
        [arr addObject:[NSString stringWithFormat:@"投保单号：%@",model.orderNo]];
    }
    if (![model.contNo isEqualToString:@"-"])
    {
        [arr addObject:[NSString stringWithFormat:@"保单号：%@",model.contNo]];
    }
    if (![model.applyDate isEqualToString:@"-"])
    {
        [arr addObject:[NSString stringWithFormat:@"投保日期：%@",model.applyDate]];
    }
    if (![model.startDate isEqualToString:@"-"])
    {
        [arr addObject:[NSString stringWithFormat:@"受理日期：%@",model.acceptDate]];
    }
    if (![model.startDate isEqualToString:@"-"])
    {
        [arr addObject:[NSString stringWithFormat:@"生效日期：%@",model.startDate]];
    }
    if (![model.receiptDate isEqualToString:@"-"])
    {
        [arr addObject:[NSString stringWithFormat:@"交回执日期：%@",model.receiptDate]];
    }
    if (![model.premium isEqualToString:@"-"])
    {
        [arr addObject:[NSString stringWithFormat:@"保费合计：%@",model.premium]];
    }
    if (![model.status isEqualToString:@"-"])
    {
        [arr addObject:[NSString stringWithFormat:@"保单状态：%@",model.status]];
    }
    return [arr copy];
}

//处理险种信息展示
- (NSArray*)getXZXXArrayWithModel:(XZDetailModel*)model
{
    NSMutableArray* arr = [NSMutableArray array];
    if (![model.payMethod isEqualToString:@"-"])
    {
        [arr addObject:[NSString stringWithFormat:@"缴费方式：%@",model.payMethod]];
    }
    if (![model.insPeriod isEqualToString:@"-"])
    {
        [arr addObject:[NSString stringWithFormat:@"保障期间：%@",model.insPeriod]];
    }
    if (![model.payPeriod isEqualToString:@"-"])
    {
        [arr addObject:[NSString stringWithFormat:@"缴费期间：%@",model.payPeriod]];
    }
    if (![model.amount isEqualToString:@"-"])
    {
        [arr addObject:[NSString stringWithFormat:@"保额：%@",model.amount]];
    }
    if (![model.premium isEqualToString:@"-"])
    {
        [arr addObject:[NSString stringWithFormat:@"保费：%@",model.premium]];
    }
    return [arr copy];
}

//处理投保人被保人信息展示
- (NSArray*)getUserArrayWithModel:(UserDetailModel*)model withType:(NSString*)type
{
    NSMutableArray* arr = [NSMutableArray array];
    if ([type isEqualToString:@"2"])
    {
        if (![model.name isEqualToString:@"-"])
        {
            [arr addObject:[NSString stringWithFormat:@"姓名：%@",model.name]];
        }
    }
    if (![model.idType isEqualToString:@"-"])
    {
        [arr addObject:[NSString stringWithFormat:@"证件类型：%@",model.idType]];
    }
    if (![model.idNo isEqualToString:@"-"])
    {
        [arr addObject:[NSString stringWithFormat:@"证件号码：%@",model.idNo]];
    }
    if (![model.birthDate isEqualToString:@"-"])
    {
        [arr addObject:[NSString stringWithFormat:@"出生日期：%@",model.birthDate]];
    }
    if (![model.mobileNo isEqualToString:@"-"])
    {
        [arr addObject:[NSString stringWithFormat:@"手机号：%@",model.mobileNo]];
    }
    if (![model.email isEqualToString:@"-"])
    {
        [arr addObject:[NSString stringWithFormat:@"电子邮箱：%@",model.email]];
    }
//    if (![model.provice isEqualToString:@"-"])
//    {
//        [arr addObject:[NSString stringWithFormat:@"所在省：%@",model.provice]];
//    }
//    if (![model.city isEqualToString:@"-"])
//    {
//        [arr addObject:[NSString stringWithFormat:@"所在市：%@",model.city]];
//    }
    if (![model.address isEqualToString:@"-"])
    {
        [arr addObject:[NSString stringWithFormat:@"详细地址：%@",model.address]];
    }
    if (![model.occupationName isEqualToString:@"-"])
    {
        [arr addObject:[NSString stringWithFormat:@"职业：%@",model.occupationName]];
    }
    return [arr copy];
}


#pragma mark - 列表代理方法

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.allDic.allKeys.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id dic = self.allDic[[NSString stringWithFormat:@"%ld",(long)section]];
    if ([dic isKindOfClass:[XQDetailModel class]])
    {
        return 1;
    }
    else if ([dic isKindOfClass:[NSArray class]])
    {
        NSArray* arr = (NSArray*)dic;
        return arr.count;
    }
    else
    {
        return [dic[@"cellArray"] count];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    id dic = self.allDic[[NSString stringWithFormat:@"%ld",(long)section]];
    if ([dic isKindOfClass:[XQDetailModel class]])
    {
        return 40.0;
    }
    else if ([dic isKindOfClass:[NSArray class]])
    {
        return 40.0;
    }
    else
        return 80.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id dic = self.allDic[[NSString stringWithFormat:@"%ld",(long)indexPath.section]];
    if ([dic isKindOfClass:[XQDetailModel class]])
    {
        return 240.0;
    }
    else if ([dic isKindOfClass:[NSArray class]])
    {
        return 168.0;
    }
    else
    {
        UITableViewCell* cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];
        return cell.bounds.size.height;
    }
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    id dic = self.allDic[[NSString stringWithFormat:@"%ld",(long)section]];
    if ([dic isKindOfClass:[XQDetailModel class]])
    {
        UIView* view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, UISCREENWEITH, 40)];
        view.backgroundColor = F3F3F3Color;
        UILabel* label = [[UILabel alloc]initWithFrame:CGRectMake(16, 10, UISCREENWEITH - 16 - 16, 20)];
        label.text = @"续期缴费信息";
        label.textColor = KMainColor;
        label.font = SetFont(14.0);
        label.textAlignment = NSTextAlignmentLeft;
        [view addSubview:label];
        return view;
    }
    else if ([dic isKindOfClass:[NSArray class]])
    {
        UIView* view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, UISCREENWEITH, 40)];
        view.backgroundColor = F3F3F3Color;
        UILabel* label = [[UILabel alloc]initWithFrame:CGRectMake(16, 10, UISCREENWEITH - 16 - 16, 20)];
        label.text = @"受益人信息";
        label.textColor = KMainColor;
        label.font = SetFont(14.0);
        label.textAlignment = NSTextAlignmentLeft;
        [view addSubview:label];
        return view;
    }
    else
    {
        UIView* view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, UISCREENWEITH, 80)];
        view.backgroundColor = F3F3F3Color;
        UILabel* label = [[UILabel alloc]initWithFrame:CGRectMake(16, 10, UISCREENWEITH - 16 - 16, 20)];
        label.text = [NSString stringWithFormat:@"%@",dic[@"section"]];
        label.textColor = KMainColor;
        label.font = SetFont(14.0);
        label.textAlignment = NSTextAlignmentLeft;
        [view addSubview:label];
        
        UIView* writeView = [[UIView alloc]initWithFrame:CGRectMake(0, 40, 16, 40)];
        writeView.backgroundColor = FFFFFFColor;
        UILabel* titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(16, 40, UISCREENWEITH - 16, 40)];
        titleLabel.text = dic[@"sectionTitle"];
        titleLabel.textColor = SHOWCOLOR(@"2C2C2C");
        titleLabel.font = SetFont(14.0);
        titleLabel.textAlignment = NSTextAlignmentLeft;
        titleLabel.backgroundColor = FFFFFFColor;
        UIView* blueView = [[UIView alloc]initWithFrame:CGRectMake(16, 79, UISCREENWEITH - 16 - 16, 1)];
        blueView.backgroundColor = F3F3F3Color;
        [view addSubview:writeView];
        [view addSubview:titleLabel];
        [view addSubview:blueView];
        return view;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id dic = self.allDic[[NSString stringWithFormat:@"%ld",(long)indexPath.section]];
    if ([dic isKindOfClass:[XQDetailModel class]])
    {
        PaymentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PaymentCell" forIndexPath:indexPath];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell.showMoreButton addTarget:self action:@selector(showMorePayment:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
    else if ([dic isKindOfClass:[NSArray class]])
    {
        SYRCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SYRCell" forIndexPath:indexPath];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        NSArray* arr = (NSArray*)dic;
        UserDetailModel* model = arr[indexPath.row][@"cellArray"];
        cell.label1.text = [NSString stringWithFormat:@"第%@受益人",[self arabicNumeralsToChinese:indexPath.row withType:@"syr"]];
        cell.label2.text = [NSString stringWithFormat:@"与被保险人关系：%@",model.insBnfType];
        cell.label3.text = [NSString stringWithFormat:@"姓名：%@",model.name];
        cell.label4.text = [NSString stringWithFormat:@"证件类型：%@",model.idType];
        cell.label5.text = [NSString stringWithFormat:@"证件号码：%@",model.idNo];
        cell.label6.text = [NSString stringWithFormat:@"受益比例：%@",model.bnfRate];
        return cell;
    }
    else
    {
        DetailPolicyCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        if (cell == nil)
        {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"DetailPolicyCell" owner:self options:nil]lastObject];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        DetailPolicyModel *model = [[DetailPolicyModel alloc]init];
        model.title = dic[@"cellArray"][indexPath.row];
        cell.model = model;
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

//点击进入查看更多缴费历史记录页面
- (void)showMorePayment:(UIButton*)sender
{
    MorePaymentController* morePaymentVC = [[MorePaymentController alloc]initWithNibName:@"MorePaymentController" bundle:nil];
    morePaymentVC.insContId = self.insContId;
    [self.navigationController pushViewController:morePaymentVC animated:YES];
}

@end
