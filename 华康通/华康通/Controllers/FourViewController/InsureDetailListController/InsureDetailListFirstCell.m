//
//  InsureDetailListFirstCell.m
//  华康通
//
//  Created by leiyu on 16/11/10.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import "InsureDetailListFirstCell.h"

#define SIZEWIDTH (UISCREENWEITH - 32)

@implementation InsureDetailListFirstCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)setModel:(InsureDetailListMode *)model
{
    self.topLabel.frame = CGRectMake(16, 8, SIZEWIDTH, 20);
    self.topLabel.attributedText = [self getLabelAllColor:model.ddStr];
    self.centerlabel.attributedText = [self getLabelAllColor:model.tbStr];
    self.tbLabel.attributedText = [self getLabelAllColor:model.bdStr];
    self.statusLabel.attributedText = [self getLabelAllColor:model.bdStuesStr];
    self.bottomLabel.attributedText = [self getLabelAllColor:model.bzStr];
    //定位发布的消息label
    CGRect rectOfText = CGRectMake(0, 0, SIZEWIDTH, 9999);
    rectOfText = [self.centerlabel textRectForBounds:rectOfText limitedToNumberOfLines:0];
    CGRect frameOfLabel = CGRectZero;
    frameOfLabel.origin.x = 16;
    frameOfLabel.origin.y = 8 + 20 + 5;
    frameOfLabel.size = rectOfText.size;
    self.centerlabel.frame = frameOfLabel;
    
    self.tbLabel.frame = CGRectMake(16, self.centerlabel.frame.size.height + 20 + 8 + 5 + 5, SIZEWIDTH, 20);
    self.statusLabel.frame = CGRectMake(16, self.centerlabel.frame.size.height + 20 + 8 + 5 + 5 + 20 + 5, SIZEWIDTH, 20);
    
    //定位发布的消息label
    CGRect rectOfText1 = CGRectMake(0, 0, SIZEWIDTH, 9999);
    rectOfText1 = [self.bottomLabel textRectForBounds:rectOfText1 limitedToNumberOfLines:0];
    CGRect frameOfLabel1 = CGRectZero;
    frameOfLabel1.origin.x = 16;
    frameOfLabel1.origin.y = self.centerlabel.frame.size.height + 20 + 8 + 5 + 5 + 20 + 5 + 20 + 5;
    frameOfLabel1.size = rectOfText1.size;
    self.bottomLabel.frame = frameOfLabel1;
    
    CGRect rect = self.bounds;
    rect.size.height = self.bottomLabel.frame.origin.y + self.bottomLabel.frame.size.height + 8;
    self.bounds = rect;
}

//得到设置label 不同的颜色
- (NSMutableAttributedString*)getLabelAllColor:(NSString*)str
{
    NSMutableAttributedString *stra = [[NSMutableAttributedString alloc] initWithString:str];
    if ([str rangeOfString:@"："].location != NSNotFound)
    {
        NSArray* arr = [str componentsSeparatedByString:@"："];
        [stra addAttribute:NSForegroundColorAttributeName value:SHOWCOLOR(@"3E3E3E") range:NSMakeRange([arr[0] length] + 1,str.length - [arr[0] length] - 1 )];
        [stra addAttribute:NSForegroundColorAttributeName value:SHOWCOLOR(@"A7A7A7") range:NSMakeRange(0,[arr[0] length] + 1)];
        if ([str rangeOfString:@"保单状态"].location != NSNotFound)
        {
            [stra addAttribute:NSForegroundColorAttributeName value:KMainColor range:NSMakeRange([arr[0] length] + 1,str.length - [arr[0] length] - 1 )];
        }
    }
    else
    {
        [stra addAttribute:NSForegroundColorAttributeName value:SHOWCOLOR(@"A7A7A7") range:NSMakeRange(0,str.length)];
    }
    return stra;
}

@end
