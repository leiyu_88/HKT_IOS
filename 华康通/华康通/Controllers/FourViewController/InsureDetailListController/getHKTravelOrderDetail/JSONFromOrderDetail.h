//
//  JSONFromOrderDetail.h
//  华康通
//
//  Created by 雷雨 on 16/11/17.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSONFromOrderDetail : NSObject

//投保人姓名
@property (nonatomic, strong) NSString* name;
//投保单号
@property (nonatomic, strong) NSString* orderId;
//投保单号
@property (nonatomic, strong) NSString* orderNo;
//投保份数
@property (nonatomic, strong) NSString* mult;
//开户行
@property (nonatomic, strong) NSString* bankName;
//订单状态
@property (nonatomic, strong) NSString* orderStatus;
//产品名称
@property (nonatomic, strong) NSString* productName;
//银行卡号
@property (nonatomic, strong) NSString* cardBookCode;
//电话号码
@property (nonatomic, strong) NSString* mobile;
//证件类型
@property (nonatomic, strong) NSString* idType;
//证件名称
@property (nonatomic, strong) NSString* idTypeName;
//证件号码
@property (nonatomic, strong) NSString* idNo;
//保单号
@property (nonatomic, strong) NSString* insuranceOrderNo;
//保险订单类型
@property (nonatomic, strong) NSString* insStatus;
//盖章url(判断标识)
@property (nonatomic, strong) NSString* statusPic;
//生效日期日期
@property (nonatomic, strong) NSString* cvaliDate;
//品牌logo
@property (nonatomic, strong) NSString* brandLogo;
//保费
@property (nonatomic, strong) NSString* amount;
//保费对象
@property (nonatomic, strong) NSDictionary* orderHKDetailAmountDTO;
//保额
@property (nonatomic, strong) NSString* amt ;
//交费间隔
@property (nonatomic, strong) NSString* payIntv;
//交费年限
@property (nonatomic, strong) NSString* payYears;
//被保人列表
@property (nonatomic, strong) NSArray* insuredList;
//被保险汽车列表
@property (nonatomic, strong) id orderCarDTO;
//受益人列表
@property (nonatomic, strong) NSArray* bnfList;
//开始生效时间
@property (nonatomic, strong) NSString* startDate;
//结束生效时间
@property (nonatomic, strong) NSString* endDate;
//法定受益人
@property (nonatomic, strong) NSString* bnfString;
//邮箱
@property (nonatomic, strong) NSString* email;
//银行开户名
@property (nonatomic, strong) NSString* cardHolder;
//交付年交
@property (nonatomic, strong) NSString* payIntvName;
//险种数组
@property (nonatomic, strong) NSArray* riskList;


+ (id)HkOrderListWithJSON:(NSDictionary*)json;

@end
