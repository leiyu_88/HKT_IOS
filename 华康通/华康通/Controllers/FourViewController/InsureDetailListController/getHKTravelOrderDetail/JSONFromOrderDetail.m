//
//  JSONFromOrderDetail.m
//  华康通
//
//  Created by 雷雨 on 16/11/17.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import "JSONFromOrderDetail.h"

@implementation JSONFromOrderDetail

- (id)initWithJSON:(NSDictionary*)json
{
    if (self = [super init])
    {
        if ([json[@"orderNo"] isKindOfClass:[NSNull class]] || !json[@"orderNo"])
        {
            self.orderNo=@"无";
        }
        else
        {
            self.orderNo=json[@"orderNo"];
        }
        if ([json[@"orderId"] isKindOfClass:[NSNull class]] || !json[@"orderId"])
        {
            self.orderId=@"无";
        }
        else
        {
            self.orderId=json[@"orderId"];
        }
        
        if ([json[@"name"] isKindOfClass:[NSNull class]] || !json[@"name"])
        {
            self.name=@"无";
        }
        else
        {
            self.name=json[@"name"];
        }
        
        if ([json[@"productName"] isKindOfClass:[NSNull class]] || !json[@"productName"])
        {
            self.productName=@"无";
        }
        else
        {
            self.productName=json[@"productName"];
        }
        
        if ([json[@"mult"] isKindOfClass:[NSNull class]] || !json[@"mult"])
        {
            self.mult=@"无";
        }
        else
        {
            self.mult=json[@"mult"];
        }
        
        
        if ([json[@"bankName"] isKindOfClass:[NSNull class]] || !json[@"bankName"])
        {
            self.bankName=@"无";
        }
        else
        {
            self.bankName=json[@"bankName"];
        }
        
        if ([json[@"orderStatus"] isKindOfClass:[NSNull class]] || !json[@"orderStatus"])
        {
            self.orderStatus=@"无";
        }
        else
        {
            self.orderStatus=json[@"orderStatus"];
        }
        
        if ([json[@"cardBookCode"] isKindOfClass:[NSNull class]] || !json[@"cardBookCode"])
        {
            self.cardBookCode=@"无";
        }
        else
        {
            self.cardBookCode=json[@"cardBookCode"];
        }
        if ([json[@"mobile"] isKindOfClass:[NSNull class]] || !json[@"mobile"])
        {
            self.mobile=@"无";
        }
        else
        {
            self.mobile=json[@"mobile"];
        }
        if ([json[@"idType"] isKindOfClass:[NSNull class]] || !json[@"idType"])
        {
            self.idType=@"无";
        }
        else
        {
            self.idType=json[@"idType"];
        }
        if ([json[@"idNo"] isKindOfClass:[NSNull class]] || !json[@"idNo"])
        {
            self.idNo=@"无";
        }
        else
        {
            self.idNo=json[@"idNo"];
        }
        if ([json[@"insuranceOrderNo"] isKindOfClass:[NSNull class]] || !json[@"insuranceOrderNo"])
        {
            self.insuranceOrderNo=@"无";
        }
        else
        {
            self.insuranceOrderNo=json[@"insuranceOrderNo"];
        }
        if ([json[@"statusPic"] isKindOfClass:[NSNull class]] || !json[@"statusPic"])
        {
            self.statusPic=@"无";
        }
        else
        {
            self.statusPic=json[@"statusPic"];
        }
        if ([json[@"cvaliDate"] isKindOfClass:[NSNull class]] || !json[@"cvaliDate"])
        {
            self.cvaliDate=@"无";
        }
        else
        {
            self.cvaliDate=json[@"cvaliDate"];
        }
        if ([json[@"amount"] isKindOfClass:[NSNull class]] || !json[@"amount"])
        {
            self.amount=@"无";
        }
        else
        {
            self.amount=[NSString stringWithFormat:@"￥%@",json[@"amount"]];
        }
        if ([json[@"orderHKDetailAmountDTO"] isKindOfClass:[NSNull class]] || !json[@"orderHKDetailAmountDTO"])
        {
            self.orderHKDetailAmountDTO = @{};
        }
        else
        {
            self.orderHKDetailAmountDTO=json[@"orderHKDetailAmountDTO"];
        }
        if ([json[@"amt"] isKindOfClass:[NSNull class]] || !json[@"amt"])
        {
            self.amt=@"无";
        }
        else
        {
            self.amt=[NSString stringWithFormat:@"￥%@",json[@"amt"]];
        }
        if ([json[@"brandLogo"] isKindOfClass:[NSNull class]] || !json[@"brandLogo"])
        {
            self.brandLogo=@"无";
        }
        else
        {
            self.brandLogo=json[@"brandLogo"];
        }
        
        if ([json[@"insStatus"] isKindOfClass:[NSNull class]] || !json[@"insStatus"])
        {
            self.insStatus=@"无";
        }
        else
        {
            self.insStatus=json[@"insStatus"];
        }
        
        if ([json[@"payIntv"] isKindOfClass:[NSNull class]] || !json[@"payIntv"])
        {
            self.payIntv=@"无";
        }
        else
        {
            self.payIntv=json[@"payIntv"];
        }
        
        
        
        if ([json[@"payYears"] isKindOfClass:[NSNull class]] || !json[@"payYears"])
        {
            self.payYears=@"无";
        }
        else
        {
            self.payYears=json[@"payYears"];
        }
        
        
        if ([json[@"insuredList"] isKindOfClass:[NSNull class]] || !json[@"insuredList"])
        {
            self.insuredList = @[];
        }
        else
        {
            self.insuredList = json[@"insuredList"];
        }
        
        if ([json[@"orderCarDTO"] isKindOfClass:[NSNull class]] || !json[@"orderCarDTO"])
        {
            self.orderCarDTO = @[];
        }
        else
        {
            self.orderCarDTO = json[@"orderCarDTO"];
        }
        
        if ([json[@"bnfList"] isKindOfClass:[NSNull class]] || !json[@"bnfList"])
        {
            self.bnfList=@[];
        }
        else
        {
            self.bnfList=json[@"bnfList"];
        }
        if ([json[@"startDate"] isKindOfClass:[NSNull class]] || !json[@"startDate"])
        {
            self.startDate=@"无";
        }
        else
        {
            self.startDate=json[@"startDate"];
        }
        if ([json[@"endDate"] isKindOfClass:[NSNull class]] || !json[@"endDate"])
        {
            self.endDate=@"无";
        }
        else
        {
            self.endDate=json[@"endDate"];
        }
        
        if ([json[@"bnfString"] isKindOfClass:[NSNull class]] || !json[@"bnfString"])
        {
            self.bnfString=@"无";
        }
        else
        {
            self.bnfString=json[@"bnfString"];
        }
        
        if ([json[@"email"] isKindOfClass:[NSNull class]] || !json[@"email"])
        {
            self.email=@"无";
        }
        else
        {
            self.email=json[@"email"];
        }
        
        if ([json[@"cardHolder"] isKindOfClass:[NSNull class]] || !json[@"cardHolder"])
        {
            self.cardHolder=@"无";
        }
        else
        {
            self.cardHolder=json[@"cardHolder"];
        }
        if ([json[@"payIntvName"] isKindOfClass:[NSNull class]] || !json[@"payIntvName"])
        {
            self.payIntvName=@"无";
        }
        else
        {
            self.payIntvName=json[@"payIntvName"];
        }
        
        if ([json[@"idTypeName"] isKindOfClass:[NSNull class]] || !json[@"idTypeName"])
        {
            self.idTypeName=@"无";
        }
        else
        {
            self.idTypeName=json[@"idTypeName"];
        }
        if ([json[@"riskList"] isKindOfClass:[NSNull class]] || !json[@"riskList"])
        {
            self.riskList = @[];
        }
        else
        {
            self.riskList=json[@"riskList"];
        }
        
    }
    return self;
}

+ (id)HkOrderListWithJSON:(NSDictionary*)json
{
    return [[self alloc]initWithJSON:json];
}

@end
