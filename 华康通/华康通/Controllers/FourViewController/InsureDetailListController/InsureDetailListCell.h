//
//  InsureDetailListCell.h
//  华康通
//
//  Created by leiyu on 16/11/9.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InsureDetailListCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *leftLabel;

@end
