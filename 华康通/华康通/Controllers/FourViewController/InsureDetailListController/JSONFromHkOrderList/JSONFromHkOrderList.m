//
//  JSONFromHkOrderList.m
//  华康通
//
//  Created by 雷雨 on 16/11/16.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import "JSONFromHkOrderList.h"

@implementation JSONFromHkOrderList

-(id)initWithJSON:(NSDictionary*)json
{
    if (self = [super init])
    {
        if ([json[@"orderId"] isKindOfClass:[NSNull class]] || !json[@"orderId"])
        {
            self.orderId=@"无";
        }
        else
        {
            self.orderId=json[@"orderId"];
        }
        if ([json[@"orderNo"] isKindOfClass:[NSNull class]] || !json[@"orderNo"])
        {
            self.orderNo=@"无";
        }
        else
        {
            self.orderNo=json[@"orderNo"];
        }
        
        if ([json[@"commitedDate"] isKindOfClass:[NSNull class]] || !json[@"commitedDate"])
        {
            self.commitedDate=@"无";
        }
        else
        {
            self.commitedDate=json[@"commitedDate"];
        }
        
        if ([json[@"productName"] isKindOfClass:[NSNull class]] || !json[@"productName"])
        {
            self.productName=@"无";
        }
        else
        {
            self.productName=json[@"productName"];
        }
        
        if ([json[@"orderAmount"] isKindOfClass:[NSNull class]] || !json[@"orderAmount"])
        {
            self.orderAmount=@"无";
        }
        else
        {
            self.orderAmount = [NSString stringWithFormat:@"%.2f",[json[@"orderAmount"] doubleValue]];
        }
        
        
        if ([json[@"businessType"] isKindOfClass:[NSNull class]] || !json[@"businessType"])
        {
            self.businessType=@"无";
        }
        else
        {
            self.businessType=json[@"businessType"];
        }
        
        if ([json[@"orderStatus"] isKindOfClass:[NSNull class]] || !json[@"orderStatus"])
        {
            self.orderStatus=@"无";
        }
        else
        {
            self.orderStatus=json[@"orderStatus"];
        }
        
        if ([json[@"brandLogo"] isKindOfClass:[NSNull class]] || !json[@"brandLogo"])
        {
            self.brandLogo=@"无";
        }
        else
        {
            self.brandLogo=json[@"brandLogo"];
        }
        
        if ([json[@"insStatus"] isKindOfClass:[NSNull class]] || !json[@"insStatus"])
        {
            self.insStatus=@"无";
        }
        else
        {
            self.insStatus=json[@"insStatus"];
        }
        
        if ([json[@"promotionExpenses"] isKindOfClass:[NSNull class]] || !json[@"promotionExpenses"])
        {
            self.promotionExpenses=@"无";
        }
        else
        {
            self.promotionExpenses=json[@"promotionExpenses"];
        }
        
        
        
        if ([json[@"isCvial"] isKindOfClass:[NSNull class]] || !json[@"isCvial"])
        {
            self.isCvial=@"无";
        }
        else
        {
            self.isCvial=json[@"isCvial"];
        }
        
        if ([json[@"statusName"] isKindOfClass:[NSNull class]] || !json[@"statusName"])
        {
            self.statusName=@"无";
        }
        else
        {
            self.statusName=json[@"statusName"];
        }
        
        if ([json[@"insCustomerName"] isKindOfClass:[NSNull class]] || !json[@"insCustomerName"])
        {
            self.insCustomerName=@"无";
        }
        else
        {
            self.insCustomerName=json[@"insCustomerName"];
        }
        
        
        if ([json[@"picUrl"] isKindOfClass:[NSNull class]] || !json[@"picUrl"])
        {
            self.picUrl=@"";
        }
        else
        {
            self.picUrl=json[@"picUrl"];
        }
        
        if ([json[@"receiptUrl"] isKindOfClass:[NSNull class]] || !json[@"receiptUrl"])
        {
            self.receiptUrl=@"";
        }
        else
        {
            self.receiptUrl=json[@"receiptUrl"];
        }
        
        if ([json[@"pageParams"] isKindOfClass:[NSNull class]] || !json[@"pageParams"])
        {
            self.pageParams=@"无";
        }
        else
        {
            self.pageParams=json[@"pageParams"];
        }
        
        if ([json[@"records"] isKindOfClass:[NSNull class]] || !json[@"records"])
        {
            self.records=@"无";
        }
        else
        {
            self.records=json[@"records"];
        }
        if ([json[@"payUrl"] isKindOfClass:[NSNull class]] || !json[@"payUrl"])
        {
            self.payUrl = @"";
        }
        else
        {
            self.payUrl = json[@"payUrl"];
        }
        
        if ([json[@"isDownPolicy"] isKindOfClass:[NSNull class]] || !json[@"isDownPolicy"])
        {
            self.isDownPolicy = @"0";
        }
        else
        {
            self.isDownPolicy = [NSString stringWithFormat:@"%@",json[@"isDownPolicy"]];
        }
        if ([json[@"payFlag"] isKindOfClass:[NSNull class]] || !json[@"payFlag"])
        {
            self.payFlag = @"";
        }
        else
        {
            self.payFlag = [NSString stringWithFormat:@"%@",json[@"payFlag"]];
        }
        
    }
    return self;
}

+(id)HkOrderListWithJSON:(NSDictionary*)json
{
    return [[self alloc]initWithJSON:json];
}


@end
