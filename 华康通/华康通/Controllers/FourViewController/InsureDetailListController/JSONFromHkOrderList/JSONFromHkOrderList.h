//
//  JSONFromHkOrderList.h
//  华康通
//
//  Created by 雷雨 on 16/11/16.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSONFromHkOrderList : NSObject

//投保单号
@property (nonatomic, strong) NSString* orderId;
//订单号
@property (nonatomic, strong) NSString* orderNo;
//下单日期
@property (nonatomic, strong) NSString* commitedDate;
//产品名称
@property (nonatomic, strong) NSString* productName;
//金额
@property (nonatomic, strong) NSString* orderAmount;
//业务类型：01-保险
@property (nonatomic, strong) NSString* businessType;
//订单状态
@property (nonatomic, strong) NSString* orderStatus;
//品牌logo
@property (nonatomic, strong) NSString* brandLogo;
//保险订单状态
@property (nonatomic, strong) NSString* insStatus;
//推广费用
@property (nonatomic, strong) NSString* promotionExpenses;
//标识(暂无用)
@property (nonatomic, strong) NSString* isCvial;
//订单状态名称（暂无）
@property (nonatomic, strong) NSString* statusName;

//投保人姓名
@property (nonatomic, strong) NSString* insCustomerName;
//盖章url
@property (nonatomic, strong) NSString* picUrl;
//分页参数
@property (nonatomic, strong) NSString* pageParams;
//数据总条数
@property (nonatomic, strong) NSString* records;
//未支付完成的支付链接
@property (nonatomic, strong) NSString* payUrl;
//提交回执的链接
@property (nonatomic, strong) NSString* receiptUrl;
//是否支持下载保单
@property (nonatomic, strong) NSString* isDownPolicy;
//按钮文字
@property (nonatomic, strong) NSString* payFlag;

+(id)HkOrderListWithJSON:(NSDictionary*)json;

@end
