//
//  InsureDetailListMode.h
//  华康通
//
//  Created by  雷雨 on 2017/12/12.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InsureDetailListMode : NSObject

//订单号
@property (strong, nonatomic) NSString* ddStr;
//投保单号
@property (strong, nonatomic) NSString* tbStr;
//保单号
@property (strong, nonatomic) NSString* bdStr;
//保单状态
@property (strong, nonatomic) NSString* bdStuesStr;
//保障期间
@property (strong, nonatomic) NSString* bzStr;

+ (id)mode;

@end
