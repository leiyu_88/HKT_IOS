//
//  InsureDetailListFirstCell.h
//  华康通
//
//  Created by leiyu on 16/11/10.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InsureDetailListMode.h"

@interface InsureDetailListFirstCell : UITableViewCell
//订单号
@property (weak, nonatomic) IBOutlet UILabel *centerlabel;
//保单状态
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *tbLabel;
//保单号
@property (strong, nonatomic) IBOutlet UILabel *topLabel;
//保障期限
@property (strong, nonatomic) IBOutlet UILabel *bottomLabel;
//状态图
//@property (strong, nonatomic) IBOutlet UIImageView *showStatusImageView;

@property (strong, nonatomic) InsureDetailListMode* model;

- (void)setModel:(InsureDetailListMode *)model;

@end
