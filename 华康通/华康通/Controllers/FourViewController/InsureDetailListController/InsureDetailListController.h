//
//  InsureDetailListController.h
//  华康通
//
//  Created by leiyu on 16/11/9.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_OPTIONS(NSUInteger, InsureDetailListControllerType)
{
    InsureDetailListControllerTypeShort = 1,
    InsureDetailListControllerTypeLong = 2
};

//给上一个页面回传值，判断是否重新刷新页面
typedef void (^ReturnBackValueBlock1) (NSString *strValue);


@interface InsureDetailListController : UITableViewController

@property (nonatomic, unsafe_unretained) InsureDetailListControllerType myType;

//订单id
@property (nonatomic, strong) NSString* orderNo;
//产品名称
@property (nonatomic, strong) NSString* productName;
//订单状态（用来判断是否隐藏立即付款按钮）
@property (nonatomic, strong) NSString* picUrl;
//未支付的链接
@property (nonatomic, strong) NSString* payUrl;
//提交回执的链接
@property (nonatomic, strong) NSString* receiptUrl;
//投保单号
@property (nonatomic, strong) NSString* orderId;
//是否显示电子保单
@property (nonatomic, strong) NSString* isDownPolicy;
//显示按钮标题
@property (nonatomic, strong) NSString* payFlag;

@property (nonatomic, strong) ReturnBackValueBlock1 returnValueBlock;


@end
