//
//  JSONFromCustomerInfo.h
//  华康通
//
//  Created by leiyu on 16/11/11.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSONFromCustomerInfo : NSObject

//产品类型id
@property (nonatomic,strong) NSString* address;
//姓名
@property (nonatomic,strong) NSString* realName;
//销售代码
@property (nonatomic, strong) NSString* agentCode;
//性别
@property (nonatomic,strong) NSString* sex;

//手机号码
@property (nonatomic,strong) NSString* bindMobile;
//用户号
@property (nonatomic,strong) NSString* customerName;
//用户邮箱
@property (nonatomic,strong) NSString* bindEmail;
//证件类型
@property (nonatomic,strong) NSString* idType;
//证件号码
@property (nonatomic,strong) NSString* idNo;
//职级代码
@property (nonatomic,strong) NSString* levelCode;
//分公司id
@property (nonatomic,strong) NSString* branchId;
//银行账号
@property (nonatomic,strong) NSString* bankNo;


//职级名称
@property (nonatomic,strong) NSString* levelCodeName;
//证件类型名称
@property (nonatomic,strong) NSString* idTypeName;
//分公司名称
@property (nonatomic,strong) NSString* branchName;
//营业部id
@property (nonatomic,strong) NSString* ubranchId;
//营业部名称
@property (nonatomic,strong) NSString* ubranchName;
//区域id
@property (nonatomic,strong) NSString* cbrandchId;
//区域名称
@property (nonatomic,strong) NSString* cbrandchName;





+(id)customerInfoWithJSON:(NSDictionary*)json;

@end
