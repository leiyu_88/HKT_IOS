//
//  JSONFromCustomerInfo.m
//  华康通
//
//  Created by leiyu on 16/11/11.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import "JSONFromCustomerInfo.h"

@implementation JSONFromCustomerInfo

-(id)initWithJSON:(NSDictionary*)json
{
    if (self = [super init])
    {
        if ([json[@"address"] isKindOfClass:[NSNull class]] || !json[@"address"])
        {
            self.address=@"无";
        }
        else
        {
            self.address=json[@"address"];
        }
        
        if ([json[@"realName"] isKindOfClass:[NSNull class]] || !json[@"realName"])
        {
            self.realName=@"无";
        }
        else
        {
            self.realName=json[@"realName"];
        }
        
        if ([json[@"agentCode"] isKindOfClass:[NSNull class]] || !json[@"agentCode"])
        {
            self.agentCode=@"无";
        }
        else
        {
            self.agentCode=json[@"agentCode"];
        }
        
        if ([json[@"sex"] isKindOfClass:[NSNull class]] || !json[@"sex"])
        {
            self.sex=@"无";
        }
        else
        {
            self.sex=json[@"sex"];
        }
        
        
        if ([json[@"bindMobile"] isKindOfClass:[NSNull class]] || !json[@"bindMobile"])
        {
            self.bindMobile=@"无";
        }
        else
        {
            self.bindMobile=json[@"bindMobile"];
        }
        
        if ([json[@"customerName"] isKindOfClass:[NSNull class]] || !json[@"customerName"])
        {
            self.customerName=@"无";
        }
        else
        {
            self.customerName=json[@"customerName"];
        }
        
        if ([json[@"bindEmail"] isKindOfClass:[NSNull class]] || !json[@"bindEmail"])
        {
            self.bindEmail=@"无";
        }
        else
        {
            self.bindEmail=json[@"bindEmail"];
        }
        
        if ([json[@"idType"] isKindOfClass:[NSNull class]] || !json[@"idType"])
        {
            self.idType=@"无";
        }
        else
        {
            self.idType=json[@"idType"];
        }
        
        if ([json[@"idNo"] isKindOfClass:[NSNull class]] || !json[@"idNo"])
        {
            self.idNo = @"无";
        }
        else
        {
            NSInteger startLocation = 3;
            NSString* str = [NSString stringWithFormat:@"%@",json[@"idNo"]];
            //下面是身份证号码加*号
            if (str.length > 7)
            {
                for (NSInteger i = 0; i < str.length - 7; i++)
                {
                    NSRange range = NSMakeRange(startLocation, 1);
                    str = [str stringByReplacingCharactersInRange:range withString:@"*"];
                    startLocation++;
                }
            }
            self.idNo = str;
        }
        

        
        if ([json[@"levelCode"] isKindOfClass:[NSNull class]] || !json[@"levelCode"])
        {
            self.levelCode=@"无";
        }
        else
        {
            self.levelCode=json[@"levelCode"];
        }
        
        if ([json[@"branchId"] isKindOfClass:[NSNull class]] || !json[@"branchId"])
        {
            self.branchId=@"无";
        }
        else
        {
            self.branchId=json[@"branchId"];
        }
        
        if ([json[@"bankNo"] isKindOfClass:[NSNull class]] || !json[@"bankNo"])
        {
            self.bankNo=@"无";
        }
        else
        {
            self.bankNo=json[@"bankNo"];
        }
        
        
        if ([json[@"levelCodeName"] isKindOfClass:[NSNull class]] || !json[@"levelCodeName"])
        {
            self.levelCodeName=@"无";
        }
        else
        {
            self.levelCodeName=json[@"levelCodeName"];
        }
        
        if ([json[@"idTypeName"] isKindOfClass:[NSNull class]] || !json[@"idTypeName"])
        {
            self.idTypeName=@"无";
        }
        else
        {
            self.idTypeName=json[@"idTypeName"];
        }
        
        if ([json[@"branchName"] isKindOfClass:[NSNull class]] || !json[@"branchName"])
        {
            self.branchName=@"无";
        }
        else
        {
            self.branchName=json[@"branchName"];
        }
        
        
        
        
        if ([json[@"ubranchId"] isKindOfClass:[NSNull class]] || !json[@"ubranchId"])
        {
            self.ubranchId=@"无";
        }
        else
        {
            self.ubranchId=json[@"ubranchId"];
        }
        if ([json[@"ubranchName"] isKindOfClass:[NSNull class]] || !json[@"ubranchName"])
        {
            self.ubranchName=@"无";
        }
        else
        {
            self.ubranchName=json[@"ubranchName"];
        }
        if ([json[@"cbrandchId"] isKindOfClass:[NSNull class]] || !json[@"cbrandchId"])
        {
            self.cbrandchId=@"无";
        }
        else
        {
            self.cbrandchId=json[@"cbrandchId"];
        }
        
        if ([json[@"cbrandchName"] isKindOfClass:[NSNull class]] || !json[@"cbrandchName"])
        {
            self.cbrandchName=@"无";
        }
        else
        {
            self.cbrandchName=json[@"cbrandchName"];
        }
    }
    return self;
}

+(id)customerInfoWithJSON:(NSDictionary*)json
{
    return [[self alloc]initWithJSON:json];
}

@end
