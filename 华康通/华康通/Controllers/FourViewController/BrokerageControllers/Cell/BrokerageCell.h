//
//  BrokerageCell.h
//  华康通
//
//  Created by  雷雨 on 2017/1/17.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BrokerageCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightLabel;
@property (weak, nonatomic) IBOutlet UIImageView *xuLineImageView;
@property (weak, nonatomic) IBOutlet UIImageView *smallImageView;

@end
