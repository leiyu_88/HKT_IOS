//
//  QTSRCell.m
//  华康通
//
//  Created by  雷雨 on 2017/1/18.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import "QTSRCell.h"

@implementation QTSRCell

- (void)setQtys:(JSONFromQTYS *)qtys
{
    _qtys = qtys;
    self.myLabel.text = qtys.summary;
    self.jqLabel.text = [NSString stringWithFormat:@"¥%.2f",[qtys.amount floatValue]];
    CGRect frameOfText = CGRectMake(0, 0, UISCREENWEITH - 61 - 16, 9999);
    frameOfText = [self.myLabel textRectForBounds:frameOfText limitedToNumberOfLines:0];
    CGRect frameOfLabel = CGRectZero;
    frameOfLabel.size = frameOfText.size;
    frameOfLabel.origin.x = 61;
    frameOfLabel.origin.y = 8;
    self.myLabel.frame = frameOfLabel;
    //设定单元格的bounds
    CGRect bounds = self.bounds;
    bounds.size.height = self.myLabel.frame.size.height + 8 + 50 + 10;
    self.bounds = bounds;
    self.jeLabel.frame = CGRectMake(16, self.bounds.size.height - 40 - 10, 45, 40);
    self.jqLabel.frame = CGRectMake(61, self.bounds.size.height - 40 - 10, UISCREENWEITH - 61 - 16, 40);
    self.zyLabel.frame = CGRectMake(16, 0, 45, 40);
    self.lineView.frame = CGRectMake(16, self.bounds.size.height - 40 - 10, UISCREENWEITH - 16, 0.5);
    self.bottomView.frame = CGRectMake(0, self.bounds.size.height - 10, UISCREENWEITH, 10);
}

@end
