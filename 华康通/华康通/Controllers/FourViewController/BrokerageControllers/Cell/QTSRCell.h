//
//  QTSRCell.h
//  华康通
//
//  Created by  雷雨 on 2017/1/18.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JSONFromQTYS.h"

@interface QTSRCell : UITableViewCell

//服务端获取的json数据
@property (nonatomic, strong) JSONFromQTYS * qtys;
@property (weak, nonatomic) IBOutlet UILabel *myLabel;
@property (weak, nonatomic) IBOutlet UILabel *zyLabel;
@property (weak, nonatomic) IBOutlet UILabel *jeLabel;
@property (weak, nonatomic) IBOutlet UILabel *jqLabel;
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;




@end
