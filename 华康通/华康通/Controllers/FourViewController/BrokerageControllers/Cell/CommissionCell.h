//
//  CommissionCell.h
//  华康通
//
//  Created by  雷雨 on 2017/1/18.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommissionCell : UITableViewCell
//投保单号
@property (weak, nonatomic) IBOutlet UILabel *topLabel;
//投保人
@property (weak, nonatomic) IBOutlet UILabel *topLeftLabel;
//被保人
@property (weak, nonatomic) IBOutlet UILabel *topRightLabel;
//保费
@property (weak, nonatomic) IBOutlet UILabel *centerLeftLabel;
//价保
@property (weak, nonatomic) IBOutlet UILabel *centerRightLabel;
//供应商
@property (weak, nonatomic) IBOutlet UILabel *bottomCenterLabel;
//佣金
@property (weak, nonatomic) IBOutlet UILabel *bottomLabel;

@end
