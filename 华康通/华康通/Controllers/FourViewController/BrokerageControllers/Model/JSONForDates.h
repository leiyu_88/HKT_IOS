//
//  JSONForDates.h
//  华康通
//
//  Created by  雷雨 on 2017/3/6.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSONForDates : NSObject

//可以查询的佣金日期
@property (nonatomic, strong) NSString* queryAbleDate;

+ (id)getDateWithJSON:(NSDictionary*)json;

@end
