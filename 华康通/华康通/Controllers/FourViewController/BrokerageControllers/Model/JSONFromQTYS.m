//
//  JSONFromQTYS.m
//  华康通
//
//  Created by  雷雨 on 2017/2/24.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import "JSONFromQTYS.h"

@implementation JSONFromQTYS

- (id)initWithJSON:(NSDictionary*)json
{
    if (self = [super init])
    {
        if ([json[@"subjectNo"] isKindOfClass:[NSNull class]] || !json[@"subjectNo"])
        {
            self.subjectNo = @"无";
        }
        else
        {
            self.subjectNo = [NSString stringWithFormat:@"%@",json[@"subjectNo"]];
        }
        if ([json[@"summary"] isKindOfClass:[NSNull class]] || !json[@"summary"])
        {
            if ([json[@"subjectNo"] isKindOfClass:[NSNull class]] || !json[@"subjectNo"])
            {
                self.summary = @"无";
            }
            else
            {
                self.summary = self.subjectNo;
            }
        }
        else
        {
            self.summary = [NSString stringWithFormat:@"%@",json[@"summary"]];
        }
        if ([json[@"amount"] isKindOfClass:[NSNull class]] || !json[@"amount"])
        {
            self.amount = @"0";
        }
        else
        {
            self.amount = [NSString stringWithFormat:@"%@",json[@"amount"]];
        }
        if ([json[@"isTaxAble"] isKindOfClass:[NSNull class]] || !json[@"isTaxAble"])
        {
            self.isTaxAble = @"无";
        }
        else
        {
            self.isTaxAble = [NSString stringWithFormat:@"%@",json[@"isTaxAble"]];
        }
    }
    return self;
}

+ (id)commissionTaxWithJSON:(NSDictionary*)json
{
    return [[self alloc]initWithJSON:json];
}


@end
