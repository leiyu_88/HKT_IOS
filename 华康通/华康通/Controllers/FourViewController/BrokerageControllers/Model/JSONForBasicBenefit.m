//
//  JSONForBasicBenefit.m
//  华康通
//
//  Created by  雷雨 on 2017/2/14.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import "JSONForBasicBenefit.h"

@implementation JSONForBasicBenefit

-(id)initWithJSON:(NSDictionary*)json
{
    if (self = [super init])
    {
        if ([json[@"agentCode"] isKindOfClass:[NSNull class]] || !json[@"agentCode"])
        {
            self.agentCode = @"无";
        }
        else
        {
            self.agentCode = json[@"agentCode"];
        }
        if ([json[@"total"] isKindOfClass:[NSNull class]] || !json[@"total"])
        {
            self.total = @"0";
        }
        else
        {
            self.total = json[@"total"];
        }
        if ([json[@"benefitDate"] isKindOfClass:[NSNull class]] || !json[@"benefitDate"])
        {
            self.benefitDate = @"0";
        }
        else
        {
            self.benefitDate = json[@"benefitDate"];
        }
        if ([json[@"outstandingAward"] isKindOfClass:[NSNull class]] || !json[@"outstandingAward"])
        {
            self.outstandingAward = @"0";
        }
        else
        {
            self.outstandingAward = json[@"outstandingAward"];
        }
        if ([json[@"socialSecurityAllowance"] isKindOfClass:[NSNull class]] || !json[@"socialSecurityAllowance"])
        {
            self.socialSecurityAllowance=@"0";
        }
        else
        {
            self.socialSecurityAllowance=json[@"socialSecurityAllowance"];
        }
        if ([json[@"recruimentTutoshipAllowance"] isKindOfClass:[NSNull class]] || !json[@"recruimentTutoshipAllowance"])
        {
            self.recruimentTutoshipAllowance=@"0";
        }
        else
        {
            self.recruimentTutoshipAllowance=json[@"recruimentTutoshipAllowance"];
        }
        if ([json[@"groupDutyAllowance"] isKindOfClass:[NSNull class]] || !json[@"groupDutyAllowance"])
        {
            self.groupDutyAllowance=@"0";
        }
        else
        {
            self.groupDutyAllowance=json[@"groupDutyAllowance"];
        }
        if ([json[@"districtDutyAllowance"] isKindOfClass:[NSNull class]] || !json[@"districtDutyAllowance"])
        {
            self.districtDutyAllowance=@"0";
        }
        else
        {
            self.districtDutyAllowance=json[@"districtDutyAllowance"];
        }
        if ([json[@"brancheDutyAllowance"] isKindOfClass:[NSNull class]] || !json[@"brancheDutyAllowance"])
        {
            self.brancheDutyAllowance=@"0";
        }
        else
        {
            self.brancheDutyAllowance=json[@"brancheDutyAllowance"];
        }
        if ([json[@"areaDutyAllowance"] isKindOfClass:[NSNull class]] || !json[@"areaDutyAllowance"])
        {
            self.areaDutyAllowance=@"0";
        }
        else
        {
            self.areaDutyAllowance=json[@"areaDutyAllowance"];
        }
        
        if ([json[@"areaDevelopAllowance"] isKindOfClass:[NSNull class]] || !json[@"areaDevelopAllowance"])
        {
            self.areaDevelopAllowance=@"0";
        }
        else
        {
            self.areaDevelopAllowance=json[@"areaDevelopAllowance"];
        }
        if ([json[@"personalAnnualBonus"] isKindOfClass:[NSNull class]] || !json[@"personalAnnualBonus"])
        {
            self.personalAnnualBonus=@"0";
        }
        else
        {
            self.personalAnnualBonus=json[@"personalAnnualBonus"];
        }
        if ([json[@"groupAnnualBonus"] isKindOfClass:[NSNull class]] || !json[@"groupAnnualBonus"])
        {
            self.groupAnnualBonus=@"0";
        }
        else
        {
            self.groupAnnualBonus=json[@"groupAnnualBonus"];
        }
        if ([json[@"districtAnnualBonus"] isKindOfClass:[NSNull class]] || !json[@"districtAnnualBonus"])
        {
            self.districtAnnualBonus=@"0";
        }
        else
        {
            self.districtAnnualBonus=json[@"districtAnnualBonus"];
        }
        if ([json[@"brancheAnnualBonus"] isKindOfClass:[NSNull class]] || !json[@"brancheAnnualBonus"])
        {
            self.brancheAnnualBonus=@"0";
        }
        else
        {
            self.brancheAnnualBonus=json[@"brancheAnnualBonus"];
        }
        
        
        if ([json[@"areaAnnualBonus"] isKindOfClass:[NSNull class]] || !json[@"areaAnnualBonus"])
        {
            self.areaAnnualBonus=@"0";
        }
        else
        {
            self.areaAnnualBonus=json[@"areaAnnualBonus"];
        }
        if ([json[@"personalContinuationRate"] isKindOfClass:[NSNull class]] || !json[@"personalContinuationRate"])
        {
            self.personalContinuationRate=@"无";
        }
        else
        {
            if ([json[@"personalContinuationRate"] isEqualToString:@"A"])
            {
                self.personalContinuationRate=@"无";
            }
            else
            {
                self.personalContinuationRate=json[@"personalContinuationRate"];
            }
        }
        if ([json[@"groupContinuationRate"] isKindOfClass:[NSNull class]] || !json[@"groupContinuationRate"])
        {
            self.groupContinuationRate=@"无";
        }
        else
        {
            if ([json[@"groupContinuationRate"] isEqualToString:@"A"])
            {
                self.groupContinuationRate=@"无";
            }
            else
            {
                self.groupContinuationRate=json[@"groupContinuationRate"];
            }
        }
        if ([json[@"districtContinuationRate"] isKindOfClass:[NSNull class]] || !json[@"districtContinuationRate"])
        {
            self.districtContinuationRate=@"无";
        }
        else
        {
            if ([json[@"districtContinuationRate"] isEqualToString:@"A"])
            {
                self.districtContinuationRate=@"无";
            }
            else
            {
                self.districtContinuationRate=json[@"districtContinuationRate"];
            }
        }
        if ([json[@"brancheContinuationRate"] isKindOfClass:[NSNull class]] || !json[@"brancheContinuationRate"])
        {
            self.brancheContinuationRate=@"无";
        }
        else
        {
            if ([json[@"brancheContinuationRate"] isEqualToString:@"A"])
            {
                self.brancheContinuationRate=@"无";
            }
            else
            {
                self.brancheContinuationRate=json[@"brancheContinuationRate"];
            }
        }
        if ([json[@"areaContinuationRate"] isKindOfClass:[NSNull class]] || !json[@"areaContinuationRate"])
        {
            self.areaContinuationRate=@"无";
        }
        else
        {
            if ([json[@"areaContinuationRate"] isEqualToString:@"A"])
            {
                self.areaContinuationRate=@"无";
            }
            else
            {
                self.areaContinuationRate=json[@"areaContinuationRate"];
            }
        }
    }
    return self;
}

+(id)basicBenefitWithJSON:(NSDictionary*)json
{
    return [[self alloc]initWithJSON:json];
}

@end
