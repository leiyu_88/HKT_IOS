//
//  JSONFromQTYS.h
//  华康通
//
//  Created by  雷雨 on 2017/2/24.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSONFromQTYS : NSObject

//科目编码
@property (nonatomic, strong) NSString* subjectNo;
//摘要
@property (nonatomic, strong) NSString* summary;
//金额
@property (nonatomic, strong) NSString* amount;
//是否应税，Y:应税，N:非应税
@property (nonatomic, strong) NSString* isTaxAble;


+ (id)commissionTaxWithJSON:(NSDictionary*)json;

@end
