//
//  JSONForCommissionDetail.h
//  华康通
//
//  Created by  雷雨 on 2017/2/15.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSONForCommissionDetail : NSObject

//工号
@property (nonatomic, strong) NSString* agentCode;
//保费
@property (nonatomic, strong) NSString* premium;
//佣金
@property (nonatomic, strong) NSString* commission;
//是否首年，0：是，1：否
@property (nonatomic, strong) NSString* isFirstYear;
//佣金年月
@property (nonatomic, strong) NSString* commissionDate;
//保单号
@property (nonatomic, strong) NSString* insOrderNo;
//投保人
@property (nonatomic, strong) NSString* policyHolder;
//被保人
@property (nonatomic, strong) NSString* insuredPerson;
//价保
@property (nonatomic, strong) NSString* priceOfInsurance;
//供应商
@property (nonatomic, strong) NSString* supplier;
//续年年份
@property (nonatomic, strong) NSString* annual;

+ (id)commissionDetailWithJSON:(NSDictionary*)json;

@end
