//
//  JSONForCommissionTax.h
//  华康通
//
//  Created by  雷雨 on 2017/2/15.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSONForCommissionTax : NSObject

//工号
@property (nonatomic, strong) NSString* agentCode;
//税金小计
@property (nonatomic, strong) NSString* total;
//个人所得税
@property (nonatomic, strong) NSString* personalIncomeTax;
//税金年月
@property (nonatomic, strong) NSString* taxDate;
//城建税
@property (nonatomic, strong) NSString* constructionTax;
//教育附加税
@property (nonatomic, strong) NSString* educationAddtionalTax;
//堤围税
@property (nonatomic, strong) NSString* embankmentTax;
//营业税
@property (nonatomic, strong) NSString* salesTax;
//偶然所得税
@property (nonatomic, strong) NSString* contingentIncomeTax;
//应税收入
@property (nonatomic, strong) NSString* taxableIncome;
//非应税收入
@property (nonatomic, strong) NSString* taxFreeIncome;
//暂时不用
@property (nonatomic, strong) NSString* createDate;
//暂时不用
@property (nonatomic, strong) NSString* isDelete;

+ (id)commissionTaxWithJSON:(NSDictionary*)json;

@end
