//
//  JSONForCommissionDetail.m
//  华康通
//
//  Created by  雷雨 on 2017/2/15.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import "JSONForCommissionDetail.h"

@implementation JSONForCommissionDetail

-(id) initWithJSON:(NSDictionary*)json
{
    if (self = [super init])
    {
        if ([json[@"agentCode"] isKindOfClass:[NSNull class]] || !json[@"agentCode"])
        {
            self.agentCode = @"无";
        }
        else
        {
            self.agentCode = json[@"agentCode"];
        }
        if ([json[@"premium"] isKindOfClass:[NSNull class]] || !json[@"premium"])
        {
            self.premium = @"0";
        }
        else
        {
            self.premium = [NSString stringWithFormat:@"%.2f",[json[@"premium"] floatValue]];
        }
        if ([json[@"commission"] isKindOfClass:[NSNull class]] || !json[@"commission"])
        {
            self.commission = @"0";
        }
        else
        {
            self.commission = [NSString stringWithFormat:@"%.2f",[json[@"commission"] floatValue]];
        }
        if ([json[@"isFirstYear"] isKindOfClass:[NSNull class]] || !json[@"isFirstYear"])
        {
            self.isFirstYear = @"无";
        }
        else
        {
            self.isFirstYear = json[@"isFirstYear"];
        }
        if ([json[@"commissionDate"] isKindOfClass:[NSNull class]] || !json[@"commissionDate"])
        {
            self.commissionDate=@"无";
        }
        else
        {
            self.commissionDate=json[@"commissionDate"];
        }
        if ([json[@"insOrderNo"] isKindOfClass:[NSNull class]] || !json[@"insOrderNo"])
        {
            self.insOrderNo=@"无";
        }
        else
        {
            self.insOrderNo=json[@"insOrderNo"];
        }
        if ([json[@"policyHolder"] isKindOfClass:[NSNull class]] || !json[@"policyHolder"])
        {
            self.policyHolder=@"无";
        }
        else
        {
            self.policyHolder=json[@"policyHolder"];
        }
        if ([json[@"insuredPerson"] isKindOfClass:[NSNull class]] || !json[@"insuredPerson"])
        {
            self.insuredPerson=@"无";
        }
        else
        {
            self.insuredPerson=json[@"insuredPerson"];
        }
        if ([json[@"priceOfInsurance"] isKindOfClass:[NSNull class]] || !json[@"priceOfInsurance"])
        {
            self.priceOfInsurance=@"0";
        }
        else
        {
            self.priceOfInsurance=[NSString stringWithFormat:@"%.2f",[json[@"priceOfInsurance"] floatValue]];
        }
        if ([json[@"supplier"] isKindOfClass:[NSNull class]] || !json[@"supplier"])
        {
            self.supplier=@"无";
        }
        else
        {
            self.supplier=json[@"supplier"];
        }
        if ([json[@"annual"] isKindOfClass:[NSNull class]] || !json[@"annual"])
        {
            self.annual=@"无";
        }
        else
        {
            self.annual=json[@"annual"];
        }
    }
    return self;
}

+ (id)commissionDetailWithJSON:(NSDictionary*)json
{
    return [[self alloc]initWithJSON:json];
}

@end
