//
//  JSONForCommissionTax.m
//  华康通
//
//  Created by  雷雨 on 2017/2/15.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import "JSONForCommissionTax.h"

@implementation JSONForCommissionTax

-(id) initWithJSON:(NSDictionary*)json
{
    if (self = [super init])
    {
        if ([json[@"agentCode"] isKindOfClass:[NSNull class]] || !json[@"agentCode"])
        {
            self.agentCode = @"无";
        }
        else
        {
            self.agentCode = json[@"agentCode"];
        }
        if ([json[@"total"] isKindOfClass:[NSNull class]] || !json[@"total"])
        {
            self.total = @"0";
        }
        else
        {
            self.total = json[@"total"];
        }
        if ([json[@"taxDate"] isKindOfClass:[NSNull class]] || !json[@"taxDate"])
        {
            self.taxDate = @"无";
        }
        else
        {
            self.taxDate = json[@"taxDate"];
        }
        if ([json[@"constructionTax"] isKindOfClass:[NSNull class]] || !json[@"constructionTax"])
        {
            self.constructionTax = @"0";
        }
        else
        {
            self.constructionTax = json[@"constructionTax"];
        }
        if ([json[@"educationAddtionalTax"] isKindOfClass:[NSNull class]] || !json[@"educationAddtionalTax"])
        {
            self.educationAddtionalTax=@"0";
        }
        else
        {
            self.educationAddtionalTax=json[@"educationAddtionalTax"];
        }
        if ([json[@"embankmentTax"] isKindOfClass:[NSNull class]] || !json[@"embankmentTax"])
        {
            self.embankmentTax=@"0";
        }
        else
        {
            self.embankmentTax=json[@"embankmentTax"];
        }
        if ([json[@"salesTax"] isKindOfClass:[NSNull class]] || !json[@"salesTax"])
        {
            self.salesTax=@"0";
        }
        else
        {
            self.salesTax=json[@"salesTax"];
        }
        if ([json[@"contingentIncomeTax"] isKindOfClass:[NSNull class]] || !json[@"contingentIncomeTax"])
        {
            self.contingentIncomeTax=@"0";
        }
        else
        {
            self.contingentIncomeTax=json[@"contingentIncomeTax"];
        }
        if ([json[@"taxableIncome"] isKindOfClass:[NSNull class]] || !json[@"taxableIncome"])
        {
            self.taxableIncome=@"0";
        }
        else
        {
            self.taxableIncome=json[@"taxableIncome"];
        }
        if ([json[@"taxFreeIncome"] isKindOfClass:[NSNull class]] || !json[@"taxFreeIncome"])
        {
            self.taxFreeIncome=@"0";
        }
        else
        {
            self.taxFreeIncome=json[@"taxFreeIncome"];
        }
        if ([json[@"personalIncomeTax"] isKindOfClass:[NSNull class]] || !json[@"personalIncomeTax"])
        {
            self.personalIncomeTax=@"0";
        }
        else
        {
            self.personalIncomeTax=json[@"personalIncomeTax"];
        }
        
        if ([json[@"createDate"] isKindOfClass:[NSNull class]] || !json[@"createDate"])
        {
            self.createDate=@"无";
        }
        else
        {
            self.createDate=json[@"createDate"];
        }
        if ([json[@"isDelete"] isKindOfClass:[NSNull class]] || !json[@"isDelete"])
        {
            self.isDelete=@"无";
        }
        else
        {
            self.isDelete=json[@"isDelete"];
        }
        
    }
    return self;
}

+ (id)commissionTaxWithJSON:(NSDictionary*)json
{
    return [[self alloc]initWithJSON:json];
}

@end
