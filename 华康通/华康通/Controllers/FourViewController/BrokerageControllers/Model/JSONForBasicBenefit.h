//
//  JSONForBasicBenefit.h
//  华康通
//
//  Created by  雷雨 on 2017/2/14.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSONForBasicBenefit : NSObject

//基本法利益小计
@property (nonatomic, strong) NSString* total;
//代理人工号
@property (nonatomic, strong) NSString* agentCode;
//基本法利益年月
@property (nonatomic, strong) NSString* benefitDate;


//卓越新人奖
@property (nonatomic, strong) NSString* outstandingAward;
//社保补助津贴
@property (nonatomic, strong) NSString* socialSecurityAllowance;
//增员辅导津贴
@property (nonatomic, strong) NSString* recruimentTutoshipAllowance;
//组职津贴
@property (nonatomic, strong) NSString* groupDutyAllowance;
//处职津贴
@property (nonatomic, strong) NSString* districtDutyAllowance;
//部职津贴
@property (nonatomic, strong) NSString* brancheDutyAllowance;
//区职津贴
@property (nonatomic, strong) NSString* areaDutyAllowance;
//营业区育成津贴
@property (nonatomic, strong) NSString* areaDevelopAllowance;


//个人年终奖
@property (nonatomic, strong) NSString* personalAnnualBonus;
//组年终奖
@property (nonatomic, strong) NSString* groupAnnualBonus;
//处年终奖
@property (nonatomic, strong) NSString* districtAnnualBonus;
//部年终奖
@property (nonatomic, strong) NSString* brancheAnnualBonus;
//区年终奖
@property (nonatomic, strong) NSString* areaAnnualBonus;


//个人继续率
@property (nonatomic, strong) NSString* personalContinuationRate;
//组继续率
@property (nonatomic, strong) NSString* groupContinuationRate;
//处继续率
@property (nonatomic, strong) NSString* districtContinuationRate;
//部继续率
@property (nonatomic, strong) NSString* brancheContinuationRate;
//区继续率
@property (nonatomic, strong) NSString* areaContinuationRate;


+(id)basicBenefitWithJSON:(NSDictionary*)json;


@end
