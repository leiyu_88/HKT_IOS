//
//  JSONForDates.m
//  华康通
//
//  Created by  雷雨 on 2017/3/6.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import "JSONForDates.h"

@implementation JSONForDates

- (id)initWithJSON:(NSDictionary*)json
{
    if (self = [super init])
    {
        if ([json[@"queryAbleDate"] isKindOfClass:[NSNull class]] || !json[@"queryAbleDate"])
        {
            self.queryAbleDate = @"无";
        }
        else
        {
            self.queryAbleDate = [NSString stringWithFormat:@"%@",json[@"queryAbleDate"]];
        }
    }
    return self;
}

+ (id)getDateWithJSON:(NSDictionary*)json
{
    return [[self alloc]initWithJSON:json];
}

@end
