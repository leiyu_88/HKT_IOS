//
//  JSONForCommission.h
//  华康通
//
//  Created by  雷雨 on 2017/2/14.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSONForCommission : NSObject

//工号
@property (nonatomic, strong) NSString* agentCode;
//汇总年月
@property (nonatomic, strong) NSString* commissionDate;
//个人FYC
@property (nonatomic, strong) NSString* personalFYC;
//直辖组FYC
@property (nonatomic, strong) NSString* groupFYC;
//直辖部FYC
@property (nonatomic, strong) NSString* branchFYC;
//直辖处FYC
@property (nonatomic, strong) NSString* districtFYC;
//营业区FYC
@property (nonatomic, strong) NSString* areaFYC;
//首年佣金
@property (nonatomic, strong) NSString* firstYearCommission;
//续年佣金
@property (nonatomic, strong) NSString* renewalYearCommission;
//基本法利益
@property (nonatomic, strong) NSString* basicLawBenefit;
//应税金额
@property (nonatomic, strong) NSString* taxAmount;
//税金合计
@property (nonatomic, strong) NSString* taxTotal;
//其他应税收入
@property (nonatomic, strong) NSString* otherTaxIncome;
//不应税收入
@property (nonatomic, strong) NSString* notTaxIncome;
//实发金额
@property (nonatomic, strong) NSString* actualAmount;

+(id)commissionWithJSON:(NSDictionary*)json;

@end
