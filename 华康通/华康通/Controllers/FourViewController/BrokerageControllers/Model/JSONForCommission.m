//
//  JSONForCommission.m
//  华康通
//
//  Created by  雷雨 on 2017/2/14.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import "JSONForCommission.h"

@implementation JSONForCommission

-(id)initWithJSON:(NSDictionary*)json
{
    if (self = [super init])
    {
        if ([json[@"agentCode"] isKindOfClass:[NSNull class]] || !json[@"agentCode"])
        {
            self.agentCode = @"无";
        }
        else
        {
            self.agentCode = json[@"agentCode"];
        }
        if ([json[@"commissionDate"] isKindOfClass:[NSNull class]] || !json[@"commissionDate"])
        {
            self.commissionDate = @"无";
        }
        else
        {
            self.commissionDate = json[@"commissionDate"];
        }
        if ([json[@"personalFYC"] isKindOfClass:[NSNull class]] || !json[@"personalFYC"])
        {
            self.personalFYC = @"0";
        }
        else
        {
            self.personalFYC = json[@"personalFYC"];
        }
        if ([json[@"groupFYC"] isKindOfClass:[NSNull class]] || !json[@"groupFYC"])
        {
            self.groupFYC = @"0";
        }
        else
        {
            self.groupFYC = json[@"groupFYC"];
        }
        if ([json[@"branchFYC"] isKindOfClass:[NSNull class]] || !json[@"branchFYC"])
        {
            self.branchFYC=@"0";
        }
        else
        {
            self.branchFYC=json[@"branchFYC"];
        }
        if ([json[@"districtFYC"] isKindOfClass:[NSNull class]] || !json[@"districtFYC"])
        {
            self.districtFYC=@"0";
        }
        else
        {
            self.districtFYC=json[@"districtFYC"];
        }
        if ([json[@"areaFYC"] isKindOfClass:[NSNull class]] || !json[@"areaFYC"])
        {
            self.areaFYC=@"0";
        }
        else
        {
            self.areaFYC=json[@"areaFYC"];
        }
        if ([json[@"firstYearCommission"] isKindOfClass:[NSNull class]] || !json[@"firstYearCommission"])
        {
            self.firstYearCommission=@"0";
        }
        else
        {
            self.firstYearCommission=json[@"firstYearCommission"];
        }
        if ([json[@"renewalYearCommission"] isKindOfClass:[NSNull class]] || !json[@"renewalYearCommission"])
        {
            self.renewalYearCommission=@"0";
        }
        else
        {
            self.renewalYearCommission=json[@"renewalYearCommission"];
        }
        if ([json[@"basicLawBenefit"] isKindOfClass:[NSNull class]] || !json[@"basicLawBenefit"])
        {
            self.basicLawBenefit=@"0";
        }
        else
        {
            self.basicLawBenefit=json[@"basicLawBenefit"];
        }
        
        if ([json[@"taxAmount"] isKindOfClass:[NSNull class]] || !json[@"taxAmount"])
        {
            self.taxAmount=@"0";
        }
        else
        {
            self.taxAmount=json[@"taxAmount"];
        }
        if ([json[@"taxTotal"] isKindOfClass:[NSNull class]] || !json[@"taxTotal"])
        {
            self.taxTotal=@"0";
        }
        else
        {
            self.taxTotal=json[@"taxTotal"];
        }
        if ([json[@"otherTaxIncome"] isKindOfClass:[NSNull class]] || !json[@"otherTaxIncome"])
        {
            self.otherTaxIncome=@"0";
        }
        else
        {
            self.otherTaxIncome=json[@"otherTaxIncome"];
        }
        if ([json[@"notTaxIncome"] isKindOfClass:[NSNull class]] || !json[@"notTaxIncome"])
        {
            self.notTaxIncome=@"0";
        }
        else
        {
            self.notTaxIncome=json[@"notTaxIncome"];
        }
        if ([json[@"actualAmount"] isKindOfClass:[NSNull class]] || !json[@"actualAmount"])
        {
            self.actualAmount=@"0";
        }
        else
        {
            self.actualAmount=json[@"actualAmount"];
        }
    }
    return self;
}

+(id)commissionWithJSON:(NSDictionary*)json
{
    return [[self alloc]initWithJSON:json];
}

@end
