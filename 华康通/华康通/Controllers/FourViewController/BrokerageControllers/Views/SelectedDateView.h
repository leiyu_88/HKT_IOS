//
//  SelectedDateView.h
//  华康通
//
//  Created by  雷雨 on 2017/1/19.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^returnDataBlock)(NSInteger yearTag,NSInteger monthTag);

@interface SelectedDateView : UIView

@property (weak, nonatomic) IBOutlet UIButton *nextButton;
//@property (weak, nonatomic) IBOutlet UIDatePicker *myDatePicker;
@property (weak, nonatomic) IBOutlet UIPickerView *pickView;

//主页面回调，拿到两个数值;
@property (strong, nonatomic) returnDataBlock block;

//主页面传过来的日期数组
@property (nonatomic, strong) NSArray* dateArrays;

//月行数
@property (nonatomic, unsafe_unretained) NSInteger monthTag;
//年行数
@property (nonatomic, unsafe_unretained) NSInteger yearTag;


+ (id)view;

- (void)returnYearAndMonthWithBlock:(returnDataBlock)block;

@end
