//
//  SelectedDateView.m
//  华康通
//
//  Created by  雷雨 on 2017/1/19.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import "SelectedDateView.h"

@interface SelectedDateView()



@end

@implementation SelectedDateView

+ (id)view
{
    return [[[NSBundle mainBundle]loadNibNamed:@"SelectedDateView" owner:nil options:nil]lastObject];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        self.autoresizingMask = UIViewAutoresizingNone;
    }
    return self;
}


//分离日期中的年月，变成数组(拿到年份数组)
- (NSArray*)getYears
{
    NSMutableArray* array = [NSMutableArray array];
    for (int i = 0; i < self.dateArrays.count; i++)
    {
        NSString* str = self.dateArrays[i];
        if ([str rangeOfString:@"-"].location != NSNotFound)
        {
            NSArray* arr = [str componentsSeparatedByString:@"-"];
            NSString* yearStr = arr[0];
            [array addObject:yearStr];
        }
    }
    //去掉重复的年份
    NSSet* set = [NSSet setWithArray:[array copy]];
    //年份按升序排列
    NSArray* newArr = [self arraySortYearsWithArray:[set allObjects]];
    return  newArr;
}

//根据年月日期中的年，拿到（月份数组）
- (NSArray*)getMonthsWithYear:(NSString*)yearStr
{
    NSMutableArray* arr = [NSMutableArray array];
    for (NSString* dateStr in self.dateArrays)
    {
        if ([dateStr rangeOfString:yearStr].location != NSNotFound)
        {
            [arr addObject:dateStr];
        }
    }
    NSMutableArray* array = [NSMutableArray array];
    for (NSString* dateStr in arr)
    {
        if ([dateStr rangeOfString:@"-"].location != NSNotFound)
        {
            NSArray* arr = [dateStr componentsSeparatedByString:@"-"];
            NSString* monthStr = arr[1];
            [array addObject:monthStr];
        }
    }
    //去掉重复的月份
    NSSet* set = [NSSet setWithArray:[array copy]];
    //月份按升序排列
    NSArray* newArr = [self arraySortYearsWithArray:[set allObjects]];
    NSLog(@"月份数组：newArr = %@",newArr);
    return  newArr;
}

//数组（年份或月份排序）
- (NSArray*)arraySortYearsWithArray:(NSArray*)arr
{
    NSArray *result = [arr sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        return [obj1 compare:obj2]; //升序
    }];
    return result;
}

#pragma mark 自定义日期选择器方法

//获取当前年
- (NSInteger)getCurrentYear
{
    NSDateFormatter* formatter = [[NSDateFormatter alloc]init];
    formatter.dateFormat = @"YYYY";
    NSString* dateStr = [formatter stringFromDate:[[NSDate alloc]init]];
    return [dateStr integerValue];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    NSLog(@"选择年月 ............................................");
    return 2;
}

//显示多少行
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component == 0)
    {
        return [[self getYears] count];
    }
    else
    {
        return [[self getMonthsWithYear:[self getYears][self.yearTag]] count];
    }
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 40.0;
}

//显示每行日期标题
- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (component == 0)
    {
        return [NSString stringWithFormat:@"%@年",[self getYears][row]];
    }
    else
    {
        return [NSString stringWithFormat:@"%@月",[self getMonthsWithYear:[self getYears][self.yearTag]][row]];
    }
}

//完成选择
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (component == 0)
    {
        self.yearTag = row;
        //刷新日期选择列（即第二列）
        [pickerView reloadComponent:1];
        //动画地默认选择第一行
        [pickerView selectRow:0 inComponent:1 animated:YES];
        self.monthTag = 0;
    }
    else
    {
        self.monthTag = row;
    }
    if (self.block)
    {
        self.block(self.yearTag,self.monthTag);
    }
}

- (void)returnYearAndMonthWithBlock:(returnDataBlock)block
{
    self.block = block;
}


@end
