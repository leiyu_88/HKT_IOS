//
//  CommissionController.m
//  华康通
//
//  Created by  雷雨 on 2017/1/18.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import "CommissionController.h"
#import "CommissionCell.h"
#import "JBLYCell.h"
#import "QTSRCell.h"
#import "JSONForBasicBenefit.h"
#import "JSONForCommissionTax.h"
#import "JSONForCommissionDetail.h"
#import "JSONFromQTYS.h"

#define LABELWIFTH ((UISCREENWEITH - 32)/2)

@interface CommissionController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightLabel;

@property (nonatomic, strong) UILabel* bottomLabel;
@property (nonatomic, strong) UIView* bottomView;

//各类津贴数组
@property (nonatomic, strong) NSArray* lists1;
//各类年终奖数组
@property (nonatomic, strong) NSArray* lists2;
//各类继续率数组
@property (nonatomic, strong) NSArray* lists3;
//各类津贴数组标题
@property (nonatomic, strong) NSArray* jtArray;
//各类年终奖数组标题
@property (nonatomic, strong) NSArray* nzjArray;
//各类继续率数组标题
@property (nonatomic, strong) NSArray* jxlArray;
//一个字典数据类型记录 基本法利益的分类数据
@property (nonatomic, strong) NSDictionary* jblyDic;

//税金数组
@property (nonatomic, strong) NSArray* sjLists;
//税金标题数组
@property (nonatomic, strong) NSArray* sjArray;
//首年或者续年佣金数组
@property (nonatomic, strong) NSMutableArray* snArray;
@property (nonatomic, strong) MJRefreshAutoGifFooter *footer;
//用一个数来记录用户上拉加载的次数
@property (nonatomic, unsafe_unretained) NSInteger slCount;

@property (nonatomic, strong) ShowNoNetworkView* workView;



//其他不应税收入数组
@property (nonatomic, strong) NSMutableArray* QTBYSSRArray;
//应税收入和不应税收入
@property (nonatomic, strong) NSMutableArray* NYSSRArray;

@end

@implementation CommissionController

- (NSMutableArray*)QTBYSSRArray
{
    if (!_QTBYSSRArray)
    {
        _QTBYSSRArray = [NSMutableArray array];
    }
    return _QTBYSSRArray;
}

- (NSMutableArray*)NYSSRArray
{
    if (!_NYSSRArray)
    {
        _NYSSRArray = [NSMutableArray array];
    }
    return _NYSSRArray;
}

- (NSMutableArray*)snArray
{
    if (!_snArray)
    {
        _snArray = [NSMutableArray array];
    }
    return _snArray;
}



- (UILabel*)bottomLabel
{
    if (!_bottomLabel)
    {
        
        _bottomLabel = [[UILabel alloc]init];
        _bottomLabel.text = self.bottomStr;
        _bottomLabel.textAlignment = NSTextAlignmentLeft;
        _bottomLabel.font = SetFont(10.0);
        _bottomLabel.textColor = E5E5EColor;
        if (self.myType == CommissionTypeJBLY)
        {
            _bottomLabel.frame = CGRectMake(8, 10, UISCREENWEITH - 16, 60);
            _bottomLabel.numberOfLines = 3;
        }
        else
        {
            _bottomLabel.frame = CGRectMake(8, 5, UISCREENWEITH - 16, 20);
            _bottomLabel.numberOfLines = 1;
        }
    }
    return _bottomLabel;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.slCount = 1;
    self.rightLabel.text = self.total;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:LoadImage(@"back1") style:UIBarButtonItemStylePlain target:self action:@selector(popRootVC)];
    HKTShowNetworkView* networking = [HKTShowNetworkView shareInstanceWithTitle:@"检测到您的设备没有连接网络!" withImageStr:@"no_record" withType:0 withFrame:self.view.frame withVC:self];
    [networking showNetworkViewWithSuccess:^{
        self.tableView.scrollEnabled = YES;
        switch (self.myType)
        {
            case 0:
            {
                [self.tableView registerNib:[UINib nibWithNibName:@"CommissionCell" bundle:nil] forCellReuseIdentifier:@"cell"];
                [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
                self.bottomView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, UISCREENWEITH, 30)];
                self.bottomView.backgroundColor = LIGHTGREYColor;
                [self.bottomView addSubview:self.bottomLabel];
                self.tableView.tableFooterView = self.bottomView;
                //            self.rightLabel.text = self.total;
                self.footer = [MJRefreshAutoGifFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
                self.tableView.footer = self.footer;
                //获取续年佣金数据
                [self getBasicBenefitWithDate:self.date withIsFirstYear:@"1" withPageSize:self.slCount withPages:10 withService:@"getFASTCommissionDetail"];
            }
                break;
            case 1:
            {
                [self.tableView registerNib:[UINib nibWithNibName:@"JBLYCell" bundle:nil] forCellReuseIdentifier:@"cell"];
                [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
                self.bottomView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, UISCREENWEITH, 80)];
                self.bottomView.backgroundColor = LIGHTGREYColor;
                [self.bottomView addSubview:self.bottomLabel];
                //            self.rightLabel.text = self.total;
                NSString* newDate = [[self.date stringByReplacingOccurrencesOfString:@"-" withString:@""] substringToIndex:6];
                self.leftLabel.text = [NSString stringWithFormat:@"%@（%@）",self.title,newDate];
                self.tableView.tableFooterView = self.bottomView;
                //获取基本法利益数据
                [self getgetFASTBasicBenefitWithDate:self.date withType:@"getFASTBasicBenefit"];
            }
                break;
            case 2:
            {
                //            JSONFromQTYS* s = [[JSONFromQTYS alloc]init];
                //            s.amount = @"666";
                //            s.summary = @"leiyu";
                //            [self.QTBYSSRArray addObject:s];
                [self.tableView registerNib:[UINib nibWithNibName:@"QTSRCell" bundle:nil] forCellReuseIdentifier:@"cell"];
                [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
                self.bottomView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, UISCREENWEITH, 30)];
                self.bottomView.backgroundColor = LIGHTGREYColor;
                [self.bottomView addSubview:self.bottomLabel];
                self.tableView.tableFooterView = self.bottomView;
                self.footer = [MJRefreshAutoGifFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
                self.tableView.footer = self.footer;
                NSString* newDate = [[self.date stringByReplacingOccurrencesOfString:@"-" withString:@""] substringToIndex:6];
                self.leftLabel.text = [NSString stringWithFormat:@"%@（%@）",self.title,newDate];
                //获取其他应税收入数据
                [self getBasicBenefitWithDate:self.date withIsFirstYear:@"2" withPageSize:self.slCount withPages:10 withService:@"getFASTCommissionTaxDetail"];
            }
                break;
            case 3:
            {
                [self.tableView registerNib:[UINib nibWithNibName:@"JBLYCell" bundle:nil] forCellReuseIdentifier:@"cell"];
                [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
                self.bottomView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, UISCREENWEITH, 30)];
                self.bottomView.backgroundColor = LIGHTGREYColor;
                [self.bottomView addSubview:self.bottomLabel];
                self.tableView.tableFooterView = self.bottomView;
                NSString* newDate = [[self.date stringByReplacingOccurrencesOfString:@"-" withString:@""] substringToIndex:6];
                self.leftLabel.text = [NSString stringWithFormat:@"%@（%@）",self.title,newDate];
                //获取税金数据
                [self getgetFASTBasicBenefitWithDate:self.date withType:@"getFASTCommissionTax"];
            }
                break;
            case 5:
            {
                [self.tableView registerNib:[UINib nibWithNibName:@"QTSRCell" bundle:nil] forCellReuseIdentifier:@"cell"];
                [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
                self.bottomView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, UISCREENWEITH, 30)];
                self.bottomView.backgroundColor = LIGHTGREYColor;
                [self.bottomView addSubview:self.bottomLabel];
                self.tableView.tableFooterView = self.bottomView;
                self.footer = [MJRefreshAutoGifFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
                self.tableView.footer = self.footer;
                NSString* newDate = [[self.date stringByReplacingOccurrencesOfString:@"-" withString:@""] substringToIndex:6];
                self.leftLabel.text = [NSString stringWithFormat:@"%@（%@）",self.title,newDate];
                //获取非应税收入数据
                [self getBasicBenefitWithDate:self.date withIsFirstYear:@"2" withPageSize:self.slCount withPages:10 withService:@"getFASTCommissionTaxDetail"];
            }
                break;
            default:
            {
                [self.tableView registerNib:[UINib nibWithNibName:@"CommissionCell" bundle:nil] forCellReuseIdentifier:@"cell"];
                [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
                self.bottomView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, UISCREENWEITH, 30)];
                self.bottomView.backgroundColor = LIGHTGREYColor;
                [self.bottomView addSubview:self.bottomLabel];
                self.tableView.tableFooterView = self.bottomView;
                //            self.rightLabel.text = self.total;
                self.footer = [MJRefreshAutoGifFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
                self.tableView.footer = self.footer;
                //获取首佣佣金数据
                [self getBasicBenefitWithDate:self.date withIsFirstYear:@"0" withPageSize:self.slCount withPages:10 withService:@"getFASTCommissionDetail"];
            }
                break;
        }
        
    } failure:^{
        self.tableView.scrollEnabled = NO;
    } click:^(NSInteger type) {
        NSLog(@"点击了什么类型的按钮%ld!",(long)type);
        if (type == 0)
        {
            //弹出网络提示
            
        }
    }];
}

//上拉加载
- (void)loadMoreData
{
    self.slCount++;
    NSLog(@"self.slCount = %ld",(long)self.slCount);
    NSLog(@"上拉加载");
    if (self.myType == 0)
    {
        //获取续年佣金数据
        [self getBasicBenefitWithDate:self.date withIsFirstYear:@"1" withPageSize:self.slCount withPages:10 withService:@"getFASTCommissionDetail"];
    }
    else if (self.myType == 2)
    {
        //获取其他应税收入
        [self getBasicBenefitWithDate:self.date withIsFirstYear:@"2" withPageSize:self.slCount withPages:10 withService:@"getFASTCommissionTaxDetail"];
    }
    else if (self.myType == 5)
    {
        //获取非应税收入
        [self getBasicBenefitWithDate:self.date withIsFirstYear:@"2" withPageSize:self.slCount withPages:10 withService:@"getFASTCommissionTaxDetail"];
    }
    else if (self.myType == 4)
    {
        //获取首佣佣金数据
        [self getBasicBenefitWithDate:self.date withIsFirstYear:@"0" withPageSize:self.slCount withPages:10 withService:@"getFASTCommissionDetail"];
    }
}

/**
 *  停止刷新
 */
-(void)endRefresh
{
    if ([self.tableView.header isRefreshing])
    {
        [self.tableView.header endRefreshing];
    }
    if ([self.tableView.footer isRefreshing])
    {
        [self.tableView.footer endRefreshing];
    }
}

- (void)popRootVC
{
    [self.navigationController popViewControllerAnimated:YES];
}



#pragma mark  //分类首佣佣金和续年佣金数据、其他收入
- (void)getBasicBenefitWithDate:(NSString*)date withIsFirstYear:(NSString*)type withPageSize:(NSInteger)pageSize withPages:(NSInteger)pages withService:(NSString*)requestService
{
    NSDictionary* json = @{@"platformType":@"1",
                           @"orderType":@"01",
                           @"requestService":requestService,
                           @"requestObject":@{@"agentCode":[TRUserAgenCode getUserAgenCode],@"queryDate":date,@"isFirstYear":type,@"pageParam":@{@"currentPage":[NSString stringWithFormat:@"%ld",(long)pageSize],@"pageSize":[NSString stringWithFormat:@"%ld",(long)pages],@"queryAll":@false}}
                           };
    if ([type isEqualToString:@"2"])
    {
        json = @{@"platformType":@"1",
                 @"orderType":@"01",
                 @"requestService":requestService,
                 @"requestObject":@{@"agentCode":[TRUserAgenCode getUserAgenCode],@"queryDate":date,@"pageParam":@{@"currentPage":[NSString stringWithFormat:@"%ld",(long)pageSize],@"pageSize":[NSString stringWithFormat:@"%ld",(long)pages],@"queryAll":@false}}
                 };
    }
    NSLog(@"post分类首佣佣金和续年佣金数据 = %@",json);
    //字符串转字符串
    NSString* str = [JSONToString dictionaryToJson:json];
    //转md5
    MyAdditions* md5 = [[MyAdditions alloc]init];
    md5.str = [NSString stringWithFormat:@"%@%@",KEY_POST,str];
    NSString* md5_Str = [md5 md5];
    NSString* url = [NSString stringWithFormat:@"%@&sign=%@",SERVER_URL,md5_Str];
    NSLog(@"post每月基本利益法信息post网址 = %@",url);
    if ([type isEqualToString:@"2"])
    {
        //其他应税收入
        [self getOtherAmountWithURL:url withParam:json];
    }
    else
    {
        //首佣佣金
        [self getFirstCommissionWithURL:url withParam:json];
    }
}


#pragma mark  //分类post数据
- (void)getgetFASTBasicBenefitWithDate:(NSString*)date withType:(NSString*)type
{
    //设置缓存的key
    NSDictionary* json = @{@"platformType":@"1",
                           @"orderType":@"01",
                           @"requestService":type,
                           @"requestObject":@{@"agentCode":[TRUserAgenCode getUserAgenCode],@"queryDate":date}
                           };
    NSLog(@"post每月基本利益法信息 = %@",json);
    //字符串转字符串
    NSString* str = [JSONToString dictionaryToJson:json];
    //转md5
    MyAdditions* md5 = [[MyAdditions alloc]init];
    md5.str = [NSString stringWithFormat:@"%@%@",KEY_POST,str];
    NSString* md5_Str = [md5 md5];
    NSString* url = [NSString stringWithFormat:@"%@&sign=%@",SERVER_URL,md5_Str];
    NSLog(@"post每月基本利益法信息post网址 = %@",url);
    if ([type isEqualToString:@"getFASTBasicBenefit"])
    {
        //基本利益法
        [self getgetFASTBasicBenefitWithURL:url withParam:json];
    }
    else if ([type isEqualToString:@"getFASTCommissionTax"])
    {
        //税金
        [self getFASTCommissionTaxWithURL:url withParam:json];
    }
}

#pragma mark  //获取其他应税收入信息
- (void)getOtherAmountWithURL:(NSString*)url withParam:(NSDictionary*)param
{
    self.sjLists = @[];
    [XHNetworking POST:url parameters:param success:^(NSData *responseObject) {
        id json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        NSLog(@"获取其他应税收入信息 = %@",json);
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            if ([json isKindOfClass:[NSDictionary class]] && json)
            {
                if ([json[@"resultCode"] isEqualToString:@"0"])
                {
                    
                    for (NSDictionary* dic in json[@"responseObject"][@"list"])
                    {
                        JSONFromQTYS* qtys1 = [JSONFromQTYS commissionTaxWithJSON:dic];
                        if (self.myType == 2)
                        {
                            if ([qtys1.isTaxAble isEqualToString:@"Y"])
                            {
                                [self.QTBYSSRArray addObject:qtys1];
                            }
                        }
                        if (self.myType == 5)
                        {
                            if ([qtys1.isTaxAble isEqualToString:@"N"])
                            {
                                [self.NYSSRArray addObject:qtys1];
                            }
                        }
                    }
                    NSLog(@"self.QTBYSSRArray的元素数量 ： %ld",(long)self.QTBYSSRArray.count);
                    if ([json[@"responseObject"][@"pageParams"] isKindOfClass:[NSDictionary class]] && json[@"responseObject"][@"pageParams"])
                    {
                        //总条数
                        NSInteger i1 = [json[@"responseObject"][@"pageParams"][@"pageTotal"] integerValue];
                        //当前页
                        NSInteger i2 = [json[@"responseObject"][@"pageParams"][@"currentPage"] integerValue];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.footer setHidden:NO];
                            if (i1 == i2)
                            {
                                [self.tableView.footer noticeNoMoreData];
                            };
                            if ([json[@"responseObject"][@"list"] count] == 0)
                            {
                                [self.tableView.footer noticeNoMoreData];
                            }
                            
                        });
                    }
                }
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [self endRefresh];
                [self addNoDataView];
                [self.tableView reloadData];
            });
        });
    } failure:^(NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self addNoDataView];
            [self endRefresh];
        });
    }];
}


#pragma mark  //获取首年佣金和续年信息
- (void)getFirstCommissionWithURL:(NSString*)url withParam:(NSDictionary*)param
{
    self.sjLists = @[];
    [XHNetworking POST:url parameters:param success:^(NSData *responseObject) {
        id json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        NSLog(@"获取首年佣金或续年佣金信息 = %@",json);
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            if ([json isKindOfClass:[NSDictionary class]] && json)
            {
                if ([json[@"resultCode"] isEqualToString:@"0"])
                {
                    for (NSDictionary* dic in json[@"responseObject"][@"list"])
                    {
                        JSONForCommissionDetail* tax = [JSONForCommissionDetail commissionDetailWithJSON:dic];
                        [self.snArray addObject:tax];
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                       self.leftLabel.text = [NSString stringWithFormat:@"合计:%@笔",json[@"responseObject"][@"pageParam"][@"records"]];
                    });
                    if ([json[@"responseObject"][@"pageParams"] isKindOfClass:[NSDictionary class]] && json[@"responseObject"][@"pageParams"])
                    {
                        //总条数
                        NSInteger i1 = [json[@"responseObject"][@"pageParams"][@"pageTotal"] integerValue];
                        //当前页
                        NSInteger i2 = [json[@"responseObject"][@"pageParams"][@"currentPage"] integerValue];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if (i1 == i2)
                            {
                                [self.tableView.footer noticeNoMoreData];
                            };
                            if ([json[@"responseObject"][@"list"] count] == 0)
                            {
                                [self.tableView.footer noticeNoMoreData];
                            }
                        });
                    }
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        self.leftLabel.text = [NSString stringWithFormat:@"合计:0笔"];
                    });
                }
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.leftLabel.text = [NSString stringWithFormat:@"合计:0笔"];
                });
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [self endRefresh];
                [self addNoDataView];
                [self.tableView reloadData];
            });
        });
    } failure:^(NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self endRefresh];
            [self addNoDataView];
            self.leftLabel.text = [NSString stringWithFormat:@"合计:0笔"];
        });
    }];
}

- (void)createWorkView
{
    if (self.workView == nil)
    {
        self.workView = [[ShowNoNetworkView alloc]initWithFrame:CGRectMake(0, 40, self.view.bounds.size.width, self.view.bounds.size.height - 40)];
        self.workView.labelText = @"暂无数据展示...";
        self.workView.imageName = @"no_record";
        self.workView.myType = MyTypeOfViewForShowError;
        [self.workView createAllSubView];
        [self.view addSubview:self.workView];
    }
}

- (void)removeWorkView
{
    self.workView.frame = CGRectZero;
    [self.workView removeFromSuperview];
    self.workView = nil;
}

//当没有数据的时候展示一个提示view
- (void)addNoDataView
{
    if (self.snArray.count != 0 || self.sjLists.count != 0 || self.lists1.count != 0 || self.QTBYSSRArray.count != 0 || self.NYSSRArray.count != 0)
    {
        [self removeWorkView];
    }
    else
    {
        
        [self createWorkView];
    }
}

#pragma mark  //获取税金信息
- (void)getFASTCommissionTaxWithURL:(NSString*)url withParam:(NSDictionary*)param
{
    self.sjLists = @[];
    [XHNetworking POST:url parameters:param success:^(NSData *responseObject) {
        id json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        NSLog(@"获取税金信息 = %@",json);
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            if ([json isKindOfClass:[NSDictionary class]] && json)
            {
                if ([json[@"resultCode"] isEqualToString:@"0"])
                {
                    NSMutableArray* arr1 = [NSMutableArray array];
                    NSMutableArray* arr2 = [NSMutableArray array];
                    id dic = json[@"responseObject"][@"list"][0];
                    if ([dic isKindOfClass:[NSDictionary class]])
                    {
                        JSONForCommissionTax* tax = [JSONForCommissionTax commissionTaxWithJSON:dic];
                        if ([tax.personalIncomeTax floatValue] != 0.0)
                        {
                            [arr1 addObject:tax.personalIncomeTax];//个人所得税
                            [arr2 addObject:@"个人所得税"];
                        }
                        
                        if ([tax.constructionTax floatValue] != 0.0)
                        {
                            [arr1 addObject:tax.constructionTax];//城建税
                            [arr2 addObject:@"城建税"];
                        }
                        
                        if ([tax.educationAddtionalTax floatValue] != 0.0)
                        {
                            [arr1 addObject:tax.educationAddtionalTax];//教育附加税
                            [arr2 addObject:@"教育附加税"];
                        }
                        
                        if ([tax.embankmentTax floatValue] != 0.0)
                        {
                            [arr1 addObject:tax.embankmentTax];//堤围税
                            [arr2 addObject:@"堤围税"];
                        }
                        
                        if ([tax.salesTax floatValue] != 0.0)
                        {
                            [arr1 addObject:tax.salesTax];//营业税
                            [arr2 addObject:@"营业税"];
                        }
                        
                        if ([tax.contingentIncomeTax floatValue] != 0.0)
                        {
                            [arr1 addObject:tax.contingentIncomeTax];//偶然所得税
                            [arr2 addObject:@"偶然所得税"];
                        }
                        
                        
                        self.sjLists = [arr1 copy];
                        self.sjArray = [arr2 copy];
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.tableView reloadData];
                        [self addNoDataView];
                    });
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self addNoDataView];
                    });
                }
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self addNoDataView];
                });
            }
        });
    } failure:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self addNoDataView];
        });
    }];
}
#pragma mark  //获取基本利益法信息
- (void)getgetFASTBasicBenefitWithURL:(NSString*)url withParam:(NSDictionary*)param
{
    self.lists1 = @[];
    self.lists2 = @[];
    self.lists3 = @[];
    [XHNetworking POST:url parameters:param success:^(NSData *responseObject) {
        id json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        NSLog(@"获取基本利益法信息 = %@",json);
        
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            if ([json isKindOfClass:[NSDictionary class]] && json)
            {
                if ([json[@"resultCode"] isEqualToString:@"0"])
                {
                    NSMutableArray* arr1 = [NSMutableArray array];
                    NSMutableArray* arr2 = [NSMutableArray array];
                    NSMutableArray* arr3 = [NSMutableArray array];
                    NSMutableArray* arr11 = [NSMutableArray array];
                    NSMutableArray* arr22 = [NSMutableArray array];
                    NSMutableArray* arr33 = [NSMutableArray array];
                    NSMutableArray* arr = [NSMutableArray array];
                    NSMutableDictionary* dic1 = [NSMutableDictionary dictionary];
                    id dic = json[@"responseObject"][@"list"][0];
                    if ([dic isKindOfClass:[NSDictionary class]])
                    {
                        
                        JSONForBasicBenefit* fit = [JSONForBasicBenefit basicBenefitWithJSON:dic];
                        if ([fit.outstandingAward floatValue] != 0.0)
                        {
                            [arr1 addObject:fit.outstandingAward];//卓越新人奖
                            [arr11 addObject:@"卓越新人奖"];
                        }
                        if ([fit.socialSecurityAllowance floatValue] != 0.0)
                        {
                            [arr1 addObject:fit.socialSecurityAllowance];//社保补助津贴
                            [arr11 addObject:@"社保补助津贴"];
                        }
                        if ([fit.recruimentTutoshipAllowance floatValue] != 0.0)
                        {
                            [arr1 addObject:fit.recruimentTutoshipAllowance];//增员辅导津贴
                            [arr11 addObject:@"增员辅导津贴"];
                        }
                        if ([fit.groupDutyAllowance floatValue] != 0.0)
                        {
                            [arr1 addObject:fit.groupDutyAllowance];//组职责津贴
                            [arr11 addObject:@"组职责津贴"];
                        }
                        if ([fit.brancheDutyAllowance floatValue] != 0.0)
                        {
                            [arr1 addObject:fit.brancheDutyAllowance];//部职责津贴
                            [arr11 addObject:@"部职责津贴"];
                        }
                        if ([fit.districtDutyAllowance floatValue] != 0.0)
                        {
                            [arr1 addObject:fit.districtDutyAllowance];//处职责津贴
                            [arr11 addObject:@"处职责津贴"];
                        }
                        if ([fit.areaDutyAllowance floatValue] != 0.0)
                        {
                            [arr1 addObject:fit.areaDutyAllowance];//区职津贴
                            [arr11 addObject:@"区职责津贴"];
                        }
                        if ([fit.areaDevelopAllowance floatValue] != 0.0)
                        {
                            [arr1 addObject:fit.areaDevelopAllowance];//营业区育成津贴
                            [arr11 addObject:@"营业区育成津贴"];
                        }
                        
                        self.lists1 = [arr1 copy];
                        self.jtArray = [arr11 copy];
                        if (self.jtArray.count > 0)
                        {
                            [arr addObject:@"各类津贴"];
                            [dic1 setObject:self.jtArray forKey:@"各类津贴"];
                        }
                        
                        
                        
                        if ([fit.personalAnnualBonus floatValue] != 0.0)
                        {
                            [arr2 addObject:fit.personalAnnualBonus];//个人年终奖
                            [arr22 addObject:@"个人年终奖"];
                        }
                        if ([fit.groupAnnualBonus floatValue] != 0.0)
                        {
                            [arr2 addObject:fit.groupAnnualBonus];//组年终奖
                            [arr22 addObject:@"组年终奖"];
                        }
                        if ([fit.brancheAnnualBonus floatValue] != 0.0)
                        {
                            [arr2 addObject:fit.brancheAnnualBonus];//部年终奖
                            [arr22 addObject:@"部年终奖"];
                        }
                        if ([fit.districtAnnualBonus floatValue] != 0.0)
                        {
                            [arr2 addObject:fit.districtAnnualBonus];//处年终奖
                            [arr22 addObject:@"处年终奖"];
                        }
                        if ([fit.areaAnnualBonus floatValue] != 0.0)
                        {
                            [arr2 addObject:fit.areaAnnualBonus];//区年终奖
                            [arr22 addObject:@"区年终奖"];
                        }
                        
                        self.lists2 = [arr2 copy];
                        self.nzjArray = [arr22 copy];
                        if (self.nzjArray.count > 0)
                        {
                            [arr addObject:@"各类年终奖"];
                            [dic1 setObject:self.nzjArray forKey:@"各类年终奖"];
                        }
                        
                        if (![fit.personalContinuationRate isEqualToString:@"无"] && [fit.personalContinuationRate floatValue] != 0.0)
                        {
                            [arr3 addObject:fit.personalContinuationRate];//个人继续率
                            [arr33 addObject:@"个人继续率"];
                        }
                        if (![fit.groupContinuationRate isEqualToString:@"无"] && [fit.groupContinuationRate floatValue] != 0.0)
                        {
                            [arr3 addObject:fit.groupContinuationRate];//组继续率
                            [arr33 addObject:@"组继续率"];
                        }
                        if (![fit.districtContinuationRate isEqualToString:@"无"] && [fit.districtContinuationRate floatValue] != 0.0)
                        {
                            [arr3 addObject:fit.districtContinuationRate];//处继续率
                            [arr33 addObject:@"处继续率"];
                        }
                        if (![fit.brancheContinuationRate isEqualToString:@"无"] && [fit.brancheContinuationRate floatValue] != 0.0)
                        {
                            [arr3 addObject:fit.brancheContinuationRate];//部继续率
                            [arr33 addObject:@"部继续率"];
                        }
                        if (![fit.areaContinuationRate isEqualToString:@"无"] && [fit.areaContinuationRate floatValue] != 0.0)
                        {
                            [arr3 addObject:fit.areaContinuationRate];//区继续率
                            [arr33 addObject:@"区继续率"];
                        }
                        self.lists3 = [arr3 copy];
                        self.jxlArray = [arr33 copy];
                        if (self.jxlArray.count > 0)
                        {
                            [arr addObject:@"各类继续率"];
                            [dic1 setObject:self.jxlArray forKey:@"各类继续率"];
                        }
                        self.sectionLabelNames = [arr copy];
                        self.jblyDic = [dic1 copy];
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.tableView reloadData];
                        [self addNoDataView];
                    });
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self addNoDataView];
                    });
                }
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self addNoDataView];
                });
            }
        });
        
        
    } failure:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self addNoDataView];
        });
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (self.myType)
    {
        case 0:
            return self.snArray.count;
            break;
        case 1:
            return [self.jblyDic[self.sectionLabelNames[section]] count];
            break;
        case 2:
            return self.QTBYSSRArray.count;
            break;
        case 3:
            return self.sjArray.count;
            break;
        case 5:
            return self.NYSSRArray.count;
            break;
        default:
            return self.snArray.count;
            break;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    switch (self.myType)
    {
        case 0:
            return 1;
            break;
        case 1:
            return self.sectionLabelNames.count;
            break;
        case 2:
            return 1;
            break;
        case 3:
            return 1;
            break;
        case 5:
            return 1;
            break;
        default:
            return 1;
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    switch (self.myType)
    {
        case 0:
            return 10.0;
            break;
        case 1:
            return 40.0;
            break;
        case 2:
            return 10.0;
            break;
        case 3:
            return 10.0;
            break;
        case 5:
            return 10.0;
            break;
        default:
            return 10.0;
            break;
    }
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView* view = [[UIView alloc]init];
    view.backgroundColor = LIGHTGREYColor;
    switch (self.myType)
    {
        case 0:
        {
            view.frame = CGRectMake(0, 0, UISCREENWEITH, 10);
        }
            break;
        case 1:
        {
            view.frame = CGRectMake(0, 0, UISCREENWEITH, 40);
            UILabel* label1 = [[UILabel alloc]initWithFrame:CGRectMake(16, 0, LABELWIFTH, 40)];
            label1.text = self.sectionLabelNames[section];
            label1.textAlignment = NSTextAlignmentLeft;
            label1.font = SetFont(15.0);
            label1.textColor = E5E5EColor;
            [view addSubview:label1];
        }
            break;
        case 2:
        {
            view.backgroundColor = F0F0F0Color;
            view.frame = CGRectMake(0, 0, UISCREENWEITH, 10);
        }
            break;
        case 3:
        {
            view.frame = CGRectMake(0, 0, UISCREENWEITH, 10);
        }
            break;
        case 5:
        {
            view.backgroundColor = F0F0F0Color;
            view.frame = CGRectMake(0, 0, UISCREENWEITH, 10);
        }
            break;
        default:
        {
            view.frame = CGRectMake(0, 0, UISCREENWEITH, 10);
        }
            break;
    }
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (self.myType)
    {
        case 0:
            return 210.0;
            break;
        case 1:
            return 40.0;
            break;
        case 2:
        {
            UITableViewCell* cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];
            return cell.bounds.size.height;
        }
            break;
        case 3:
            return 40.0;
            break;
        case 5:
        {
            UITableViewCell* cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];
            return cell.bounds.size.height;
        }
            break;
        default:
            return 210.0;
            break;
    }
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (self.myType)
    {
        case 0:
        {
            CommissionCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            if (self.snArray.count > 0)
            {
                JSONForCommissionDetail* tax = self.snArray[indexPath.row];
                cell.topLabel.text = [NSString stringWithFormat:@"投保单号：%@",tax.insOrderNo];
                cell.topLeftLabel.text = [NSString stringWithFormat:@"投保人：%@",tax.policyHolder];
                cell.topRightLabel.text = [NSString stringWithFormat:@"被保人：%@",tax.insuredPerson];
                cell.centerLeftLabel.text = [NSString stringWithFormat:@"保费：¥%@",tax.premium];
                cell.centerRightLabel.text = [NSString stringWithFormat:@"年度：%@",tax.annual];
                cell.bottomCenterLabel.text = [NSString stringWithFormat:@"供应商：%@",tax.supplier];
                cell.bottomLabel.text = [NSString stringWithFormat:@"佣金：¥%@",tax.commission];
            }
            return cell;
        }
            break;
        case 1:
        {
            JBLYCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            if ([self.sectionLabelNames[indexPath.section] isEqualToString:@"各类津贴"])
            {
                cell.leftLabel.text = self.jtArray[indexPath.row];
                if (self.lists1.count > 0)
                {
                    cell.rightLabel.text = [NSString stringWithFormat:@"¥%.2f",[self.lists1[indexPath.row] floatValue]];
                }
                else
                {
                    cell.rightLabel.text = @"...";
                }
            }
            else if ([self.sectionLabelNames[indexPath.section] isEqualToString:@"各类年终奖"])
            {
                cell.leftLabel.text = self.nzjArray[indexPath.row];
                if (self.lists1.count > 0)
                {
                    cell.rightLabel.text = [NSString stringWithFormat:@"¥%.2f",[self.lists2[indexPath.row] floatValue]];
                }
                else
                {
                    cell.rightLabel.text = @"...";
                }
            }
            else
            {
                cell.leftLabel.text = self.jxlArray[indexPath.row];
                if (self.lists1.count > 0)
                {
                    cell.rightLabel.text = [NSString stringWithFormat:@"%.2f%%",[self.lists3[indexPath.row] floatValue]];
                }
                else
                {
                    cell.rightLabel.text = @"...";
                }
            }
            return cell;
        }
            break;
        case 2:
        {
            QTSRCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            if (self.QTBYSSRArray.count > 0)
            {
                //展示应税收入
                cell.qtys = self.QTBYSSRArray[indexPath.row];
            }
            return cell;
        }
            break;
        case 3:
        {
            JBLYCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            if (self.sjArray.count > 0)
            {
                cell.leftLabel.text = self.sjArray[indexPath.row];
            }
            if (self.sjLists.count > 0)
            {
                cell.rightLabel.text = [NSString stringWithFormat:@"¥%.2f",[self.sjLists[indexPath.row] floatValue]];
                NSString* str = [NSString stringWithFormat:@"%@",self.sjLists[indexPath.row]];
                if ([str isEqualToString:@""] || [str isEqualToString:@"无"])
                {
                    cell.rightLabel.text = str;
                }
            }
            return cell;
        }
            break;
        case 5:
        {
            QTSRCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            if (self.NYSSRArray.count > 0)
            {
                //展示非应税收入
                cell.qtys = self.NYSSRArray[indexPath.row];
            }
            return cell;
        }
            break;
        default:
        {
            CommissionCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            if (self.snArray.count > 0)
            {
                JSONForCommissionDetail* tax = self.snArray[indexPath.row];
                cell.topLabel.text = [NSString stringWithFormat:@"投保单号：%@",tax.insOrderNo];
                cell.topLeftLabel.text = [NSString stringWithFormat:@"投保人：%@",tax.policyHolder];
                cell.topRightLabel.text = [NSString stringWithFormat:@"被保人：%@",tax.insuredPerson];
                cell.centerLeftLabel.text = [NSString stringWithFormat:@"保费：¥%@",tax.premium];
                cell.centerRightLabel.text = [NSString stringWithFormat:@"价保：¥%@",tax.priceOfInsurance];
                cell.bottomCenterLabel.text = [NSString stringWithFormat:@"供应商：%@",tax.supplier];
                cell.bottomLabel.text = [NSString stringWithFormat:@"佣金：¥%@",tax.commission];
            }
            return cell;
        }
            break;
    }
}


@end
