//
//  CommissionController.h
//  华康通
//
//  Created by  雷雨 on 2017/1/18.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_OPTIONS(NSUInteger, CommissionType) {
    CommissionTypeXNYJ = 0,
    CommissionTypeJBLY = 1,
    CommissionTypeQTSR = 2,
    CommissionTypeSJ = 3,
    CommissionTypeSYYJ = 4,
    CommissionTypeNSYYJ = 5,
};


@interface CommissionController : UIViewController

@property (nonatomic, unsafe_unretained) CommissionType myType;
//上个页面传进来的 section显示标题数组
@property (nonatomic, strong) NSArray* sectionLabelNames;
//表格底部显示的说明
@property (nonatomic, strong) NSString* bottomStr;
//上个页面传过来的年月
@property (nonatomic, strong) NSString* date;

//总费用小计展示
@property (nonatomic, strong) NSString* total;

@end
