//
//  AllCalledPhoneController.m
//  华康通
//
//  Created by leiyu on 16/10/24.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import "AllCalledPhoneController.h"
#import "AllCalledPhoneCell.h"
#import "CallCenter.h"

@interface AllCalledPhoneController ()

@property (nonatomic, strong) NSArray* indexArray;

@property (nonatomic, strong) NSMutableArray* suoyinCityList;

@end

@implementation AllCalledPhoneController

- (NSMutableArray*)suoyinCityList
{
    if (!_suoyinCityList)
    {
        _suoyinCityList = [NSMutableArray array];
    }
    return _suoyinCityList;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"保险理赔热线";
    self.tableView.sectionIndexColor = KMainColor;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:LoadImage(@"back1") style:UIBarButtonItemStylePlain target:self action:@selector(popRootVC)];
    [self.tableView registerNib:[UINib nibWithNibName:@"AllCalledPhoneCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    [self updateDataForTableView];
    for(char c ='A';c<='Z';c++)
    {
        [self.suoyinCityList addObject:[NSString stringWithFormat:@"%c",c]];
    }
    self.indexArray = [self.suoyinCityList copy];
    [self.suoyinCityList insertObject:@"0" atIndex:0];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"AllCalledPhoneController"];//("PageOne"为页面名称，可自定义)
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"AllCalledPhoneController"];
}

- (void)updateDataForTableView
{
    
}

- (void)popRootVC
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.suoyinCityList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[CallCenter allPhone][self.suoyinCityList[section]] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section != 0)
    {
        if ([[CallCenter allPhone][self.suoyinCityList[section]] count] == 0)
        {
            return 0.0;
        }
        else
        {
            return 30.0;
        }
    }
    else
        return 0.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headView = [[UIView alloc]init];
    if (section!=0)
    {
        headView.frame = CGRectMake(0, 0, self.view.bounds.size.width, 30);
        headView.backgroundColor = SHOWCOLOR(@"F3F3F3");
        //标题文字
        UILabel *lblBiaoti = [[UILabel alloc]init];
        lblBiaoti.backgroundColor = ClEARColor;
        lblBiaoti.textAlignment = NSTextAlignmentLeft;
        lblBiaoti.font = [UIFont systemFontOfSize:15];
        lblBiaoti.textColor = YYColor;
        lblBiaoti.frame = CGRectMake(18, 7.5, 200, 15);
        lblBiaoti.text = [self.suoyinCityList objectAtIndex:section];
        [headView addSubview:lblBiaoti];
    }
    else
    {
        headView.frame = CGRectZero;
    }
    return headView;
}

//增加索引
-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return self.indexArray;
}

//索引列点击事件
-(NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    NSLog(@"===%@  ===%ld",title,(long)index);
    if ([[CallCenter allPhone][self.suoyinCityList[index+1]] count] == 0)
    {
        NSLog(@"........");
        //点击一个空索引 应该不变化
    }
    else
    {
        //点击索引，列表跳转到对应索引的行
        [tableView
         scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:index + 1]
         atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    return index + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AllCalledPhoneCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.leftImageView.image = LoadImage([CallCenter allPhone][self.suoyinCityList[indexPath.section]][indexPath.row][@"icon"]);
    cell.topLabel.text = [CallCenter allPhone][self.suoyinCityList[indexPath.section]][indexPath.row][@"name"];
    cell.bottomLabel.text = [CallCenter allPhone][self.suoyinCityList[indexPath.section]][indexPath.row][@"phone"];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 54.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    AllCalledPhoneCell *cell = (AllCalledPhoneCell*)[tableView cellForRowAtIndexPath:indexPath];
    NSMutableString* callStr=[[NSMutableString alloc]initWithFormat:@"tel://%@",cell.bottomLabel.text];
    UIWebView* callWebView=[[UIWebView alloc]init];
    [callWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:callStr]]];
    [self.view addSubview:callWebView];
}

@end
