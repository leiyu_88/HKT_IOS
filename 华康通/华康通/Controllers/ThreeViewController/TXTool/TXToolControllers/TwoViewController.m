//
//  TwoViewController.m
//  华康通
//
//  Created by  雷雨 on 2017/11/29.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import "TwoViewController.h"
#import "SeveralViewController.h"
#import "TXJHView.h"

@interface TwoViewController ()<UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *bottomButton;
@property (nonatomic, strong) TXJHView* textView;

@end


@implementation TwoViewController

- (TXJHView*)textView
{
    if (!_textView)
    {
        _textView = [TXJHView view];
        _textView.frame = CGRectMake(0, 0, UISCREENWEITH, 800);
        //直接显示男女退休的年龄
        UITextField* textField1 = (UITextField*)_textView.manTextFields[0];
        textField1.text = [self getRetireAgeWithArr:self.manXXDic];
        UITextField* textField2 = (UITextField*)_textView.womenTextField[0];
        textField2.text = [self getRetireAgeWithArr:self.womenXXDic];
        //直接显示您希望为自己准备多少年的退休金？(年)
        UITextField* textField3 = (UITextField*)_textView.manTextFields[1];
        textField3.text = [self getPensionOfHowManyYearsWithDic:self.manXXDic];
        UITextField* textField4 = (UITextField*)_textView.womenTextField[1];
        textField4.text = [self getPensionOfHowManyYearsWithDic:self.womenXXDic];
    }
    return _textView;
}

//根据上个页面传来的值算出多少年的退休金？(年)
- (NSString*)getPensionOfHowManyYearsWithDic:(NSDictionary*)dic
{
    NSString* str1 = dic[@"age"];
    NSString* str2 = dic[@"retire"];
    NSString* str3 = dic[@"life"];
    NSInteger age = [str3 integerValue] - [str1 integerValue] - [str2 integerValue];
    return [NSString stringWithFormat:@"%ld",(long)age];
}

//根据上个页面传过来的 字典，算出退休年龄
- (NSString*)getRetireAgeWithArr:(NSDictionary*)dic
{
    NSString* str1 = dic[@"age"];
    NSString* str2 = dic[@"retire"];
    NSInteger age = [str1 integerValue] + [str2 integerValue];
    return [NSString stringWithFormat:@"%ld",(long)age];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"退休计划";
//    NSLog(@"男人基本信息：%@\n女人基本信息：%@\n宏观经济假设数据：%@\n",self.manXXDic,self.womenXXDic,self.otherXXDic);
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:LoadImage(@"back1") style:UIBarButtonItemStylePlain target:self action:@selector(popRootVC)];
    self.scrollView.contentSize = self.textView.size;
    [self.scrollView addSubview:self.textView];
    
}

- (void)popRootVC
{
    [self.navigationController popViewControllerAnimated:YES];
}

//下一步
- (IBAction)next:(id)sender
{
    if ([JudgeValueOfTextField isHaveValueForTextFieldWithArray:self.textView.manTextFields] && [JudgeValueOfTextField isHaveValueForTextFieldWithArray:self.textView.womenTextField])
    {
        SeveralViewController* vc = [[SeveralViewController alloc]initWithNibName:@"SeveralViewController" bundle:nil];
        vc.manXXDic = self.manXXDic;
        vc.womenXXDic = self.womenXXDic;
        vc.otherXXDic = self.otherXXDic;
        vc.manTXJHDic = [self getTextFieldForWithArr:self.textView.manTextFields];
        vc.womenTXJHDic = [self getTextFieldForWithArr:self.textView.womenTextField];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else
    {
        [JudgeValueOfTextField judgeIsHaveValueWithMessage:@"您的输入信息不完整，请检查后补充完整哦！么么哒！"];
    }
}

//拿到男女退休计划的相关数据
/*
 * 您希望几岁退休：retireYear
 * 您希望为自己准备多少年的退休金：retireMoney
 * 每月需要多少个人生活费：livingExpenses
 * 每月多少家庭固定支出：householdExpenditure
 * 每年预留多少医疗保健费用：healthMoney
 * 每年多少旅游或爱好支出：tourismExpenditure
 * 多少现金遗产：heritage
 *
 */
- (NSDictionary*)getTextFieldForWithArr:(NSArray*)arr
{
    NSMutableArray* newArr = [NSMutableArray array];
    for (UITextField* textField in arr)
    {
        [newArr addObject:textField.text];
    }
    NSDictionary * dic = @{@"retireYear":newArr[0],
                           @"retireMoney":newArr[1],
                           @"livingExpenses":newArr[2],
                           @"householdExpenditure":newArr[3],
                           @"healthMoney":newArr[4],
                           @"tourismExpenditure":newArr[5],
                           @"heritage":newArr[6]
                           };
    return dic;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
}


@end
