//
//  SeveralViewController.m
//  华康通
//
//  Created by  雷雨 on 2017/11/29.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import "SeveralViewController.h"
#import "TXZYView.h"
#import "SeniorViewController.h"

@interface SeveralViewController ()<UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *bottomButton;
@property (nonatomic, strong) TXZYView* textView;

@end

@implementation SeveralViewController

- (TXZYView*)textView
{
    if (!_textView)
    {
        _textView = [TXZYView view];
        _textView.frame = CGRectMake(0, 0, UISCREENWEITH, 700);
        //直接显示男女首次购买社保的年份
        UITextField* textField1 = (UITextField*)_textView.manTextFields[1];
        textField1.text = [self getPaymentYearWithArr:self.manXXDic];
        UITextField* textField2 = (UITextField*)_textView.womenTextFields[1];
        textField2.text = [self getPaymentYearWithArr:self.womenXXDic];
    }
    return _textView;
}

//根据上个页面传过来的 字典，算出首次购买社保的年份
- (NSString*)getPaymentYearWithArr:(NSDictionary*)dic
{
    //拿到当前年份
    NSDate* date = [[NSDate alloc]init];
    NSDateFormatter* formatter = [[NSDateFormatter alloc]init];
    formatter.dateFormat = @"YYYY";
    NSString* nowDate = [formatter stringFromDate:date];
    NSString* str1 = dic[@"paymentYear"];
    NSInteger year = [nowDate integerValue] - [str1 integerValue];
    return [NSString stringWithFormat:@"%ld",(long)year];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"退休资源";
//    NSLog(@"男人基本信息：%@\n女人基本信息：%@\n宏观经济假设数据：%@\n退休计划男士数据：%@\n退休计划女女士数据：%@\n",self.manXXDic,self.womenXXDic,self.otherXXDic,self.manTXJHDic,self.womenTXJHDic);
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:LoadImage(@"back1") style:UIBarButtonItemStylePlain target:self action:@selector(popRootVC)];
    self.scrollView.contentSize = self.textView.size;
    [self.scrollView addSubview:self.textView];
}

- (void)popRootVC
{
    [self.navigationController popViewControllerAnimated:YES];
}

//测算出结果
- (IBAction)next:(id)sender
{
    if ([JudgeValueOfTextField isHaveValueForTextFieldWithArray:self.textView.manTextFields] && [JudgeValueOfTextField isHaveValueForTextFieldWithArray:self.textView.womenTextFields])
    {
        SeniorViewController* vc = [[SeniorViewController alloc]initWithNibName:@"SeniorViewController" bundle:nil];
        vc.manXXDic = self.manXXDic;
        vc.womenXXDic = self.womenXXDic;
        vc.otherXXDic = self.otherXXDic;
        vc.manTXJHDic = self.manTXJHDic;
        vc.womenTXJHDic = self.womenTXJHDic;
        vc.manTXZYDic = [self getTextFieldForWithArr:self.textView.manTextFields];
        vc.womenTXZYDic = [self getTextFieldForWithArr:self.textView.womenTextFields];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else
    {
        [JudgeValueOfTextField judgeIsHaveValueWithMessage:@"您的输入信息不完整，请检查后补充完整哦！么么哒！"];
    }
}

//男女退休资源的相关数据
/*
 * 您是否参加社保：isSocialSecurity
 * 社保是哪一年开始缴纳社保的：whichYear
 * 再打算缴费几年：payAFewYears
 * 社保个人账户余额：accountBalance
 *
 */
- (NSDictionary*)getTextFieldForWithArr:(NSArray*)arr
{
    NSMutableArray* newArr = [NSMutableArray array];
    for (UITextField* textField in arr)
    {
        [newArr addObject:textField.text];
    }
    NSDictionary * dic = @{@"isSocialSecurity":[self returnIntergerWithIsSocialSecurity:newArr[0]],
                           @"whichYear":newArr[1],
                           @"payAFewYears":newArr[2],
                           @"accountBalance":newArr[3],
                           };
    return dic;
}

//是否参加社保把汉字转化为bool数字
- (NSString*)returnIntergerWithIsSocialSecurity:(NSString*)str
{
    return [str isEqualToString:@"是"] ? @"1" : @"0";
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
}

@end
