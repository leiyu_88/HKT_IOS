
#import "OneViewController.h"
#import "TextFieldView.h"
#import "TwoViewController.h"

@interface OneViewController ()<UIScrollViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) TextFieldView* textView;
@property (weak, nonatomic) IBOutlet UIButton *bottomButton;

//
@property (nonatomic, strong) NSString* t1;
@property (nonatomic, strong) NSString* t2;
@property (nonatomic, strong) NSString* t3;
@property (nonatomic, strong) NSString* t4;
@property (nonatomic, strong) NSString* t11;
@property (nonatomic, strong) NSString* t22;

@property (nonatomic, strong) UIPickerView* pickerView1;
@property (nonatomic, strong) UIPickerView* pickerView2;

@end

@implementation OneViewController

//用pickerView自定义一个选择年份的选择器
- (UIPickerView*)pickerView1
{
    if (!_pickerView1)
    {
        _pickerView1 = [[UIPickerView alloc]init];
        _pickerView1.frame = CGRectMake(0, self.view.frame.size.height - 200, UISCREENWEITH, 200);
        _pickerView1.dataSource = self;
        _pickerView1.delegate = self;
    }
    return _pickerView1;
}

//用pickerView自定义一个选择年份的选择器
- (UIPickerView*)pickerView2
{
    if (!_pickerView2)
    {
        _pickerView2 = [[UIPickerView alloc]init];
        _pickerView2.frame = CGRectMake(0, self.view.frame.size.height - 200, UISCREENWEITH, 200);
        _pickerView2.dataSource = self;
        _pickerView2.delegate = self;
    }
    return _pickerView2;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [[self getYears] count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString* str = [self getYears][row];
    return str;
}
//输入框显示年份
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (pickerView == self.pickerView1)
    {
        UITextField* textField = (UITextField*)_textView.mainTextFields[5];
        textField.text = [self getYears][row];
        [self.pickerView2 removeSubviews];
    }
    else
    {
        UITextField* textField = (UITextField*)_textView.womenTextFields[5];
        textField.text = [self getYears][row];
        [self.pickerView1 removeSubviews];
    }
}

//从今年到以后的100年创建一个年份数组
- (NSArray*)getYears
{
    NSMutableArray* arr = [NSMutableArray array];
    for (NSInteger i = [[self getCurrectDate] integerValue]; i < [[self getCurrectDate] integerValue] + 101; i++)
    {
        [arr addObject:[NSString stringWithFormat:@"%ld",(long)i]];
    }
    return [arr copy];
}


//获取当前年份
- (NSString*)getCurrectDate
{
    //开始规划年份默认为今年
    NSDate* date = [[NSDate alloc]init];
    NSDateFormatter* formatter = [[NSDateFormatter alloc]init];
    formatter.dateFormat = @"YYYY";
    NSString* nowDate = [formatter stringFromDate:date];
    return nowDate;
}

- (TextFieldView*)textView
{
    if (!_textView)
    {
        _textView = [TextFieldView view];
        _textView.frame = CGRectMake(0, 0, UISCREENWEITH, 1050);

        UITextField* textField1 = (UITextField*)_textView.mainTextFields[5];
        textField1.inputView = self.pickerView1;
        //默认填写当前年份
        textField1.text = [self getCurrectDate];
        self.t11 = textField1.text;
        [textField1 addTarget:self action:@selector(endEditing1:) forControlEvents:UIControlEventEditingDidEnd];
        
        UITextField* t1 = (UITextField*)_textView.mainTextFields[2];
        [t1 addTarget:self action:@selector(endEditing1:) forControlEvents:UIControlEventEditingDidEnd];
        UITextField* t2 = (UITextField*)_textView.mainTextFields[3];
        [t2 addTarget:self action:@selector(endEditing1:) forControlEvents:UIControlEventEditingDidEnd];
        
        UITextField* textField2 = (UITextField*)_textView.womenTextFields[5];
        textField2.inputView = self.pickerView2;
        //默认填写当前年份
        textField2.text = [self getCurrectDate];
        self.t22 = textField2.text;
        [textField2 addTarget:self action:@selector(endEditing2:) forControlEvents:UIControlEventEditingDidEnd];
        
        UITextField* t3 = (UITextField*)_textView.womenTextFields[2];
        [t3 addTarget:self action:@selector(endEditing2:) forControlEvents:UIControlEventEditingDidEnd];
        UITextField* t4 = (UITextField*)_textView.womenTextFields[3];
        [t4 addTarget:self action:@selector(endEditing2:) forControlEvents:UIControlEventEditingDidEnd];
        
    }
    return _textView;
}

//男性基本资料输入年龄、预期寿命结束编辑的时候
- (void)endEditing1:(UITextField *)sender
{
    if (![sender.text isEqualToString:@""] && sender.text)
    {
        if (sender.tag == 2)
        {
            self.t1 = sender.text;
        }
        if (sender.tag == 3)
        {
            self.t2 = sender.text;
        }
        if (sender.tag == 5)
        {
            self.t11 = sender.text;
        }
    }
    if (self.t1 && self.t2 && ![self.t1 isEqualToString:@""] && ![self.t2 isEqualToString:@""] && ![self.t11 isEqualToString:@""] && self.t11)
    {
        NSInteger i = [self.t2 integerValue] - [self.t1 integerValue];
        if (i > 0)
        {
            UITextField* textField = (UITextField*)_textView.mainTextFields[6];
            NSInteger date = [self.t11 integerValue] + i;
            textField.text = [NSString stringWithFormat:@"%ld",(long)date];
        }
    }
}

//女性基本资料输入年龄、预期寿命结束编辑的时候
- (void)endEditing2:(UITextField *)sender
{
    if (![sender.text isEqualToString:@""] && sender.text)
    {
        if (sender.tag == 2)
        {
            self.t3 = sender.text;
        }
        if (sender.tag == 3)
        {
            self.t4 = sender.text;
        }
        if (sender.tag == 5)
        {
            self.t22 = sender.text;
        }
    }
    if (self.t3 && self.t4 && ![self.t3 isEqualToString:@""] && ![self.t4 isEqualToString:@""] && ![self.t22 isEqualToString:@""] && self.t22)
    {
        NSInteger i = [self.t4 integerValue] - [self.t3 integerValue];
        if (i > 0)
        {
            UITextField* textField = (UITextField*)_textView.womenTextFields[6];
            NSInteger date = [self.t22 integerValue] + i;
            textField.text = [NSString stringWithFormat:@"%ld",(long)date];
        }
    }
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:LoadImage(@"back1") style:UIBarButtonItemStylePlain target:self action:@selector(popRootVC)];
    self.scrollView.contentSize = self.textView.size;
    [self.scrollView addSubview:self.textView];
}

- (void)popRootVC
{
    [self.navigationController popViewControllerAnimated:YES];
}

//跳转到下一页
- (IBAction)next:(id)sender
{
    if ([JudgeValueOfTextField isHaveValueForTextFieldWithArray:self.textView.mainTextFields] && [JudgeValueOfTextField isHaveValueForTextFieldWithArray:self.textView.womenTextFields] &&[JudgeValueOfTextField isHaveValueForTextFieldWithArray:self.textView.otherTextFields])
    {
        TwoViewController* vc = [[TwoViewController alloc]initWithNibName:@"TwoViewController" bundle:nil];
        //拿到相关输入框的值，健值对的形式传入下个页面
        vc.manXXDic = [self getTextFieldForJBXXWithArr:self.textView.mainTextFields];
        vc.womenXXDic = [self getTextFieldForJBXXWithArr:self.textView.womenTextFields];
        vc.otherXXDic = [self getTextFieldForSJJSWithArr:self.textView.otherTextFields];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else
    {
        [JudgeValueOfTextField judgeIsHaveValueWithMessage:@"您的输入信息不完整，请检查后补充完整哦！么么哒！"];
    }
}

//拿到男女基本信息的相关数据
/*
 * 计税工资：wages
 * 社保缴费年数：paymentYear
 * 年龄：age
 * 预期寿命：life
 * 几年退休：retire
 * 开始规划年份：startPlanningYear
 * 结束规划年份：endPlanningYear
 *
 */
- (NSDictionary*)getTextFieldForJBXXWithArr:(NSArray*)arr
{
    NSMutableArray* newArr = [NSMutableArray array];
    for (UITextField* textField in arr)
    {
        [newArr addObject:textField.text];
    }
    NSDictionary * dic = @{@"wages":newArr[0],
                           @"paymentYear":newArr[1],
                           @"age":newArr[2],
                           @"life":newArr[3],
                           @"retire":newArr[4],
                           @"startPlanningYear":newArr[5],
                           @"endPlanningYear":newArr[6]
                           };
    return dic;
}

//拿到宏观经济数据假设数据
/*
 * 社会平均工资：averageWage
 * 通货膨胀率：inflationRate
 * 社会平均工资增长率：averageWageRate
 * 保守投资回报率：investmentRate
 * 长期银行存款利率：bankInterestRate
 * 风险测评：riskAssessment
 * 工作工资增长率：wageGrowthRate
 *
 */
- (NSDictionary*)getTextFieldForSJJSWithArr:(NSArray*)arr
{
    NSMutableArray* newArr = [NSMutableArray array];
    for (UITextField* textField in arr)
    {
        [newArr addObject:textField.text];
    }
    NSDictionary * dic = @{@"averageWage":newArr[0],
                           @"inflationRate":newArr[1],
                           @"averageWageRate":newArr[2],
                           @"investmentRate":newArr[3],
                           @"bankInterestRate":newArr[4],
                           @"riskAssessment":newArr[5],
                           @"wageGrowthRate":newArr[6]
                           };
    return dic;
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
}

@end
