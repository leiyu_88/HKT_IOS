//
//  FifthViewController.m
//  华康通
//
//  Created by  雷雨 on 2017/11/29.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import "FifthViewController.h"
#import "TXNJMBView.h"

@interface FifthViewController ()<UIScrollViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *bottomButton;

@property (nonatomic, strong) TXNJMBView* textView;
@property (nonatomic, strong) UIPickerView* pickerView;

@end

@implementation FifthViewController

//用pickerView自定义一个选择年份的选择器
- (UIPickerView*)pickerView
{
    if (!_pickerView)
    {
        _pickerView = [[UIPickerView alloc]init];
        _pickerView.frame = CGRectMake(0, self.view.frame.size.height - 200, UISCREENWEITH, 200);
        _pickerView.dataSource = self;
        _pickerView.delegate = self;
    }
    return _pickerView;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [[self getYears] count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString* str = [self getYears][row];
    return str;
}

//输入框显示年份
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{

    UITextField* textField = self.textView.dvTopTextField;
    textField.text = [self getYears][row];
}

//得到多少年后退休其中最小的值
- (NSInteger)getRetirementYear
{
    NSInteger i1 = [self.manXXDic[@"retire"] integerValue];
    NSInteger i2 = [self.womenXXDic[@"retire"] integerValue];
    if (i1 == i2)
    {
        return i1;
    }
    else if (i1 > i2)
    {
        return i2;
    }
    else
    {
        return i1;
    }
}

//从今年到退休的年数组
- (NSArray*)getYears
{
    NSMutableArray* arr = [NSMutableArray array];
    for (NSInteger i = 1; i <= [self getRetirementYear]; i++)
    {
        [arr addObject:[NSString stringWithFormat:@"%ld",(long)i]];
    }
    return [arr copy];
}

- (TXNJMBView*)textView
{
    if (!_textView)
    {
        _textView = [TXNJMBView view];
        _textView.frame = CGRectMake(0, 0, UISCREENWEITH, 485);
        _textView.topView.layer.cornerRadius = 8.0;
        _textView.topView.layer.masksToBounds = YES;
        _textView.bottomView.layer.cornerRadius = 8.0;
        _textView.bottomView.layer.masksToBounds = YES;
        _textView.centerView.layer.cornerRadius = 8.0;
        _textView.centerView.layer.masksToBounds = YES;
        _textView.dbView.layer.cornerRadius = 8.0;
        _textView.dbView.layer.masksToBounds = YES;
        _textView.dvTopTextField.inputView = self.pickerView;
        _textView.centerTopLabel.text = self.annuity;
        [_textView.centerCenterTextField addTarget:self action:@selector(changeAnnuity:) forControlEvents:UIControlEventEditingDidEnd];
        [_textView.dvTopTextField addTarget:self action:@selector(changeInsurance:) forControlEvents:UIControlEventEditingDidEnd];
    }
    return _textView;
}

//输入框年（金领取年数）输入结束的时候调用
- (void)changeAnnuity:(UITextField*)textField
{
    if (![textField.text isEqualToString:@""] && textField.text)
    {
        self.textView.centerBottomLabel.text = [self getAnnuityForYearWithYear:textField.text];
    }
    //展示预计年交金额
    [self getInsuranceWithTextField:self.textView.dvTopTextField];
}

//输入框年（预计缴费年数）输入结束的时候调用
- (void)changeInsurance:(UITextField*)textField
{
    [self getInsuranceWithTextField:textField];
}

//得到每年的预计年交金额
- (void)getInsuranceWithTextField:(UITextField*)textField
{
//    if (self.textView.centerBottomLabel.text && ![self.textView.centerBottomLabel.text isEqualToString:@""])
//    {
        NSInteger rate = [self.otherXXDic[@"investmentRate"] integerValue];
        if (![textField.text isEqualToString:@""] && textField.text)
        {
            NSInteger y = [textField.text integerValue];
            //PV算值
            CGFloat f = pow((1 + (rate/100.0)), y);
            CGFloat f1 = [self.annuity doubleValue];
            CGFloat d = f1/f;
            //PMT
            CGFloat s = [LYComputational getAnnuityForYearWithCapital:d withRate:rate withYears:y];
            self.textView.dvBottomLabel.text = [NSString stringWithFormat:@"%.2lf",s];
        }
//    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"退休年金保险测算";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:LoadImage(@"back1") style:UIBarButtonItemStylePlain target:self action:@selector(popRootVC)];
    self.textView.dvTopTextField.text = [NSString stringWithFormat:@"%ld",(long)[self getRetirementYear]];
    //展示预计年交金额
    [self getInsuranceWithTextField:self.textView.dvTopTextField];
    self.scrollView.contentSize = self.textView.size;
    [self.scrollView addSubview:self.textView];
}

- (void)popRootVC
{
    [self.navigationController popViewControllerAnimated:YES];
}

//已知年金，和总金额，算得每年领取的年金
- (NSString*)getAnnuityForYearWithYear:(NSString*)yearStr
{
    NSInteger year = [yearStr integerValue];
    CGFloat capital = [self.annuity doubleValue];
    NSInteger rate = [self.otherXXDic[@"investmentRate"] integerValue];
    CGFloat f = [LYComputational getAnnuityForYearWithCapital:capital withRate:rate withYears:year];
    return [NSString stringWithFormat:@"%.2lf",f];
}

//回到首页
- (IBAction)goBackFVC:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
}

@end
