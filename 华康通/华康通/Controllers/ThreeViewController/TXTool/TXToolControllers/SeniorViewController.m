//
//  SeniorViewController.m
//  华康通
//
//  Created by  雷雨 on 2017/11/29.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import "SeniorViewController.h"
#import "TXCSCell.h"
#import "QKCSTopView.h"
#import "FifthViewController.h"

@interface SeniorViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *bottomButton;
@property (nonatomic, strong) QKCSTopView* textView;

@end

@implementation SeniorViewController

- (QKCSTopView*)textView
{
    if (!_textView)
    {
        _textView = [QKCSTopView view];
        _textView.frame = CGRectMake(0, 0, UISCREENWEITH, 50);
    }
    return _textView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"每年退休缺口测算结果";
//    NSLog(@"男人基本信息：%@\n女人基本信息：%@\n宏观经济假设数据：%@\n退休计划男士数据：%@\n退休计划女女士数据：%@\n",self.manXXDic,self.womenXXDic,self.otherXXDic,self.manTXJHDic,self.womenTXJHDic);
//    NSLog(@"退休资源男士数据：%@\n退休资源女士数据：%@\n",self.manTXZYDic,self.womenTXZYDic);
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:LoadImage(@"back1") style:UIBarButtonItemStylePlain target:self action:@selector(popRootVC)];
    [self.tableView registerNib:[UINib nibWithNibName:@"TXCSCell" bundle:nil] forCellReuseIdentifier:@"TXCSCell"];
}

- (void)popRootVC
{
    [self.navigationController popViewControllerAnimated:YES];
}
//得到多少年后退休其中最小的值
- (NSInteger)getRetirementYear
{
    NSInteger i1 = [self.manXXDic[@"retire"] integerValue];
    NSInteger i2 = [self.womenXXDic[@"retire"] integerValue];
    if (i1 == i2)
    {
        return i1;
    }
    else if (i1 > i2)
    {
        return i2;
    }
    else
    {
        return i1;
    }
}
//得到离预期死亡最长的值
- (NSInteger)getDifferenceAge
{
    NSString* str1 = self.manXXDic[@"age"];
    NSString* str2 = self.manXXDic[@"life"];
    NSInteger i1 = [str2 integerValue] - [str1 integerValue];
    str1 = self.womenXXDic[@"age"];
    str2 = self.womenXXDic[@"life"];
    NSInteger i2 = [str2 integerValue] - [str1 integerValue];
    if (i1 == i2)
    {
        return i1;
    }
    else if (i1 > i2)
    {
        return i1;
    }
    else
    {
        return i2;
    }
}

//得到最大退休年龄
- (NSInteger)getRetirementAge
{
    NSString* str1 = self.manXXDic[@"age"];
    NSString* str2 = self.manXXDic[@"retire"];
    NSInteger i1 = [str2 integerValue] + [str1 integerValue];
    str1 = self.womenXXDic[@"age"];
    str2 = self.womenXXDic[@"retire"];
    NSInteger i2 = [str2 integerValue] + [str1 integerValue];
    if (i1 == i2)
    {
        return i1;
    }
    else if (i1 > i2)
    {
        return i1;
    }
    else
    {
        return i2;
    }
}

//替换数组变成适合整个列表的数组
- (NSArray*)changeArrWithArr:(NSArray*)arr
{
    NSMutableArray* newArr = [NSMutableArray array];
    NSArray* a = [RetirementCalculation returnYearswithDifferenceAge:[self getDifferenceAge] withRetirementYear:[self getRetirementYear]];
    NSInteger count = a.count;
    for (NSInteger i = 0; i < count; i++)
    {
        [newArr addObject:@""];
    }
    for (NSInteger i = 0; i < arr.count; i++)
    {
        [newArr replaceObjectAtIndex:i withObject:arr[i]];
    }
    return [newArr copy];
}

//下一步
- (IBAction)next:(id)sender
{
    FifthViewController* vc = [[FifthViewController alloc]initWithNibName:@"FifthViewController" bundle:nil];
    vc.manXXDic = self.manXXDic;
    vc.womenXXDic = self.womenXXDic;
    vc.otherXXDic = self.otherXXDic;
    vc.manTXJHDic = self.manTXJHDic;
    vc.womenTXJHDic = self.womenTXJHDic;
    vc.manTXZYDic = self.manTXZYDic;
    vc.womenTXZYDic = self.womenTXZYDic;
    //总年金
    CGFloat f = [LYComputational getNPVWithRate:[self.otherXXDic[@"investmentRate"] integerValue] withArr:[self getAnnuitys]];
    vc.annuity = [NSString stringWithFormat:@"%.2lf",f];
    NSLog(@"总年金 = %@",vc.annuity);
    [self.navigationController pushViewController:vc animated:YES];
}

- (NSArray*)getTotalExpenditure
{
    //得到家庭总支出和
    CGFloat manExpenditure = ([self.manTXJHDic[@"livingExpenses"] doubleValue] * 12) + [self.manTXJHDic[@"healthMoney"] doubleValue] + [self.manTXJHDic[@"tourismExpenditure"] doubleValue];
    CGFloat womenExpenditure = ([self.womenTXJHDic[@"livingExpenses"] doubleValue] * 12) + [self.womenTXJHDic[@"healthMoney"] doubleValue] + [self.womenTXJHDic[@"tourismExpenditure"] doubleValue];
    CGFloat manJTZC = [self.manTXJHDic[@"householdExpenditure"] doubleValue] * 12;
    CGFloat womenJTZC = [self.womenTXJHDic[@"householdExpenditure"] doubleValue] * 12;
    NSInteger rate = [self.otherXXDic[@"inflationRate"] integerValue];
    NSInteger m = [self.manXXDic[@"life"] integerValue] - [self.manXXDic[@"age"] integerValue];
    NSInteger w = [self.womenXXDic[@"life"] integerValue] - [self.womenXXDic[@"age"] integerValue];
    NSInteger tx = [self getDifferenceAge];
    NSInteger year = tx;
    NSArray* arr = [LYComputational getTotalExpenditureWithManExpenditure:manExpenditure withWomenExpenditure:womenExpenditure withYears:year withManJTZC:manJTZC withWomenJTZC:womenJTZC withRate:rate withManExpectedYear:m withWomenExpectedYear:w];
    return arr;
}

- (void)getManAndWomenSocialSecurityBalance
{
    
}
//得到每年年金缺口数组
- (NSArray*)getAnnuitys
{
    NSMutableArray* arr = [NSMutableArray array];
    NSArray* arr1 = [self getTotalExpenditure];
    NSArray* arr2 = [self getFamilyGeneralIncome];
    NSInteger a = [self getRetirementYear];
    for (NSInteger i = 0; i < [[RetirementCalculation returnYearswithDifferenceAge:[self getDifferenceAge] withRetirementYear:[self getRetirementYear]] count]; i++)
    {
        CGFloat f = [arr1[i + a] doubleValue] - [arr2[i] doubleValue];
        //取绝对值
        [arr addObject:[NSString stringWithFormat:@"%.2lf",fabs(f)]];
    }
    return [arr copy];
}
//总的年金缺口 所有行缺口求和
- (NSString*)getTotalAnnuity
{
    NSArray* arr = [self getAnnuitys];
    NSNumber *sum = [arr valueForKeyPath:@"@sum.floatValue"];
    return [NSString stringWithFormat:@"%@",sum];
}

//得到家庭总收入年数组
- (NSArray*)getFamilyGeneralIncome
{
    //拿到男士每年的社保账户余额
    CGFloat manS = [LYComputational getSocialSecurityBalanceWithBalance:[self.manTXZYDic[@"accountBalance"] doubleValue] withBankRate:[self.otherXXDic[@"bankInterestRate"] integerValue] withIndex:[self.manXXDic[@"retire"] integerValue] + 2];
    NSLog(@"manS = %.2lf",manS);
    //拿到女士每年的社保账户余额
    CGFloat womenS = [LYComputational getSocialSecurityBalanceWithBalance:[self.womenTXZYDic[@"accountBalance"] doubleValue] withBankRate:[self.otherXXDic[@"bankInterestRate"] integerValue] withIndex:[self.womenXXDic[@"retire"] integerValue] + 2];
    NSLog(@"womenS = %.2lf",womenS);
    //拿到男士个人累计账户余额
    CGFloat manL = [LYComputational getSavingsWithWage:[self.manXXDic[@"wages"] doubleValue] withRate:[self.otherXXDic[@"wageGrowthRate"] integerValue] withYears:([self.manXXDic[@"retire"] integerValue] + 1) withBankRate:[self.otherXXDic[@"bankInterestRate"] integerValue]];
    NSLog(@"manL = %.2f",manL);
    //拿到女士个人累计账户余额
    CGFloat womenL = [LYComputational getSavingsWithWage:[self.womenXXDic[@"wages"] doubleValue] withRate:[self.otherXXDic[@"wageGrowthRate"] integerValue] withYears:([self.womenXXDic[@"retire"] integerValue] + 1) withBankRate:[self.otherXXDic[@"bankInterestRate"] integerValue]];
    NSLog(@"womenL = %.2lf",womenL);
    //拿到展示数据的最大行年份
    NSInteger x = [self getDifferenceAge];
    //拿到最初展示数据的第一行年份
    NSInteger y = [self getRetirementYear];
    //拿到男士死亡时的最大行年份
    NSInteger x1 = [self.manXXDic[@"life"] integerValue] - [self.manXXDic[@"age"] integerValue];
    //拿到女士死亡时的最大年龄
    NSInteger y1 = [self.womenXXDic[@"life"] integerValue] - [self.womenXXDic[@"age"] integerValue];
    NSMutableArray * arr = [NSMutableArray array];
    for (NSInteger i = y; i <= x ; i++)
    {
        //拿到展示列每年的上一年度社会化平均工资
        CGFloat txWages = [LYComputational getPeopleMonthlyWageWithWage:[self.otherXXDic[@"averageWage"] doubleValue] withInflationRate:[self.otherXXDic[@"averageWageRate"] integerValue] withIndex:i + 1];
//        NSLog(@"拿到展示列每年的上一年度社会化平均工资 = %.2f",txWages);
        //拿到展示列每年的上一年度本人(男士)工资
        CGFloat brManWages = [LYComputational getPeopleMonthlyWageWithWage:[self.manXXDic[@"wages"] doubleValue] withInflationRate:[self.otherXXDic[@"wageGrowthRate"] integerValue] withIndex:i + 1];
//        NSLog(@"拿到展示列每年的上一年度本人(男士)工资 = %.2f",brManWages);
        //拿到展示列每年的上一年度本人(女士)工资
        CGFloat brWomenWages = [LYComputational getPeopleMonthlyWageWithWage:[self.womenXXDic[@"wages"] doubleValue] withInflationRate:[self.otherXXDic[@"wageGrowthRate"] integerValue] withIndex:i + 1];
        //拿到指数区间(男士)
        CGFloat manR = [LYComputational getRateWithPeopleMonthlyWage:brManWages withAveragePayWage:txWages];
        //拿到指数区间(女士)
        CGFloat womenR = [LYComputational getRateWithPeopleMonthlyWage:brWomenWages withAveragePayWage:txWages];
        
//        //拿到(男士)每年统筹账户余额
//        CGFloat manT = [LYComputational getPensionWithPeopleMonthlyWage:txWages withExponentialRange:manR withPaymentYears:[self.manXXDic[@"paymentYear"] integerValue] withRetirementAge:[self.manXXDic[@"age"] integerValue] + [self.manXXDic[@"retire"] integerValue] withAccountBalance:manS + manL];
//        //拿到(女士)每年统筹账户余额
//        CGFloat womenT = [LYComputational getPensionWithPeopleMonthlyWage:txWages withExponentialRange:womenR withPaymentYears:[self.womenXXDic[@"paymentYear"] integerValue] withRetirementAge:[self.womenXXDic[@"age"] integerValue] + [self.womenXXDic[@"retire"] integerValue] withAccountBalance:womenS + womenL];
        
        //拿到(男士)每年统筹账户余额
        CGFloat manT = [LYComputational getPensionWithPeopleMonthlyWage:txWages withExponentialRange:manR withPaymentYears:[self.manXXDic[@"paymentYear"] integerValue] withRetirementAge:[self getRetirementAge] withAccountBalance:manS + manL];
        //拿到(女士)每年统筹账户余额
        CGFloat womenT = [LYComputational getPensionWithPeopleMonthlyWage:txWages withExponentialRange:womenR withPaymentYears:[self.womenXXDic[@"paymentYear"] integerValue] withRetirementAge:[self getRetirementAge] withAccountBalance:womenS + womenL];
        
        //得到每年的家庭总收入
        CGFloat z = manT + womenT;
        if (i > x1)
        {
            z = womenT;
        }
        if (i > y1)
        {
            z = manT;
        }
        [arr addObject:[NSString stringWithFormat:@"%.2lf",z]];
    }
    return [arr copy];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[RetirementCalculation returnYearswithDifferenceAge:[self getDifferenceAge] withRetirementYear:[self getRetirementYear]] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TXCSCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TXCSCell" forIndexPath:indexPath];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    //得到规划退休年份列表
    cell.label1.text = [RetirementCalculation returnYearswithDifferenceAge:[self getDifferenceAge] withRetirementYear:[self getRetirementYear]][indexPath.row];
    
    //得到男士年龄列表
    NSArray* manAges = [RetirementCalculation returnAgesWithNowAge:[self.manXXDic[@"age"] integerValue] withBigAge:[self.manXXDic[@"life"] integerValue] withRetirementYear:[self getRetirementYear]];
    cell.label2.text = [self changeArrWithArr:manAges][indexPath.row];
    
    //得到女士年龄列表
    NSArray* womenAges = [RetirementCalculation returnAgesWithNowAge:[self.womenXXDic[@"age"] integerValue] withBigAge:[self.womenXXDic[@"life"] integerValue] withRetirementYear:[self getRetirementYear]];
    cell.label3.text = [self changeArrWithArr:womenAges][indexPath.row];
    
    //得到每年家庭总收入
    cell.label4.text = [self getFamilyGeneralIncome][indexPath.row];
    
    //得到每年家庭总支出和
    cell.label5.text = [self getTotalExpenditure][indexPath.row + [self getRetirementYear]];
    //得到当年缺口
    cell.label6.text = [self getAnnuitys][indexPath.row];
    
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50.0;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView* view = self.textView;
    return view;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.tableView)
    {
        CGPoint point = scrollView.contentOffset;
        if (point.y < 0)
        {
            point.y = 0;
        }
        self.tableView.contentOffset = point;
    }
}


@end
