//
//  SeveralViewController.h
//  华康通
//
//  Created by  雷雨 on 2017/11/29.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SeveralViewController : UIViewController

//拿到上个页面的基本信息字典
//
//男士基本信息
@property (nonatomic, strong) NSDictionary* manXXDic;
//女士基本信息
@property (nonatomic, strong) NSDictionary* womenXXDic;
//宏观经济数据假设
@property (nonatomic, strong) NSDictionary* otherXXDic;
//退休计划男士部分数据
@property (nonatomic, strong) NSDictionary* manTXJHDic;
//退休计划女士部分数据
@property (nonatomic, strong) NSDictionary* womenTXJHDic;

@end
