

#import "LYComputational.h"

@implementation LYComputational

//退休时当地上年度职工月平均工资\社会化平均工资 rate:工资增长率\平均工资增长率 wage:个人工资\平均工资 index:多少年后
+ (CGFloat)getPeopleMonthlyWageWithWage:(CGFloat)wage withInflationRate:(NSInteger)rate withIndex:(NSInteger)index
{
    CGFloat a = (1.0 + (rate/100.0));
    CGFloat f = pow(a, index - 1);
    return wage * f;
}

//比较个人月平均工资和社会平均工资，取指数 指数范围 0.6 - 3.0；
+ (CGFloat)getRateWithPeopleMonthlyWage:(CGFloat)wage1 withAveragePayWage:(CGFloat)wage2
{
    if (wage1/wage2 > 3.0)
    {
        return 3.0;
    }
    else if (wage1/wage2 < 0.6)
    {
        return 0.6;
    }
    else
    {
        return wage1/wage2;
    }
}

//计算到退休那年的个人社保页面余额
/*
 *
 *  balance:计划开始当前年的社保余额
 *  rate:银行利率
 *  index:多少年退休
 *
 */
+ (CGFloat)getSocialSecurityBalanceWithBalance:(CGFloat)balance withBankRate:(NSInteger)rate withIndex:(NSInteger)index
{
    CGFloat a = (1.0 + (rate/100.0));
    CGFloat f = pow(a, index - 1);
    return balance * f;
}

//计算个人账户累计余额
/*
 *
 *  wage:本人计划时的月工资
 *  rate:工资增长率
 *  bankRate:银行利率
 *  year:多少年退休
 *
 */
+ (CGFloat)getSavingsWithWage:(CGFloat)wage withRate:(NSInteger)rate withYears:(NSInteger)year withBankRate:(NSInteger)bankRate
{
    CGFloat a = (1.0 + (rate/100.0));
    //计算退休之前个人累积社保金额(除去利息)
    NSMutableArray* arr = [NSMutableArray array];
    for (NSInteger i = 0; i < year; i++)
    {
        CGFloat f = pow(a, i);
        //计算出退休之前个人每年要交的社保金额
        CGFloat b = wage * f * 0.08 * 12;
        //加入数组
        [arr addObject:[NSString stringWithFormat:@"%.2lf",b]];
    }
    //得到总的缴费社保金额(除去利息)
//    NSNumber *manSum = [[arr copy] valueForKeyPath:@"@sum.floatValue"];
    //计算社保金额每年的利息总和
    NSMutableArray* arr1 = [NSMutableArray array];
    for (NSInteger i = 1; i <= year; i++)
    {
        CGFloat f = pow((1.0 + (bankRate/100.0)), (year - i + 1));
        //拿到每年的社保账户缴纳的金钱
        CGFloat l = [[arr copy][i - 1] doubleValue];
        //拿到每年的利息
        CGFloat b = l * f;
        //加入数组
        [arr1 addObject:[NSString stringWithFormat:@"%.2lf",b]];
    }
    //得到利息总和
    NSNumber *lxSum = [[arr1 copy] valueForKeyPath:@"@sum.floatValue"];
    NSLog(@"个人账户累计余额总和 = %.2lf",[lxSum doubleValue]);
    return [lxSum doubleValue];
}

//计算出个人每年领取社保退休金金额 (统筹金额)
/*
 *
 *  wage:上年度社会化平均工资
 *  rate:指数区间
 *  year:缴费年限
 *  retirementAge:退休年龄
 *  balance:个人社保账户余额
 *
 */
+ (CGFloat)getPensionWithPeopleMonthlyWage:(CGFloat)wage withExponentialRange:(CGFloat)rate withPaymentYears:(NSInteger)year withRetirementAge:(NSInteger)retirementAge withAccountBalance:(CGFloat)balance
{
    CGFloat i = [LYAgeToMonth getMonthWithAge:retirementAge];
//    NSLog(@"计发月数 = %.2lf",i);
//    NSLog(@"个人社保账户余额 = %.2lf",balance);
//    NSLog(@"统筹金额：= %.2lf",(((wage * (1.0 + rate)/2.0) * year / 100.0) * 12));
    return (((wage * (1.0 + rate)/2.0) * year / 100.0) + (balance / i)) * 12 ;
}

//每年家庭总支出数组 manExpenditure：男士其他总支出 womenExpenditure：女士其他总支出 year:预算退休后开始的第一年与预期计划开始时的年数——预算退休后开始的第一年与预期计划结束那年的年数 manJTZC:男士家庭固定支出 womenJTZC:女士家庭固定支出 rate:通货膨胀率 m:男士到预期岁数还剩多少年 w:女士到预期岁数还剩多少年
+ (NSArray*)getTotalExpenditureWithManExpenditure:(CGFloat)manExpenditure withWomenExpenditure:(CGFloat)womenExpenditure withYears:(NSInteger)year withManJTZC:(CGFloat)manJTZC withWomenJTZC:(CGFloat)womenJTZC withRate:(NSInteger)rate withManExpectedYear:(NSInteger)m withWomenExpectedYear:(NSInteger)w
{
    NSMutableArray* manArr = [NSMutableArray array];
//    NSMutableArray* womenArr = [NSMutableArray array];
    CGFloat a = (1.0 + (rate/100.0));
    for (NSInteger i = 0; i <= year; i++)
    {
        CGFloat f = pow(a, i);
        CGFloat y = (manExpenditure + manJTZC) * f;
        CGFloat y1 = (womenExpenditure + womenJTZC) * f;
        NSString* str = [NSString stringWithFormat:@"%.2lf",y + y1];
//        NSLog(@"str = %@",str);
        [manArr addObject:str];
    }
    return [manArr copy];
}

//NPV计算公式 NPV是一种进行资本预算的已贴现现金流量方法.rate:投资回报率 arr:每年年金数组
+ (CGFloat)getNPVWithRate:(NSInteger)rate withArr:(NSArray*)arr
{
    NSMutableArray * array = [NSMutableArray array];
    for (NSInteger i = 0; i < arr.count; i++)
    {
        CGFloat d = [arr[i] doubleValue];
        CGFloat f = pow((1 + (rate/100.0)), i + 1);
        CGFloat NPV = d / f;
        [array addObject:[NSString stringWithFormat:@"%.2lf",NPV]];
    }
    NSNumber *sum = [[array copy] valueForKeyPath:@"@sum.floatValue"];
    return [sum doubleValue];
}

//固定计算保险缴费的NPV capital:每年资金 year:年数
+ (NSString*)getInsuranceNPVWithRate:(NSInteger)rate withCapital:(CGFloat)capital withYears:(NSInteger)year
{
    NSMutableArray * array = [NSMutableArray array];
    for (NSInteger i = 0; i < year; i++)
    {
        CGFloat f = pow((1 + (rate/100.0)), i + 1);
        CGFloat NPV = capital / f;
        [array addObject:[NSString stringWithFormat:@"%.2lf",NPV]];
    }
    NSNumber *sum = [[array copy] valueForKeyPath:@"@sum.floatValue"];
    return [NSString stringWithFormat:@"%@",sum];
}


//PMT计算公式 计算每年领取多少年金 rate:投资回报率 capital:总资金 year:年数
+ (CGFloat)getAnnuityForYearWithCapital:(CGFloat)capital withRate:(NSInteger)rate withYears:(NSInteger)year
{
    CGFloat f1 = pow((1 + (rate/100.0)), year);
    CGFloat z = capital * rate * 0.01 * f1 / (f1 - 1);
    return z;
}


@end
