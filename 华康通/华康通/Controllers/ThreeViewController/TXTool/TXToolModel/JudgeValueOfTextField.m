//
//  JudgeValueOfTextField.m
//  华康通
//
//  Created by  雷雨 on 2017/11/30.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import "JudgeValueOfTextField.h"

@implementation JudgeValueOfTextField

//判断所以输入框是否都有值
+ (BOOL)isHaveValueForTextFieldWithArray:(NSArray*)arr
{
    NSMutableArray* newArr = [NSMutableArray array];
    for (UITextField* textField in arr)
    {
        if (textField.text == nil || [textField.text isEqualToString:@""])
        {
            [newArr addObject:textField];
        }
    }
    NSLog(@"newArr.count = %ld",(long)newArr.count);
    return newArr.count == 0 ? YES : NO;
}

//判断后弹出提示框
+ (void)judgeIsHaveValueWithMessage:(NSString*)message 
{
    CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
    TextStatusView *customView  = [TextStatusView view];
    customView.frame = CGRectMake(10, 10, UISCREENWEITH - 80, ALERTVIEW_HEIGHT_B);
    customView.bottomLabel.text = message;
    [alertView setContainerView:customView];
    [alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"好的", nil]];
    [alertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
        switch (buttonIndex)
        {
            case 0:
                return ;
                break;
            default:
                return;
                break;
        }
    }];
    [alertView show];
}


@end
