//
//  RetirementCalculation.h
//  华康通
//
//  Created by  雷雨 on 2017/11/30.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RetirementCalculation : NSObject

//根据规划年份(通过年龄相差的数字和 今年的年份算出来表格展示的年份列表)
+ (NSArray*)returnYearswithDifferenceAge:(NSInteger)differenceAge withRetirementYear:(NSInteger)year;

//根据规划年份(通过年龄相差的数字和 计算出男女年龄展示列表)
+ (NSArray*)returnAgesWithNowAge:(NSInteger)nowAge withBigAge:(NSInteger)bigAge withRetirementYear:(NSInteger)year;

@end
