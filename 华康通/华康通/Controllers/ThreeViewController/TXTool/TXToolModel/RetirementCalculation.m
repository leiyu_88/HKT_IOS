//
//  RetirementCalculation.m
//  华康通
//
//  Created by  雷雨 on 2017/11/30.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import "RetirementCalculation.h"

@implementation RetirementCalculation

+ (NSArray*)returnYearswithDifferenceAge:(NSInteger)differenceAge withRetirementYear:(NSInteger)year
{
    NSMutableArray* arr = [NSMutableArray array];
    NSDate* date = [[NSDate alloc]init];
    NSDateFormatter* formatter = [[NSDateFormatter alloc]init];
    formatter.dateFormat = @"YYYY";
    NSString* nowDate = [formatter stringFromDate:date];
    //算出今年的年份并转化为数字
    NSInteger nowYear = [nowDate integerValue];

    for (NSInteger i = nowYear + year; i <= nowYear + differenceAge; i++)
    {
        [arr addObject:[NSString stringWithFormat:@"%ld",(long)i]];
    }
    return [arr copy];
}

+ (NSArray*)returnAgesWithNowAge:(NSInteger)nowAge withBigAge:(NSInteger)bigAge withRetirementYear:(NSInteger)year
{
    NSMutableArray* arr = [NSMutableArray array];
    for (NSInteger i = nowAge + year; i < bigAge + 1; i++)
    {
        [arr addObject:[NSString stringWithFormat:@"%ld",(long)i]];
    }
    return [arr copy];
}

@end
