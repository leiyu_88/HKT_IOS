

#import <Foundation/Foundation.h>

@interface LYComputational : NSObject

//退休时当地上年度职工月平均工资\社会化平均工资 rate:工资增长率\平均工资增长率 wage:个人工资\平均工资 index:多少年后
+ (CGFloat)getPeopleMonthlyWageWithWage:(CGFloat)wage withInflationRate:(NSInteger)rate withIndex:(NSInteger)index;

//比较个人月平均工资和社会平均工资，取指数 指数范围 0.6 - 3.0； wage1:个人月平均工资 wage2:社会平均工资
+ (CGFloat)getRateWithPeopleMonthlyWage:(CGFloat)wage1 withAveragePayWage:(CGFloat)wage2;

//计算到退休那年的个人社保页面余额
/*
 *
 *  balance:计划开始当前年的社保余额
 *  rate:银行利率
 *  index:多少年退休
 *
 */
+ (CGFloat)getSocialSecurityBalanceWithBalance:(CGFloat)balance withBankRate:(NSInteger)rate withIndex:(NSInteger)index;

//计算个人账户累计余额
/*
 *
 *  wage:本人计划时的月工资
 *  rate:工资增长率
 *  bankRate:银行利率
 *  year:多少年退休
 *
 */
+ (CGFloat)getSavingsWithWage:(CGFloat)wage withRate:(NSInteger)rate withYears:(NSInteger)year withBankRate:(NSInteger)bankRate;

//计算出个人每年领取社保退休金金额 (统筹金额)
/*
 *
 *  wage:上年度社会化平均工资
 *  rate:指数区间
 *  year:缴费年限
 *  retirementAge:退休年龄
 *  balance:个人社保账户余额
 *
 */
+ (CGFloat)getPensionWithPeopleMonthlyWage:(CGFloat)wage withExponentialRange:(CGFloat)rate withPaymentYears:(NSInteger)year withRetirementAge:(NSInteger)retirementAge withAccountBalance:(CGFloat)balance;

//每年家庭总支出数组 manExpenditure：男士其他总支出 womenExpenditure：女士其他总支出 year:预算退休后开始的第一年与预期计划开始时的年数——预算退休后开始的第一年与预期计划结束那年的年数 manJTZC:男士家庭固定支出 womenJTZC:女士家庭固定支出 rate:通货膨胀率 m:男士到预期岁数还剩多少年 w:女士到预期岁数还剩多少年
+ (NSArray*)getTotalExpenditureWithManExpenditure:(CGFloat)manExpenditure withWomenExpenditure:(CGFloat)womenExpenditure withYears:(NSInteger)year withManJTZC:(CGFloat)manJTZC withWomenJTZC:(CGFloat)womenJTZC withRate:(NSInteger)rate withManExpectedYear:(NSInteger)m withWomenExpectedYear:(NSInteger)w;

//NPV计算公式 NPV是一种进行资本预算的已贴现现金流量方法.rate:投资回报率 arr:每年年金数组
+ (CGFloat)getNPVWithRate:(NSInteger)rate withArr:(NSArray*)arr;

//固定计算保险缴费的NPV capital:每年资金 year:年数
+ (NSString*)getInsuranceNPVWithRate:(NSInteger)rate withCapital:(CGFloat)capital withYears:(NSInteger)year;

//PMT计算公式 计算每年领取多少年金 rate:投资回报率 capital:总资金 year:年数
+ (CGFloat)getAnnuityForYearWithCapital:(CGFloat)capital withRate:(NSInteger)rate withYears:(NSInteger)year;


@end
