//
//  JudgeValueOfTextField.h
//  华康通
//
//  Created by  雷雨 on 2017/11/30.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JudgeValueOfTextField : NSObject

//判断所有输入框是否都有值
+ (BOOL)isHaveValueForTextFieldWithArray:(NSArray*)arr;

//判断后弹出提示框
+ (void)judgeIsHaveValueWithMessage:(NSString*)message;


@end
