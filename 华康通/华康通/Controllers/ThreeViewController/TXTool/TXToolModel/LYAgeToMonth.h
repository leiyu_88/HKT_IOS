//
//  LYAgeToMonth.h
//  华康通
//
//  Created by  雷雨 on 2017/12/1.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LYAgeToMonth : NSObject

//已知退休年龄拿到计发月数
+ (CGFloat)getMonthWithAge:(NSInteger)age;

@end
