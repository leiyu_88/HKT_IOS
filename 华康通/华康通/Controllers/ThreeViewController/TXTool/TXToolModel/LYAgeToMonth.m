//
//  LYAgeToMonth.m
//  华康通
//
//  Created by  雷雨 on 2017/12/1.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import "LYAgeToMonth.h"

@implementation LYAgeToMonth

+ (CGFloat)getMonthWithAge:(NSInteger)age
{
    switch (age)
    {
        case 55:
            return 170.0;
            break;
        case 56:
            return 164.0;
            break;
        case 57:
            return 158.0;
            break;
        case 58:
            return 152.0;
            break;
        case 59:
            return 145.0;
            break;
        case 60:
            return 139.0;
            break;
        case 61:
            return 132.0;
            break;
        case 62:
            return 125.0;
            break;
        case 63:
            return 117.0;
            break;
        case 64:
            return 109.0;
            break;
        case 65:
            return 101.0;
            break;
        case 66:
            return 93.0;
            break;
        case 67:
            return 84.0;
            break;
        case 68:
            return 75.0;
            break;
        case 69:
            return 65.0;
            break;
        case 70:
            return 56.0;
            break;
        default:
        {
            if (age < 55)
            {
                return 170.0;
            }
            else
            {
                return 56.0;
            }
        }
            break;
    }
}

@end
