//
//  TXZYView.h
//  华康通
//
//  Created by  雷雨 on 2017/11/29.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TXZYView : UIView

@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *manTextFields;
@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *womenTextFields;


+ (id)view;

@end
