//
//  TextFieldView.m
//  华康通
//
//  Created by  雷雨 on 2017/11/28.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import "TextFieldView.h"

@implementation TextFieldView

+ (id)view
{
    return [[[NSBundle mainBundle]loadNibNamed:@"TextFieldView" owner:nil options:nil]lastObject];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self=[super initWithCoder:aDecoder])
    {
        self.autoresizingMask = UIViewAutoresizingNone;
    }
    return self;
}

- (IBAction)closeTextField:(UITextField *)sender
{
    [sender resignFirstResponder];
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self endEditing:YES];
}

@end
