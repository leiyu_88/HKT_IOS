//
//  TXZYView.m
//  华康通
//
//  Created by  雷雨 on 2017/11/29.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import "TXZYView.h"

@implementation TXZYView

+ (id)view
{
    return [[[NSBundle mainBundle]loadNibNamed:@"TXZYView" owner:nil options:nil]lastObject];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self=[super initWithCoder:aDecoder])
    {
        self.autoresizingMask = UIViewAutoresizingNone;
    }
    return self;
}
- (IBAction)closeKeyboard:(id)sender
{
    [sender resignFirstResponder];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self endEditing:YES];
}

@end
