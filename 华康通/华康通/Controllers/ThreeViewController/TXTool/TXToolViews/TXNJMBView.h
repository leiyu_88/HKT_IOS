//
//  TXNJMBView.h
//  华康通
//
//  Created by  雷雨 on 2017/11/29.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TXNJMBView : UIView
//上部红色view
@property (weak, nonatomic) IBOutlet UIView *topView;
//下步红色view
@property (weak, nonatomic) IBOutlet UIView *bottomView;
//中间模块view
@property (weak, nonatomic) IBOutlet UIView *centerView;
//最下方view
@property (weak, nonatomic) IBOutlet UIView *dbView;
//退休时的年金现值
@property (weak, nonatomic) IBOutlet UILabel *centerTopLabel;
//预计年金领取年期
@property (weak, nonatomic) IBOutlet UITextField *centerCenterTextField;
//预计年金领取金额
@property (weak, nonatomic) IBOutlet UILabel *centerBottomLabel;
//预计缴费年期
@property (weak, nonatomic) IBOutlet UITextField *dvTopTextField;
//预计年缴金额
@property (weak, nonatomic) IBOutlet UILabel *dvBottomLabel;

+ (id)view;

@end
