//
//  TextFieldView.h
//  华康通
//
//  Created by  雷雨 on 2017/11/28.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TextFieldView : UIView

//男士基本信息填写框
@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *mainTextFields;
//女士基本信息填写框
@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *womenTextFields;
//数据假设填写框
@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *otherTextFields;

+ (id)view;


@end
