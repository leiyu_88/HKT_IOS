//
//  FLJSViewController.m
//  华康通
//
//  Created by  雷雨 on 2017/11/27.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import "FLJSViewController.h"

@interface FLJSViewController ()
@property (weak, nonatomic) IBOutlet UIWebView *myWebView;

@end

@implementation FLJSViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:LoadImage(@"back1") style:UIBarButtonItemStylePlain target:self action:@selector(popRootVC)];
    [self loadURl];
}

- (void)popRootVC
{
    [self.navigationController popViewControllerAnimated:YES];
}

//加载链接
- (void)loadURl
{
    NSString *resPath = [[NSBundle mainBundle] resourcePath];
    NSString *htmlFilePath = nil;
    NSURL* url = nil;
    if (self.myType == FLJSTypeFL)
    {
        NSString* path = [[NSBundle mainBundle] pathForResource:@"calc" ofType:@"html"];
        url = [NSURL fileURLWithPath:path];
        htmlFilePath = [resPath stringByAppendingPathComponent:@"calc.html"];
    }
    else if (self.myType == FLJSTypeLC)
    {
        NSString* path = [[NSBundle mainBundle] pathForResource:@"bmi" ofType:@"html"];
        url = [NSURL fileURLWithPath:path];
        htmlFilePath = [resPath stringByAppendingPathComponent:@"bmi.html"];
    }
    NSString *string = [NSString stringWithContentsOfFile:htmlFilePath encoding:NSUTF8StringEncoding error:nil];
    [self.myWebView loadHTMLString:string baseURL:url];
}

@end
