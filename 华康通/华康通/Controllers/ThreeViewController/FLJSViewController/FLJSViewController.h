//
//  FLJSViewController.h
//  华康通
//
//  Created by  雷雨 on 2017/11/27.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, FLJSType) {
    FLJSTypeFL,            // 复利计算
    FLJSTypeYL,            // 养老
    FLJSTypeLC,            // 理财
};

@interface FLJSViewController : UIViewController

@property (nonatomic, unsafe_unretained) FLJSType myType;

@end
