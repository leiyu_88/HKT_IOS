//
//  ThreeViewController.m
//  华康通
//
//  Created by leiyu on 16/9/23.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import "ThreeViewController.h"
#import "ToolCell.h"
#import "DetailToolController.h"
#import "AllCalledPhoneController.h"
#import "RiskAppraiseController.h"
#import "UMMobClick/MobClick.h"
#import "FLJSViewController.h"
#import "OneViewController.h"
#import "ThreeToolView.h"

@interface ThreeViewController ()<UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) UITableView *tableView;

@property (strong, nonatomic) ThreeToolView* buttonsView;

@end

@implementation ThreeViewController

- (ThreeToolView*)buttonsView
{
    if (!_buttonsView)
    {
        _buttonsView = [[ThreeToolView alloc]initWithFrame:CGRectMake(10, 10, UISCREENWEITH - 20, 524)];
    }
    return _buttonsView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"工具";
    self.scrollView.contentSize = CGSizeMake(UISCREENWEITH, self.buttonsView.size.height + 20);
    [self.scrollView addSubview:self.buttonsView];
    [self.tabBarController.tabBar setTintColor:KMainColor];
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getPushNotification:) name:@"ThreeToolView" object:nil];
    ShowTabbar;
    [MobClick beginLogPageView:@"ThreeViewController"];//("PageOne"为页面名称，可自定义)
}

- (void)viewWillDisappear:(BOOL)animated
{
    
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ThreeToolView" object:nil];
    HiddenTabbar;
    [MobClick endLogPageView:@"ThreeViewController"];
}

- (void)getPushNotification:(NSNotification*)notification
{
    switch ([notification.userInfo[@"number"] integerValue])
    {
        case 0:
        {
            FLJSViewController* vc = [[FLJSViewController alloc]initWithNibName:@"FLJSViewController" bundle:nil];
            vc.title = @"单复利计算器";
            vc.myType = FLJSTypeFL;
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 1:
        {
            FLJSViewController* vc = [[FLJSViewController alloc]initWithNibName:@"FLJSViewController" bundle:nil];
            vc.title = @"标准体重计算器";
            vc.myType = FLJSTypeLC;
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 2:
        {
            RiskAppraiseController* riskVC = [[RiskAppraiseController alloc]initWithNibName:@"RiskAppraiseController" bundle:nil];
            riskVC.title = @"保险需求评估";
            riskVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:riskVC animated:YES];
        }
            break;
        case 3:
        {
//            OneViewController* vc = [[OneViewController alloc]initWithNibName:@"OneViewController" bundle:nil];
//            vc.title = @"年金计算器";
//            vc.hidesBottomBarWhenPushed = YES;
//            [self.navigationController pushViewController:vc animated:YES];
            
/*
 *********************
 *********************
 *********************
 *********************测试弹出发送短信的视图
 *********************
 *********************
 *********************
 */
            
//            ShareView* shareView = [[ShareView alloc]initWithFrame:CGRectMake(0, UISCREENHEIGHT - 125, UISCREENWEITH, 125) withIndex:2];
//            [[UIApplication sharedApplication].keyWindow addSubview:shareView.backView];
//            [[UIApplication sharedApplication].keyWindow addSubview:shareView];
            
            [self showNoPush];
        }
            break;
        case 4:
        {
            DetailToolController* detailToolVC = [[DetailToolController alloc]initWithNibName:@"DetailToolController" bundle:nil];
            detailToolVC.title = @"房贷计算";
            detailToolVC.url = @"https://m.fang.com/tools/daikuan.html";
            detailToolVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:detailToolVC animated:YES];
        }
            break;
        case 5:
        {
            DetailToolController* detailToolVC = [[DetailToolController alloc]initWithNibName:@"DetailToolController" bundle:nil];
            detailToolVC.title = @"车贷计算";
            detailToolVC.url = @"http://car.bitauto.com/gouchejisuanqi";
            detailToolVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:detailToolVC animated:YES];
        }
            break;
        case 6:
        {
            AllCalledPhoneController* allCalledPhoneVC = [[AllCalledPhoneController alloc]initWithNibName:@"AllCalledPhoneController" bundle:nil];
            allCalledPhoneVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:allCalledPhoneVC animated:YES];
        }
            break;
        default:
        {
            [self showNoPush];
        }
            break;
    }
}

//弹出提示功能正在开发
- (void)showNoPush
{
    CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
    TextStatusView *customView  = [TextStatusView view];
    customView.frame = CGRectMake(10, 10, UISCREENWEITH - 80, ALERTVIEW_HEIGHT_B);
    customView.bottomLabel.text = @"该功能正在加紧研发中，敬请期待！";
    //    customView.layer.cornerRadius = 7.0;
    //    customView.layer.masksToBounds = YES;
    [alertView setContainerView:customView];
    [alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"确定", nil]];
    [alertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
        switch (buttonIndex)
        {
            case 0:
                return ;
                break;
            default:
                return;
                break;
        }
    }];
    [alertView show];
}


@end
