//
//  ThreeToolView.m
//  华康通
//
//  Created by  雷雨 on 2017/12/13.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import "ThreeToolView.h"

#define COUNT 8
//按钮宽
#define BUTTONWIGTH ((UISCREENWEITH - 20 - 1) / 2)
//按钮高
#define BUTTONHRIGHT 120.0
//label高
#define LABELHEIHT 15.0

@interface ThreeToolView ()

@property (strong, nonatomic) NSArray* images;
@property (strong, nonatomic) NSArray* topArray;


@end

@implementation ThreeToolView

- (NSArray*)images
{
    if (!_images)
    {
        _images = @[@"tool_calculates",@"tool_weight",@"tool_need",@"tool_rente",@"tool_housingloan",@"tool_autoloan",@"tool_Customer",@"tool_add"];
    }
    return _images;
}
- (NSArray*)topArray
{
    if (!_topArray)
    {
        _topArray = @[@"单复利计算器",@"标准体重计算器",@"保险需求评估",@"年金计算器",@"房贷计算器",@"车贷计算器",@"保险理赔热线",@"期待更多"];
    }
    return _topArray;
}


- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        self.backgroundColor = LIGHTGREYColor;
        [self createView];
        [self createButtons];
    }
    return self;
}

- (void)createView
{
    UIView* view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, UISCREENWEITH - 20, 40)];
    view.backgroundColor = FFFFFFColor;
    [self addSubview:view];
    UIImageView* imageView = [[UIImageView alloc]init];
    imageView.frame = CGRectMake(12, 11.5, 17, 20);
    imageView.image = LoadImage(@"tool_calculate");
    [view addSubview:imageView];
    UILabel* label = [[UILabel alloc]initWithFrame:CGRectMake(37, 0, UISCREENWEITH - 20 - 37, 40)];
    label.text = @"展业小助手";
    label.textAlignment = NSTextAlignmentLeft;
    label.textColor = C3C3CColor;
    label.font = SetFont(15.0);
    [view addSubview:label];
}

- (void)createButtons
{
    for (NSInteger i = 0; i < COUNT; i++)
    {
        NSInteger a = i / 2;//确定行
        NSInteger b = i % 2;//确定列
        UIButton* button = [[UIButton alloc]init];
        button.tag = i;
        [button setTitle:@"" forState:UIControlStateNormal];
        [button setBackgroundColor:FFFFFFColor];
        button.frame = CGRectMake((b * (BUTTONWIGTH + 1)), (a * (BUTTONHRIGHT + 1)) + 41, BUTTONWIGTH, BUTTONHRIGHT);
        
        UIImageView* imageView = [[UIImageView alloc]init];
        imageView.frame = CGRectMake((BUTTONWIGTH - 50)/2, 15, 50, 50);
        imageView.image = LoadImage(self.images[i]);
        [button addSubview:imageView];
        UILabel* label = [[UILabel alloc]initWithFrame:CGRectMake(0, 50 + 15 + 12,BUTTONWIGTH, LABELHEIHT)];
        label.text = self.topArray[i];
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = C3C3CColor;
        label.font = SetFont(15.0);
        [button addSubview:label];
        [button addTarget:self action:@selector(pushVC:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
    }
}

- (void)pushVC:(UIButton*)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ThreeToolView" object:nil userInfo:@{@"number":[NSString stringWithFormat:@"%ld",(long)sender.tag]}];
}



@end
