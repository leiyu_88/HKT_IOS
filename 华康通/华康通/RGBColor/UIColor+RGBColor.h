//
//  UIColor+RGBColor.h
//  习本课堂
//
//  Created by leiyu on 16/8/18.
//  Copyright © 2016年 亮信科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (RGBColor)

+ (UIColor *)getColor:(NSString *)hexColor;

@end
