//
//  FirstPageProductCell.h
//  华康通
//
//  Created by 雷雨 on 16/9/24.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstPageProductCell : UITableViewCell
//产品快照
@property (weak, nonatomic) IBOutlet UIImageView *productPhotoView;
//显示产品标题
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
//显示产品副标题
@property (weak, nonatomic) IBOutlet UILabel *detailTitleLabel;
//显示推广费用
@property (weak, nonatomic) IBOutlet UILabel *otherLabel;
//投保按钮
@property (weak, nonatomic) IBOutlet UIButton *TBButton;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
//显示什么类型产品的标签
@property (strong, nonatomic) IBOutlet UILabel *typeLabel;
//显示什么类型产品的标签的底图图片
@property (strong, nonatomic) IBOutlet UIImageView *typeLabelImageView;

@end
