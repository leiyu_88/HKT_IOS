
//

#import "CacheData.h"

@implementation CacheData

//用字符串为key来保存缓存
+ (void)saveCache:(NSString*)code andCache:(id)Cache
{
    NSUserDefaults * settings = [NSUserDefaults standardUserDefaults];
    NSString * key = [NSString stringWithFormat:@"%@",code];
    [settings setObject:Cache forKey:key];
    [settings synchronize];
}

//用字符串为key来取缓存数据
+ (id)getCache:(NSString*)code
{
    NSUserDefaults * settings = [NSUserDefaults standardUserDefaults];
    NSString * key = [NSString stringWithFormat:@"%@",code];
    id value = [settings objectForKey:key];
    return value;
}

//把数据保存到沙盒
+ (void)saveCache:(int)type andID:(int)dataID andCache:(id)Cache
{
    NSUserDefaults * settings = [NSUserDefaults standardUserDefaults];
    NSString * key = [NSString stringWithFormat:@"detail-%d-%d",type, dataID];
    [settings setObject:Cache forKey:key];
    [settings synchronize];
}

//保存用户登录名和密码
+ (void)saveUserInfoWithUserName:(NSString*)name withuserPassword:(NSString*)password
{
    NSDictionary* users = @{@"userName":name,@"userKey":password};
    NSUserDefaults * settings = [NSUserDefaults standardUserDefaults];
    NSString * key = [NSString stringWithFormat:@"userInfo"];
    [settings setObject:users forKey:key];
    [settings synchronize];
}

//读取用户登录名和密码
+ (NSDictionary*)getUserInfo
{
    NSUserDefaults * settings = [NSUserDefaults standardUserDefaults];
    NSString *key = [NSString stringWithFormat:@"userInfo"];
    id value = [settings objectForKey:key];
    return (NSDictionary*)value;
}

//保存加密后的用户登录名和密码
+ (void)saveBase64_UserInfoWithUserName:(NSString*)name withuserPassword:(NSString*)password
{
    NSDictionary* users = @{@"userName":name,@"userKey":password};
    NSUserDefaults * settings = [NSUserDefaults standardUserDefaults];
    NSString * key = [NSString stringWithFormat:@"Base64_UserInfo"];
    [settings setObject:users forKey:key];
    [settings synchronize];
}

//读取加密后的用户登录名和密码
+ (NSDictionary*)getBase64_UserInfo
{
    NSUserDefaults * settings = [NSUserDefaults standardUserDefaults];
    NSString *key = [NSString stringWithFormat:@"Base64_UserInfo"];
    id value = [settings objectForKey:key];
    return (NSDictionary*)value;
}

//删除本地的用户登录名和密码
+ (void)removeUserInfo
{
    NSUserDefaults * settings = [NSUserDefaults standardUserDefaults];
    NSString *key = [NSString stringWithFormat:@"userInfo"];
    [settings removeObjectForKey:key];
}

//读取本地沙盒 (读取之前首先根据type和dataID判断本地是否有)
+ (id)getCache:(int)type andID:(int)dataID
{
    NSUserDefaults * settings = [NSUserDefaults standardUserDefaults];
    NSString *key = [NSString stringWithFormat:@"detail-%d-%d",type, dataID];
    id value = [settings objectForKey:key];
    return value;
}


//清除缓存
+ (void)removeCache:(int)type andID:(int)dataID
{
    NSUserDefaults * settings = [NSUserDefaults standardUserDefaults];
    NSString *key = [NSString stringWithFormat:@"detail-%d-%d",type, dataID];
    [settings removeObjectForKey:key];
    //    [settings synchronize];
}

//清除按字符串储存的缓存
+ (void)removeCacheWith:(NSString*)code
{
    NSUserDefaults * settings = [NSUserDefaults standardUserDefaults];
    NSString *key = [NSString stringWithFormat:@"%@",code];
    [settings removeObjectForKey:key];
    [settings synchronize];
}

//清除app内部所有缓存 （除了用户的登陆信息不删除，其余通通删除）
+ (void)clearAllCache
{
    NSUserDefaults * settings = [NSUserDefaults standardUserDefaults];
    NSDictionary *dic = [settings dictionaryRepresentation];
    for (id key in dic)
    {
        if ([key isKindOfClass:[NSString class]])
        {
            //除了用户的登陆信息不删除，其余通通删除
            if (![key isEqualToString:@"userInformation_0_0"] || ![key isEqualToString:@"userInfo"] || ![key isEqualToString:@"token"] || ![key isEqualToString:@"Base64_UserInfo"])
            {
                [settings removeObjectForKey:key];
            }
        }
    }
    [settings synchronize];
}
//把json数据（包括缓存的数据）转化为字符串 NSString 格式
+ (NSString*)convertToJSONData:(id)infoDict
{
    NSError *error;
    NSString *jsonString = @"";
    if (infoDict == nil)
    {
        NSLog(@"Got an error: %@", error);
    }
    else
    {
        //如果是数据格式 数据格式转字符串
        if ([infoDict isKindOfClass:[NSData class]])
        {
            jsonString = [[NSString alloc] initWithData:infoDict encoding:NSUTF8StringEncoding];
        }
    }
    //    jsonString = [jsonString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];  //去除掉首尾的空白字符和换行字符
    //    [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    return jsonString;
}

//JSON字符串转化为字典
+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString
{
    if (jsonString == nil)
    {
        return nil;
    }
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers
                                                          error:&err];
    if(err)
    {
        NSLog(@"json解析失败：%@",err);
        return nil;
    }
    return dic;
}
@end
