

#import <Foundation/Foundation.h>

@interface CacheData : NSObject

//用字符串为key来保存缓存
+ (void)saveCache:(NSString*)code andCache:(id)Cache;

//用字符串为key来取缓存数据
+ (id)getCache:(NSString*)code;

//把数据保存到沙盒
+ (void)saveCache:(int)type andID:(int)dataID andCache:(id)Cache;

//读取本地沙盒 (读取之前首先根据type和dataID判断本地是否有)
+ (id)getCache:(int)type andID:(int)dataID;

//保存用户登录名和密码
+ (void)saveUserInfoWithUserName:(NSString*)name withuserPassword:(NSString*)password;

//删除本地的用户登录名和密码
+ (void)removeUserInfo;

//读取用户登录名和密码
+ (NSDictionary*)getUserInfo;

//保存加密后的用户登录名和密码
+ (void)saveBase64_UserInfoWithUserName:(NSString*)name withuserPassword:(NSString*)password;

//读取加密后的用户登录名和密码
+ (NSDictionary*)getBase64_UserInfo;

//把json数据（包括缓存的数据）转化为字符串 NSString 格式
+ (NSString*)convertToJSONData:(id)infoDict;

//JSON字符串转化为字典
+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString;

//清除缓存
+ (void)removeCache:(int)type andID:(int)dataID;

//清除按字符串储存的缓存
+ (void)removeCacheWith:(NSString*)code;

//清除app内部所有缓存 （除了用户的登陆信息不删除，其余通通删除）
+ (void)clearAllCache;

@end
