

#import "DetailToolController.h"
#import "SVProgressHUD.h"
#import "UMMobClick/MobClick.h"

@interface DetailToolController ()<UIWebViewDelegate, NJKWebViewProgressDelegate>

@property (strong, nonatomic) IBOutlet UIWebView *myWebView;
@property (strong, nonatomic) NJKWebViewProgressView* webViewProgressView;
@property (strong, nonatomic) NJKWebViewProgress* webViewProgress;

@end

@implementation DetailToolController


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:LoadImage(@"back1") style:UIBarButtonItemStylePlain target:self action:@selector(popRootVC)];
    [self showProgressView];
}

//加载链接
- (void)loadURl
{
    NSURL* url = [NSURL URLWithString:self.url];
    NSURLRequest* request = [NSURLRequest requestWithURL:url];
    [self.myWebView loadRequest:request];
}

//创建进度条
- (void)showProgressView
{
    self.webViewProgress = [[NJKWebViewProgress alloc] init];
    self.myWebView.delegate = self.webViewProgress;
    self.webViewProgress.webViewProxyDelegate = self;
    self.webViewProgress.progressDelegate = self;
    CGRect navBounds = self.navigationController.navigationBar.bounds;
    CGRect barFrame = CGRectMake(0,
                                 navBounds.size.height - 3,
                                 navBounds.size.width,
                                 3);
    self.webViewProgressView = [[NJKWebViewProgressView alloc] initWithFrame:barFrame];
    self.webViewProgressView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    [self.webViewProgressView setProgress:0 animated:YES];
    [self loadURl];
    [self.navigationController.navigationBar addSubview:self.webViewProgressView];
}

#pragma mark - NJKWebViewProgressDelegate
- (void)webViewProgress:(NJKWebViewProgress *)webViewProgress updateProgress:(float)progress
{
    [self.webViewProgressView setProgress:progress animated:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"DetailToolController"];//("PageOne"为页面名称，可自定义)
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.webViewProgressView removeSubviews];
    [MobClick endLogPageView:@"DetailToolController"];
}

////创建一条类似网络加载条的线
//- (void)createProgressView
//{
//    
//}
////计时器工作
//- (void)changeProgress
//{
//    self.progressView.progress += 0.2;
//    if (self.progressView.progress > 1)
//    {
//        //停止计时器
//        [self.timer invalidate];
//        [self.progressView setHidden:YES];
//    }
//    
//}
- (void)popRootVC
{
    if ([self.myWebView canGoBack])
    {
        [self.myWebView goBack];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    
}

//在webview完成加载的时候
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.getElementsByClassName('banner')[0].style.display = 'none'"];
    [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.getElementsByClassName('hongbao')[0].style.display = 'none'"];
    [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.getElementsByClassName('logo')[0].style.display = 'none'"];
    [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.getElementsByClassName('w990')[0].style.display = 'none'"];
    [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.getElementsByClassName('result-footer')[0].style.display = 'none'"];
    [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.getElementsByClassName('article-list')[0].style.display = 'none'"];
    [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.getElementsByClassName('footer')[0].style.display = 'none'"];
    [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.getElementsByClassName('header')[0].style.display = 'none'"];
}

- (void)viewDidDisappear:(BOOL)animated
{
    
}


@end
