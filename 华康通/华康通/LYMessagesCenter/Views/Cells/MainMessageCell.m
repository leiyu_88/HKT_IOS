//
//  MainMessageCell.m
//  华康通
//
//  Created by  雷雨 on 2017/10/18.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import "MainMessageCell.h"

#define SIZEWIDTH (UISCREENWEITH - 32.0)
#define IMAGEHEIGHT ((SIZEWIDTH - 27.0) * (528.0/938.0))

@implementation MainMessageCell


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(LYPagingMessageModel *)model
{
    self.backView.layer.cornerRadius = 3.0;
    self.backView.layer.masksToBounds = YES;
    self.dateLabel.text = model.date;
    self.titleLabel.text = model.title;
    self.detailLabel.text = model.detail;
    [self.pImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",model.imageURL]]
                             placeholderImage:LoadImage(@"news_Defaultpicture.png")];
    
    self.dateLabel.frame = CGRectMake((UISCREENWEITH - 68)/2, 13.5, 90, 18);
    
    self.titleLabel.frame = CGRectMake(13.5, 0, SIZEWIDTH - 27, 46);
    
    self.pImageView.frame = CGRectMake(13.5, 46, SIZEWIDTH - 27, IMAGEHEIGHT);
    
    self.detailLabel.frame = CGRectMake(13.5, 46 + IMAGEHEIGHT, SIZEWIDTH - 27, 50);
    
    self.backView.frame = CGRectMake(16, 45, SIZEWIDTH, 46 + IMAGEHEIGHT + 50);
    
    CGRect rect = self.bounds;
    
    rect.size.height = 46 + IMAGEHEIGHT + 50 + 45;
    
    self.bounds = rect;
}



@end
