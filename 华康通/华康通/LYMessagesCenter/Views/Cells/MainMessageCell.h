//
//  MainMessageCell.h
//  华康通
//
//  Created by  雷雨 on 2017/10/18.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LYPagingMessageModel.h"

@interface MainMessageCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIImageView *pImageView;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;

@property (strong, nonatomic) LYPagingMessageModel* model;

- (void)setModel:(LYPagingMessageModel *)model;

@end
