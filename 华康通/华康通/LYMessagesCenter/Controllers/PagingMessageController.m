//
//  PagingMessageController.m
//  华康通
//
//  Created by  雷雨 on 2017/10/17.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import "PagingMessageController.h"
#import "PagingCell.h"

@interface PagingMessageController ()

@end

@implementation PagingMessageController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:LoadImage(@"back1") style:UIBarButtonItemStylePlain target:self action:@selector(popRootVC)];
    [self.tableView registerNib:[UINib nibWithNibName:@"PagingCell" bundle:nil] forCellReuseIdentifier:@"PagingCell"];
}

- (void)popRootVC
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PagingCell* cell = (PagingCell*)[tableView dequeueReusableCellWithIdentifier:@"PagingCell" forIndexPath:indexPath];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    //园切角
    cell.backView.layer.cornerRadius = 8.0;
    cell.backView.layer.masksToBounds = YES;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //推出消息h5页面
}


@end
