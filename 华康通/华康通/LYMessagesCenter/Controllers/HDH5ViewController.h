//
//  HDH5ViewController.h
//  华康通
//
//  Created by  雷雨 on 2017/12/12.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HDH5ViewController : UIViewController

//url
@property (nonatomic, strong) NSString* html;

//外链产品图片url
@property (nonatomic, strong) NSString* small_picture;
//外链产品描述
@property (nonatomic, strong) NSString* product_features;
//标题
@property (nonatomic, strong) NSString* myTitle;

@end
