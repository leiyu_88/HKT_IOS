//
//  MainMessagesController.h
//  华康通
//
//  Created by  雷雨 on 2017/10/17.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainMessagesController : UITableViewController

//消息的json数组
@property (nonatomic, strong) NSMutableArray* messages;



@end
