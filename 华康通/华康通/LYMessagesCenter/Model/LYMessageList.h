//
//  LYMessageList.h
//  华康通
//
//  Created by  雷雨 on 2017/12/13.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LYMessageList : NSObject

@property (nonatomic,strong) NSString* myId;
@property (nonatomic,strong) NSString* title;
@property (nonatomic,strong) NSString* cover;
@property (nonatomic,strong) NSString* content;
@property (nonatomic,strong) NSString* createDate;
@property (nonatomic,strong) NSString* type;
@property (nonatomic,strong) NSString* pic;

+(id)messageListWithJSON:(NSDictionary*)json;

@end
