//
//  LYPagingMessageModel.h
//  华康通
//
//  Created by  雷雨 on 2017/12/14.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LYPagingMessageModel : NSObject

//标题
@property (strong, nonatomic) NSString* title;
//日期
@property (strong, nonatomic) NSString* date;
//图片地址
@property (strong, nonatomic) NSString* imageURL;
//描述
@property (strong, nonatomic) NSString* detail;

+ (id)mode;


@end
