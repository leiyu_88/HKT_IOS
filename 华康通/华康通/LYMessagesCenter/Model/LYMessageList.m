//
//  LYMessageList.m
//  华康通
//
//  Created by  雷雨 on 2017/12/13.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import "LYMessageList.h"

@implementation LYMessageList

-(id)initWithJSON:(NSDictionary*)json
{
    if (self = [super init])
    {
        if ([json[@"id"] isKindOfClass:[NSNull class]] || !json[@"id"])
        {
            self.myId = @"";
        }
        else
        {
            self.myId = json[@"id"];
        }
        
        if ([json[@"title"] isKindOfClass:[NSNull class]] || !json[@"title"])
        {
            self.title = @"";
        }
        else
        {
            self.title = json[@"title"];
        }
        
        if ([json[@"cover"] isKindOfClass:[NSNull class]] || !json[@"cover"])
        {
            self.cover = @"";
        }
        else
        {
            self.cover = json[@"cover"];
        }
        
        if ([json[@"content"] isKindOfClass:[NSNull class]] || !json[@"content"])
        {
            self.content = @"";
        }
        else
        {
            self.content = json[@"content"];
        }
        
        if ([json[@"createDate"] isKindOfClass:[NSNull class]] || !json[@"createDate"])
        {
            self.createDate = @"";
        }
        else
        {
            NSString* str = json[@"createDate"];
//            if ([json[@"createDate"] length] > 9)
//            {
//                NSArray* arr = [json[@"createDate"] componentsSeparatedByString:@"-"];
//                if (arr.count == 3)
//                {
//                    str = [NSString stringWithFormat:@"%@-%@-%@",arr[0],arr[1],[arr[2] substringToIndex:2]];
//                }
//            }
            self.createDate = str;
        }
        
        if ([json[@"type"] isKindOfClass:[NSNull class]] || !json[@"type"])
        {
            self.type = @"";
        }
        else
        {
            self.type = json[@"type"];
        }
        
        if ([json[@"pic"] isKindOfClass:[NSNull class]] || !json[@"pic"])
        {
            self.pic = @"";
        }
        else
        {
            self.pic = json[@"pic"];
        }
    }
    return self;
}

+(id)messageListWithJSON:(NSDictionary*)json
{
    return [[self alloc]initWithJSON:json];
}


@end
