//
//  ExamineTool.h
//  华康通
//
//  Created by leiyu on 16/7/13.
//  Copyright © 2016年 华康集团. All rights reserved.
//

//一个检验身份证、邮箱、手机号码的类
#import <Foundation/Foundation.h>

@interface ExamineTool : NSObject

//邮箱正确表达式验证
+ (BOOL)isValidateEmail:(NSString *)email;
//验证身份证是否填写正确
+ (BOOL)verifyIDCardNumber:(NSString *)value;
//根据身份证号获取生日
+ (NSString *)birthdayStrFromIdentityCard:(NSString *)numberStr;
//根据身份证号性别
+ (NSString *)getIdentityCardSex:(NSString *)numberStr;
//验证手机号码是否正确
+ (BOOL)verityPhoneWithString:(NSString*)str;
//验证用户登录和设置密码的合法性
+ (BOOL)verityKeyWithString:(NSString*)str;
//是否为纯数字
+ (BOOL)isPureNumandCharacters:(NSString *)string;
//是否为纯字母
+ (BOOL)PureLetters:(NSString*)str;
//验证是否为正确的工号
+ (BOOL)isUserCodeWithStr:(NSString*)str;

@end
