//
//  AboutUSController.m
//  华康通
//
//  Created by 雷雨 on 16/5/15.
//  Copyright © 2016年 华康集团. All rights reserved.
//

#import "AboutUSController.h"
#import "AboutUSDetailController.h"
#import "UMMobClick/MobClick.h"

@interface AboutUSController ()

@property (nonatomic, strong) NSArray* titles;
@property (nonatomic, strong) NSDictionary* urlDic;
@property (nonatomic, strong) UIView* myView;
@end

@implementation AboutUSController
- (UIView*)myView
{
    if (!_myView)
    {
        _myView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
        _myView.backgroundColor = [UIColor colorWithRed:246.0/255.0 green:246.0/255.0 blue:246.0/255.0 alpha:1.0];
    }
    return _myView;
}
- (NSDictionary*)urlDic
{
    if (!_urlDic)
    {
        _urlDic = @{@"0":@"http://u.eqxiu.com/s/3Tgs7twD",
                    @"1":@"https://v.qq.com/x/page/q0388cgy5z2.html",
                    @"2":@"http://cd.webportal.cc/v/1jrcdZ36/?slv=0&sid=&from=groupmessage&isappinstalled=0",
                    @"3":@"http://h5.eqxiu.com/s/DrgRHHfN",
                    @"4":@"http://c.eqxiu.com/s/FlHEWOO3",
                    @"5":@"http://v.qq.com/page/n/d/t/n0197lccqdt.html"};
    }
    return _urlDic;
}
- (NSArray*)titles
{
    if (!_titles)
    {
        _titles = @[@"公司简介",@"华康最新企业宣传片（视频版）",@"华康最新企业宣传片",@"华康分公司风采展示",@"华康保险超市合作供应商",@"华康通介绍"];
    }
    return _titles;
}
-(void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"关于我们";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:LoadImage(@"back1") style:UIBarButtonItemStylePlain target:self action:@selector(popRootVC)];
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"AboutUSController"];//("PageOne"为页面名称，可自定义)
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"AboutUSController"];
}

- (void)popRootVC
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidAppear:(BOOL)animated
{
    [self updateDataForTableView];
}
- (void)updateDataForTableView
{
    [self.myView removeFromSuperview];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.titles.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1.0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.textLabel.text = self.titles[indexPath.row];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    AboutUSDetailController* aboutUSDetailVC = [[AboutUSDetailController alloc]initWithNibName:@"AboutUSDetailController" bundle:nil];
    aboutUSDetailVC.url = self.urlDic[[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
    aboutUSDetailVC.title = self.titles[indexPath.row];
    [self.navigationController pushViewController:aboutUSDetailVC animated:YES];
}

@end
