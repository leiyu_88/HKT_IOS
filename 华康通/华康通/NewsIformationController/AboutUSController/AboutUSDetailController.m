//
//  AboutUSDetailController.m
//  华康通
//
//  Created by 雷雨 on 16/5/16.
//  Copyright © 2016年 华康集团. All rights reserved.
//

#import "AboutUSDetailController.h"
#import "SVProgressHUD.h"
#import "UMMobClick/MobClick.h"

@interface AboutUSDetailController ()<UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *myWebView;


@end

@implementation AboutUSDetailController

- (void)viewDidAppear:(BOOL)animated
{
    [self updateDataForTableView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"AboutUSDetailController"];//("PageOne"为页面名称，可自定义)
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"AboutUSDetailController"];
}

//重新加载数据
- (void)updateData
{
    [self updateDataForTableView];
}

- (void)updateDataForTableView
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
//    ReachabilityStatus* status = [ReachabilityStatus reachability];
//    if (status.isReach)
//    {
        NSLog(@"网络连接正常....");
        [SVProgressHUD showInfoWithStatus:@""];
//        [self.reachView removeFromSuperview];
        NSURL* url = [NSURL URLWithString:self.url];
        NSLog(@"%@",self.url);
        NSURLRequest* request = [NSURLRequest requestWithURL:url];
        [self.myWebView loadRequest:request];
//    }
//    else
//    {
//        NSLog(@"网络不正常...");
//        [self.view addSubview:self.reachView];
//    }
//    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:LoadImage(@"back1") style:UIBarButtonItemStylePlain target:self action:@selector(popRootVC)];
    self.myWebView.delegate = self;
    
}
- (void)popRootVC
{
    [self.navigationController popViewControllerAnimated:YES];
}
//在webview完成加载的时候
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    //
    [SVProgressHUD dismiss];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (void)viewDidDisappear:(BOOL)animated
{
    //    [self.timer invalidate];
    [SVProgressHUD dismiss];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

@end
