//
//  AboutUSDetailController.h
//  华康通
//
//  Created by 雷雨 on 16/5/16.
//  Copyright © 2016年 华康集团. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutUSDetailController : UIViewController

@property (nonatomic, strong) NSString* url;

@end
