//
//  TRNews.h
//  华康通
//
//  Created by leiyu on 15/7/21.
//  Copyright (c) 2015年 华康集团. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TRNews : NSObject

@property (nonatomic,strong) NSString* imageInformation;
@property (nonatomic,strong) NSString* titleInformation;
@property (nonatomic,strong) NSString* detailInformation;
@property (nonatomic,strong) NSString* smallTitleInformation;
@property (nonatomic,strong) NSString* createDate;

+(id)newsWithJSON:(NSDictionary*)json;

@end
