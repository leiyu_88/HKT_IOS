//
//  TRNews.m
//  华康通
//
//  Created by leiyu on 15/7/21.
//  Copyright (c) 2015年 华康集团. All rights reserved.
//

#import "TRNews.h"

@implementation TRNews

-(id)initWithJSON:(NSDictionary*)json
{
    if (self=[super init])
    {
        if ([json[@"introduce"] isKindOfClass:[NSNull class]] || !json[@"introduce"])
        {
            self.detailInformation=@"无";
        }
        else
        {
            self.detailInformation=json[@"introduce"];
        }
        if ([json[@"pic"] isKindOfClass:[NSNull class]] || !json[@"pic"])
        {
            self.imageInformation=@"";
        }
        else
        {
            self.imageInformation=json[@"pic"];
        }
        if ([json[@"title"] isKindOfClass:[NSNull class]] || !json[@"title"])
        {
            self.titleInformation=@"无";
        }
        else
        {
            self.titleInformation=json[@"title"];
        }
        if ([json[@"id"] isKindOfClass:[NSNull class]] || !json[@"id"])
        {
            self.smallTitleInformation=@"无";
        }
        else
        {
            self.smallTitleInformation=json[@"id"];
        }
        if ([json[@"createDate"] isKindOfClass:[NSNull class]] || !json[@"createDate"])
        {
            self.createDate=@"无";
        }
        else
        {
            self.createDate=json[@"createDate"];
        }
    }
    return self;
}

+(id)newsWithJSON:(NSDictionary*)json
{
    return [[self alloc]initWithJSON:json];
}


@end
