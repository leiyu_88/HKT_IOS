//
//  NewsIformationDetailController.h
//  华康通
//
//  Created by user on 16/6/23.
//  Copyright © 2016年 华康集团. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UMMobClick/MobClick.h"

@interface NewsIformationDetailController : UIViewController

@property (nonatomic, strong) NSString* url;
@property (nonatomic, unsafe_unretained) BOOL isPush;

@end
