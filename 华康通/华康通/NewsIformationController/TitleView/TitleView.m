//
//  TitleView.m
//  华康通
//
//  Created by 雷雨 on 16/4/19.
//  Copyright © 2016年 华康集团. All rights reserved.
//

#import "TitleView.h"

@implementation TitleView

- (NSMutableArray*)buttons
{
    if (!_buttons)
    {
        _buttons = [NSMutableArray array];
    }
    return _buttons;
}
- (NSMutableArray*)allButtons
{
    if (!_allButtons)
    {
        _allButtons = [NSMutableArray array];
    }
    return _allButtons;
}

//更新表格
- (void)changeData:(UIButton*)sender
{
    for (UIView* btn in self.buttons)
    {
        if (btn.tag == sender.tag)
        {
            btn.hidden = NO;
        }
        else
        {
            btn.hidden = YES;
            
        }
    }
    [[NSNotificationCenter defaultCenter]postNotificationName:@"buttonChange3" object:self userInfo:@{@"buttonTag":[NSString stringWithFormat:@"%ld",(long)sender.tag],@"buttonArray":self.buttons}];
}
- (void)scrollView
{
    self.myScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    self.myScrollView.contentSize = CGSizeMake(self.buttonTitles.count * 100, self.frame.size.height);
    self.myScrollView.userInteractionEnabled = YES;
    self.myScrollView.bounces = YES;
    self.myScrollView.scrollEnabled = YES;
    self.myScrollView.pagingEnabled = NO;
    self.myScrollView.showsHorizontalScrollIndicator = NO;
    self.myScrollView.showsVerticalScrollIndicator = NO;
    for (int i = 0; i<self.buttonTitles.count; i++)
    {
        UIButton* button = [UIButton buttonWithType:UIButtonTypeSystem];
        [button setTitle:self.buttonTitles[i] forState:UIControlStateNormal];
        [button setTitleColor:FFFFFFColor forState:UIControlStateNormal];
        button.frame = CGRectMake(100 * i, 0, 100, self.myScrollView.frame.size.height);
        [button addTarget:self action:@selector(changeData:) forControlEvents:UIControlEventTouchUpInside];
        button.tag = i;
        UIView* view1 = [[UIView alloc]initWithFrame:CGRectMake(0, button.frame.size.height - 3, 100, 2)];
        view1.tag = i;
        view1.hidden = YES;
        view1.backgroundColor = FFFFFFColor;
        [self.buttons addObject:view1];
        [button addSubview:view1];
        if (i == 0)
        {
            view1.hidden = NO;
        }
//        UIView* view2 = [[UIView alloc]initWithFrame:CGRectMake(button.frame.size.width - 0.5, 13, 0.5, button.frame.size.height - 26)];
//        view2.backgroundColor = FFFFFFColor;
//        [button addSubview:view2];
        [self.allButtons addObject:button];
        [self.myScrollView addSubview:button];
    }
    //        NSLog(@"%@",self.buttons);
    [self addSubview:self.myScrollView];
}
- (void)removeScrollView
{
    [self.myScrollView removeFromSuperview];
}
- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        self.backgroundColor = KMainColor;
    }
    return self;
}

@end
