//
//  VidoCollectionController.m
//  华康通
//
//  Created by leiyu on 16/4/18.
//  Copyright © 2016年 华康集团. All rights reserved.
//

#import "VidoCollectionController.h"
#import "VidoCollectionCell.h"
#import "VidoDetailController.h"
#import "PlayVidoController.h"
#import "PPTView.h"


@interface VidoCollectionController ()<UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (nonatomic, strong) UITableView* tableView;
@property (nonatomic, strong) UIImageView* myImageView;
@property (nonatomic, strong) PPTView* pptView;
//播放图标
@property (nonatomic, strong) UIButton* playImageView;
//播放图标的背景图
@property (nonatomic, strong) UIView* backView;
//表视图上的头视图
@property (nonatomic, strong) UIView* headerView;
//是否为视频课件
@property (nonatomic, unsafe_unretained) BOOL isVido;

//列表数组
@property (nonatomic, strong) NSArray* lists;
//记录ppt 按钮的下标
@property (nonatomic, unsafe_unretained) NSInteger index;


@end

@implementation VidoCollectionController

- (UIView*)headerView
{
    if (!_headerView)
    {
        _headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 150)];
        _headerView.backgroundColor = FFFFFFColor;
    }
    return _headerView;
}
- (UIView*)backView
{
    if (!_backView)
    {
        _backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 150)];
        _backView.backgroundColor = YYColor;
        _backView.alpha = 0.5;
    }
    return _backView;
}
- (PPTView*)pptView
{
    if (!_pptView)
    {
        _pptView = [[PPTView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50)];
    }
    return _pptView;
}
- (UIButton*)playImageView
{
    if (!_playImageView)
    {
        _playImageView = [[UIButton alloc]init];
        _playImageView.frame = CGRectMake(([UIScreen mainScreen].bounds.size.width - 70)/2, (150 - 70)/2, 70, 70);
        [_playImageView setImage:LoadImage(@"playvidoe") forState:UIControlStateNormal];
        [_playImageView setTitle:@"" forState:UIControlStateNormal];
        [_playImageView addTarget:self action:@selector(showVido) forControlEvents:UIControlEventTouchUpInside];
    }
    return _playImageView;
}
- (UIImageView*)myImageView
{
    if (!_myImageView)
    {
        _myImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 150)];
        _myImageView.image = LoadImage(@"vido01.png");
    }
    return _myImageView;
}
- (UITableView*)tableView
{
    if (!_tableView)
    {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - 64) style:UITableViewStylePlain];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor colorWithRed:246.0/255.0 green:246.0/255.0 blue:246.0/255.0 alpha:1.0];
    }
    return _tableView;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.lists = [VidoDic vidos];
    self.index = 0;
    [self.view addSubview:self.tableView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(PPTButton:) name:@"PPTButton" object:nil];
    self.isVido = YES;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:LoadImage(@"back1") style:UIBarButtonItemStylePlain target:self action:@selector(popRootVC)];
    [self.tableView registerNib:[UINib nibWithNibName:@"VidoCollectionCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    [self.headerView addSubview:self.myImageView];
    [self.headerView addSubview:self.backView];
    [self.headerView addSubview:self.playImageView];
    self.tableView.tableHeaderView = self.headerView;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"VidoCollectionController"];//("PageOne"为页面名称，可自定义)
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"VidoCollectionController"];
}

- (void)popRootVC
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)showVido
{
    PlayVidoController* playtVidoVC = [[PlayVidoController alloc]initWithNibName:@"PlayVidoController" bundle:nil];
    playtVidoVC.isVido = YES;
    playtVidoVC.title = @"与保险相关的法律（完整版）";
    playtVidoVC.url = @"http://v.qq.com/page/s/j/l/s0306j8agjl.html";
    [self.navigationController pushViewController:playtVidoVC animated:YES];
}
//ppt课件的分类
- (void)PPTButton:(NSNotification*)notification
{
    NSInteger index = [notification.userInfo[@"buttonTag"] integerValue];
    NSLog(@"index = %ld",(long)index);
    self.index = index;
    self.lists = [VidoDic ppt][[NSString stringWithFormat:@"%ld",(long)self.index]];
    //刷新表格
    [self.tableView reloadData];
}

- (IBAction)changeVidos:(UISegmentedControl *)sender
{
    if (sender.selectedSegmentIndex == 0)
    {
        //精品视频
        self.isVido = YES;
        [self.pptView removeFromSuperview];
        [self.headerView addSubview:self.myImageView];
        [self.headerView addSubview:self.backView];
        [self.headerView addSubview:self.playImageView];
        self.tableView.tableHeaderView = self.headerView;
        self.lists = [VidoDic vidos];
        [self.tableView reloadData];
    }
    else
    {
        //ppt课件
        self.isVido = NO;
        [self.myImageView removeFromSuperview];
        [self.playImageView removeFromSuperview];
        [self.backView removeFromSuperview];
        [self.headerView removeFromSuperview];
        self.tableView.tableHeaderView = self.pptView;
        self.pptView.buttonTitles = @[@"险种分析",@"营销技巧",@"早会经营",@"增员专题"];
        [self.pptView scrollView];
        self.lists = [VidoDic ppt][[NSString stringWithFormat:@"%ld",(long)self.index]];
        [self.tableView reloadData];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.lists.count;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90.0;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    VidoCollectionCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.myImageView.layer.borderColor = [UIColor colorWithRed:248.0/255.0 green:248.0/255.0 blue:248.0/255.0 alpha:1.0].CGColor;
    cell.myImageView.layer.borderWidth = 1.0;
    cell.topLabel.text = self.lists[indexPath.row][@"name"];
    cell.bottomLabel.text = self.lists[indexPath.row][@"detail"];
    cell.myImageView.image = LoadImage(self.lists[indexPath.row][@"image1"]);
    return cell;
}

//进入播放详情页面
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.isVido)
    {
        VidoDetailController* vidoDetailVC = [[VidoDetailController alloc]initWithNibName:@"VidoDetailController" bundle:nil];
        vidoDetailVC.name = self.lists[indexPath.row][@"name"];
        vidoDetailVC.detail = self.lists[indexPath.row][@"detail"];
        vidoDetailVC.isVido = self.isVido;
        vidoDetailVC.url = self.lists[indexPath.row][@"url"];
        vidoDetailVC.imageName = self.lists[indexPath.row][@"image2"];
        vidoDetailVC.type = self.lists[indexPath.row][@"type"];
        vidoDetailVC.pptName = self.lists[indexPath.row][@"pptName"];
        [self.navigationController pushViewController:vidoDetailVC animated:YES];
    }
    else
    {
        PlayVidoController* playtVidoVC = [[PlayVidoController alloc]initWithNibName:@"PlayVidoController" bundle:nil];
        playtVidoVC.title = self.lists[indexPath.row][@"name"];
        playtVidoVC.url = self.lists[indexPath.row][@"url"];
        playtVidoVC.isVido = self.isVido;
        playtVidoVC.type = self.lists[indexPath.row][@"type"];
        playtVidoVC.pptName = self.lists[indexPath.row][@"pptName"];
        [self.navigationController pushViewController:playtVidoVC animated:YES];
    }
}

@end
