//
//  PPTView.m
//  华康通
//
//  Created by leiyu on 16/4/28.
//  Copyright © 2016年 华康集团. All rights reserved.
//

#import "PPTView.h"

@implementation PPTView

- (NSMutableArray*)buttons
{
    if (!_buttons)
    {
        _buttons = [NSMutableArray array];
    }
    return _buttons;
}
- (NSMutableArray*)allButtons
{
    if (!_allButtons)
    {
        _allButtons = [NSMutableArray array];
    }
    return _allButtons;
}

//更新表格
- (void)changeData:(UIButton*)sender
{
    for (UIView* btn in self.buttons)
    {
        if (btn.tag == sender.tag)
        {
            btn.hidden = NO;
        }
        else
        {
            btn.hidden = YES;
            
        }
    }
    [[NSNotificationCenter defaultCenter]postNotificationName:@"PPTButton" object:self userInfo:@{@"buttonTag":[NSString stringWithFormat:@"%ld",(long)sender.tag],@"buttonArray":self.buttons}];
}
- (void)scrollView
{
    self.myScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(5, 5, [UIScreen mainScreen].bounds.size.width - 10, self.frame.size.height - 10)];
    self.myScrollView.backgroundColor = [UIColor colorWithRed:230.0/255.0 green:230.0/255.0  blue:230.0/255.0  alpha:1.0];
    self.myScrollView.contentSize = CGSizeMake(self.buttonTitles.count * (([UIScreen mainScreen].bounds.size.width - 10)/4), self.frame.size.height - 10);
    self.myScrollView.userInteractionEnabled = YES;
    self.myScrollView.bounces = YES;
    self.myScrollView.scrollEnabled = YES;
    self.myScrollView.pagingEnabled = NO;
    self.myScrollView.showsHorizontalScrollIndicator = NO;
    self.myScrollView.showsVerticalScrollIndicator = NO;
    for (int i = 0; i<self.buttonTitles.count; i++)
    {
        UIButton* button = [UIButton buttonWithType:UIButtonTypeSystem];
        [button setTitle:self.buttonTitles[i] forState:UIControlStateNormal];
        [button setTitleColor:YYColor forState:UIControlStateNormal];
        [button setBackgroundColor:ClEARColor];
        button.frame = CGRectMake((([UIScreen mainScreen].bounds.size.width - 10)/4) * i, 0,([UIScreen mainScreen].bounds.size.width - 10)/4, self.myScrollView.frame.size.height);
        [button addTarget:self action:@selector(changeData:) forControlEvents:UIControlEventTouchUpInside];
        button.tag = i;
        UIView* view1 = [[UIView alloc]initWithFrame:CGRectMake(0, button.frame.size.height - 4, ([UIScreen mainScreen].bounds.size.width - 10)/4, 3)];
        view1.tag = i;
        view1.hidden = YES;
        view1.backgroundColor = KMainColor;
        [self.buttons addObject:view1];
        [button addSubview:view1];
        if (i == 0)
        {
            view1.hidden = NO;
        }
        [self.allButtons addObject:button];
        [self.myScrollView addSubview:button];
    }
    //        NSLog(@"%@",self.buttons);
    [self addSubview:self.myScrollView];
}
- (void)removeScrollView
{
    [self.myScrollView removeFromSuperview];
}
- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        self.backgroundColor = [UIColor colorWithRed:246.0/255.0 green:246.0/255.0  blue:246.0/255.0  alpha:1.0];
    }
    return self;
}

@end
