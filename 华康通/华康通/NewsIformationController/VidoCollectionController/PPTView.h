//
//  PPTView.h
//  华康通
//
//  Created by leiyu on 16/4/28.
//  Copyright © 2016年 华康集团. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PPTView : UIView
@property (nonatomic, strong) UIScrollView* myScrollView;
@property (nonatomic, strong) NSMutableArray* buttons;
@property (nonatomic, strong) NSArray* buttonTitles;
@property (nonatomic, strong) NSMutableArray* allButtons;

- (void)scrollView;
- (void)removeScrollView;

@end
