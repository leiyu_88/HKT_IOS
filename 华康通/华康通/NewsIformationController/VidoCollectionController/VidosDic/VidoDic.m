//
//  VidoDic.m
//  华康通
//
//  Created by leiyu on 16/4/19.
//  Copyright © 2016年 华康集团. All rights reserved.
//

#import "VidoDic.h"

@implementation VidoDic

+ (NSArray*)vidos
{
    return @[@{@"name":@"与保险相关的法律（完整版）",@"url":@"http://v.qq.com/page/s/j/l/s0306j8agjl.html",@"detail":@"随着人们的生活水平提升和保险意识逐渐增加，请大胆买保险，因为法律会保护你！",@"image1":@"vido1.png",@"image2":@"vido01.png"},@{@"name":@"车险知识（完整版）",@"url":@"http://v.qq.com/page/e/l/b/e0306kwdzlb.html",@"detail":@"买车的人越来越多，不少车主在为自己的爱车选择保险时往往一头雾水。究竟应该如何选择最适合自己的车险产品呢？",@"image1":@"vido2.png",@"image2":@"vido02.png"},@{@"name":@"为什么保险是人人为我，我为人人",@"url":@"http://v.qq.com/page/o/m/o/o0194ktazmo.html",@"detail":@"人们为了分担风险而发明了保险，可以说保险的初衷就是人人为我，我为人人。当前由于保险业的发展保险的功能有了很大的扩展保险也成为了一种稳健的理财方式。",@"image1":@"vido3.png",@"image2":@"vido03.png"},@{@"name":@"原位癌的介绍",@"url":@"http://v.qq.com/page/b/b/k/b0194ltwkbk.html",@"detail":@"一般人都认为，得了癌症肯定就是大病，但保险条款中规定，原位癌患者不在赔偿范围之内。实际上，原位癌是癌症的最早期，原位癌患者要占到所有癌症患者的绝大部分。原位癌是癌的最早期，此时如手术切除即可完全治愈。",@"image1":@"vido4.png",@"image2":@"vido04.png"},@{@"name":@"定期寿险优缺点",@"url":@"http://v.qq.com/page/y/o/f/y019446dwof.html",@"detail":@"定期寿险是指在保险合同约定的期间内，如果被保险人死亡或全残，则保险公司按照约定的保险金额给付保险金；若保险期限届满被保险人健在，则保险合同自然终止，保险公司不再承担保险责任，并且不退回保险费。定期寿险优缺点分析。",@"image1":@"vido5.png",@"image2":@"vido05.png"},@{@"name":@"保险合同是什么样的合同",@"url":@"http://v.qq.com/page/b/8/l/b0194qy2j8l.html",@"detail":@"保险合同属于典型的格式合同，是投保人与保险人之间设立、变更、终止保险法律关系的协议。这是一份很重要的法律文书，它记载了投保人和保险公司各自的权利和义务，直接关系到保险所能给予的保障程度。",@"image1":@"vido6.png",@"image2":@"vido06.png"},@{@"name":@"保险公司收到保费去哪",@"url":@"http://v.qq.com/page/z/t/c/z0194ho3vtc.html",@"detail":@"广大的消费者把保险费交给了保险公司，保险公司到底拿这笔钱干嘛用了呢？让我们一起了解一下。",@"image1":@"vido7.png",@"image2":@"vido07.png"}];
}

+ (NSDictionary*)ppt
{
    return @{@"0":@[@{@"name":@"31家保险公司重疾险产品汇总",@"url":@"https://wap.kangbaotong.cn/images/sale/classroom/risk/20190129163101.pdf",@"detail":@"汇总市面主流保险公司的重疾险",@"image2":@"xzfx0001.png",@"image1":@"xzfx1.png",@"pptName":@"pdf01",@"type":@"pdf"},
                    @{@"name":@"18家保险公司年金险产品汇总",@"url":@"https://wap.kangbaotong.cn/images/sale/classroom/risk/20190129163102.pdf",@"detail":@"公司全、产品齐、分红受益比较",@"image2":@"xzfx0002.png",@"image1":@"xzfx2.png",@"pptName":@"pdf02",@"type":@"pdf"},
                    @{@"name":@"7家保险公司百万医疗险产品汇总",@"url":@"https://wap.kangbaotong.cn/images/sale/classroom/risk/20190129163103.pdf",@"detail":@"汇总市面主流保险公司百万医疗产品",@"image2":@"xzfx0003.png",@"image1":@"xzfx3.png",@"pptName":@"pdf03",@"type":@"pdf"},
                    @{@"name":@"10家保险产品意外险产品汇总",@"url":@"https://wap.kangbaotong.cn/images/sale/classroom/risk/20190129163104.pdf",@"detail":@"广泛收集市场意外险，多方位对比",@"image2":@"xzfx0004.png",@"image1":@"xzfx4.png",@"pptName":@"pdf04",@"type":@"pdf"},
                    @{@"name":@"11家保险公司终身寿险产品汇总",@"url":@"https://wap.kangbaotong.cn/images/sale/classroom/risk/20190129163105.pdf",@"detail":@"高净值人群主推产品",@"image2":@"xzfx0005.png",@"image1":@"xzfx5.png",@"pptName":@"pdf05",@"type":@"pdf"}
                    ],
             @"1":@[@{@"name":@"养老险销售逻辑",@"url":@"https://dev.wap.kangbaotong.cn/images/sale/classroom/yxjq/20190129122603.pdf",@"detail":@"养老保险是每个人都需要的，高端客户也不例外，高端客户也是普通人，叧要真诚地为他着想，其实也很容易沟通。",@"image1":@"2ppt1.png",@"image2":@"2ppt001.png",@"pptName":@"ppt09",@"type":@"pdf"},
                   @{@"name":@"绩优分享客户经营五步走",@"url":@"https://dev.wap.kangbaotong.cn/images/sale/classroom/yxjq/20190129122602.pdf",@"detail":@"做到客户经营5个步骤，所有的客户经营动作都做要做到前面，而且要做到位，签单自然水到渠成，加保更是易如反掌。",@"image1":@"2ppt2.png",@"image2":@"2ppt002.png",@"pptName":@"ppt10",@"type":@"pdf"},
                   @{@"name":@"十种类型客户的推销方法",@"url":@"https://dev.wap.kangbaotong.cn/images/sale/classroom/yxjq/20190129122601.pdf",@"detail":@"推销要达到成交的目的，就必须了解对方乐于接受什么样的推销方式，以便有的放矢、取得最佳效果。",@"image1":@"2ppt3.png",@"image2":@"2ppt003.png",@"pptName":@"ppt11",@"type":@"pdf"},
                   @{@"name":@"养老及退休的财务规划",@"url":@"https://dev.wap.kangbaotong.cn/images/sale/classroom/yxjq/20190129122605.pdf",@"detail":@"退休是为了保证个人在将来有一个自立、尊严、高品质的退休生活，而从现在开始积极实施的理财方案。一个科学合理的养老及退休的的财务计划的制定和执行，将会为人们幸福的晚年生活保驾护抗。而保险兼顾强制储蓄、长期理财、持续理财、抵御通胀、专款专用等优势，在退休财务规划中扮演重要角色。",@"image1":@"2ppt4.png",@"image2":@"2ppt004.png",@"pptName":@"ppt12",@"type":@"pdf"},
                    @{@"name":@"把客户带到营销职场的好处",@"url":@"https://dev.wap.kangbaotong.cn/images/sale/classroom/yxjq/20190129122604.pdf",@"detail":@"公司的职场有很多资源可以利用，对赢得客户的好感和信任非常有帮助。让客户对行业、公司和团队有更多的了解，对保险理念有深入理解，认识到保险营销是正当的专业的工作。尤其是一些优秀的团队或个人，在显眼的位置摆放各种荣誉旗帜或奖牌，更能让客户产生或增强信任感。",@"image1":@"2ppt5.png",@"image2":@"2ppt005.png",@"pptName":@"ppt13",@"type":@"pdf"}
                     ],
             @"2":@[@{@"name":@"如何有效运作成功分享",@"url":@"https://dev.wap.kangbaotong.cn/images/sale/classroom/zhjy/20190129122803.pdf",@"detail":@"我们平常在早会上是如何进行成功分享？分步骤进行，随时调整，方能达到事半功倍的效果。",@"image1":@"3ppt1.png",@"image2":@"3ppt001.png",@"pptName":@"ppt14",@"type":@"pdf"},
                    @{@"name":@"四个故事告诉你什么是保险",@"url":@"https://dev.wap.kangbaotong.cn/images/sale/classroom/zhjy/20190129122802.pdf",@"detail":@"或许这只是一个故事,或者这只是一个传说,但无论如何,我知道生活中确实时时处处潜藏着许多风险……同时,这也就是保险保障的作用之一",@"image1":@"3ppt2.png",@"image2":@"3ppt002.png",@"pptName":@"ppt15",@"type":@"pdf"},
                    @{@"name":@"准客户开拓记录客户身边人",@"url":@"https://dev.wap.kangbaotong.cn/images/sale/classroom/zhjy/20190129122801.pdf",@"detail":@"记录，贵在坚持。市场上到处都有我们的潜在客户，但机会只留给有记录的人。",@"image1":@"3ppt3.png",@"image2":@"3ppt003.png",@"pptName":@"ppt16",@"type":@"pdf"},
                    @{@"name":@"保险需求最高的五类人群",@"url":@"https://dev.wap.kangbaotong.cn/images/sale/classroom/zhjy/20190129122805.pdf",@"detail":@"普通工人、职业女性等五类人群，在工作和生活中都扮演不同的角色。为什么他们都需要为自己做好保障计划，以免因为自己生病或是发生意外而影响到整个家庭。",@"image1":@"3ppt4.png",@"image2":@"3ppt004.png",@"pptName":@"ppt17",@"type":@"pdf"},
                    @{@"name":@"6个小故事看明白让你成为真正的销售高手",@"url":@"https://dev.wap.kangbaotong.cn/images/sale/classroom/zhjy/20190129122804.pdf",@"detail":@"这也许不光是一种销售的手法，更是一种让我们的工作生活充满创意的思考方式！人的观念没有什么不可改变，关键是角度，要善于揣摩客户心理。深入挖掘客户的需求，从而轻松成交客户。",@"image1":@"3ppt5.png",@"image2":@"3ppt005.png",@"pptName":@"ppt18",@"type":@"pdf"}
                    ],
             
             @"3":@[@{@"name":@"从奔跑的力量谈组织发展增员",@"url":@"https://dev.wap.kangbaotong.cn/images/sale/classroom/zyzt/20190129123101.pdf",@"detail":@"奔跑就是机会，任何一个行业都是奔跑29天幸福最后一天。时刻穿好你的跑鞋增员成功就在前方。",@"image1":@"4ppt1.png",@"image2":@"4ppt001.png",@"pptName":@"ppt19",@"type":@"pdf"},
                    @{@"name":@"增员心态分享",@"url":@"https://dev.wap.kangbaotong.cn/images/sale/classroom/zyzt/20190129123103.pdf",@"detail":@"面对熟人不理解，还有陌生人的拒绝，增员经常碰壁。如果找到了方法，就不会太难。而增员就是在拜师学艺，也是在完善自己。",@"image1":@"4ppt3.png",@"image2":@"4ppt003.png",@"pptName":@"ppt21",@"type":@"pdf"},
                    @{@"name":@"名人明星保险人做保险的七大理由",@"url":@"https://dev.wap.kangbaotong.cn/images/sale/classroom/zyzt/20190129123102.pdf",@"detail":@"名人也有加入保险大家族。保险是一个充满机会与挑战、前程似锦的朝阳产业。免巨额创业资金，低风险、高回报，简简单单圆你老板梦，成就属于自己一生的事业。保险是自己决定工作时间、工作强度、服务对象、收入水平的自由行业。",@"image1":@"4ppt4.png",@"image2":@"4ppt004.png",@"pptName":@"ppt22",@"type":@"pdf"}
                     ]
             };
}

@end
