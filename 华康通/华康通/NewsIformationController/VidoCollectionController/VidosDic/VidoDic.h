//
//  VidoDic.h
//  华康通
//
//  Created by leiyu on 16/4/19.
//  Copyright © 2016年 华康集团. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VidoDic : NSObject

//保险视频信息
+ (NSArray*)vidos;
//保险ppt教程
+ (NSDictionary*)ppt;

@end
