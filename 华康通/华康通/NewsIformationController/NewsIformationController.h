//
//  NewsIformationController.h
//  华康通
//
//  Created by 雷雨 on 16/4/19.
//  Copyright © 2016年 华康集团. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UMMobClick/MobClick.h"

@interface NewsIformationController : UITableViewController

@property (nonatomic, strong) NSString* LoginId;
@property (nonatomic, strong) NSString* AgentCode;
//LoginCode
@property (nonatomic, strong) NSString* LoginCode;

@end
