//
//  PlayVidoController.h
//  华康通
//
//  Created by leiyu on 16/4/18.
//  Copyright © 2016年 华康集团. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UMMobClick/MobClick.h"


@interface PlayVidoController : UIViewController

@property (nonatomic, strong) NSString* url;
@property (nonatomic, unsafe_unretained) BOOL isVido;
@property (nonatomic, strong) NSString* type;
@property (nonatomic, strong) NSString* pptName;

@end
