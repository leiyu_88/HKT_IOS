//
//  VidoDetailController.h
//  华康通
//
//  Created by leiyu on 16/4/18.
//  Copyright © 2016年 华康集团. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UMMobClick/MobClick.h"

@interface VidoDetailController : UIViewController

@property (nonatomic, strong) NSString* url;
@property (nonatomic, strong) NSString* detail;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, unsafe_unretained) BOOL isVido;
@property (nonatomic, strong) NSString* imageName;
//文件类型
@property (nonatomic, strong) NSString* type;
//文件名
@property (nonatomic, strong) NSString* pptName;

@end
