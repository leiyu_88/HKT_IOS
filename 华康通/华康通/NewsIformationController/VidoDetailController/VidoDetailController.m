//
//  VidoDetailController.m
//  华康通
//
//  Created by leiyu on 16/4/18.
//  Copyright © 2016年 华康集团. All rights reserved.
//

#import "VidoDetailController.h"
//播放外链视频网用到的库文件
#import <MediaPlayer/MediaPlayer.h>
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>
#import "PlayVidoController.h"

@interface VidoDetailController ()
//视频快照图
@property (strong, nonatomic) IBOutlet UIImageView *vidoImageView;
//黑色有透明的视图
@property (strong, nonatomic) IBOutlet UIView *myView;
//视频标题
@property (strong, nonatomic) IBOutlet UILabel *myLabel;
//播放按钮图标
@property (strong, nonatomic) IBOutlet UIImageView *playImageView;
//视频简介内容
@property (strong, nonatomic) IBOutlet UITextView *textView;
//播放按钮
@property (strong, nonatomic) IBOutlet UIButton *playButton;

@end

@implementation VidoDetailController
//点击播放
- (IBAction)play:(id)sender
{
    PlayVidoController* playtVidoVC = [[PlayVidoController alloc]initWithNibName:@"PlayVidoController" bundle:nil];
    playtVidoVC.title = self.myLabel.text;
    playtVidoVC.url = self.url;
    playtVidoVC.isVido = self.isVido;
    playtVidoVC.type = self.type;
    playtVidoVC.pptName = self.pptName;
    [self.navigationController pushViewController:playtVidoVC animated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:LoadImage(@"back1") style:UIBarButtonItemStylePlain target:self action:@selector(popRootVC)];
    self.title = self.name;
    self.vidoImageView.image = LoadImage(self.imageName);
    self.textView.text = self.detail;
    self.myLabel.text = self.name;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"VidoDetailController"];//("PageOne"为页面名称，可自定义)
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"VidoDetailController"];
}

- (void)popRootVC
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
