//
//  PlayVidoController.m
//  华康通
//
//  Created by leiyu on 16/4/18.
//  Copyright © 2016年 华康集团. All rights reserved.
//

#import "PlayVidoController.h"
#import "SVProgressHUD.h"

@interface PlayVidoController ()<UIWebViewDelegate>
@property (strong, nonatomic) IBOutlet UIWebView *myWebView;
////网络连不上的view
//@property (nonatomic, strong) ReachView* reachView;

@end

@implementation PlayVidoController


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self updateDataForTableView];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:LoadImage(@"back1") style:UIBarButtonItemStylePlain target:self action:@selector(popRootVC)];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"PlayVidoController"];//("PageOne"为页面名称，可自定义)
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"PlayVidoController"];
}

- (void)popRootVC
{
    [self.navigationController popViewControllerAnimated:YES];
}

//重新加载数据
- (void)updateData
{
    [self updateDataForTableView];
}

- (void)updateDataForTableView
{
    if (self.isVido)
    {
        [SVProgressHUD showInfoWithStatus:@""];
        NSURL* url = [NSURL URLWithString:self.url];
        NSURLRequest* request = [NSURLRequest requestWithURL:url];
        [self.myWebView loadRequest:request];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    }
    else
    {
        [SVProgressHUD showInfoWithStatus:@""];
        NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.url]];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        [self.myWebView loadRequest:request];
    }
}

//在webview完成加载的时候
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [SVProgressHUD dismiss];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [SVProgressHUD dismiss];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

@end
