//
//  UIFont+SelfFont.m
//  习本课堂
//
//  Created by leiyu on 16/9/8.
//  Copyright © 2016年 亮信科技. All rights reserved.
//

#import "UIFont+SelfFont.h"

@implementation UIFont (SelfFont)

//
+ (UIFont *)getFontHiraginoSansGBWith:(CGFloat)size
{
    return [UIFont fontWithName:@"PingFang SC" size:size];
}
//
+ (UIFont *)getFontFZShaoErM11SWith:(CGFloat)size
{
    return [UIFont boldSystemFontOfSize:14.0];
}

//
+ (UIFont *)getFontJTWithSize:(CGFloat)size
{
    return [UIFont fontWithName:@"PingFang SC Semibold" size:size];
}

@end
