//
//  UIFont+SelfFont.h
//  习本课堂
//
//  Created by leiyu on 16/9/8.
//  Copyright © 2016年 亮信科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (SelfFont)

//东青字体(W3)东青黑色简体中文
+ (UIFont *)getFontHiraginoSansGBWith:(CGFloat)size;
//其它(fangzhengshaoer)方正楷体简体
+ (UIFont *)getFontFZShaoErM11SWith:(CGFloat)size;
//楷体简体
+ (UIFont *)getFontJTWithSize:(CGFloat)size;

@end
