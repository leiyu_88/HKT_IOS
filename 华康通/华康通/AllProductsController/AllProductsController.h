//
//  AllProductsController.h
//  华康通
//
//  Created by leiyu on 16/4/29.
//  Copyright © 2016年 华康集团. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AllProductsController : UITableViewController

//是否首页搜索页面进来的
@property (nonatomic, unsafe_unretained) BOOL isFirst;
//是否首页平安产品进来的
@property (nonatomic, unsafe_unretained) BOOL isPingAn;
//产品的product_detailtype_id细分
@property (nonatomic, strong) NSString* product_detailtype_id;
//品牌细分的brand_id(有产品细分id 就没有品牌id)
@property (nonatomic, strong) NSString* brand_id;

@end
