//
//  QKCSTopView.m
//  华康通
//
//  Created by  雷雨 on 2017/11/29.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import "QKCSTopView.h"

@implementation QKCSTopView

+ (id)view
{
    return [[[NSBundle mainBundle]loadNibNamed:@"QKCSTopView" owner:nil options:nil]lastObject];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self=[super initWithCoder:aDecoder])
    {
        self.autoresizingMask = UIViewAutoresizingNone;
    }
    return self;
}

@end
