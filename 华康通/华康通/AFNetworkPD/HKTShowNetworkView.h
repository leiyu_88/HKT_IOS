//
//  HKTShowNetworkView.h
//  华康通
//
//  Created by  雷雨 on 2018/11/14.
//  Copyright © 2018年 com.huakang. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef void(^httpRequestNoNetworking) ();
typedef void(^httpRequestYesNetworking) ();
//传入是哪种类型
typedef void(^clickButtonForChangeBlock) (NSInteger type);

@interface HKTShowNetworkView : NSObject

//要展示的措辞
@property (nonatomic,strong) NSString* title;
//图片文件名
@property (nonatomic,strong) NSString* imageStr;
//传入的参数，以便显示是什么类型
@property (nonatomic,assign) NSInteger type;
//传入frame
@property (nonatomic,assign) CGRect frame;
//传入控制器类
@property (nonatomic, strong) UIViewController* vc;


//创建
+ (HKTShowNetworkView*)shareInstanceWithTitle:(NSString*)title withImageStr:(NSString*)imageStr withType:(NSInteger)type withFrame:(CGRect)frame withVC:(UIViewController*)vc;

//判断是否有网络情况
- (void)showNetworkViewWithSuccess:(httpRequestYesNetworking)success failure:(httpRequestNoNetworking)failure click:(clickButtonForChangeBlock)click;

//没有显示网络视图的创建
+ (HKTShowNetworkView*)shareInstance;

//判断是否有网络情况
- (void)showErrorWithSuccess:(httpRequestYesNetworking)success failure:(httpRequestNoNetworking)failure;


@end

