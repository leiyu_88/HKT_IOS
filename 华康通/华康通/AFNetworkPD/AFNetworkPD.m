//
//  AFNetworkPD.m
//  精选平台
//
//  Created by  雷雨 on 2018/11/13.
//  Copyright © 2018年  雷雨. All rights reserved.
//

#import "AFNetworkPD.h"

@interface AFNetworkPD ()

@property (nonatomic, strong) CustomIOSAlertView *alertView;


@end

@implementation AFNetworkPD

+ (AFNetworkPD*)shareInstance
{
    static dispatch_once_t onceToken;
    static AFNetworkPD *pd;
    dispatch_once(&onceToken, ^
    {
        pd = [[self alloc] init];
    });
    return pd;
}

- (id)init
{
    if (self = [super init])
    {
        self.str = @"检测到您的设备没有连接网络。请点击'去开启网络'跳转到您手机设备的设置中心开启网络！";
    }
    return self;
}

//使用AFN框架来检测网络状态的改变
- (void)AFNReachability
{
    //1.创建网络监听管理者
    AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
    //2.监听网络状态的改变
    [manager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        switch (status) {
            case AFNetworkReachabilityStatusUnknown:
                NSLog(@"未知网络");
                if (self.alertView)
                {
                    [self.alertView close];
                }
                [self.alertView show];
                break;
            case AFNetworkReachabilityStatusNotReachable:
                NSLog(@"无网络");
                [self.alertView show];
                break;
            case AFNetworkReachabilityStatusReachableViaWWAN:
                NSLog(@"3G");
                [self dismissAlertView];
                break;
            case AFNetworkReachabilityStatusReachableViaWiFi:
                NSLog(@"WIFI");
                [self dismissAlertView];
                break;
            default:
                break;
        }
    }];
    //开始检测网络
    [manager startMonitoring];
}

- (void)showAlertViewWithTitle:(NSString*)str
{
    
}

- (CustomIOSAlertView*)alertView
{
    if (!_alertView)
    {
        _alertView = [[CustomIOSAlertView alloc] init];
        TextStatusView *customView  = [TextStatusView view];
        customView.frame = CGRectMake(10, 10, UISCREENWEITH - 80, ALERTVIEW_HEIGHT_B);
        customView.bottomLabel.text = self.str;
        [_alertView setContainerView:customView];
        [_alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"残忍拒绝",@"立即前往设置", nil]];
        [_alertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
            switch (buttonIndex)
            {
                case 0:
                    return ;
                    break;
                default:
                {
                    //弹出网络提示
                    [SetNetwork setNetwork];
                }
                    break;
            }
        }];
    }
    return _alertView;
}

- (void)dismissAlertView
{
    [self.alertView close];
}

//当类销毁的时候，关闭弹出框
- (void)dealloc
{
    [self.alertView close];
}

@end
