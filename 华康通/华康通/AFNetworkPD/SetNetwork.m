

#import "SetNetwork.h"

@implementation SetNetwork

//跳转到设置中心设置网络
+ (void)setNetwork
{
    //弹出网络提示
    NSString* SZStr = UIApplicationOpenSettingsURLString;
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:SZStr]])
    {
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 10.0)
        {
            //设备系统为IOS 10.0或者以上的
            if (@available(iOS 10.0, *)) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:SZStr] options:@{} completionHandler:^(BOOL success) {
                    nil;
                }];
            } else
            {
                // Fallback on earlier versions
            }
        }
        else
        {
            //设备系统为IOS 10.0以下的
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:SZStr]];
        }
    }
}

@end
