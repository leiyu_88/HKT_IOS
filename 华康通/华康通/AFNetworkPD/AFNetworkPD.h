//
//  AFNetworkPD.h
//  精选平台
//
//  Created by  雷雨 on 2018/11/13.
//  Copyright © 2018年  雷雨. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface AFNetworkPD : NSObject

@property (nonatomic, strong) NSString* str;

//监控网络状态
//使用AFN框架来检测网络状态的改变
+ (AFNetworkPD*)shareInstance;
//使用AFN框架来检测网络状态的改变
- (void)AFNReachability;


@end

NS_ASSUME_NONNULL_END
