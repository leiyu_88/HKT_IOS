//
//  HKTShowNetworkView.m
//  华康通
//
//  Created by  雷雨 on 2018/11/14.
//  Copyright © 2018年 com.huakang. All rights reserved.
//

#import "HKTShowNetworkView.h"

@interface HKTShowNetworkView()

@property (nonatomic, strong)ShowNoNetworkView* workView;

@end

@implementation HKTShowNetworkView


- (ShowNoNetworkView*)workView
{
    if (!_workView)
    {
        _workView = [[ShowNoNetworkView alloc]initWithFrame:self.frame];
        _workView.labelText = @"产品待开发中...";
        if (self.title && ![self.title isEqualToString:@""])
        {
            _workView.labelText = self.title;
        }
        _workView.imageName = @"no_record";
        if (self.imageStr && ![self.imageStr isEqualToString:@""])
        {
            _workView.imageName = self.imageStr;
        }
        _workView.myType = MyTypeOfViewForShowNetwork;
        switch (self.type)
        {
            case 0:
                _workView.myType = MyTypeOfViewForShowNetwork;
                break;
            case 1:
                _workView.myType = MyTypeOfViewForShowNoLists;
                break;
            default:
                _workView.myType = MyTypeOfViewForShowError;
                break;
        }
        [_workView createAllSubView];
        [_workView addActionWithBlock:^{
            //点击按钮执行操作
            //弹出网络提示
            [SetNetwork setNetwork];
        }];
    }
    return _workView;
}

- (id)initWithTitle:(NSString*)title withImageStr:(NSString*)imageStr withType:(NSInteger)type withFrame:(CGRect)frame withVC:(UIViewController*)vc
{
    if (self = [super init])
    {
        self.title = title;
        self.imageStr = imageStr;
        self.type = type;
        self.frame = frame;
        self.vc = vc;
    }
    return self;
}

//创建
+ (HKTShowNetworkView*)shareInstanceWithTitle:(NSString*)title withImageStr:(NSString*)imageStr withType:(NSInteger)type withFrame:(CGRect)frame withVC:(UIViewController*)vc
{
    return [[self alloc] initWithTitle:title withImageStr:imageStr withType:type withFrame:frame withVC:vc];
}


//显示网络错误页面
- (void)showNetworkViewWithSuccess:(httpRequestYesNetworking)success failure:(httpRequestNoNetworking)failure click:(clickButtonForChangeBlock)click
{
    NSLog(@"manager = %@",CreatManager);
    __weak HKTShowNetworkView* weakSelf = self;
    [CreatManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        if (status == AFNetworkReachabilityStatusNotReachable || status == AFNetworkReachabilityStatusUnknown)
        {
            //无网络连接
            NSLog(@"无网络连接。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。");
            dispatch_async(dispatch_get_main_queue(), ^{
                XYCustomStatusBar* statusBar = [[XYCustomStatusBar alloc]init];
                [statusBar showStatusMessage:@"检测到您的设备没有连接网络!"];
                if (weakSelf.workView)
                {
                    if (weakSelf.vc)
                    {
                        if (weakSelf.workView)
                        {
                            [weakSelf.workView removeFromSuperview];
                            weakSelf.workView = nil;
                        }
                        
                        [weakSelf.vc.view addSubview:weakSelf.workView];
                    }
                    else
                    {
                        [[UIApplication sharedApplication].keyWindow addSubview:weakSelf.workView];
                    }
                }
            });
            failure();
        }
        else
        {
            NSLog(@"有网络连接。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。");
            dispatch_async(dispatch_get_main_queue(), ^{
                [self dismissNetworkViewWithView:self.workView];
            });
            success();
        }
    }];
    [CreatManager startMonitoring];
}

//没有显示网络视图的创建
+ (HKTShowNetworkView*)shareInstance
{
    return [[self alloc]init];
}

- (void)showErrorWithSuccess:(httpRequestYesNetworking)success failure:(httpRequestNoNetworking)failure
{
    NSLog(@"manager+++++++++++++++++++++ = %@",CreatManager);
    [CreatManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        if (status == AFNetworkReachabilityStatusNotReachable || status == AFNetworkReachabilityStatusUnknown)
        {
            //无网络连接
            NSLog(@"无网络连接。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。");
            dispatch_async(dispatch_get_main_queue(), ^{
                XYCustomStatusBar* statusBar = [[XYCustomStatusBar alloc]init];
                [statusBar showStatusMessage:@"检测到您的设备没有连接网络!"];
            });
            failure();
        }
        else
        {
            NSLog(@"有网络连接。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。");
            success();
        }
    }];
    [CreatManager startMonitoring];
}

//隐藏关闭网络错误页面
- (void)dismissNetworkViewWithView:(ShowNoNetworkView*)workView
{
    [workView removeFromSuperview];
    workView = nil;
}

//当类销毁的时候，移除网络提示图
- (void)dealloc
{
    [self.workView removeFromSuperview];
    self.workView = nil;
}

@end
