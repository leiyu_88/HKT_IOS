//
//  SetNetwork.h
//  华康通
//
//  Created by  雷雨 on 2018/11/14.
//  Copyright © 2018年 com.huakang. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SetNetwork : NSObject

//跳转到设置中心设置网络
+ (void)setNetwork;

@end

NS_ASSUME_NONNULL_END
