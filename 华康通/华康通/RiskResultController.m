//
//  RiskResultController.m
//  华康通
//
//  Created by 雷雨 on 16/5/17.
//  Copyright © 2016年 华康集团. All rights reserved.
//

#import "RiskResultController.h"
#import "RiskResultCell.h"
#import "UMMobClick/MobClick.h"

@interface RiskResultController ()

//判断使用哪张图片
@property (nonatomic, strong) NSString* imageName;
//表格cell高度
@property (nonatomic, unsafe_unretained) CGFloat heigt;
//打败了全国人数百分百
@property (nonatomic, unsafe_unretained) NSInteger score;
//得分
@property (nonatomic, unsafe_unretained) NSInteger mark;

@end

@implementation RiskResultController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"测评结果";
    [self showSelectionForQuestion];
    srand((unsigned)time(0));
    self.mark = (arc4random()%4)*2 + 90;
    self.score = ((arc4random()%6)+1)*6 + 60;
    NSLog(@"section = %@",self.selection);
    [self.tableView registerNib:[UINib nibWithNibName:@"RiskResultCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:LoadImage(@"back1") style:UIBarButtonItemStylePlain target:self action:@selector(popRootVC)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"首页" style:UIBarButtonItemStylePlain target:self action:@selector(gotoFirstVC)];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"RiskResultController"];//("PageOne"为页面名称，可自定义)
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"RiskResultController"];
}

- (void)showSelectionForQuestion
{
    NSLog(@"....................................%@",self.selection);
    for (int i = 0; i < self.selection.count; i++)
    {
        if (i == 1)
        {
            
            NSInteger result = [self.selection[i] integerValue];
            NSInteger result1 = [self.selection[i+1] integerValue];
            NSInteger result2 = [self.selection[i+2] integerValue];
            NSInteger result3 = [self.selection[i+3] integerValue];
            if (result == 1)
            {
                NSLog(@"未成年阶段");
                self.imageName = @"fenx1.png";
                self.heigt = 753.0*([UIScreen mainScreen].bounds.size.width/375.0);
            }
            if (result != 1 && result1 == 2)
            {
                NSLog(@"青年单身阶段");
                self.imageName = @"fenx2.png";
                self.heigt = 643.5*([UIScreen mainScreen].bounds.size.width/375.0);
            }
            if (result != 1 && result1 == 1 && result2 == 2)
            {
                NSLog(@"家庭形成期");
                self.imageName = @"fenx3.png";
                self.heigt = 619*([UIScreen mainScreen].bounds.size.width/375.0);
            }
            if (result != 1 && result1 == 1 && result2 == 1 && result3 == 1)
            {
                NSLog(@"家庭成长期");
                self.imageName = @"fenx4.png";
                self.heigt = 751.5*([UIScreen mainScreen].bounds.size.width/375.0);
            }
            if (result != 1 && result1 == 1 && result2 == 1 && result3 == 2)
            {
                NSLog(@"家庭成熟期");
                self.imageName = @"fenx5.png";
                self.heigt = 698.5*([UIScreen mainScreen].bounds.size.width/375.0);
            }
        }
    }
}
- (void)popRootVC
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)gotoFirstVC
{
    //跳转到主界面
    [self.navigationController popToRootViewControllerAnimated:YES];
//    [self presentViewController:[[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"TABBAR"] animated:YES completion:^{
//        nil;
//    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return self.heigt;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RiskResultCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.myImageView.image = LoadImage(self.imageName);
    cell.topLabel.text = [NSString stringWithFormat:@"%ld",(long)self.mark];
    cell.bottomLabel.text = [NSString stringWithFormat:@"击败了全国%ld%%的人",(long)self.score];
    return cell;
}

@end
