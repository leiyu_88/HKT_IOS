//
//  ShowProvisionViewController.m
//  华康通
//
//  Created by leiyu on 16/6/15.
//  Copyright © 2016年 华康集团. All rights reserved.
//

#import "ShowProvisionViewController.h"

@interface ShowProvisionViewController ()
@property (strong, nonatomic) IBOutlet UIWebView *myWebView;

@end

@implementation ShowProvisionViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = self.titleStr;
    NSLog(@"provisionStr = %@",self.provisionStr);
    NSString *path = [[NSBundle mainBundle] pathForResource:self.provisionStr ofType:@"htm"];
    NSURL *url = [NSURL fileURLWithPath:path];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.myWebView loadRequest:request];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"ShowProvisionViewController"];//("PageOne"为页面名称，可自定义)
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"ShowProvisionViewController"];
}


@end
