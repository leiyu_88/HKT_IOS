//
//  ShowProvisionViewController.h
//  华康通
//
//  Created by leiyu on 16/6/15.
//  Copyright © 2016年 华康集团. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UMMobClick/MobClick.h"

@interface ShowProvisionViewController : UIViewController

@property (nonatomic, strong) NSString* provisionStr;
@property (nonatomic, strong) NSString* titleStr;

@end
