$(document).ready(function () {
    $(".checkboxdiv").on("click touch", function () {
        if($(this).hasClass("disabled")){
            return false;
        }
        if ($(".checkbox").hasClass("checkbox-checked")) {
            $(".checkbox").removeClass("checkbox-checked");
            $(".selectBox,.sexbox").hide();
        } else {
            $(".checkbox").addClass("checkbox-checked");
            $(".selectBox,.sexbox").show();
        }
    });
    $(".sexbox").on("click touch", function () {
        if($(this).hasClass("disabled")){
            return false;
        }
        if ($(".male").is(":hidden")) {
            $(".female").hide();
            $(".male").show();
        } else {
            $(".male").hide();
            $(".female").show();
        }
    });

    //计算
    $(".calculation").on("click touch", function () {
        if($(this).hasClass("calculation-disabled")){
            shareParams = "";
            $(this).text("计算");
            $(this).removeClass("calculation-disabled");
            $(".check").prop("readonly",false);
            $(".checkobj").removeClass("disabled");
            $("#age").prop("disabled",false);
            return false;
        }
        var weight = parseFloat($("#weight").val());
        var height = parseInt($("#height").val());
        if (isNaN(weight) || isNaN(height)) {
            return false;
        }
        if(!weight>0 || !height>0 ){
                         return false;
        }
        $("#weight").val(weight);
        $("#height").val(height);
        var bmi = weight / Math.pow(height / 100, 2);
        bmi = bmi.toFixed(1);

        if ($(".checkbox").hasClass("checkbox-checked")) {
            //未成年
            $("#bmi-juvenile").text(bmi);
            var age = parseInt($("#age").val());
            if ($(".male").is(":hidden")) {
                shareParams = weight+"|"+height+"|"+age+"|0";
                $.each(juvenileBMI.female, function (i, item) {
                    if (age == item.age && item.min <= bmi && bmi < item.max) {
                        $(".bmi-ret").attr("src", "./css/images/" + item.icon);
                    }
                });
            } else {
                shareParams = weight+"|"+height+"|"+age+"|1";
                $.each(juvenileBMI.male, function (i, item) {
                    if (age == item.age && item.min <= bmi && bmi < item.max) {
                        $(".bmi-ret").attr("src", "./css/images/" + item.icon);
                    }
                });
            }
            $(".container-result-adult").hide();
            $(".container-result-juvenile").show();
        } else {
            //成年人
            shareParams = weight+"|"+height;
            $("#bmi-adult").text(bmi);
            $.each(adultBMI.suggest, function (i, item) {
                if (item.min <= bmi && bmi < item.max) {
                    $(".text01").text(item.desc);
                    return false;
                }
            });
            $.each(adultBMI.who, function (i, item) {
                if (item.min <= bmi && bmi < item.max) {
                    $(".text02").text(item.desc);
                    return false;
                }
            });
            $.each(adultBMI.asia, function (i, item) {
                if (item.min <= bmi && bmi < item.max) {
                    $(".text03").text(item.desc);
                    return false;
                }
            });
            $.each(adultBMI.china, function (i, item) {
                if (item.min <= bmi && bmi < item.max) {
                    $(".text04").text(item.desc);
                    return false;
                }
            });
            $(".container-result-juvenile").hide();
            $(".container-result-adult").show();
        }
        if(window.location.search == ""){
            shareParams = "?p="+encodeURIComponent(shareParams);
        }else{
            shareParams = "&p="+encodeURIComponent(shareParams);
        }
        $(".check").prop("readonly",true);
        $(".checkobj").addClass("disabled");
        $("#age").prop("disabled",true);
        $(this).text("返回修改");
        $(this).addClass("calculation-disabled");
    });
        //若为分享出来的链接
    if(requestparams != "" && requestparams != "null"){
        requestparams = requestparams.split("|");
        $("#weight").val(requestparams[0]);
        $("#height").val(requestparams[1]);
        if(requestparams.length == '4'){
            $(".checkboxdiv").click();
            $("#age").val(requestparams[2]);
            if(requestparams[2] == "0"){
                $(".sexbox").click();
            }
        }
        $(".calculation").click();
        $(".calculation").parents(".item").hide();
    }
});

