//
//  PushLoginVC.m
//  华康通
//
//  Created by  雷雨 on 2018/11/26.
//  Copyright © 2018年 com.huakang. All rights reserved.
//

#import "PushLoginVC.h"

@implementation PushLoginVC

+ (PushLoginVC*)sharaVC
{
    static dispatch_once_t onceToken;
    static PushLoginVC *vc;
    dispatch_once(&onceToken, ^
                  {
                      vc = [[self alloc] init];
                  });
    return vc;
}

- (id)init
{
    if (self = [super init])
    {
        
    }
    return self;
}

- (void)showLoginVCWithVC:(UINavigationController*)navi
{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    UserLoginController* loginVC = [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
    loginVC.hidesBottomBarWhenPushed = YES;
    [navi pushViewController:loginVC animated:YES];
}


@end
