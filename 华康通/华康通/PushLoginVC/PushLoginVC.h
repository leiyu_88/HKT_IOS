//
//  PushLoginVC.h
//  华康通
//
//  Created by  雷雨 on 2018/11/26.
//  Copyright © 2018年 com.huakang. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PushLoginVC : NSObject


+ (PushLoginVC*)sharaVC;
- (void)showLoginVCWithVC:(UINavigationController*)navi;

@end

NS_ASSUME_NONNULL_END
