//
//  RiskModel.m
//  华康通
//
//  Created by 雷雨 on 16/5/17.
//  Copyright © 2016年 华康集团. All rights reserved.
//

#import "RiskModel.h"

@implementation RiskModel

+ (NSDictionary*)riskQuestions
{
    return @{@"0":@{@"title":@"1、您的性别多少？",@"answer":@[@"A:男",@"B:女"]},
             @"1":@{@"title":@"2、您的年龄是多少？",@"answer":@[@"A:0-18岁",@"B:18-30岁",@"C:30-40岁",@"D:40-50岁",@"E:50-60岁",@"F:60岁以上"]},
             @"2":@{@"title":@"3、您是否结婚？",@"answer":@[@"A:是",@"B:否"]},
             @"3":@{@"title":@"4、您是否有小孩？",@"answer":@[@"A:是",@"B:否"]},
             @"4":@{@"title":@"5、您孩子的年龄是多少？",@"answer":@[@"A:0-18岁",@"B:18岁以上"]},
             @"5":@{@"title":@"6、您家庭年收入范围？",@"answer":@[@"A:3万-8万",@"B:8万-15万",@"C:15万-30万",@"D:30万-100万",@"E:100万以上"]}
             };
}

@end
