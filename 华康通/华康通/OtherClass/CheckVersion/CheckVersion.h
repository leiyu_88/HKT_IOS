//
//  CheckVersion.h
//  华康通测试版
//
//  Created by leiyu on 16/12/13.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CheckNewVersionView.h"

@interface CheckVersion : NSObject

@property (nonatomic, strong) UILabel* myLabel;
@property (nonatomic, strong) CheckNewVersionView* checkView;
//遮罩图层
@property (nonatomic, strong) UIView* backView;
@property (nonatomic, unsafe_unretained) BOOL isSetSelf;
//检查版本
- (void)checkBuild;

@end
