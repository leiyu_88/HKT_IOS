//
//  ZTVertyStatus.m
//  华康通
//
//  Created by leiyu on 16/9/13.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import "ZTVertyStatus.h"


@interface ZTVertyStatus ()

@property (strong, nonatomic) TrueStatusView* vertyView;
//遮罩图层
@property (nonatomic, strong) UIView* backView;
//时间
@property (nonatomic, strong) NSTimer* timer;
//记录时间的计数器
@property (nonatomic, unsafe_unretained) NSInteger second;

@property (nonatomic, unsafe_unretained) NSInteger pushType;

@end

@implementation ZTVertyStatus

- (instancetype)init
{
    if (self = [super init])
    {
        self.vertyStatusViewFrame = CGRectMake(30, (UISCREENHEIGHT - NavigationBarHeight - StatusBarHeight - ALERTVIEW_HEIGHT_C)/2, UISCREENWEITH - 60, ALERTVIEW_HEIGHT_C);
        self.second = 3;
    }
    return self;
}

- (UIView*)backView
{
    if (!_backView)
    {
        _backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, UISCREENWEITH, UISCREENHEIGHT)];
        _backView.backgroundColor = YYColor;
        _backView.alpha = 0.5;
    }
    return _backView;
}

- (TrueStatusView*)vertyView
{
    if (!_vertyView)
    {
        _vertyView = [TrueStatusView view];
        _vertyView.layer.cornerRadius = 7.0;
        _vertyView.layer.masksToBounds = YES;
    }
    return _vertyView;
}

- (void)createVertyStatusViewWithMessage:(NSString*)message withPushType:(NSInteger)pushType withReturnStatus:(returnStatus)status
{
    self.pushType = pushType;
    self.vertyView.frame = self.vertyStatusViewFrame;
    self.vertyView.mylabel.text = message;
    self.vertyView.timeLabel.text = [NSString stringWithFormat:@"%ld S",(long)self.second];
    [[UIApplication sharedApplication].keyWindow addSubview:self.backView];
    [[UIApplication sharedApplication].keyWindow addSubview:self.vertyView];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timeChange) userInfo:nil repeats:YES];
}

- (void)timeChange
{
    if (self.status)
    {
        self.status(self.pushType,self.second);
    }
    if (self.second <= 1)
    {
        self.second = 1;
        [self removeTimer];
        [self.vertyView removeFromSuperview];
        [self.backView removeFromSuperview];
    }
    else
    {
        self.second --;
    }
    self.vertyView.timeLabel.text = [NSString stringWithFormat:@"%ld S",(long)self.second];
}

- (void)removeTimer
{
    if ([self.timer isValid])
    {
        [self.timer invalidate];
        self.timer = nil;
        self.second = 3;
    }
}

@end
