//
//  ZTVertyStatus.h
//  华康通
//
//  Created by leiyu on 16/9/13.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import <Foundation/Foundation.h>


/*
 
 *
 *
 *   block回调返回参数类型为string类型  
 *   状态 ：
 *   
 *
 *
 */

typedef void(^returnStatus)(NSInteger vertyStatus,NSInteger second);

//typedef enum : NSUInteger
//{
//    ZTVertyStatusTrue,
//    ZTVertyStatusFalse,
//} ZTVertyStatusType;

@interface ZTVertyStatus : NSObject

//展示检查状态的类型
//@property (nonatomic, unsafe_unretained) ZTVertyStatusType myType;
@property (nonatomic, strong) returnStatus status;


//自定义framne （默认。。。）
@property (nonatomic, unsafe_unretained) CGRect vertyStatusViewFrame;


//自定义按钮的颜色
//@property (nonatomic, strong) UIColor* buttonColor;
//自定义按钮的背景颜色
//@property (nonatomic, strong) UIColor* buttonBackgroundColor;


//pushType只是作为 正确检视图的一个判断参数(跳转参数)跳转到哪个 控制器 
- (void)createVertyStatusViewWithMessage:(NSString*)message withPushType:(NSInteger)pushType withReturnStatus:(returnStatus)status;

@end
