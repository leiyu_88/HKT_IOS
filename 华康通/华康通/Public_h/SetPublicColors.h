//
//  SetPublicColors.h
//  华康通
//
//  Created by  雷雨 on 2018/11/23.
//  Copyright © 2018年 com.huakang. All rights reserved.
//

#ifndef SetPublicColors_h
#define SetPublicColors_h


#define KMainColor                 SHOWCOLOR(@"EC1B17")//主色调：红色

#define LIGHTGREYColor             SHOWCOLOR(@"F5F5F5")//底色：浅灰色

#define D83366Color                SHOWCOLOR(@"d83366")//

#define FCFCFCColor                SHOWCOLOR(@"FCFCFC")//

#define FFFFFFColor                SHOWCOLOR(@"FFFFFF")//

#define B4B4B4Color                SHOWCOLOR(@"B4B4B4")//

#define B4B4BColor                 SHOWCOLOR(@"4B4B4B")//

#define C3C3CColor                 SHOWCOLOR(@"3C3C3C")//

#define LLCColor                   SHOWCOLOR(@"787878")//

#define F0F0F0Color                SHOWCOLOR(@"F0F0F0")//

#define E5E5E5Color                SHOWCOLOR(@"E5E5E5")//

#define E7A7AColor                 SHOWCOLOR(@"7E7A7A")//

#define C0C0C0Color                SHOWCOLOR(@"C0C0C0")//

#define F1F1F1Color                SHOWCOLOR(@"F1F1F1")//

#define F3F3F3Color                SHOWCOLOR(@"F3F3F3") //

#define E7E7EColor                 SHOWCOLOR(@"7E7E7E") //

#define YYColor                    SHOWCOLOR(@"000000")//

#define E5E5EColor                 SHOWCOLOR(@"5e5e5e")//

#define ClEARColor                 [UIColor clearColor]//

#define SHOWCOLOR(str)             [UIColor getColor:str]//


#endif 
