//
//  SetPublicFrame.h
//  华康通
//
//  Created by  雷雨 on 2018/11/23.
//  Copyright © 2018年 com.huakang. All rights reserved.
//

#ifndef SetPublicFrame_h
#define SetPublicFrame_h

/*屏幕高度*/
#define UISCREENHEIGHT        ([UIScreen mainScreen].bounds.size.height)
/*屏幕宽度*/
#define UISCREENWEITH         ([UIScreen mainScreen].bounds.size.width)
/*是否刘海屏*/
#define kIs_iphone            (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
//是否带刘海屏手机(iPhoneX)
#define kIs_iPhoneX           UISCREENWEITH >= 375.0f && UISCREENHEIGHT >= 812.0f && kIs_iphone

/*状态栏高度*/
#define StatusBarHeight       (CGFloat)(kIs_iPhoneX?(44.0):(20.0))
/*导航栏高度*/
#define NavigationBarHeight   44.0
/*TabBar高度*/
#define TabBarHeight          (CGFloat)(kIs_iPhoneX?(49.0 + 34.0):(49.0))
/*顶部安全区域远离高度*/
#define kTopBarSafeHeight     (CGFloat)(kIs_iPhoneX?(44.0):(0))
/*底部安全区域远离高度*/
#define kBottomSafeHeight     (CGFloat)(kIs_iPhoneX?(34.0):(0))
/*iPhoneX的状态栏高度差值*/
#define kTopBarDifHeight      (CGFloat)(kIs_iPhoneX?(24.0):(0))

 //iPhone4S
#define IS_iPhone_4S ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO)
// iPhone5iPhone5s iPhoneSE
#define IS_iPhone_5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)
// iPhone678
#define IS_iPhone_6 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? (CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size) || CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size)) : NO)
// iPhone6plus  iPhone7plus iPhone8plus
#define IS_iPhone6_Plus ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? (CGSizeEqualToSize(CGSizeMake(1125, 2001), [[UIScreen mainScreen] currentMode].size) || CGSizeEqualToSize(CGSizeMake(1242, 2208), [[UIScreen mainScreen] currentMode].size)) : NO)
// iPhoneX
#define IS_iPhoneX ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO)
// iPhoneXr
#define IS_iPhoneXr ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(828, 1792), [[UIScreen mainScreen] currentMode].size) : NO)
// iPhoneXs Max
#define IS_iPhoneXs_Max ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242, 2688), [[UIScreen mainScreen] currentMode].size) : NO)







#define ALERTVIEW_HEIGHT_A    130
#define ALERTVIEW_HEIGHT_B    115
#define ALERTVIEW_HEIGHT_C    185


#endif 
