//
//  SetPublicMethods.h
//  华康通
//
//  Created by  雷雨 on 2018/11/23.
//  Copyright © 2018年 com.huakang. All rights reserved.
//

#ifndef SetPublicMethods_h
#define SetPublicMethods_h


//加载本地图片
#define LoadImage(imageName)                    [UIImage imageNamed:imageName]



//设置字体
#define SetFont(size)                           [UIFont getFontHiraginoSansGBWith:size]

#define ShowAlertView(str,textField)            [self presentAlerControllerWithMessage:str withTextField:textField]

#define ShowAlertViewWithYes(str)               [self presentAlerControllerWithMessage:str]

//push到登陆页面
#define ShowLogin(navi)                         [[PushLoginVC sharaVC]showLoginVCWithVC:navi]

//隐藏Tabbar
#define HiddenTabbar                            [self.tabBarController.tabBar setHidden:YES]
//显示Tabbar
#define ShowTabbar                              [self.tabBarController.tabBar setHidden:NO]


#endif 
