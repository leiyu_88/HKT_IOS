//
//  MyLayout.m
//  手写代码 collectionview
//
//  Created by 全日制 on 15/3/25.
//  Copyright (c) 2015年 全日制. All rights reserved.
//

#import "MyLayout.h"

@implementation MyLayout

-(id)init
{
    self=[super init];
    if (self)
    {
        //设定流式布局中的各参数数值
        self.itemSize=CGSizeMake((UISCREENWEITH - 21)/2, ((UISCREENWEITH - 21)/2)*66/178);
        self.minimumLineSpacing = 1;
        self.minimumInteritemSpacing = 1;
        self.sectionInset = UIEdgeInsetsMake(10, 10, 20, 10);
        self.scrollDirection = UICollectionViewScrollDirectionVertical;
    }
    return self;
}
@end
