//
//  FirstPageCollectionCell.h
//  华康通
//
//  Created by leiyu on 15/9/21.
//  Copyright © 2015年 华康集团. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstPageCollectionCell : UICollectionViewCell
//@property(nonatomic, strong) UILabel* textlabel;
@property(nonatomic, strong) UIImageView* myImageView;
//@property(nonatomic, strong) UIButton* myButton;
@end
