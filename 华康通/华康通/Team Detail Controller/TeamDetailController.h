//
//  TeamDetailController.h
//  华康通
//
//  Created by leiyu on 15/7/22.
//  Copyright (c) 2015年 华康集团. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "UMMobClick/MobClick.h"

@interface TeamDetailController : UITableViewController
//用户登陆时用户id
@property (nonatomic,strong) NSString* customerId;

@property (nonatomic, unsafe_unretained) BOOL isFirst;

@end
