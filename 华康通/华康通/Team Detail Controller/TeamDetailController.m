//
//  TeamDetailController.m
//  华康通
//
//  Created by leiyu on 15/7/22.
//  Copyright (c) 2015年 华康集团. All rights reserved.
//

#import "TeamDetailController.h"
#import "JSONFromCustomerInfo.h"
#import "UMMobClick/MobClick.h"

@interface TeamDetailController ()<UIGestureRecognizerDelegate>
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (strong, nonatomic) IBOutlet UIButton *headerButton;
@property (strong, nonatomic) IBOutlet UIImageView *headerImageView;
@property (strong, nonatomic) IBOutlet UILabel *label1;
@property (strong, nonatomic) IBOutlet UILabel *label2;
@property (strong, nonatomic) IBOutlet UILabel *label3;
@property (strong, nonatomic) IBOutlet UILabel *label4;

@property (nonatomic,strong) NSArray* leftArray;
@property (nonatomic,strong) UIAlertController* alertView;
@property (nonatomic,strong) NSArray* array;

@end

@implementation TeamDetailController

- (NSArray*)leftArray
{
    if (!_leftArray)
    {
        _leftArray=@[@"手机号码:",@"分公司:",@"区域:",@"营业部:",@"证件名称:",@"证件号码:",@"银行账号:",@"联系地址:",@"客户邮箱:"];
    }
    return _leftArray;
}

//点击头像 图片防大
- (IBAction)showBigImageView:(UIButton *)sender
{
    if (self.headerImageView.image)
    {
        [ScanImage scanBigImageWithImageView:self.headerImageView];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"个人中心";
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithImage:LoadImage(@"back1.png") style:UIBarButtonItemStylePlain target:self action:@selector(popRootVC)];
    self.navigationController.interactivePopGestureRecognizer.delegate=self;
    self.navigationController.interactivePopGestureRecognizer.enabled=YES;
    self.tableView.showsVerticalScrollIndicator=NO;
    self.tableView.showsHorizontalScrollIndicator=NO;
    [self getCustomerInfo];
}
- (void)popRootVC
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    if (self.navigationController.viewControllers.count==1)
    {
        return NO;
    }
    else
    {
        return YES;
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"TeamDetailController"];//("PageOne"为页面名称，可自定义)
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [MobClick endLogPageView:@"TeamDetailController"];
}

#pragma mark  //获取用户信息主页
- (void)getCustomerInfo
{
    //设置缓存的key
    NSString* cacheStr = [NSString stringWithFormat:@"getCustomerInfo_self"];
    NSDictionary* json = @{@"platformType":@"1",
                           @"orderType":@"01",
                           @"requestService":@"getHKCustomerInfo",
                           @"requestObject":@{@"customerId":[TRUserAgenCode getCustomerId1]}
                           };
    NSLog(@"json = %@",json);
    //字符串转字符串
    NSString* str = [JSONToString dictionaryToJson:json];
    //转md5
    MyAdditions* md5 = [[MyAdditions alloc]init];
    md5.str = [NSString stringWithFormat:@"%@%@",KEY_POST,str];
    NSString* md5_Str = [md5 md5];
    NSString* url = [NSString stringWithFormat:@"%@&sign=%@",SERVER_URL,md5_Str];
    [self getCustomerInfoWithURL:url withParam:json withCacheStr:cacheStr];
}

//读取网络或本地的首页分组列表数据
- (void)readCustomerInfoWithData:(NSData*)data
{
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSLog(@"获取用户信息主页json = %@",json);
    if ([json isKindOfClass:[NSDictionary class]] && json)
    {
        if ([json[@"resultCode"] isEqualToString:@"0"])
        {
            dispatch_async(dispatch_get_global_queue(0, 0), ^{
                
                NSMutableArray* arr = [NSMutableArray array];
                JSONFromCustomerInfo* customerInfo = [JSONFromCustomerInfo customerInfoWithJSON:json[@"responseObject"]];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    //用户姓名
                    self.label1.text = customerInfo.realName;
                    //用户当前职级代码
                    self.label2.text = customerInfo.levelCodeName;
                    //用户销售代码
                    self.label3.text = customerInfo.agentCode;
                    //用户性别
                    self.label4.text = customerInfo.sex;
                    [arr addObject:customerInfo.bindMobile];
                    [arr addObject:customerInfo.branchName];
                    [arr addObject:customerInfo.cbrandchName];
                    [arr addObject:customerInfo.ubranchName];
                    [arr addObject:customerInfo.idTypeName];
                    [arr addObject:customerInfo.idNo];
                    [arr addObject:customerInfo.bankNo];
                    [arr addObject:customerInfo.address];
                    [arr addObject:customerInfo.bindEmail];
                    self.array = [arr copy];
                    [self.tableView reloadData];
                });
            });
            
        }
        else
        {
            //            [self endRefresh];
            dispatch_async(dispatch_get_main_queue(), ^{
                
            });
        }
    }
}

- (void)getCustomerInfoWithURL:(NSString*)url withParam:(NSDictionary*)param withCacheStr:(NSString*)cacheStr
{
    NSLog(@"cacheStr = %@",cacheStr);
    
    HKTShowNetworkView* networking = [HKTShowNetworkView shareInstanceWithTitle:@"检测到您的设备没有连接网络!" withImageStr:@"no_record" withType:0 withFrame:self.view.frame withVC:self];
    [networking showNetworkViewWithSuccess:^{
        
        [XHNetworking POST:url parameters:param cacheStr:cacheStr jsonCache:^(id jsonCache) {
            //无网络得到缓存 从缓存读取缓存信息;
            [self readCustomerInfoWithData:jsonCache];
        } success:^(NSData *responseObject) {
            //网络读取数据
            [self readCustomerInfoWithData:responseObject];
        } failure:^(NSError *error) {
            NSLog(@"error.code = %@",error.userInfo);
            if (error.code == -1001)
            {
                NSLog(@"网络请求超时.....................");
            }
            
        }];
        
    } failure:^{
        nil;
    } click:^(NSInteger type) {
        NSLog(@"点击了什么类型的按钮%ld!",(long)type);
        if (type == 0)
        {
            
        }
    }];

}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.array.count!=0)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.array.count!=0)
    {
        return self.array.count;
    }
    else
    {
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (self.array.count!=0)
    {
        if (cell==nil)
        {
            cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"cell"];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        }
        cell.textLabel.text=self.leftArray[indexPath.row];
        cell.textLabel.font = SetFont(15.0);
        cell.detailTextLabel.text = self.array[indexPath.row];
        cell.detailTextLabel.font = SetFont(15.0);
        cell.detailTextLabel.textColor = KMainColor;
        return cell;
    }
    else
    return cell;
}
- (void)viewDidDisappear:(BOOL)animated
{
    
}

@end
