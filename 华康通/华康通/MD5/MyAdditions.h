//
//  MyAdditions.h
//  MD5
//
//  Created by leiyu on 15/7/15.
//  Copyright (c) 2015年 华康集团. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyAdditions : NSObject

@property (nonatomic,strong) NSString* str;

-(NSString*)md5;

@end
