//
//  ClearCacha.m
//  习本课堂
//
//  Created by 雷雨 on 16/9/6.
//  Copyright © 2016年 亮信科技. All rights reserved.
//

#import "ClearCacha.h"
//#import "SDImageCache.h"

@implementation ClearCacha

//计算单个文件大小
+ (float)fileSizeAtPath:(NSString *)path
{
    NSFileManager *fileManager=[NSFileManager defaultManager];
    if([fileManager fileExistsAtPath:path])
    {
        long long size=[fileManager attributesOfItemAtPath:path error:nil].fileSize;
        return size/1024.0/1024.0;
    }
    else
        return 0;
}

//计算目录大小
+ (float)folderSizeAtPath:(NSString *)path
{
    NSFileManager *fileManager=[NSFileManager defaultManager];
    float folderSize = 0.0;
    if ([fileManager fileExistsAtPath:path])
    {
        NSArray *childerFiles=[fileManager subpathsAtPath:path];
        for (NSString *fileName in childerFiles)
        {
            NSString *absolutePath=[path stringByAppendingPathComponent:fileName];
            folderSize = folderSize + [self fileSizeAtPath:absolutePath];
        }
        //SDWebImage框架自身计算缓存的实现
        //        folderSize+=[[SDImageCache sharedImageCache] getSize]/1024.0/1024.0;
        return folderSize;
    }
    else
        return 0;
}
//清理缓存文件
+ (void)clearCache:(NSString *)path
{
    NSFileManager *fileManager=[NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:path])
    {
        NSArray *childerFiles=[fileManager subpathsAtPath:path];
        for (NSString *fileName in childerFiles)
        {
            //如有需要，加入条件，过滤掉不想删除的文件
            NSString *absolutePath=[path stringByAppendingPathComponent:fileName];
            NSLog(@"缓存目录:%@",absolutePath);
            [fileManager removeItemAtPath:absolutePath error:nil];
//            if ([fileManager removeItemAtPath:absolutePath error:nil])
//            {
//                NSLog(@"清除缓存成功");
//            }
        }
    }
//    [[SDImageCache sharedImageCache] cleanDisk];
}


//清除文件夹  Documents  tmp   Caches  Documentation 文件夹里面的所有文件
+ (void)clearAllFile
{
    //清除缓存
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSArray *paths1 = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSArray *paths2 = NSSearchPathForDirectoriesInDomains(NSDocumentationDirectory, NSUserDomainMask, YES);
    NSString *filePath = [paths lastObject];
    NSString *filePath1 = [paths1 lastObject];
    NSString *filePath2 = [paths2 lastObject];
    NSString *filePath3 = NSTemporaryDirectory();
    if ([ClearCacha folderSizeAtPath:filePath] != 0)
    {
        NSLog(@"缓冲区缓存大小为%.2lfM  缓存目录:%@",[ClearCacha folderSizeAtPath:filePath],filePath);
        [ClearCacha clearCache:filePath];
    }
    if ([ClearCacha folderSizeAtPath:filePath1] != 0)
    {
        NSLog(@"Document缓存大小为%.2lfM 缓存目录:%@",[ClearCacha folderSizeAtPath:filePath1],filePath1);
        [ClearCacha clearCache:filePath1];
    }
    if ([ClearCacha folderSizeAtPath:filePath2] != 0)
    {
        NSLog(@"Documentation缓存大小为%.2lfM 缓存目录:%@",[ClearCacha folderSizeAtPath:filePath2],filePath2);
        [ClearCacha clearCache:filePath2];
    }
    if ([ClearCacha folderSizeAtPath:filePath3] != 0)
    {
        NSLog(@"tmp缓存大小为%.2lfM 缓存目录:%@",[ClearCacha folderSizeAtPath:filePath3],filePath3);
        [ClearCacha clearCache:filePath3];
    }
}

@end
