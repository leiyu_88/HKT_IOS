//
//  ClearCacha.h
//  习本课堂
//
//  Created by 雷雨 on 16/9/6.
//  Copyright © 2016年 亮信科技. All rights reserved.
//

#import <Foundation/Foundation.h>


//清除缓存类

@interface ClearCacha : NSObject

//计算单个文件大小
+ (float)fileSizeAtPath:(NSString *)path;
//计算目录大小
+ (float)folderSizeAtPath:(NSString *)path;
//清理缓存文件
+ (void)clearCache:(NSString *)path;

//清除文件夹  Documents  tmp   Caches  Documentation 文件夹里面的所有文件
+ (void)clearAllFile;

@end
