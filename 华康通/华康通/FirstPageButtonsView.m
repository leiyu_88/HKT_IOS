//
//  FirstPageButtonsView.m
//  华康通
//
//  Created by 雷雨 on 16/9/24.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import "FirstPageButtonsView.h"

#define BUTTONWIGTH 54
#define JIANXI_BUTTON  ((UISCREENWEITH - (4 * BUTTONWIGTH))/4)

//#define JIANXI_LABEL  ((UISCREENWEITH - (4 * 60))/5)

@interface FirstPageButtonsView()

@property (nonatomic, strong) NSArray* titleArray;
@property (nonatomic, strong) NSArray* buttons;

@end

@implementation FirstPageButtonsView

- (NSArray*)titleArray
{
    if (!_titleArray)
    {
        _titleArray = @[@"真话保险",@"华康课堂",@"新闻资讯",@"关于华康"];
    }
    return _titleArray;
}

- (NSArray*)buttons
{
    if (!_buttons)
    {
        _buttons = @[@"home_bar_about-copy-1",@"home_bar_about-copy-2",@"home_bar_about-copy-3",@"home_bar_about-copy-4"];
//        _buttons = @[@"home_bar_zero",@"home_bar_class",@"home_bar_news",@"home_bar_about"];
    }
    return _buttons;
}


- (NSArray*)getNoNetworkIcons
{
    return @[@"icon_1",@"icon_1",@"icon_1",@"icon_1"];
}


- (id)initWithFrame:(CGRect)frame withIsNetwork:(BOOL)isNetwork
{
    if (self = [super initWithFrame:frame])
    {
        [self createButtonsWithIsNetwork:isNetwork];
        self.backgroundColor = FFFFFFColor;
        UIView* view = [[UIView alloc]initWithFrame:CGRectMake(0, frame.size.height - 1, frame.size.width, 1)];
        view.backgroundColor = LIGHTGREYColor;
        [self addSubview:view];
    }
    return self;
}

- (void)returnButtonTag:(returnButtonTag)block
{
    self.buttonTagBlock = block;
}

- (void)createButtonsWithIsNetwork:(BOOL)isNetwork
{
    for (int i = 0; i < self.buttons.count; i++)
    {
        UIButton* button = [[UIButton alloc]init];
        button.tag = i;
        button.frame = CGRectMake((JIANXI_BUTTON/2) + ((JIANXI_BUTTON + BUTTONWIGTH)*i), 14, BUTTONWIGTH, BUTTONWIGTH + 12 + 13);
        [button setTitle:@"" forState:UIControlStateNormal];
        [button addTarget:self action:@selector(clickButton:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
        
        UIImageView* imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, BUTTONWIGTH, BUTTONWIGTH)];
        imageView.tag = i + 10;
        if (isNetwork)
        {
            imageView.image =  LoadImage(self.buttons[i]);
        }
        else
        {
            imageView.image =  LoadImage([self getNoNetworkIcons][i]);
        }
        [button addSubview:imageView];
        
        UILabel* label = [[UILabel alloc]init];
        label.frame = CGRectMake(0, BUTTONWIGTH + 12, BUTTONWIGTH, 13);
        label.text = self.titleArray[i];
        label.textColor = C3C3CColor;
        label.textAlignment = NSTextAlignmentCenter;
        label.font = SetFont(12.0);
        [button addSubview:label];
    }
}

- (void)clickButton:(UIButton*)sender
{
    if (self.buttonTagBlock)
    {
        self.buttonTagBlock(sender.tag);
    }
}

@end
