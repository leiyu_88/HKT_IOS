//
//  PayViewController.m
//  华康通
//
//  Created by 雷雨 on 16/11/24.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import "PayViewController.h"
#import "UMMobClick/MobClick.h"

@interface PayViewController ()<UIWebViewDelegate, NJKWebViewProgressDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *myWebView;
@property (strong, nonatomic) NJKWebViewProgressView* webViewProgressView;
@property (strong, nonatomic) NJKWebViewProgress* webViewProgress;

@end

@implementation PayViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:LoadImage(@"back1") style:UIBarButtonItemStylePlain target:self action:@selector(popRootVC)];
    [self showProgressView];
}

//加载链接
- (void)loadURl
{
    NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.content]];
    [self.myWebView loadRequest:request];
}

//创建进度条
- (void)showProgressView
{
    self.webViewProgress = [[NJKWebViewProgress alloc] init];
    self.myWebView.delegate = self.webViewProgress;
    self.webViewProgress.webViewProxyDelegate = self;
    self.webViewProgress.progressDelegate = self;
    
    
    CGRect navBounds = self.navigationController.navigationBar.bounds;
    CGRect barFrame = CGRectMake(0,
                                 navBounds.size.height - 3,
                                 navBounds.size.width,
                                 3);
    self.webViewProgressView = [[NJKWebViewProgressView alloc] initWithFrame:barFrame];
    self.webViewProgressView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    [self.webViewProgressView setProgress:0 animated:YES];
    [self loadURl];
    [self.navigationController.navigationBar addSubview:self.webViewProgressView];
}

#pragma mark - NJKWebViewProgressDelegate
- (void)webViewProgress:(NJKWebViewProgress *)webViewProgress updateProgress:(float)progress
{
    [self.webViewProgressView setProgress:progress animated:YES];
    self.title = [self.myWebView stringByEvaluatingJavaScriptFromString:@"document.title"];
}

- (void)popRootVC
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"PayViewController"];//("PageOne"为页面名称，可自定义)
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.webViewProgressView removeSubviews];
    [MobClick endLogPageView:@"PayViewController"];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    //web视图加载完成
    
}



@end
