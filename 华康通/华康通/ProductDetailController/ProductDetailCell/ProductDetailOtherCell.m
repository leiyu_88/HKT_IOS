//
//  ProductDetailOtherCell.m
//  华康通
//
//  Created by leiyu on 16/4/11.
//  Copyright © 2016年 华康集团. All rights reserved.
//

#import "ProductDetailOtherCell.h"

@implementation ProductDetailOtherCell

- (void)setMessage:(NSString *)message
{
    _message = message;
    self.myLabel.text = _message;
    CGRect frameOfText = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width - 20, 9999);
    frameOfText = [self.myLabel textRectForBounds:frameOfText limitedToNumberOfLines:0];
    CGRect frameOfLabel = CGRectZero;
    frameOfLabel.size = frameOfText.size;
    frameOfLabel.origin.x = 10;
    frameOfLabel.origin.y = 10;
    self.myLabel.frame = frameOfLabel;
    CGRect frameOfCell = self.frame;
    frameOfCell.size.height = frameOfLabel.size.height + 20;
    self.frame = frameOfCell;
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
