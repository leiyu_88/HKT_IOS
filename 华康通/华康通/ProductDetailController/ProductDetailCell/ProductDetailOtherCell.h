//
//  ProductDetailOtherCell.h
//  华康通
//
//  Created by leiyu on 16/4/11.
//  Copyright © 2016年 华康集团. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductDetailOtherCell : UITableViewCell

@property (nonatomic, strong) NSString* message;
@property (strong, nonatomic) IBOutlet UILabel *myLabel;

@end
