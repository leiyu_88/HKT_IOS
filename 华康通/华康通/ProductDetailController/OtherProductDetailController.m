//
//  OtherProductDetailController.m
//  华康通
//
//  Created by 雷雨 on 16/5/19.
//  Copyright © 2016年 华康集团. All rights reserved.
//

#import "OtherProductDetailController.h"

@interface OtherProductDetailController ()<UIWebViewDelegate, NJKWebViewProgressDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *myWebView;
@property (strong, nonatomic) NJKWebViewProgressView* webViewProgressView;
@property (strong, nonatomic) NJKWebViewProgress* webViewProgress;

@end

@implementation OtherProductDetailController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:LoadImage(@"back") style:UIBarButtonItemStylePlain target:self action:@selector(popRootVC)];
    [self showProgressView];
}

//加载链接
- (void)loadURl
{
    NSURL *baseURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]];
    NSString *path = [[NSBundle mainBundle] pathForResource:self.html ofType:@"html"];
    NSString *html = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    [self.myWebView loadHTMLString:html baseURL:baseURL];
}

//创建进度条
- (void)showProgressView
{
    self.webViewProgress = [[NJKWebViewProgress alloc] init];
    self.myWebView.delegate = self.webViewProgress;
    self.webViewProgress.webViewProxyDelegate = self;
    self.webViewProgress.progressDelegate = self;
    CGRect navBounds = self.navigationController.navigationBar.bounds;
    CGRect barFrame = CGRectMake(0,
                                 navBounds.size.height - 3,
                                 navBounds.size.width,
                                 3);
    self.webViewProgressView = [[NJKWebViewProgressView alloc] initWithFrame:barFrame];
    self.webViewProgressView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    [self.webViewProgressView setProgress:0 animated:YES];
    [self loadURl];
    [self.navigationController.navigationBar addSubview:self.webViewProgressView];
}

#pragma mark - NJKWebViewProgressDelegate
- (void)webViewProgress:(NJKWebViewProgress *)webViewProgress updateProgress:(float)progress
{
    [self.webViewProgressView setProgress:progress animated:YES];
    self.title = [self.myWebView stringByEvaluatingJavaScriptFromString:@"document.title"];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    
    [MobClick beginLogPageView:@"OtherProductDetailController"];//("PageOne"为页面名称，可自定义)
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.webViewProgressView removeSubviews];
//    [self.navigationController.navigationBar setBarTintColor:KMainColor];
//    [self.navigationController.navigationBar setTintColor:FFFFFFColor];
//    [self.navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName:SetFont(20.0),NSForegroundColorAttributeName:FFFFFFColor}];
//    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
//    [self setNeedsStatusBarAppearanceUpdate];
    
    [MobClick endLogPageView:@"OtherProductDetailController"];
}

- (void)popRootVC
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
