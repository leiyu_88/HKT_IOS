//
//  TRProductType.m
//  华康通
//
//  Created by leiyu on 16/10/25.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import "TRProductType.h"

@implementation TRProductType

+ (NSDictionary*)getProductType
{
    return @{@"0":@"旅游险",@"1":@"意外险",@"2":@"健康险",@"3":@"人寿险",@"4":@"家财险",@"5":@"派车险"};
}

@end
