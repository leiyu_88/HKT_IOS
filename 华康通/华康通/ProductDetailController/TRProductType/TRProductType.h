//
//  TRProductType.h
//  华康通
//
//  Created by leiyu on 16/10/25.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TRProductType : NSObject

+ (NSDictionary*)getProductType;

@end
