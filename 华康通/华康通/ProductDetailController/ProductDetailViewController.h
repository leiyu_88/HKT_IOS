//
//  ProductDetailViewController.h
//  华康通
//
//  Created by leiyu on 16/4/8.
//  Copyright © 2016年 华康集团. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UMMobClick/MobClick.h"


//typedef void(^returnSectionListsToFirtPageBlock)(NSArray* sectionLists);
//typedef void(^returnProductsToFirtPageBlock)(NSArray* products);
//typedef void(^returnImagesURLStringsToFirtPageBlock)(NSArray* imagesURLStrings);

@interface ProductDetailViewController : UIViewController

@property (nonatomic, unsafe_unretained) BOOL isFirst;
@property (nonatomic, unsafe_unretained) BOOL isPush;
//产品id
@property (nonatomic, strong) NSString* productId;
@property (nonatomic, strong) NSString* html;
@property (nonatomic, strong) NSString* isSell;
//外链产品链接
@property (nonatomic, strong) NSString* gotoUrl;
//外链产品图片url
@property (nonatomic, strong) NSString* small_picture;
//外链产品描述
@property (nonatomic, strong) NSString* product_features;
//判断是外部产品还是自有产品
@property (nonatomic,strong) NSString* target;
//自有产品拼接好的链接
@property (nonatomic,strong) NSString* selfURL;

////首页产品分类数组对象
//@property (nonatomic, strong) NSArray* sectionLists;
////首页产品列表数组对象
//@property (nonatomic, strong) NSArray* products;
////首页图片数组对象
//@property (nonatomic, strong) NSArray* imagesURLStrings;
//
//@property (nonatomic, strong) returnSectionListsToFirtPageBlock sBlock;
//@property (nonatomic, strong) returnProductsToFirtPageBlock pBlock;
//@property (nonatomic, strong) returnImagesURLStringsToFirtPageBlock iBlock;
//
//
//- (void)returnSectionListsToFirtPageBlock:(returnSectionListsToFirtPageBlock)block;
//- (void)returnProductsToFirtPageBlock:(returnProductsToFirtPageBlock)block;
//- (void)returnImagesURLStringsToFirtPageBlock:(returnImagesURLStringsToFirtPageBlock)block;

//@property (nonatomic, strong) NSString* str;


@end
