//
//  OtherProductDetailController.h
//  华康通
//
//  Created by 雷雨 on 16/5/19.
//  Copyright © 2016年 华康集团. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UMMobClick/MobClick.h"

@interface OtherProductDetailController : UIViewController
//是否为首页直接进来
@property (nonatomic, unsafe_unretained) BOOL isFirstPage;
@property (nonatomic, strong) NSString* html;

@end
