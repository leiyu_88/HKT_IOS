//
//  SexSelectedCell.h
//  华康通
//
//  Created by leiyu on 16/4/21.
//  Copyright © 2016年 华康集团. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SexSelectedCell : UITableViewCell
//选择男
@property (strong, nonatomic) IBOutlet UIButton *leftButton;
//选择女
@property (strong, nonatomic) IBOutlet UIButton *rightButton;
@property (strong, nonatomic) IBOutlet UILabel *leftLabel;

@end
