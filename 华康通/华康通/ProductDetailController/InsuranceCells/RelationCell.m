//
//  RelationCell.m
//  华康通
//
//  Created by leiyu on 16/4/20.
//  Copyright © 2016年 华康集团. All rights reserved.
//

#import "RelationCell.h"

@implementation RelationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    self.contentView.backgroundColor = selected ? [UIColor colorWithRed:255.0/255.0 green:212.0/255.0 blue:204.0/255.0 alpha:1.0] : FFFFFFColor;
}

@end
