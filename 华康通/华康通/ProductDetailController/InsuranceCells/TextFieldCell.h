//
//  TextFieldCell.h
//  华康通
//
//  Created by leiyu on 16/4/21.
//  Copyright © 2016年 华康集团. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextFieldCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *LeftLabel;
@property (strong, nonatomic) IBOutlet UITextField *textField;

@end
