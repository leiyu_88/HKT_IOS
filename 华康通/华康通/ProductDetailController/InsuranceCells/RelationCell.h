//
//  RelationCell.h
//  华康通
//
//  Created by leiyu on 16/4/20.
//  Copyright © 2016年 华康集团. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RelationCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *myLabel;

@end
