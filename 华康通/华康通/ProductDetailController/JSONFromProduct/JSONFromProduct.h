//
//  JSONFromProduct.h
//  华康通
//
//  Created by leiyu on 16/10/25.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSONFromProduct : NSObject

@property (nonatomic,strong) NSString* impart;
@property (nonatomic,strong) NSString* productCode;
@property (nonatomic,strong) NSString* productName;
@property (nonatomic,strong) NSString* productType;
@property (nonatomic,strong) NSString* product_features;
@property (nonatomic,strong) NSString* product_price;
@property (nonatomic,strong) NSString* product_unit;
@property (nonatomic,strong) NSString* promotion_expenses;
@property (nonatomic,strong) NSString* small_picture;
@property (nonatomic,strong) NSString* groupName;
@property (nonatomic,strong) NSString* isSell;
@property (nonatomic,strong) NSString* gotoUrl;

+(id)productWithJSON:(NSDictionary*)json;

@end
