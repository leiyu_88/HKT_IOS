//
//  JSONFromProduct.m
//  华康通
//
//  Created by leiyu on 16/10/25.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import "JSONFromProduct.h"

@implementation JSONFromProduct

-(id)initWithJSON:(NSDictionary*)json
{
    if (self = [super init])
    {
        if ([json[@"impart"] isKindOfClass:[NSNull class]] || !json[@"impart"])
        {
            self.impart = @"无";
        }
        else
        {
            self.impart = json[@"impart"];
        }
        if ([json[@"productCode"] isKindOfClass:[NSNull class]] || !json[@"productCode"])
        {
            self.productCode = @"无";
        }
        else
        {
            self.productCode=json[@"productCode"];
        }
        if ([json[@"productName"] isKindOfClass:[NSNull class]] && !json[@"productName"])
        {
            self.productName=@"无";
        }
        else
        {
            self.productName=json[@"productName"];
        }
        if ([json[@"productType"] isKindOfClass:[NSNull class]] || !json[@"productType"])
        {
            self.productType=@"无";
        }
        else
        {
            self.productType=[NSString stringWithFormat:@"%@",json[@"productType"]];
        }
        
        
        
        if ([json[@"product_features"] isKindOfClass:[NSNull class]] || !json[@"product_features"])
        {
            self.product_features = @"无";
        }
        else
        {
            self.product_features = json[@"product_features"];
        }
        if ([json[@"product_price"] isKindOfClass:[NSNull class]] || !json[@"product_price"])
        {
            self.product_price = @"无";
        }
        else
        {
            self.product_price=[NSString stringWithFormat:@"%@",json[@"product_price"]];
        }
        if ([json[@"product_unit"] isKindOfClass:[NSNull class]] || !json[@"product_unit"])
        {
            self.product_unit=@"";
        }
        else
        {
            self.product_unit=json[@"product_unit"];
        }
        if ([json[@"promotion_expenses"] isKindOfClass:[NSNull class]] || !json[@"promotion_expenses"])
        {
            self.promotion_expenses=@" 无";
        }
        else
        {
            self.promotion_expenses=json[@"promotion_expenses"];
        }
        
        if ([json[@"small_picture"] isKindOfClass:[NSNull class]] || !json[@"small_picture"])
        {
            self.small_picture=@"";
        }
        else
        {
            self.small_picture=json[@"small_picture"];
        }
        
        if ([json[@"groupName"] isKindOfClass:[NSNull class]] || !json[@"groupName"])
        {
            self.groupName=@"";
        }
        else
        {
            self.groupName=json[@"groupName"];
        }
        
        if ([json[@"isSell"] isKindOfClass:[NSNull class]] || !json[@"isSell"])
        {
            self.isSell = @"0";
        }
        else
        {
            self.isSell = [NSString stringWithFormat:@"%@",json[@"isSell"]];
        }
        if ([json[@"gotoUrl"] isKindOfClass:[NSNull class]] || !json[@"gotoUrl"])
        {
            self.gotoUrl = @"";
        }
        else
        {
            self.gotoUrl = [NSString stringWithFormat:@"%@",json[@"gotoUrl"]];
        }
    }
    return self;
}

+(id)productWithJSON:(NSDictionary*)json
{
    return [[self alloc]initWithJSON:json];
}


@end
