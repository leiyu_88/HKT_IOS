//
//  JSONFromGroupTypeList.h
//  华康通
//
//  Created by leiyu on 16/10/26.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSONFromGroupTypeList : NSObject

@property (nonatomic,strong) NSString* group_type_id;
@property (nonatomic,strong) NSString* group_type_name;

+(id)groupTypeListWithJSON:(NSDictionary*)json;

@end
