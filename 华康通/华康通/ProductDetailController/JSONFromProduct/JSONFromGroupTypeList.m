//
//  JSONFromGroupTypeList.m
//  华康通
//
//  Created by leiyu on 16/10/26.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import "JSONFromGroupTypeList.h"

@implementation JSONFromGroupTypeList

-(id)initWithJSON:(NSDictionary*)json
{
    if (self = [super init])
    {
        if ([json[@"group_type_id"] isKindOfClass:[NSNull class]] || !json[@"group_type_id"])
        {
            self.group_type_id=@"";
        }
        else
        {
            self.group_type_id=json[@"group_type_id"];
        }
        
        if ([json[@"group_type_name"] isKindOfClass:[NSNull class]] || !json[@"group_type_name"])
        {
            self.group_type_name=@"";
        }
        else
        {
            self.group_type_name=json[@"group_type_name"];
        }
    }
    return self;
}

+(id)groupTypeListWithJSON:(NSDictionary*)json
{
    return [[self alloc]initWithJSON:json];
}

@end
