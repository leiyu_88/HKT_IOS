//
//  InsuranceListHeaderView.h
//  华康通
//
//  Created by leiyu on 16/8/8.
//  Copyright © 2016年 华康集团. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InsuranceListHeaderView : UIView

@property (nonatomic, strong) UIScrollView* myScrollView;
@property (nonatomic, strong) NSArray* buttonTitles;
@property (nonatomic, strong) NSMutableArray* allButtons;
@property (nonatomic, strong) UIView* bottomView;

- (void)scrollView;
- (void)removeScrollView;

@end
