//
//  InsurancePolicyCell.h
//  华康通
//
//  Created by leiyu on 16/5/5.
//  Copyright © 2016年 华康集团. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InsurancePolicyCell : UITableViewCell
//产品标题
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
//产品logo
@property (strong, nonatomic) IBOutlet UIImageView *logoImageView;
//是否支付勋章
@property (strong, nonatomic) IBOutlet UIImageView *typeImageView;
//投保人
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
//单号
@property (strong, nonatomic) IBOutlet UILabel *timeLabel;
//投保金额
@property (strong, nonatomic) IBOutlet UILabel *priceLabel;
//查看保单按钮
@property (strong, nonatomic) IBOutlet UIButton *myButton;
//投保单号 or 保单号
@property (weak, nonatomic) IBOutlet UILabel *noTypeLabel;
//推广费
@property (weak, nonatomic) IBOutlet UILabel *gainLabel;
@property (weak, nonatomic) IBOutlet UIButton *payButton;
//投保时间
@property (strong, nonatomic) IBOutlet UILabel *timeLabel1;
//生效时间
@property (strong, nonatomic) IBOutlet UILabel *timeLabel2;

@end
