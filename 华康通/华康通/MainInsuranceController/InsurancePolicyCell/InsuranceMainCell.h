//
//  InsuranceMainCell.h
//  华康通
//
//  Created by leiyu on 16/11/10.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InsuranceMainCell : UITableViewCell
//产品标题
@property (strong, nonatomic) IBOutlet UILabel *mainLabel;
//产品logo
@property (strong, nonatomic) IBOutlet UIImageView *productImageView;
//投保人名字
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
//订单号
@property (strong, nonatomic) IBOutlet UILabel *orderIDLabel;
//保费
@property (strong, nonatomic) IBOutlet UILabel *priceLabel;
//提交时间
@property (strong, nonatomic) IBOutlet UILabel *timeLabel;
//推广费用
@property (strong, nonatomic) IBOutlet UILabel *tuiGuangLabel1;
//立即付款按钮
@property (strong, nonatomic) IBOutlet UIButton *payButton;
//进入保单详情页面的按钮
@property (strong, nonatomic) IBOutlet UIButton *nextButton;

@property (weak, nonatomic) IBOutlet UILabel *statusLabel;

@end
