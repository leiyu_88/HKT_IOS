//
//  MainInsuranceController.h
//  华康通
//
//  Created by leiyu on 16/8/8.
//  Copyright © 2016年 华康集团. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface MainInsuranceController : UIViewController

//是否是带导航栏的页面跳转过来
@property (nonatomic ,unsafe_unretained) BOOL isNavi;
@property (nonatomic, strong) NSArray* titles;
//是否短期险
@property (nonatomic, unsafe_unretained) BOOL isSort;

//h5页面传过来的值，
@property (nonatomic, strong) NSString* h5Str;

@end
