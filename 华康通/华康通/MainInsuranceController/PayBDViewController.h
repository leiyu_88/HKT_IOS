//
//  PayBDViewController.h
//  华康通
//
//  Created by  雷雨 on 2017/7/24.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import <UIKit/UIKit.h>

//给上一个页面回传值，判断是否重新刷新页面
typedef void (^ReturnValueBlock1) (NSString *strValue);


@interface PayBDViewController : UIViewController

@property (nonatomic, strong) NSString* url ;

@property (nonatomic, unsafe_unretained) NSInteger index;

@property (nonatomic, strong) NSString* orderNo ;

@property (nonatomic, strong) ReturnValueBlock1 returnValueBlock;

@end
