//
//  ShowAgreementController.h
//  华康通
//
//  Created by leiyu on 16/5/12.
//  Copyright © 2016年 华康集团. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UMMobClick/MobClick.h"

@interface ShowAgreementController : UIViewController

@property (nonatomic, strong) NSString* productTitle;

@end
