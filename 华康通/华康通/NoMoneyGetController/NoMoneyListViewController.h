//
//  NoMoneyListViewController.h
//  华康通
//
//  Created by leiyu on 16/12/2.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoMoneyListViewController : UIViewController

@property (nonatomic, strong) NSString* html;
@property (nonatomic, unsafe_unretained) BOOL isPush;

@end
