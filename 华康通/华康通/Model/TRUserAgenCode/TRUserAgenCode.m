//
//  TRUserAgenCode.m
//  华康通
//
//  Created by user on 16/11/8.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import "TRUserAgenCode.h"

@implementation TRUserAgenCode

+ (NSString*)getUserAgenCode
{
    NSData* data = (NSData*)[CacheData getCache:@"UserLogin"];
    if (data != nil)
    {
        NSDictionary* result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        if  (result != nil)
        {
            if ([result[@"responseObject"] isKindOfClass:[NSDictionary class]])
            {
                if ([result[@"responseObject"][@"loginResult"] isEqualToString:@"0"])
                {
                    return result[@"responseObject"][@"userInfo"][@"agentCode"];
                }
                else
                {
                    return @"";
                }
            }
            else
            {
                return @"";
            }
        }
        else
        {
            return @"";
        }
    }
    else
    {
        return @"";
    }
}

+ (NSString*)getUserName
{
    NSData* data = (NSData*)[CacheData getCache:@"UserLogin"];
    if (data != nil)
    {
        NSDictionary* result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"result = %@",result);
        if (data != nil)
        {
            NSDictionary* result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            if  (result != nil)
            {
                if ([result[@"responseObject"] isKindOfClass:[NSDictionary class]])
                {
                    if ([result[@"responseObject"][@"loginResult"] isEqualToString:@"0"])
                    {
                        if (![result[@"responseObject"][@"userInfo"][@"realName"] isEqualToString:@""])
                        {
                            return result[@"responseObject"][@"userInfo"][@"realName"];
                        }
                        else
                        {
                            if (![result[@"responseObject"][@"userInfo"][@"customerName"] isEqualToString:@""])
                            {
                                return result[@"responseObject"][@"userInfo"][@"customerName"];
                            }
                            else
                            {
                                return result[@"responseObject"][@"userInfo"][@"mobile"];
                            }
                        }
                    }
                    else
                    {
                        return @"";
                    }
                }
                else
                {
                    return @"";
                }
            }
            else
            {
                return @"";
            }
        }
        else
        {
            return @"";
        }
    }
    else
    {
        return @"";
    }
}

+ (BOOL)isLogin
{
    NSData* data = (NSData*)[CacheData getCache:@"UserLogin"];
    
    if (data != nil )
    {
        NSDictionary* result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        if  (result != nil)
        {
            if ([result[@"responseObject"] isKindOfClass:[NSDictionary class]])
            {
                if ([result[@"responseObject"][@"loginResult"] isEqualToString:@"0"])
                {
                    return YES;
                }
                else
                {
                    return NO;
                }
            }
            else
            {
                return NO;
            }
        }
        else
        {
            return NO;
        }
    }
    else
    {
        return NO;
    }
}

+ (NSString*)getCustomerId
{
    NSData* data = (NSData*)[CacheData getCache:@"UserLogin"];
    if (data != nil)
    {
        NSDictionary* result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        if  (result != nil)
        {
            if ([result[@"responseObject"] isKindOfClass:[NSDictionary class]])
            {
                if ([result[@"responseObject"][@"loginResult"] isEqualToString:@"0"])
                {
                    if (![[NSString stringWithFormat:@"%@",result[@"responseObject"][@"userInfo"][@"customerId"]] isEqualToString:@""])
                    {
                        return [NSString stringWithFormat:@"%@",result[@"responseObject"][@"userInfo"][@"customerId"]];
                    }
                    else
                    {
                        return @"";
                    }
                }
                else
                {
                    return @"";
                }
            }
            else
            {
                return @"";
            }
        }
        else
        {
            return @"";
        }
    }
    else
    {
        return @"";
    }
}

+ (NSNumber*)getCustomerId1
{
    NSData* data = (NSData*)[CacheData getCache:@"UserLogin"];
    if (data != nil)
    {
        NSDictionary* result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        if  (result != nil)
        {
            if ([result[@"responseObject"] isKindOfClass:[NSDictionary class]])
            {
                if ([result[@"responseObject"][@"loginResult"] isEqualToString:@"0"])
                {
                    if (![[NSString stringWithFormat:@"%@",result[@"responseObject"][@"userInfo"][@"customerId"]] isEqualToString:@""])
                    {
                        return result[@"responseObject"][@"userInfo"][@"customerId"];
                    }
                    else
                    {
                        return [NSNumber numberWithInt:0];
                    }
                }
                else
                {
                    return [NSNumber numberWithInt:0];
                }
            }
            else
            {
                return [NSNumber numberWithInt:0];
            }
        }
        else
        {
            return [NSNumber numberWithInt:0];
        }
    }
    else
    {
        return [NSNumber numberWithInt:0];
    }
}

@end
