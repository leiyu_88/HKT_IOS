//
//  TRUserAgenCode.h
//  华康通
//
//  Created by user on 16/11/8.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TRUserAgenCode : NSObject

//获取登录后用户的工号
+ (NSString*)getUserAgenCode;
//得到用户的用户名
+ (NSString*)getUserName;
//判断是否已经登录
+ (BOOL)isLogin;
//得到客户id
+ (NSString*)getCustomerId;

//得到客户id
+ (NSNumber*)getCustomerId1;

@end
