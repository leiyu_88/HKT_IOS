//
//  TRJSExport.m
//  华康通
//
//  Created by leiyu on 16/11/2.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import "TRJSExport.h"

@implementation TRJSExport

- (void)pushPayVCWithContent:(NSString*)content
{
    NSLog(@"js已经调用了ios的推出支付页面的方法!");
    if (self.payBlock)
    {
        self.payBlock(content);
    }
}

- (void)closeActivity:(NSInteger)isBack
{
    NSLog(@"js已经调用了ios的返回方法!");
    NSLog(@"++++++++++++++++++++++++++++++isBack = %ld",(long)isBack);
    if (isBack == 0)
    {
        NSLog(@"++++++++++++++++++++++++++++++isBack = %ld",(long)isBack);
        if (self.jsBlock)
        {
            self.jsBlock(0);
        }
    }
    else if (!isBack)
    {
        NSLog(@"++++++++++++++++++++++++++++++isBack = %ld",(long)isBack);
        if (self.jsBlock)
        {
            self.jsBlock(0);
        }
    }
    else
    {
        NSLog(@"++++++++++++++++++++++++++++++isBack = %ld",(long)isBack);
        if (self.jsBlock)
        {
            self.jsBlock(isBack);
        }
    }
}

//分享按钮
- (void)shareWithTitle:(NSString*)message
{
    NSLog(@"js已经调用了ios的分享方法!");
    if (self.shareBlock)
    {
        self.shareBlock(message);
    }
}

//拍照按钮js调用的方法
- (void)photographWithType:(NSString*)type
{
    NSLog(@"js已经调用了ios的拍照弹出方法!");
    if (self.photoBlock)
    {
        self.photoBlock(type);
    }
}

//拍照按钮js调用的方法
- (void)photograph
{
    NSLog(@"js已经调用了ios的拍照弹出方法!");
    if (self.photoBlock)
    {
        self.photoBlock(@"1");
    }
}

//签名按钮js调用方法
- (void)drawSignatureWithType:(NSString*)type
{
    NSLog(@"签名按钮js调用方法!");
    if (self.drawBlock)
    {
        self.drawBlock(type);
    }
}

//h5调用我的方法给我传来页面的标题
- (void)getTitleWithText:(NSString*)title
{
    NSLog(@"h5调用我的方法给我传来页面的标题!");
    if (self.titleBlock)
    {
        self.titleBlock(title);
    }
}

//签名按钮js调用方法
- (void)drawSignature
{
    NSLog(@"签名按钮js调用方法!");
    if (self.drawBlock)
    {
        self.drawBlock(@"1");
    }
}

//分享按钮(4个参数)
- (void)shareWithTitle:(NSString*)title withContent:(NSString*)content withURL:(NSString*)url withImage:(NSString*)imageUrl
{
    NSLog(@"js已经调用了ios的分享方法!");
    if (self.shareMyBlock)
    {
        self.shareMyBlock(title,content,url,imageUrl);
    }
}

//h5调用我的方法隐藏或显示导航了
- (void)shareBtnClosed:(NSString*)message
{
    NSLog(@"h5调用我的方法告诉我是否隐藏分享按钮!");
    if (self.hideShareBtBlock)
    {
        self.hideShareBtBlock(message);
    }
}

//h5调用我的方法传参判断是否是个人电子签名页面
- (void)signShareWithTitle:(NSString*)message
{
    NSLog(@"h5调用我的方法传参判断是否是个人电子签名页面!");
    if (self.isSignPageBlock)
    {
        self.isSignPageBlock(message);
    }
}

//h5调用我的方法传参判断是否刷新上一个页面
- (void)repaySuccess:(NSString*)message
{
    NSLog(@"h5调用我的方法传参判断是否刷新上一个页面!");
    if (self.jsIsSXBlock)
    {
        self.jsIsSXBlock(message);
    }
}

//h5调用我的方法传参获取签名标题
- (void)getApkVersion:(NSString*)message
{
    NSLog(@"h5调用我的方法传参获取签名标题");
    if (self.getSignTitleBlock)
    {
        self.getSignTitleBlock(message);
    }
}

//监测h5调用我的方法push分享投保链接
- (void)judeURL:(NSString*)message
{
    NSLog(@"h5调用我的方法判断push分享投保链接");
    if (self.getURLBlock)
    {
        self.getURLBlock(message);
    }
}


/* 返回按钮是否关闭 */
- (void)backBtnClosed:(NSString*)message
{
    NSLog(@"返回按钮是否关闭");
    if (self.backBlock)
    {
        self.backBlock(message);
    }
}

/* 关闭按钮是否隐藏 */
- (void)closeBtnClosed:(NSString*)message
{
    NSLog(@"关闭按钮是否隐藏");
    if (self.closeBtnBlock)
    {
        self.closeBtnBlock(message);
    }
}

/* 是否刷新 */
- (void)toRefresh:(NSString*)message
{
    NSLog(@"是否刷新");
    if (self.hasRefreshURLBlock)
    {
        self.hasRefreshURLBlock(message);
    }
}

/* 判断是否安装微信 */
- (int)judgingIsInstallWechat
{
    NSLog(@"判断是否安装微信");
    if (self.installWeixinBlock)
    {
        self.installWeixinBlock();
    }
    static bool b;
    dispatch_async(dispatch_get_main_queue(), ^{
        bool wechat = [[UIApplication sharedApplication]canOpenURL:[NSURL URLWithString:@"weixin://"]];
        b = wechat;
    });
    if (b)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}


//完成分享(弹出视图)
- (void)finishShared
{
    
}

- (void)returnBoolForJS:(JSBackActionBlock)block
{
    self.jsBlock = block;
}

- (void)returnMessageForJS:(JSShareBlock)block
{
    self.shareBlock = block;
}

- (void)returnMessageForJS1:(JSShareMyBlock)block
{
    self.shareMyBlock = block;
}

- (void)returnPayForJS:(JSPayBlock)block
{
    self.payBlock = block;
}

- (void)returnhideShareButtonForJS:(JSHideShareButton)block
{
    self.hideShareBtBlock = block;
}

//弹出拍照按钮
- (void)returnPhotoForJS:(JSPhotoActionBlock)block
{
    self.photoBlock = block;
}

//弹出签名按钮
- (void)returnDrawForJS:(JSDrawActionBlock)block
{
    self.drawBlock = block;
}

- (void)returnTitleForJS:(JSGetTitleBlock)block
{
    self.titleBlock = block;
}

- (void)returnJudgSignPageForJS:(JSJudgSignPage)block
{
    self.isSignPageBlock = block;
}

- (void)returnJSIsSXForJS:(JSIsSX)block
{
    self.jsIsSXBlock = block;
}

- (void)getSignTitle:(JSGetSignTitle)block
{
    self.getSignTitleBlock = block;
}

- (void)returnMSForURL:(JSGetTBURL)block
{
    self.getURLBlock = block;
}

- (void)returnBackBtnClosed:(JSBackBtnClosed)block
{
    self.backBlock = block;
}

- (void)returnCloseBtnClosed:(JSCloseBtnClosed)block
{
    self.closeBtnBlock = block;
}

- (void)returnHasRefreshed:(JSHasRefreshed)block
{
    self.hasRefreshURLBlock = block;
}

- (void)postMessageForJS:(JSInstallWeixinBlock)block
{
    self.installWeixinBlock = block;
}

@end
