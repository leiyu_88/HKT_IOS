//
//  TRJSExport.h
//  华康通
//
//  Created by leiyu on 16/11/2.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import <Foundation/Foundation.h>

//监测是否点击了返回按钮（js是否调用了返回的方法）
typedef void(^JSBackActionBlock) (NSInteger isBack);
//支付页面（带导航栏）回调参数
typedef void(^JSPayBlock) (NSString* content);
//监测是否点击了分享按钮（js是否调用了返回的方法）
typedef void(^JSShareBlock) (NSString* message);
typedef void(^JSShareMyBlock) (NSString* title,NSString* content,NSString* url,NSString* imageURL);
//监测是否点击了拍照按钮（js是否调用了拍照的方法）
typedef void(^JSPhotoActionBlock) (NSString* index);
//监测是否点击了签名按钮（js是否调用了签名的方法）
typedef void(^JSDrawActionBlock) (NSString* index);
//监测是否更新了产品标题（js是否调用了签名的方法）
typedef void(^JSGetTitleBlock) (NSString* title);
//监测是否需要隐藏或显示导航了
typedef void(^JSHideShareButton) (NSString* message);
//监测h5调用我的方法传参判断是否是个人电子签名页面
typedef void(^JSJudgSignPage) (NSString* message);
//监测h5调用我的方法传参判断是否刷新上一个页面
typedef void(^JSIsSX) (NSString* message);
//监测h5调用我的方法传参获取的签名标题
typedef void(^JSGetSignTitle) (NSString* message);
//监测h5调用我的方法push分享投保链接
typedef void(^JSGetTBURL) (NSString* message);


typedef void(^JSBackBtnClosed) (NSString* message);
typedef void(^JSCloseBtnClosed) (NSString* message);
typedef void(^JSHasRefreshed) (NSString* message);

typedef void(^JSInstallWeixinBlock) (void);


//首先创建一个实现了JSExport协议的协议
@protocol TestJSObjectProtocol <JSExport>

//支付页面（带导航栏）
- (void)pushPayVCWithContent:(NSString*)content;
//此处我们测试无参数的情况
- (void)closeActivity:(NSInteger)isBack;
//分享测试
//分享按钮(传一个参数)
- (void)shareWithTitle:(NSString*)title;
//分享按钮(4个参数)
- (void)shareWithTitle:(NSString*)title withContent:(NSString*)content withURL:(NSString*)url withImage:(NSString*)imageUrl;
//拍照按钮js调用的方法
- (void)photographWithType:(NSString*)type;
//签名按钮js调用方法
- (void)drawSignatureWithType:(NSString*)type;
//拍照按钮js调用的方法(无参数)
- (void)photograph;
//签名按钮js调用方法(无参数)
- (void)drawSignature;
//h5调用我的方法给我传来页面的标题
- (void)getTitleWithText:(NSString*)title;
//h5调用我的方法隐藏或显示导航了
- (void)shareBtnClosed:(NSString*)message;
//h5调用我的方法传参判断是否是个人电子签名页面
- (void)signShareWithTitle:(NSString*)message;
//h5调用我的方法传参判断是否刷新上一个页面
- (void)repaySuccess:(NSString*)message;
//h5调用我的方法传参获取签名标题
- (void)getApkVersion:(NSString*)message;
//h5调用我的方法判断push分享投保链接
- (void)judeURL:(NSString*)message;

/* 返回按钮是否关闭 */
- (void)backBtnClosed:(NSString*)message;
/* 关闭按钮是否隐藏 */
- (void)closeBtnClosed:(NSString*)message;
/* 是否刷新 */
- (void)toRefresh:(NSString*)message;
/* 判断是否安装微信 */
- (int)judgingIsInstallWechat;


@end

@interface TRJSExport : NSObject<TestJSObjectProtocol>

@property (nonatomic, strong) JSBackActionBlock jsBlock;
@property (nonatomic, strong) JSShareBlock shareBlock;
@property (nonatomic, strong) JSShareMyBlock shareMyBlock;
@property (nonatomic, strong) JSPayBlock payBlock;
@property (nonatomic, strong) JSPhotoActionBlock photoBlock;
@property (nonatomic, strong) JSDrawActionBlock drawBlock;
@property (nonatomic, strong) JSGetTitleBlock titleBlock;
@property (nonatomic, strong) JSHideShareButton hideShareBtBlock;
@property (nonatomic, strong) JSJudgSignPage isSignPageBlock;
@property (nonatomic, strong) JSIsSX jsIsSXBlock;
@property (nonatomic, strong) JSGetSignTitle getSignTitleBlock;
@property (nonatomic, strong) JSGetTBURL getURLBlock;

@property (nonatomic, strong) JSBackBtnClosed backBlock;
@property (nonatomic, strong) JSCloseBtnClosed closeBtnBlock;
@property (nonatomic, strong) JSHasRefreshed hasRefreshURLBlock;
@property (nonatomic, strong) JSInstallWeixinBlock installWeixinBlock;

- (void)returnBoolForJS:(JSBackActionBlock)block;
- (void)returnMessageForJS:(JSShareBlock)block;
- (void)returnMessageForJS1:(JSShareMyBlock)block;
- (void)returnPayForJS:(JSPayBlock)block;
- (void)returnPhotoForJS:(JSPhotoActionBlock)block;
- (void)returnDrawForJS:(JSDrawActionBlock)block;
- (void)returnTitleForJS:(JSGetTitleBlock)block;
- (void)returnhideShareButtonForJS:(JSHideShareButton)block;
- (void)returnJudgSignPageForJS:(JSJudgSignPage)block;
- (void)returnJSIsSXForJS:(JSIsSX)block;
- (void)getSignTitle:(JSGetSignTitle)block;
- (void)returnMSForURL:(JSGetTBURL)block;

- (void)returnBackBtnClosed:(JSBackBtnClosed)block;
- (void)returnCloseBtnClosed:(JSCloseBtnClosed)block;
- (void)returnHasRefreshed:(JSHasRefreshed)block;

- (void)postMessageForJS:(JSInstallWeixinBlock)block;

@end
