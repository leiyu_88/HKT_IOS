//
//  CallCenter.m
//  华康通
//
//  Created by 雷雨 on 16/5/23.
//  Copyright © 2016年 华康集团. All rights reserved.
//

#import "CallCenter.h"

@implementation CallCenter

+ (NSDictionary*)allPhone
{
    return @{@"0":
  @[
  @{@"name":@"安邦产险",@"phone":@"95569",@"icon":@"bxlg7.png",@"letter":@"A"},
  @{@"name":@"国寿寿险",@"phone":@"95519",@"icon":@"bxlg22.png",@"letter":@"G"},
  @{@"name":@"美亚保险",@"phone":@"4008208858",@"icon":@"bxlg47.png",@"letter":@"M"},
  @{@"name":@"中意财险",@"phone":@"4006002700",@"icon":@"bxlg102.png",@"letter":@"Z"}],
             @"A":
  @[@{@"name":@"安邦产险",@"phone":@"95569",@"icon":@"bxlg7.png",@"letter":@"A"},
    @{@"name":@"安邦人寿",@"phone":@"4008895569",@"icon":@"bxlg6.png",@"letter":@"A"},
    @{@"name":@"安诚保险",@"phone":@"4000500000",@"icon":@"bxlg5.png",@"letter":@"A"},
    @{@"name":@"安华农险",@"phone":@"95540",@"icon":@"bxlg2.png",@"letter":@"A"},
    @{@"name":@"安联财险",@"phone":@"4008002020",@"icon":@"bxlg1.png",@"letter":@"A"},
    @{@"name":@"安盛天平",@"phone":@"95550",@"icon":@"bxlg4.png",@"letter":@"A"},
    @{@"name":@"安信农险",@"phone":@"4008060606",@"icon":@"bxlg3.png",@"letter":@"A"}
    ],@"B":@[
    @{@"name":@"百年人寿",@"phone":@"95542",@"icon":@"bxlg8.png",@"letter":@"B"},
    @{@"name":@"北部湾财产 ",@"phone":@"4009909999",@"icon":@"bxlg103.png",@"letter":@"B"},
    @{@"name":@"北大方正",@"phone":@"4008205882",@"icon":@"bxlg10.png",@"letter":@"B"},
    @{@"name":@"渤海保险",@"phone":@"4006116666",@"icon":@"bxlg9.png",@"letter":@"B"}
    ],@"C":@[
    @{@"name":@"长安责任",@"phone":@"95592",@"icon":@"bxlg12.png",@"letter":@"C"},
    @{@"name":@"长城人寿",@"phone":@"95576",@"icon":@"bxlg14.png",@"letter":@"C"},
    @{@"name":@"长江养老",@"phone":@"4008209966",@"icon":@"bxlg11.png",@"letter":@"C"},
    @{@"name":@"长生人寿",@"phone":@"4008208599",@"icon":@"bxlg13.png",@"letter":@"C"}
    ],@"D":@[
    @{@"name":@"大地保险",@"phone":@"95590",@"icon":@"bxlg19.png",@"letter":@"D"},
    @{@"name":@"大都会人寿",@"phone":@"4008188168",@"icon":@"bxlg15.png",@"letter":@"D"},
    @{@"name":@"鼎和保险",@"phone":@"4008888136",@"icon":@"bxlg16.png",@"letter":@"D"},
    @{@"name":@"东吴人寿",@"phone":@"4008256789",@"icon":@"bxlg18.png",@"letter":@"D"},
    @{@"name":@"都邦保险",@"phone":@"95586",@"icon":@"bxlg17.png",@"letter":@"D"}
    ],@"F":@[
    @{@"name":@"复星保德信",@"phone":@"4008216808",@"icon":@"bxlg21.png",@"letter":@"F"},
    @{@"name":@"富德产险",@"phone":@"4006695535",@"icon":@"bxlg20.png",@"letter":@"F"}
    ],@"G":@[
    @{@"name":@"工银安盛",@"phone":@"95359",@"icon":@"bxlg25.png",@"letter":@"G"},
    @{@"name":@"光大永明",@"phone":@"95105698",@"icon":@"bxlg23.png",@"letter":@"G"},
    @{@"name":@"国华人寿",@"phone":@"95549",@"icon":@"bxlg26.png",@"letter":@"G"},
    @{@"name":@"国寿寿险",@"phone":@"95519",@"icon":@"bxlg22.png",@"letter":@"G"},
    @{@"name":@"国泰产险",@"phone":@"4008202288",@"icon":@"bxlg24.png",@"letter":@"G"}
    ],@"H":@[
    @{@"name":@"合众人寿",@"phone":@"95515",@"icon":@"bxlg28.png",@"letter":@"H"},
    @{@"name":@"恒安标准",@"phone":@"4008188699",@"icon":@"bxlg29.png",@"letter":@"H"},
    @{@"name":@"恒大人寿",@"phone":@"4006368888",@"icon":@"bxlg37.png",@"letter":@"H"},
    @{@"name":@"弘康人寿",@"phone":@"4008500365",@"icon":@"bxlg32.png",@"letter":@"H"},
    @{@"name":@"华安保险",@"phone":@"95556",@"icon":@"bxlg31.png",@"letter":@"H"},
    @{@"name":@"华汇人寿",@"phone":@"4008788788",@"icon":@"bxlg33.png",@"letter":@"H"},
    @{@"name":@"华农保险",@"phone":@"4000100000",@"icon":@"bxlg30.png",@"letter":@"H"},
    @{@"name":@"华泰保险",@"phone":@"95509",@"icon":@"bxlg36.png",@"letter":@"H"},
    @{@"name":@"华泰财险",@"phone":@"4006095509",@"icon":@"bxlg34.png",@"letter":@"H"},
    @{@"name":@"华泰人寿",@"phone":@"4008895509",@"icon":@"bxlg35.png",@"letter":@"H"},
    @{@"name":@"华夏人寿",@"phone":@"95300",@"icon":@"bxlg27.png",@"letter":@"H"}
    ],@"J":@[
    @{@"name":@"吉祥人寿",@"phone":@"4008003003",@"icon":@"bxlg40.png",@"letter":@"J"},
    @{@"name":@"建信人寿",@"phone":@"95331",@"icon":@"bxlg41.png",@"letter":@"J"},
    @{@"name":@"交银康联",@"phone":@"4008211211",@"icon":@"bxlg38.png",@"letter":@"J"},
    @{@"name":@"锦泰财产 ",@"phone":@"4008666555",@"icon":@"bxlg105.png",@"letter":@"J"},
    @{@"name":@"君康人寿",@"phone":@"4008893311",@"icon":@"bxlg42.png",@"letter":@"J"},
    @{@"name":@"君龙人寿",@"phone":@"4006368888",@"icon":@"bxlg39.png",@"letter":@"J"}
    ],@"K":@[
    @{@"name":@"昆仑健康",@"phone":@"4008118899",@"icon":@"bxlg44.png",@"letter":@"K"}
    ],@"L":@[
    @{@"name":@"利安人寿",@"phone":@"4008080080",@"icon":@"bxlg43.png",@"letter":@"L"},
    @{@"name":@"利宝保险",@"phone":@"4008882008",@"icon":@"bxlg45.png",@"letter":@"L"}
    ],@"M":@[
    @{@"name":@"美亚保险",@"phone":@"4008208858",@"icon":@"bxlg47.png",@"letter":@"M"},
    @{@"name":@"民安保险",@"phone":@"95506",@"icon":@"bxlg48.png",@"letter":@"M"},
    @{@"name":@"民生保险",@"phone":@"95596",@"icon":@"bxlg46.png",@"letter":@"M"}
    ],@"N":@[
    @{@"name":@"农银人寿",@"phone":@"95581",@"icon":@"bxlg49.png",@"letter":@"N"}
    ],@"P":@[
    @{@"name":@"平安保险",@"phone":@"95511",@"icon":@"bxlg50.png",@"letter":@"P"},
    @{@"name":@"前海人寿",@"phone":@"4008896333",@"icon":@"bxlg51.png",@"letter":@"P"}
    ],@"R":@[
    @{@"name":@"人保财险",@"phone":@"95518",@"icon":@"bxlg53.png",@"letter":@"R"},
    @{@"name":@"人保健康",@"phone":@"95591",@"icon":@"bxlg54.png",@"letter":@"R"},
    @{@"name":@"人保寿险",@"phone":@"4008895518",@"icon":@"bxlg52.png",@"letter":@"R"},
    @{@"name":@"瑞泰人寿",@"phone":@"4008109939",@"icon":@"bxlg55.png",@"letter":@"R"}
    ],@"S":@[
    @{@"name":@"三星财险",@"phone":@"4009333000",@"icon":@"bxlg58.png",@"letter":@"S"},
    @{@"name":@"生命人寿",@"phone":@"95535",@"icon":@"bxlg57.png",@"letter":@"S"},
    @{@"name":@"史带财险",@"phone":@"95507",@"icon":@"bxlg56.png",@"letter":@"S"},
    @{@"name":@"苏黎世保险",@"phone":@"4006155156",@"icon":@"bxlg59.png",@"letter":@"S"}
    ],@"T":@[
    @{@"name":@"太保安联",@"phone":@"95500",@"icon":@"bxlg70.png",@"letter":@"T"},
    @{@"name":@"太平保险",@"phone":@"95500",@"icon":@"bxlg69.png",@"letter":@"T"},
    @{@"name":@"太平财险",@"phone":@"95589",@"icon":@"bxlg64.png",@"letter":@"T"},
    @{@"name":@"太平洋财险",@"phone":@"95500",@"icon":@"bxlg65.png",@"letter":@"T"},
    @{@"name":@"太平洋人寿",@"phone":@"95500",@"icon":@"bxlg62.png",@"letter":@"T"},
    @{@"name":@"太平养老",@"phone":@"95589",@"icon":@"bxlg71.png",@"letter":@"T"},
    @{@"name":@"太阳联合",@"phone":@"4008205918",@"icon":@"bxlg67.png",@"letter":@"T"},
    @{@"name":@"泰安人寿",@"phone":@"4000555800",@"icon":@"bxlg63.png",@"letter":@"T"},
    @{@"name":@"泰康保险",@"phone":@"95522",@"icon":@"bxlg60.png",@"letter":@"T"},
    @{@"name":@"泰康在线",@"phone":@"4000095522",@"icon":@"bxlg72.png",@"letter":@"T"},
    @{@"name":@"泰山财险 ",@"phone":@"4006077777",@"icon":@"bxlg104.png",@"letter":@"T"},
    @{@"name":@"天安财险",@"phone":@"4000555800",@"icon":@"bxlg61.png",@"letter":@"T"},
    @{@"name":@"天安财险",@"phone":@"95505",@"icon":@"bxlg68.png",@"letter":@"T"},
    @{@"name":@"同方全球",@"phone":@"95105768",@"icon":@"bxlg66.png",@"letter":@"T"}
    ],@"X":@[
    @{@"name":@"新光海航",@"phone":@"4008008008",@"icon":@"bxlg76.png",@"letter":@"X"},
    @{@"name":@"新华保险",@"phone":@"95567",@"icon":@"bxlg73.png",@"letter":@"X"},
    @{@"name":@"信诚人寿",@"phone":@"95558",@"icon":@"bxlg77.png",@"letter":@"X"},
    @{@"name":@"信泰保险",@"phone":@"4006008890",@"icon":@"bxlg74.png",@"letter":@"X"},
    @{@"name":@"幸福人寿",@"phone":@"95560",@"icon":@"bxlg75.png",@"letter":@"X"},
    @{@"name":@"阳光保险",@"phone":@"95510",@"icon":@"bxlg78.png",@"letter":@"X"},
    @{@"name":@"英大人寿",@"phone":@"4000188688",@"icon":@"bxlg81.png",@"letter":@"X"}
    ],@"Y":@[
    @{@"name":@"永安保险",@"phone":@"95502",@"icon":@"bxlg79.png",@"letter":@"Y"},
    @{@"name":@"友邦保险",@"phone":@"4008203588",@"icon":@"bxlg80.png",@"letter":@"Y"}
    ],@"Z":@[
    @{@"name":@"招商信诺",@"phone":@"4008888288",@"icon":@"bxlg85.png",@"letter":@"Z"},
    @{@"name":@"浙商保险",@"phone":@"4008666777",@"icon":@"bxlg88.png",@"letter":@"Z"},
    @{@"name":@"中德安联",@"phone":@"4008883636",@"icon":@"bxlg94.png",@"letter":@"Z"},
    @{@"name":@"中国平安",@"phone":@"95511",@"icon":@"bxlg84.png",@"letter":@"Z"},
    @{@"name":@"中国人保",@"phone":@"95518",@"icon":@"bxlg98.png",@"letter":@"Z"},
    @{@"name":@"中国人寿",@"phone":@"95519",@"icon":@"bxlg83.png",@"letter":@"Z"},
    @{@"name":@"中国太平",@"phone":@"95589",@"icon":@"bxlg97.png",@"letter":@"Z"},
    @{@"name":@"中韩人寿",@"phone":@"4009800800",@"icon":@"bxlg101.png",@"letter":@"Z"},
    @{@"name":@"中航安盟保险",@"phone":@"4008868199",@"icon":@"bxlg93.png",@"letter":@"Z"},
    @{@"name":@"中航三星人寿",@"phone":@"4008101888",@"icon":@"bxlg90.png",@"letter":@"Z"},
    @{@"name":@"中荷人寿",@"phone":@"4008161688",@"icon":@"bxlg91.png",@"letter":@"Z"},
    @{@"name":@"中宏保险",@"phone":@"4008188888",@"icon":@"bxlg92.png",@"letter":@"Z"},
    @{@"name":@"中华财险",@"phone":@"95585",@"icon":@"bxlg96.png",@"letter":@"Z"},
    @{@"name":@"中意财险",@"phone":@"4006002700",@"icon":@"bxlg102.png",@"letter":@"Z"},
    @{@"name":@"中意人寿",@"phone":@"4008889888",@"icon":@"bxlg87.png",@"letter":@"Z"},
    @{@"name":@"中银保险",@"phone":@"4006995566",@"icon":@"bxlg89.png",@"letter":@"Z"},
    @{@"name":@"中英人寿",@"phone":@"95545",@"icon":@"bxlg82.png",@"letter":@"Z"},
    @{@"name":@"中邮保险",@"phone":@"4008909999",@"icon":@"bxlg95.png",@"letter":@"Z"},
    @{@"name":@"众安保险",@"phone":@"4009999595",@"icon":@"bxlg100.png",@"letter":@"Z"},
    @{@"name":@"珠江人寿",@"phone":@"4006833866",@"icon":@"bxlg99.png",@"letter":@"Z"},
    @{@"name":@"紫金保险",@"phone":@"4008280018",@"icon":@"bxlg86.png",@"letter":@"Z"}
    ]};
}

@end
