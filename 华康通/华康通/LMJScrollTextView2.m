


#import "LMJScrollTextView2.h"

#define ScrollTime 1.f

@implementation LMJScrollTextView2
{
    UILabel * _scrollLabel;
    NSInteger _index;
    
    BOOL _needStop;
    bool _isRunning;
}

- (id)init{
    self = [super init];
    if (self) {
        self.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 20); // 设置一个初始的frame
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.clipsToBounds = YES;
        
        _index = 0;
        _needStop  = NO;
        _isRunning = NO;
        
        _textDataArr = @[@"您好,欢迎使用华康通。"];
        _textFont    = [UIFont systemFontOfSize:12];
        _textColor   = YYColor;
        _scrollLabel = nil;
    }
    return self;
}

- (void)setTextFont:(UIFont *)textFont{
    _textFont = textFont;
    _scrollLabel.font = textFont;
}
- (void)setTextColor:(UIColor *)textColor{
    _textColor = textColor;
    _scrollLabel.textColor = textColor;
}

- (void)createScrollLabel{
    _scrollLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.frame.size.height, self.frame.size.width, self.frame.size.height)];
    _scrollLabel.text          = @"";
    _scrollLabel.textAlignment = NSTextAlignmentLeft;
    _scrollLabel.textColor     = _textColor;
    _scrollLabel.font          = _textFont;
    [self addSubview:_scrollLabel];
    
    _scrollButton = [[UIButton alloc]init];
    _scrollButton.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    [_scrollButton setTitle:@"" forState:UIControlStateNormal];
    [_scrollButton setBackgroundColor:ClEARColor];
    [_scrollButton addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_scrollButton];
}

- (void)startScrollBottomToTop{
    
    if (_isRunning) return;
    if (_scrollLabel == nil) [self createScrollLabel];
    
    _index     = 0;
    _needStop  = NO;
    _isRunning = YES;
    [self scrollBottomToTop];
}

- (void)startScrollTopToBottom{
    
    if (_isRunning) return;
    if (_scrollLabel == nil) [self createScrollLabel];
    
    _index     = 0;
    _needStop  = NO;
    _isRunning = YES;
    [self scrollTopToBottom];
}

- (void)stop{
    _needStop = YES;
}

- (void)scrollBottomToTop{
    
    if (![self isCurrentViewControllerVisible:[self viewController]]) {  // 处于非当前页面
        
        [self performSelector:@selector(scrollBottomToTop) withObject:nil afterDelay:ScrollTime];
        
    }else{                                                               // 处于当前页面
        
        _scrollLabel.frame = CGRectMake(0, self.frame.size.height, _scrollLabel.frame.size.width, _scrollLabel.frame.size.height);
        _scrollLabel.text  = _textDataArr[_index];
        
        
        [UIView animateWithDuration:0.01 animations:^{
            
            self->_scrollLabel.frame = CGRectMake(0, 0, self->_scrollLabel.frame.size.width, self->_scrollLabel.frame.size.height);
            
        } completion:^(BOOL finished) {
            
            [UIView animateWithDuration:ScrollTime delay:ScrollTime * 3 options:0 animations:^{
                
                self->_scrollLabel.frame = CGRectMake(0, -self.frame.size.height, self->_scrollLabel.frame.size.width, self->_scrollLabel.frame.size.height);
                
            } completion:^(BOOL finished) {
                
                self->_index ++;
                if (self->_index == self->_textDataArr.count) {
                    self->_index = 0;
                }
                
                if (self->_needStop == NO) {
                    [self scrollBottomToTop];
                }else{
                    self->_isRunning = NO;
                }
            }];
        }];
    }
}

- (void)scrollTopToBottom{
    
    if (![self isCurrentViewControllerVisible:[self viewController]]) { // 处于非当前页面
        
        [self performSelector:@selector(scrollTopToBottom) withObject:nil afterDelay:ScrollTime];
        
    }else{                                                              // 处于当前页面
    
//        if ([self.delegate respondsToSelector:@selector(scrollTextView2:currentTextIndex:)]) { // 代理回调
//            [self.delegate scrollTextView2:self currentTextIndex:_index];
//        }
        
        _scrollLabel.frame = CGRectMake(0, -self.frame.size.height, _scrollLabel.frame.size.width, _scrollLabel.frame.size.height);
        _scrollLabel.text  = _textDataArr[_index];
        
        
        
        [UIView animateWithDuration:ScrollTime animations:^{
            
            self->_scrollLabel.frame = CGRectMake(0, 0, self->_scrollLabel.frame.size.width, self->_scrollLabel.frame.size.height);
            
        } completion:^(BOOL finished) {
            
            [UIView animateWithDuration:ScrollTime delay:0 options:0 animations:^{
                
                self->_scrollLabel.frame = CGRectMake(0, self.frame.size.height, self->_scrollLabel.frame.size.width, self->_scrollLabel.frame.size.height);
                
            } completion:^(BOOL finished) {
                
                self->_index ++;
                if (self->_index == self->_textDataArr.count) {
                    self->_index = 0;
                }
                
                if (self->_needStop == NO) {
                    [self scrollTopToBottom];
                }else{
                    self->_isRunning = NO;
                }
            }];
        }];
    }
}

- (void)click
{
    if ([self.delegate respondsToSelector:@selector(scrollTextView2:currentTextIndex:)]) { // 代理回调
        [self.delegate scrollTextView2:self currentTextIndex:_index];
    }
}

-(BOOL)isCurrentViewControllerVisible:(UIViewController *)viewController{
    return (viewController.isViewLoaded && viewController.view.window && [UIApplication sharedApplication].applicationState == UIApplicationStateActive);
}

- (UIViewController *)viewController {
    for (UIView * next = [self superview]; next; next = next.superview) {
        UIResponder * nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            return (UIViewController *)nextResponder;
        }
    }
    return nil;
}

@end
