//
//  NotchScreen.m
//  华康通
//
//  Created by  雷雨 on 2018/11/15.
//  Copyright © 2018年 com.huakang. All rights reserved.
//

#import "NotchScreen.h"

@implementation NotchScreen

+ (BOOL)isNotchScreenWithView:(UIView*)view
{
    if (@available(iOS 11.0, *))
    {
        if (!UIEdgeInsetsEqualToEdgeInsets(view.safeAreaInsets, UIEdgeInsetsZero))
        {
            return YES;
        }
        else
        {
            return NO;
        }
    }
    else
    return NO;
}

@end
