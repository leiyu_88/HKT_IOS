//
//  NotchScreen.h
//  华康通
//
//  Created by  雷雨 on 2018/11/15.
//  Copyright © 2018年 com.huakang. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NotchScreen : NSObject

//判断是否为带刘海屏
+ (BOOL)isNotchScreenWithView:(UIView*)view;

@end

NS_ASSUME_NONNULL_END
