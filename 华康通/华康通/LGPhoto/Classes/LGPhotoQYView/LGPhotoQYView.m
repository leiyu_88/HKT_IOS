
#import "LGPhotoQYView.h"
#import "FourLinesView.h"

@interface LGPhotoQYView()

@property (nonatomic, strong) NSArray* paths;
@property (nonatomic, strong) CALayer *myLayer;
@property (nonatomic, strong) CALayer *tempLayer;
@property (nonatomic, strong) FourLinesView* linesView;

@end

@implementation LGPhotoQYView

- (id)initWithFrame:(CGRect)frame withLayer:(CALayer *)layer
{
    if (self = [super init])
    {
        self.backgroundColor = ClEARColor;
        self.myLayer = [[CALayer alloc]init];
        self.myLayer = layer;
        self.tempLayer = [[CALayer alloc] init];
        self.tempLayer.frame = CGRectMake(40, 40, frame.size.width - 80 , frame.size.height - 80);
        self.tempLayer.borderColor = [UIColor colorWithWhite:1.0 alpha:0.4].CGColor;
        self.tempLayer.borderWidth = 1.0;
        self.tempLayer.backgroundColor = ClEARColor.CGColor;
        [self.myLayer addSublayer:self.tempLayer];
        // 顶部layer的创建
        CALayer *top_layer = [[CALayer alloc] init];
        CGFloat top_layerX = 0;
        CGFloat top_layerY = 0;
        CGFloat top_layerW = frame.size.width;
        CGFloat top_layerH = 40;
        top_layer.frame = CGRectMake(top_layerX, top_layerY, top_layerW, top_layerH);
        top_layer.backgroundColor = [YYColor colorWithAlphaComponent:0.5].CGColor;
        [self.layer addSublayer:top_layer];
        // 左侧layer的创建
        CALayer *left_layer = [[CALayer alloc] init];
        CGFloat left_layerX = 0;
        CGFloat left_layerY = 40;
        CGFloat left_layerW = 40;
        CGFloat left_layerH = frame.size.height - 80;
        left_layer.frame = CGRectMake(left_layerX, left_layerY, left_layerW, left_layerH);
        left_layer.backgroundColor = [YYColor colorWithAlphaComponent:0.5].CGColor;
        [self.layer addSublayer:left_layer];
        // 右侧layer的创建
        CALayer *right_layer = [[CALayer alloc] init];
        CGFloat right_layerX = frame.size.width - 40;
        CGFloat right_layerY = 40;
        CGFloat right_layerW = 40;
        CGFloat right_layerH = frame.size.height - 80;
        right_layer.frame = CGRectMake(right_layerX, right_layerY, right_layerW, right_layerH);
        right_layer.backgroundColor = [YYColor colorWithAlphaComponent:0.5].CGColor;
        [self.layer addSublayer:right_layer];
        // 下面layer的创建
        CALayer *bottom_layer = [[CALayer alloc] init];
        CGFloat bottom_layerX = 0;
        CGFloat bottom_layerY = frame.size.height - 40;
        CGFloat bottom_layerW = frame.size.width;
        CGFloat bottom_layerH = 40;
        bottom_layer.frame = CGRectMake(bottom_layerX, bottom_layerY, bottom_layerW, bottom_layerH);
        bottom_layer.backgroundColor = [YYColor colorWithAlphaComponent:0.5].CGColor;
        [self.layer addSublayer:bottom_layer];
        // 提示Label
        UILabel *promptLabel = [[UILabel alloc] init];
        promptLabel.numberOfLines = 2;
        promptLabel.backgroundColor = ClEARColor;
        CGFloat promptLabelX = 40;
        CGFloat promptLabelY = 50;
        CGFloat promptLabelW = frame.size.width - 80;
        CGFloat promptLabelH = 50;
        promptLabel.frame = CGRectMake(promptLabelX, promptLabelY, promptLabelW, promptLabelH);
        promptLabel.textAlignment = NSTextAlignmentCenter;
        promptLabel.font = SetFont(20.0);
        promptLabel.textColor = [KMainColor colorWithAlphaComponent:0.5];
        promptLabel.text = @"请将证件原型放在方形区域内";
        [self addSubview:promptLabel];
        //边框四角加线条
        self.linesView = [[FourLinesView alloc]init];
        self.linesView.frame = frame;
        [self addSubview:self.linesView];
    }
    
    return self;
}




@end
