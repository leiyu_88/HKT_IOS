
#import "FourLinesView.h"

@implementation FourLinesView

- (instancetype)init
{
    if (self = [super init])
    {
        self.backgroundColor = ClEARColor;
    }
    return self;
}

- (void)addFourLinesToRightAngleWithRect:(CGRect)rect
{
    UIBezierPath* path1 = [UIBezierPath bezierPath];
    path1.lineCapStyle = kCGLineCapButt;
    path1.lineJoinStyle = kCGLineJoinMiter;
    path1.lineWidth = 3.0;
    [path1 moveToPoint:CGPointMake(40, 70)];
    [path1 addLineToPoint:CGPointMake(40, 40)];
    [path1 addLineToPoint:CGPointMake(70, 40)];
    [KMainColor setStroke];
    [path1 stroke];
    
    UIBezierPath* path2 = [UIBezierPath bezierPath];
    path2.lineCapStyle = kCGLineCapButt;
    path2.lineJoinStyle = kCGLineJoinMiter;
    path2.lineWidth = 3.0;
    [path2 moveToPoint:CGPointMake(rect.size.width - 40, 70)];
    [path2 addLineToPoint:CGPointMake(rect.size.width - 40, 40)];
    [path2 addLineToPoint:CGPointMake(rect.size.width - 70, 40)];
    [KMainColor setStroke];
    [path2 stroke];
    
    UIBezierPath* path3 = [UIBezierPath bezierPath];
    path3.lineCapStyle = kCGLineCapButt;
    path3.lineJoinStyle = kCGLineJoinMiter;
    path3.lineWidth = 3.0;
//    CGFloat dash[] = {8.0,4.0};
//    [path3 setLineDash:dash count:2 phase:0];
    [path3 moveToPoint:CGPointMake(rect.size.width - 40, rect.size.height - 70)];
    [path3 addLineToPoint:CGPointMake(rect.size.width - 40, rect.size.height - 40)];
    [path3 addLineToPoint:CGPointMake(rect.size.width - 70, rect.size.height - 40)];
    [KMainColor setStroke];
    [path3 stroke];
    
    UIBezierPath* path4 = [UIBezierPath bezierPath];
    path4.lineCapStyle = kCGLineCapButt;
    path4.lineJoinStyle = kCGLineJoinMiter;
    path4.lineWidth = 3.0;
    [path4 moveToPoint:CGPointMake(40, rect.size.height - 70)];
    [path4 addLineToPoint:CGPointMake(40, rect.size.height - 40)];
    [path4 addLineToPoint:CGPointMake(70, rect.size.height - 40)];
    [KMainColor setStroke];
    [path4 stroke];
}

- (void)drawRect:(CGRect)rect
{
    [self addFourLinesToRightAngleWithRect:rect];
}


@end
