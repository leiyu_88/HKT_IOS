//
//  SetSelfInformationController.m
//  华康通
//
//  Created by leiyu on 16/4/18.
//  Copyright © 2016年 华康集团. All rights reserved.
//

#import "SetSelfInformationController.h"

#import "SVProgressHUD.h"
#import "SDImageCache.h"
#import "ResetKeyController.h"
#import "UMMobClick/MobClick.h"
#import <StoreKit/SKStoreReviewController.h>

@interface SetSelfInformationController ()
@property (nonatomic, strong) NSArray* titles;
@property (nonatomic, strong) UIView* myView;

@end

@implementation SetSelfInformationController

- (UIView*)myView
{
    if (!_myView)
    {
        _myView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
        _myView.backgroundColor = [UIColor colorWithRed:246.0/255.0 green:246.0/255.0 blue:246.0/255.0 alpha:1.0];
    }
    return _myView;
}
- (NSArray*)titles
{
    if (!_titles)
    {
        _titles = @[@"修改密码"];
    }
    return _titles;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"SetSelfInformationController"];//("PageOne"为页面名称，可自定义)
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [MobClick endLogPageView:@"SetSelfInformationController"];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //app内部跳出软件评分弹出窗口，让用户直接在此界面完成app的评分 10.3版本新特征
    //[SKStoreReviewController requestReview];
    self.title = @"设置";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:LoadImage(@"back1") style:UIBarButtonItemStylePlain target:self action:@selector(popRootVC)];
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    
}
- (void)viewDidAppear:(BOOL)animated
{
    [self updateDataForTableView];
}

- (void)updateDataForTableView
{
    NSLog(@"网络连接正常....");
    [self.myView removeFromSuperview];
}

- (void)popRootVC
{
    [self.navigationController popViewControllerAnimated:YES];
}

//是否显示推广费用
- (void)switchAction:(UISwitch*)sender
{
    NSNumber* result = [NSNumber numberWithBool:sender.on];
    [CacheData saveCache:@"ISSHOWTGF" andCache:result];
    //记录是否操作过开关
    [CacheData saveCache:@"ISSHOWTGF_666" andCache:@"111"];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 1;
    }
    else if (section == 1)
    {
        return self.titles.count;
    }
    else
    {
        return 1;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10.0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.textLabel.font = SetFont(15);
    if (indexPath.section == 0)
    {
        cell.textLabel.text = @"显示推广费用";
        UISwitch* mySwitch = [[UISwitch alloc]initWithFrame:CGRectMake(0, 15, 50, 30)];
        NSNumber* number = [CacheData getCache:@"ISSHOWTGF"];
        if (number != nil)
        {
            mySwitch.on = [number boolValue];
        }
        else
        {
            mySwitch.on = YES;
        }
        mySwitch.onTintColor = B4B4B4Color;
        mySwitch.thumbTintColor = KMainColor;
        mySwitch.tintColor = B4B4B4Color;
        mySwitch.backgroundColor = FFFFFFColor;
        mySwitch.layer.cornerRadius = mySwitch.frame.size.height/2.0;
        mySwitch.layer.masksToBounds = true;
        [mySwitch addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
        cell.accessoryView = mySwitch;
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    else if (indexPath.section == 1)
    {
        cell.textLabel.text = self.titles[indexPath.row];
    }
    else
    {
        cell.textLabel.text = @"退出登录";
    }
    
    return cell;
}


#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    NSString* path=[[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"]stringByAppendingPathComponent:@"userLogin.plist"];
//    NSDictionary* userDict = [NSDictionary dictionaryWithContentsOfFile:path];
    if (indexPath.section == 1 && indexPath.row == 0)
    {
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        ResetKeyController* firstVC = [storyboard instantiateViewControllerWithIdentifier:@"ResetKeyVC"];
        [self.navigationController pushViewController:firstVC animated:YES];
    }
    else if (indexPath.section == 2 && indexPath.row == 0)
    {
        CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
        TextStatusView *customView  = [TextStatusView view];
        customView.frame = CGRectMake(10, 10, UISCREENWEITH - 80, ALERTVIEW_HEIGHT_B);
        customView.bottomLabel.text = @"是否退出当前账号?";
        //    customView.layer.cornerRadius = 7.0;
        //    customView.layer.masksToBounds = YES;
        [alertView setContainerView:customView];
        [alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"取消", @"确认", nil]];
        [alertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
            switch (buttonIndex)
            {
                case 0:
                    return ;
                    break;
                default:
                {
                    [SVProgressHUD showInfoWithStatus:@""];
                    dispatch_time_t delay = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC));
                    dispatch_after(delay, dispatch_get_main_queue(), ^{
                        XYCustomStatusBar* statusBar = [[XYCustomStatusBar alloc]init];
                        [statusBar showStatusMessage:@"退出账号成功"];
                        [CacheData removeCacheWith:@"UserLogin"];
                        //跳转到首页
                        [self.navigationController popToRootViewControllerAnimated:YES];
                        
                        //                        MyTabBarController* firstVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"TABBAR"];
                        //                        firstVC.index = 3;
                        //                        firstVC.selectedIndex = 3;
                        //                        [CacheData removeCacheWith:@"UserLogin"];
                        //                        [self presentViewController:firstVC animated:YES completion:^{
                        //                            [SVProgressHUD dismiss];
                        //                        }];
                    });
                }
                    break;
            }
        }];
        [alertView show];
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [SVProgressHUD dismiss];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

@end
