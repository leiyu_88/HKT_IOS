//
//  AnswerQuestionCell.m
//  华康通
//
//  Created by leiyu on 15/8/6.
//  Copyright (c) 2015年 华康集团. All rights reserved.
//

#import "AnswerQuestionCell.h"

@implementation AnswerQuestionCell

- (void)setMessage:(NSString *)message
{
    _message=message;
    self.myLabel.text = _message;
    CGRect frameOfText = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width - 60, 999);
    frameOfText = [self.myLabel textRectForBounds:frameOfText limitedToNumberOfLines:0];
    CGRect frameOfLabel = CGRectZero;
    frameOfLabel.size = frameOfText.size;
    frameOfLabel.origin.x = 30;
    frameOfLabel.origin.y = 10;
    self.myLabel.frame = frameOfLabel;
    CGRect frameOfCell = self.frame;
    frameOfCell.size.height = self.myLabel.frame.size.height + 20;
    self.frame = frameOfCell;
}

@end
