//
//  AnswerQuestionCell.h
//  华康通
//
//  Created by leiyu on 15/8/6.
//  Copyright (c) 2015年 华康集团. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface AnswerQuestionCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *myLabel;
@property (nonatomic,strong ) NSString* message;
@end
