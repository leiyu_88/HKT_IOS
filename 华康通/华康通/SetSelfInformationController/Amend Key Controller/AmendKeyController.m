//
//  AmendKeyController.m
//  华康通
//
//  Created by leiyu on 15/8/5.
//  Copyright (c) 2015年 华康集团. All rights reserved.
//

#import "AmendKeyController.h"
#import "AnswerQuestionCell.h"


@interface AmendKeyController ()
@property (nonatomic, strong) UIButton* sectionHeaderButton;
@property (nonatomic,strong) NSArray* array1;
@property (nonatomic,strong) NSMutableDictionary* dicShowRow;
@property (nonatomic,strong) NSArray* questions;
@property (nonatomic, strong) UIView* myView;
@end

@implementation AmendKeyController
- (UIView*)myView
{
    if (!_myView)
    {
        _myView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
        _myView.backgroundColor = [UIColor colorWithRed:246.0/255.0 green:246.0/255.0 blue:246.0/255.0 alpha:1.0];
    }
    return _myView;
}
- (NSArray*)questions
{
    if (!_questions)
    {
        _questions=@[@"汇集各大保险公司的险种，实现对不同的保险公司及不同保险产品的分析比较，帮助您向客户推荐适合的保险产品并提供更优惠的价格。",@"首先注册成为华康通的会员（推荐使用手机注册）然后浏览您需要的保险产品，选定之后点击立即投保，填写相关投保信息，提交支付即可。",@"如果您忘记密码，可以在登陆页面点击“忘记密码”，通过您注册的手机重设密码。",@"7X24小时服务热线400-700-8889"];
    }
    return _questions;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title=@"常见问题";
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithImage:LoadImage(@"back1.png") style:UIBarButtonItemStylePlain target:self action:@selector(popRootVC)];
    [self.tableView registerNib:[UINib nibWithNibName:@"AnswerQuestionCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    self.tableView.showsVerticalScrollIndicator=NO;
    [self initData];
}
- (void)viewDidAppear:(BOOL)animated
{
    [self updateDataForTableView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"AmendKeyController"];//("PageOne"为页面名称，可自定义)
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"AmendKeyController"];
}

- (void)updateDataForTableView
{
//    ReachabilityStatus* status = [ReachabilityStatus reachability];
//    if (status.isReach)
//    {
        NSLog(@"网络连接正常....");
        [self.myView removeFromSuperview];
//    }
//    else
//    {
//        NSLog(@"网络不正常...");
//        self.tableView.tableHeaderView = self.myView;
//    }
//    [self.tableView reloadData];
}
- (void)popRootVC
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)initData
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^
    {
        self.array1 = @[@"1.华康提供哪些服务？", @"2.如何投保？", @"3.忘记密码怎么办？", @"4.华康客服电话"];
        self.dicShowRow = [NSMutableDictionary new];
        for (NSString* str in self.array1)
        {
            [self.dicShowRow setObject:[NSNumber numberWithBool:NO] forKey:str];
        }
        dispatch_async(dispatch_get_main_queue(), ^
        {
            [self.tableView reloadData];
        });
    });
}
- (void)showQuestion:(UIButton*)sender
{
    NSString *key = [self.array1 objectAtIndex:(sender.tag - 1)];
    BOOL b = [[_dicShowRow objectForKey:key] boolValue];
    [self.dicShowRow setObject:[NSNumber numberWithBool:!b] forKey:key];
    //重新刷新sections
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:(sender.tag - 1)] withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.tableView setNeedsLayout];
}
#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.array1.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *key = [self.array1 objectAtIndex:section];
    BOOL bShowRow = [[self.dicShowRow objectForKey:key] boolValue];
    if (bShowRow)
    {
        return 1;
    }
    else
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AnswerQuestionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.message = self.questions[indexPath.section];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AnswerQuestionCell* cell=(AnswerQuestionCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.frame.size.height;
}
- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView* view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, UISCREENWEITH, 40)];
    view.backgroundColor=FFFFFFColor;
    UIButton *sectionHeaderButton = [UIButton buttonWithType:UIButtonTypeSystem];
    sectionHeaderButton.frame = CGRectMake(15, 17, 6, 6);
    [sectionHeaderButton setImage:LoadImage(@"yuan") forState:UIControlStateNormal];
    [sectionHeaderButton setTitle:@"" forState:UIControlStateNormal];
    sectionHeaderButton.backgroundColor = ClEARColor;
    sectionHeaderButton.tintColor = KMainColor;
    [view addSubview:sectionHeaderButton];
    
    UIView* bottomView = [[UIView alloc]initWithFrame:CGRectMake(37, 39, UISCREENWEITH, 1)];
    bottomView.backgroundColor = [UIColor colorWithWhite:0.9 alpha:0.5];
    [view addSubview:bottomView];
    
    UILabel* label = [[UILabel alloc]initWithFrame:CGRectMake(37, 2, view.frame.size.width-42, 36)];
    label.textAlignment = NSTextAlignmentLeft;
    label.font = [UIFont systemFontOfSize:15];
    label.textColor = [UIColor lightGrayColor];
    label.numberOfLines = 2;
    label.text=[self.array1 objectAtIndex:section];
    [view addSubview:label];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    button.tag = section+1;
    button.frame = CGRectMake(0, 0, self.view.bounds.size.width, 40);
    [button setTitle:@"" forState:UIControlStateNormal];
    button.backgroundColor = ClEARColor;
    [button addTarget:self action:@selector(showQuestion:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:button];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40.0;
}

@end
