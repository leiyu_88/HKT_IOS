//
//  RiskAppraiseNextController.m
//  华康通
//
//  Created by 雷雨 on 16/5/17.
//  Copyright © 2016年 华康集团. All rights reserved.
//

#import "RiskAppraiseNextController.h"
#import "RiskModel.h"
#import "RiskResultController.h"
#import "SVProgressHUD.h"
#import "UMMobClick/MobClick.h"

@interface RiskAppraiseNextController ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;


@end

@implementation RiskAppraiseNextController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"风险评估";
    self.titleLabel.text = [RiskModel riskQuestions][[NSString stringWithFormat:@"%ld",(long)self.count]][@"title"];
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:LoadImage(@"back1") style:UIBarButtonItemStylePlain target:self action:@selector(popRootVC)];
}

- (void)popRootVC
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"RiskAppraiseNextController"];//("PageOne"为页面名称，可自定义)
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"RiskAppraiseNextController"];
}

- (void)viewDidAppear:(BOOL)animated
{
    [self updateDataForTableView];
}


- (void)updateDataForTableView
{
//    ReachabilityStatus* status = [ReachabilityStatus reachability];
//    if (status.isReach)
//    {
//        NSLog(@"网络连接正常....");
//        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
//    }
//    else
//    {
//        NSLog(@"网络不正常...");
//        self.tableView.contentInset = UIEdgeInsetsMake(50, 0, 0, 0);
//        
//    }
    //    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[RiskModel riskQuestions][[NSString stringWithFormat:@"%ld",(long)self.count]][@"answer"] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    [cell setSelectionStyle:UITableViewCellSelectionStyleDefault];
    cell.contentView.backgroundColor = cell.selected ? [UIColor colorWithRed:255.0/255.0 green:212.0/255.0 blue:204.0/255.0 alpha:1.0] : FFFFFFColor;
    cell.textLabel.text = [RiskModel riskQuestions][[NSString stringWithFormat:@"%ld",(long)self.count]][@"answer"][indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    RiskAppraiseNextController* riskVC = [[RiskAppraiseNextController alloc]initWithNibName:@"RiskAppraiseNextController" bundle:nil];
    if (self.count == 0)
    {
        self.selection = [NSString stringWithFormat:@"%ld、",(long)indexPath.row + 1];
        riskVC.selection = self.selection;
    }
    else
    {
        riskVC.selection = [self.selection stringByAppendingString:[NSString stringWithFormat:@"%ld、",(long)indexPath.row + 1]];
        if (self.count == 5)
        {
            self.selection  = riskVC.selection;
        }
    }
    
    NSLog(@"selection = %@",riskVC.selection);
    
    if (self.count < 5)
    {
        if (self.count == 3 && indexPath.row == 1)
        {
            riskVC.count = self.count + 2;
        }
        else
        {
            riskVC.count = self.count + 1;
        }
        [self.navigationController pushViewController:riskVC animated:YES];
    }
    else
    {
        NSArray* arr = [self.selection componentsSeparatedByString:@"、"];
        NSMutableArray* array = [NSMutableArray array];
        for (NSString* str in arr)
        {
            if (![str isEqualToString:@""])
            {
                [array addObject:str];
            }
        }
        [SVProgressHUD showInfoWithStatus:@""];
        RiskResultController* riskResultVC = [[RiskResultController alloc]initWithNibName:@"RiskResultController" bundle:nil];
        riskResultVC.selection = [array copy];
        dispatch_time_t delay = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8*NSEC_PER_SEC));
        dispatch_after(delay, dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            [self.navigationController pushViewController:riskResultVC animated:YES];
        });
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    //    [self.timer invalidate];
    [SVProgressHUD dismiss];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

@end
