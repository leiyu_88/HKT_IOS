//
//  ScanImage.m
//  习本课堂
//
//  Created by 雷雨 on 16/9/15.
//  Copyright © 2016年 亮信科技. All rights reserved.
//

#import "ScanImage.h"

@implementation ScanImage

//原始尺寸
static CGRect oldframe;

/**
 *  浏览大图
 *
 *  @param scanImageView 图片所在的imageView
 */
+ (void)scanBigImageWithImageView:(UIImageView *)currentImageview
{
    //当前imageview的图片
    UIImage *image = currentImageview.image;
    //当前视图
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    //背景
    UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, UISCREENWEITH, UISCREENHEIGHT)];
    //当前imageview的原始尺寸->将像素currentImageview.bounds由currentImageview.bounds所在视图转换到目标视图window中，返回在目标视图window中的像素值
    oldframe = [currentImageview convertRect:currentImageview.bounds toView:window];
    [backgroundView setBackgroundColor:YYColor];
    //此时视图不会显示
    [backgroundView setAlpha:0];
    //将所展示的imageView重新绘制在Window中
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:oldframe];
    [imageView setImage:image];
    [imageView setTag:10];
    [backgroundView addSubview:imageView];
    //将原始视图添加到背景视图中
    [window addSubview:backgroundView];
    
    
    //添加点击事件同样是类方法 -> 作用是再次点击回到初始大小
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideImageView:)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    tapGestureRecognizer.numberOfTouchesRequired = 1;
    [backgroundView addGestureRecognizer:tapGestureRecognizer];
    
    
    UILongPressGestureRecognizer* longGR = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(preserveImage:)];
    longGR.numberOfTouchesRequired = 1;
    longGR.minimumPressDuration = 1.0;
    [backgroundView addGestureRecognizer:longGR];
    
    //动画放大所展示的ImageView
    
    [UIView animateWithDuration:0.4 animations:^{
        CGFloat y,width,height;
        y = (UISCREENHEIGHT - image.size.height * UISCREENWEITH / image.size.width) * 0.5;
        //宽度为屏幕宽度
        width = UISCREENWEITH;
        //高度 根据图片宽高比设置
        height = image.size.height * UISCREENWEITH / image.size.width;
        [imageView setFrame:CGRectMake(0, y, width, height)];
        //重要！ 将视图显示出来
        [backgroundView setAlpha:1];
    } completion:^(BOOL finished) {
    }];
    
}

/**
 *  恢复imageView原始尺寸
 *
 *  @param tap 点击事件
 */
+ (void)hideImageView:(UITapGestureRecognizer *)tap
{
    NSLog(@"点击了图片");
    UIView *backgroundView = tap.view;
    //原始imageview
    UIImageView *imageView = [tap.view viewWithTag:10];
    backgroundView.backgroundColor = ClEARColor;
    //恢复
    [UIView animateWithDuration:0.4 animations:^{
        [imageView setFrame:oldframe];
        
    } completion:^(BOOL finished) {
//        if (self.tableViewHeight)
//        {
//            self.tableViewHeight(StatusBarHeight);
//        }
        //完成后操作->将背景视图删掉
        [backgroundView removeFromSuperview];
    }];
}

+ (void)preserveImage:(UILongPressGestureRecognizer*)longGR
{
    if (longGR.state == UIGestureRecognizerStateBegan)
    {
        [SRActionSheet sr_showActionSheetViewWithTitle:@"保存图片到手机相册"
                                     cancelButtonTitle:@"取消"
                                destructiveButtonTitle:nil
                                     otherButtonTitles:@[@"保存"]
                                      selectSheetBlock:^(SRActionSheet *actionSheetView, NSInteger actionIndex) {
                                          switch (actionIndex)
                                          {
                                              case -1:
                                                  return;
                                                  break;
                                              case 0:
                                              {
                                                  //原始imageview
                                                  UIImageView *imageView = [longGR.view viewWithTag:10];
                                                  UIImage* image = imageView.image;// myImage为自己的图片
//                                                  NSData* imageData =  UIImagePNGRepresentation(image);
//
//                                                  UIImage* newImage = [UIImage imageWithData:imageData];
                                                  //把图片保存到用户手机相册
                                                  UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
                                              }
                                                  break;
                                          }
                                      }];
    }
    NSLog(@"长按手势");
}


+ (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (error == nil)
    {
        XYCustomStatusBar* statusBar = [[XYCustomStatusBar alloc]init];
        [statusBar showStatusMessage:@"保存成功"];
    }
    else
    {
        XYCustomStatusBar* statusBar = [[XYCustomStatusBar alloc]init];
        [statusBar showStatusMessage:@"保存失败"];
    }
}

//允许多个手势
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

@end
