//
//  ScanImage.h
//  习本课堂
//
//  Created by 雷雨 on 16/9/15.
//  Copyright © 2016年 亮信科技. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^retutnHeight)(CGFloat height);

@interface ScanImage : NSObject

@property (strong, nonatomic) retutnHeight tableViewHeight;

/**
 *  浏览大图
 *
 *  @param scanImageView 图片所在的imageView
 */
+ (void)scanBigImageWithImageView:(UIImageView *)currentImageview;

@end
