//
//  FirstPageButtonsView.h
//  华康通
//
//  Created by 雷雨 on 16/9/24.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^returnButtonTag)(NSInteger buttonTag);

@interface FirstPageButtonsView : UIView

@property (nonatomic, strong) returnButtonTag buttonTagBlock;


- (id)initWithFrame:(CGRect)frame withIsNetwork:(BOOL)isNetwork;

- (void)returnButtonTag:(returnButtonTag)block;

@end
