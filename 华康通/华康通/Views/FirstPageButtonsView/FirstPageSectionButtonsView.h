//
//  FirstPageSectionButtonsView.h
//  华康通
//
//  Created by 雷雨 on 16/9/24.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^returnMyButtonTag)(NSInteger buttonTag,NSString* group_type_id,CGFloat distance,CGFloat superDistance);

@interface FirstPageSectionButtonsView : UIView

@property (nonatomic, strong) NSArray* buttons;
@property (nonatomic, strong) UIScrollView* myScrollView;

@property (nonatomic, strong) UIView* bottomView;
@property (nonatomic, unsafe_unretained) CGFloat distance;
@property (nonatomic, unsafe_unretained) CGFloat superDistance;
//按钮的下标(显示选中按钮的下标)
@property (nonatomic, unsafe_unretained) NSInteger buttonIndex;


@property (nonatomic, strong) returnMyButtonTag buttonTagBlock;

- (void)returnButtonTag:(returnMyButtonTag)block;
- (void)createButtons;

@end
