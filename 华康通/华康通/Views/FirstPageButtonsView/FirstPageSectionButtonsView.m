//
//  FirstPageSectionButtonsView.m
//  华康通
//
//  Created by 雷雨 on 16/9/24.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import "FirstPageSectionButtonsView.h"
#import "JSONFromGroupTypeList.h"
#import "JSONFromGroupTypeList.h"

@interface FirstPageSectionButtonsView()


@property (nonatomic, strong) NSMutableArray* allButtons;
@property (nonatomic, strong) NSMutableArray* allViews;

@end

@implementation FirstPageSectionButtonsView

- (NSMutableArray*)allViews
{
    if (!_allViews)
    {
        _allViews = [NSMutableArray array];
    }
    return _allViews;
}

- (NSMutableArray*)allButtons
{
    if (!_allButtons)
    {
        _allButtons = [NSMutableArray array];
    }
    return _allButtons;
}
//更新表格
- (void)changeData:(UIButton*)sender
{
    for (UIButton* button in self.allButtons)
    {
        if (button.tag == sender.tag)
        {
            for (UIView* view in self.allViews)
            {
                if (view.tag == sender.tag)
                {
                    view.backgroundColor = KMainColor;
                }
                else
                {
                    view.backgroundColor = FFFFFFColor;
                }
            }
            [button setTitleColor:KMainColor forState:UIControlStateNormal];
        }
        else
        {
            [button setTitleColor:B4B4BColor forState:UIControlStateNormal];
        }
    }
    if (self.buttonTagBlock)
    {
        CGPoint point = self.myScrollView.contentOffset;
        JSONFromGroupTypeList* group = self.buttons[sender.tag];
        self.buttonTagBlock(sender.tag,group.group_type_id,point.x,self.superDistance);
    }
}

- (void)returnButtonTag:(returnMyButtonTag)block
{
    self.buttonTagBlock = block;
}

- (void)createButtons
{
    self.myScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, UISCREENWEITH, self.frame.size.height)];
    self.myScrollView.contentSize = CGSizeMake(self.buttons.count * UISCREENWEITH / 5, self.frame.size.height);
    self.myScrollView.contentOffset = CGPointMake(self.distance, 0);
    self.myScrollView.userInteractionEnabled = YES;
    self.myScrollView.bounces = YES;
    self.myScrollView.scrollEnabled = YES;
    self.myScrollView.pagingEnabled = NO;
    self.myScrollView.showsHorizontalScrollIndicator = NO;
    self.myScrollView.showsVerticalScrollIndicator = NO;
    for (int i = 0; i<self.buttons.count; i++)
    {
        UIButton* button = [UIButton buttonWithType:UIButtonTypeSystem];
        button.titleLabel.font = SetFont(13.0);
        JSONFromGroupTypeList* group = self.buttons[i];
        [button setTitle:group.group_type_name forState:UIControlStateNormal];
        [button setTitleColor:B4B4BColor forState:UIControlStateNormal];
        button.frame = CGRectMake((UISCREENWEITH/5) * i, 0, UISCREENWEITH/5, self.myScrollView.frame.size.height);
        [button addTarget:self action:@selector(changeData:) forControlEvents:UIControlEventTouchUpInside];
        button.tag = i;
        UIView* view = [[UIView alloc]initWithFrame:CGRectMake((UISCREENWEITH/5) * i, self.myScrollView.frame.size.height - 4, button.frame.size.width, 2)];
        view.tag = i;
        view.backgroundColor = FFFFFFColor;
        if (i == self.buttonIndex)
        {
            [view setBackgroundColor:KMainColor];
            [button setTitleColor:KMainColor forState:UIControlStateNormal];
        }
        [self.allViews addObject:view];
        [self.allButtons addObject:button];
        [self.myScrollView addSubview:button];
        [self.myScrollView addSubview:view];
    }
    UIView* lineView = [[UIView alloc]initWithFrame:CGRectMake(0, self.frame.size.height - 2, self.frame.size.width, 1)];
    lineView.backgroundColor = E5E5E5Color;
    [self addSubview:self.myScrollView];
    [self addSubview:lineView];
}

- (void)removeScrollView
{
    [self.myScrollView removeFromSuperview];
}
- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        self.backgroundColor = FFFFFFColor;
        self.superDistance = -64.0;
    }
    
    return self;
}

@end
