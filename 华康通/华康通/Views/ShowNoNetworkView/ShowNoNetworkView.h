//
//  ShowNoNetworkView.h
//  华康通
//
//  Created by leiyu on 16/11/8.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#pragma mark - 一个自定义支持显示“无列表纪录”“无网络状况下的题视图”

#import <UIKit/UIKit.h>

typedef void(^ShowUpdateDataBlock)();

typedef NS_OPTIONS(NSUInteger, MyTypeOfView)
{
    MyTypeOfViewForShowNetwork = 0,
    MyTypeOfViewForShowNoLists = 1,
    MyTypeOfViewForShowError = 2
};

@interface ShowNoNetworkView : UIView

@property (nonatomic, strong) NSString* imageName;
@property (nonatomic, strong) NSString* labelText;
@property (nonatomic, unsafe_unretained) MyTypeOfView myType;
@property (nonatomic, strong) ShowUpdateDataBlock myBlock;

- (void)addActionWithBlock:(ShowUpdateDataBlock)block;
- (void)createAllSubView;

@end
