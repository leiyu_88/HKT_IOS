//
//  ShowNoNetworkView.m
//  华康通
//
//  Created by leiyu on 16/11/8.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import "ShowNoNetworkView.h"

@interface ShowNoNetworkView()

@property (nonatomic, strong) UIImageView* imageView;
@property (nonatomic, strong) UILabel* label;
@property (nonatomic, strong) UIButton* button;

@end

@implementation ShowNoNetworkView

- (UIImageView*)imageView
{
    if (!_imageView)
    {
        _imageView = [[UIImageView alloc]initWithFrame:CGRectMake((self.frame.size.width - 88)/2, ((self.frame.size.height - 88)/2) - 44, 88, 88)];
        _imageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _imageView;
}

- (UILabel*)label
{
    if (!_label)
    {
        _label = [[UILabel alloc]initWithFrame:CGRectMake(0, (self.frame.size.height/2) + 54 - 44, self.frame.size.width, 30)];
        _label.textAlignment = NSTextAlignmentCenter;
        _label.font = SetFont(16.0);
        _label.textColor = E7A7AColor;
    }
    return _label;
}

- (UIButton*)button
{
    if (!_button)
    {
        _button = [[UIButton alloc]init];
        _button.frame = CGRectMake((self.frame.size.width - 118)/2, (self.frame.size.height/2) + 54 + 30 + 20 - 44, 118, 36);
        [_button setTitleColor:KMainColor forState:UIControlStateNormal];
        _button.layer.cornerRadius = 2.0;
        _button.layer.masksToBounds = YES;
        _button.layer.borderColor = KMainColor.CGColor;
        _button.layer.borderWidth = 1.0;
        [_button addTarget:self action:@selector(updateWithAgain) forControlEvents:UIControlEventTouchUpInside];
    }
    return _button;
}

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        self.backgroundColor = LIGHTGREYColor;
    }
    return self;
}

- (void)createAllSubView
{
    self.imageView.image = LoadImage(self.imageName);
    [self addSubview:self.imageView];
    self.label.text = self.labelText;
    [self addSubview:self.label];
    if (self.myType == MyTypeOfViewForShowNetwork)
    {
//        [self.button setTitle:@"前往设置" forState:UIControlStateNormal];
//        [self addSubview:self.button];
    }
    else if (self.myType == MyTypeOfViewForShowNoLists)
    {
        [self.button setTitle:@"立即刷新" forState:UIControlStateNormal];
        [self addSubview:self.button];
    }
    else
    {
        
    }
}

//再次刷新 请求数据
- (void)updateWithAgain
{
    if (self.myBlock)
    {
        self.myBlock();
    }
}

- (void)addActionWithBlock:(ShowUpdateDataBlock)block
{
    self.myBlock = block;
}

@end
