//
//  TextStatusView.h
//  华康通
//
//  Created by leiyu on 16/9/19.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextStatusView : UIView


@property (strong, nonatomic) IBOutlet UILabel *topLabel;

@property (strong, nonatomic) IBOutlet UILabel *bottomLabel;

+(id)view;

@end
