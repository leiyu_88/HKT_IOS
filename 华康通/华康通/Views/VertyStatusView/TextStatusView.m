//
//  TextStatusView.m
//  华康通
//
//  Created by leiyu on 16/9/19.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import "TextStatusView.h"

@implementation TextStatusView

+(id)view
{
    return [[[NSBundle mainBundle]loadNibNamed:@"TextStatusView" owner:nil options:nil]lastObject];
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    if (self=[super initWithCoder:aDecoder])
    {
        self.autoresizingMask = UIViewAutoresizingNone;
    }
    return self;
}

@end
