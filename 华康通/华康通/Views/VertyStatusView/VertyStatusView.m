//
//  VertyStatusView.m
//  华康通
//
//  Created by leiyu on 16/9/12.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import "VertyStatusView.h"

@implementation VertyStatusView


+ (id)view
{
    return [[[NSBundle mainBundle]loadNibNamed:@"VertyStatusView" owner:nil options:nil]lastObject];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self=[super initWithCoder:aDecoder])
    {
        self.autoresizingMask = UIViewAutoresizingNone;
    }
    return self;
}


@end
