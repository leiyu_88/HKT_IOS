//
//  TrueStatusView.h
//  华康通
//
//  Created by leiyu on 16/9/20.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TrueStatusView : UIView
@property (strong, nonatomic) IBOutlet UIImageView *iconImageView;
@property (strong, nonatomic) IBOutlet UILabel *mylabel;
@property (strong, nonatomic) IBOutlet UILabel *timeLabel;

+(id)view;

@end
