//
//  TrueStatusView.m
//  华康通
//
//  Created by leiyu on 16/9/20.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import "TrueStatusView.h"

@implementation TrueStatusView

+(id)view
{
    return [[[NSBundle mainBundle]loadNibNamed:@"TrueStatusView" owner:nil options:nil]lastObject];
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    if (self=[super initWithCoder:aDecoder])
    {
        self.autoresizingMask = UIViewAutoresizingNone;
    }
    return self;
}

@end
