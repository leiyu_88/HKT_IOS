//
//  VertyStatusView.h
//  华康通
//
//  Created by leiyu on 16/9/12.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VertyStatusView : UIView

@property (strong, nonatomic) IBOutlet UIImageView *topImageView;

//当显示一段话时 用到
@property (strong, nonatomic) IBOutlet UILabel *otherLabel;


+ (id)view;

@end
