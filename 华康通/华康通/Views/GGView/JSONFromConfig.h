//
//  JSONFromConfig.h
//  华康通
//
//  Created by  雷雨 on 2017/7/27.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSONFromConfig : NSObject

//图片类型种类
@property (nonatomic, strong) NSString* imageType;
//显示日期
@property (nonatomic, strong) NSString* showDate;
//网络图片链接
@property (nonatomic, strong) NSString* imageUrlFirst;
//h5链接
@property (nonatomic, strong) NSString* imageUrlSecond;
//图片标题
@property (nonatomic, strong) NSString* imageTitle;
//内容
@property (nonatomic, strong) NSString* imageDetailtype;


+(id)configWithJSON:(NSDictionary*)json;

@end
