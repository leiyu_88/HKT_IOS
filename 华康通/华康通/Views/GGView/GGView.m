
#import "GGView.h"
#import "CycleView.h"

@interface GGView()<SDCycleScrollViewDelegate>

@property (nonatomic, strong) SDCycleScrollView *cycleScrollView;
@property (nonatomic, strong) UIImageView* backImageView;
@property (nonatomic, unsafe_unretained) NSInteger timeCount;
@property (nonatomic, strong) NSTimer* timer;
@property (nonatomic, strong) UIButton* button;
@property (nonatomic, strong) UILabel* label;
@property (nonatomic, strong) CycleView* cycle;


@end


@implementation GGView



//创建按钮和标签
- (void)addButtonAndLabel
{
    self.label = [[UILabel alloc]initWithFrame:CGRectMake(UISCREENWEITH - 53, 30, 33, 33)];
    NSString* str = [NSString stringWithFormat:@"跳过"];
//    NSMutableAttributedString *stra = [[NSMutableAttributedString alloc] initWithString:str];
//    [stra addAttribute:NSFontAttributeName value:SetFont(14.0) range:NSMakeRange(0,4)];
//    [stra addAttribute:NSFontAttributeName value:SetFont(20.0) range:NSMakeRange(4,str.length - 5)];
//    [stra addAttribute:NSFontAttributeName value:SetFont(14.0) range:NSMakeRange(str.length - 1,1)];
    self.label.text = str;
    self.label.layer.cornerRadius = 16.5;
    self.label.layer.masksToBounds = YES;
    self.label.textColor = FFFFFFColor;
    self.label.backgroundColor = D83366Color;
    self.label.font = SetFont(12.0);
    self.label.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.label];
    
    self.cycle = [[CycleView alloc]initWithFrame:CGRectMake(UISCREENWEITH - 53, 30, 33, 33)];
    [self addSubview:self.cycle];

    self.button = [[UIButton alloc]init];
    self.button.frame = CGRectMake(UISCREENWEITH - 53, 30, 33, 33);
    self.button.layer.cornerRadius = 16.5;
    self.button.layer.masksToBounds = YES;
    [self.button setTitle:@"" forState:UIControlStateNormal];
    [self.button setBackgroundColor:ClEARColor];
    self.button.alpha = 1.0;
    [self.button addTarget:self action:@selector(pushMainVC) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.button];
}

- (void)pushMainVC
{
    //关闭动画
    self.hidden = YES;
    [self removeFromSuperview];
    [self removeTimer];
    // 广告页面关闭后，首页创建一个消息按钮
    [[NSNotificationCenter defaultCenter] postNotificationName:@"showButton" object:self userInfo:@{}];
}


- (NSString *)getLaunchImageName
{
    NSString *viewOrientation = @"Portrait";
    if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) {
        viewOrientation = @"Landscape";
    }
    NSString *launchImageName = nil;
    NSArray* imagesDict = [[[NSBundle mainBundle] infoDictionary] valueForKey:@"UILaunchImages"];
    
    UIWindow *currentWindow = [[UIApplication sharedApplication].windows firstObject];
    CGSize viewSize = currentWindow.bounds.size;
    for (NSDictionary* dict in imagesDict)
    {
        CGSize imageSize = CGSizeFromString(dict[@"UILaunchImageSize"]);
        
        if (CGSizeEqualToSize(imageSize, viewSize) && [viewOrientation isEqualToString:dict[@"UILaunchImageOrientation"]])
        {
            launchImageName = dict[@"UILaunchImageName"];
        }
    }
    return launchImageName;
}

//获取当前项目的启动图
- (UIImage *)getLaunchImage
{
    return LoadImage([self getLaunchImageName]);
}

//在广告轮播图的底部放一张启动图
- (void)addBackImageView
{
    self.backImageView = [[UIImageView alloc]initWithImage:[self getLaunchImage]];
    self.backImageView.frame = CGRectMake(0, 0, UISCREENWEITH, UISCREENHEIGHT);
    [self addSubview:self.backImageView];
}

//创建一个滚动视图和一排按钮
- (void)createHeaderViewWithArray:(NSArray*)arr withRatio:(CGFloat)ratio
{
    // 网络加载图片的轮播器
//    SDCycleScrollView *cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, UISCREENWEITH, UISCREENWEITH * self.ratio) delegate:self placeholderImage:nil];
    SDCycleScrollView *cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, UISCREENWEITH, UISCREENWEITH * ratio) imageURLStringsGroup:[self getURLSForImagesWithArr:arr]];
    NSLog(@"++++++++++++++++++++++++++++++++++图片链接++++++++++++++++++++++++++++++++++ = %@ 宽高比：%lf",[self getURLSForImagesWithArr:arr],ratio);
    cycleScrollView.imageURLStringsGroup = [self getURLSForImagesWithArr:arr];
    cycleScrollView.titlesGroup = [self getTitlesForImagesWithArr:arr];
    cycleScrollView.titleLabelTextFont = SetFont(16.0);
    cycleScrollView.titleLabelHeight = 50.0;
    cycleScrollView.titleLabelTextColor = FFFFFFColor;
    cycleScrollView.titleLabelBackgroundColor = ClEARColor;
//    CGSize size = LoadImage(@"11.png").size;
//        SDCycleScrollView *cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, UISCREENWEITH, UISCREENWEITH * (size.height/size.width)) shouldInfiniteLoop:YES imageNamesGroup:arr];
    cycleScrollView.backgroundColor = FCFCFCColor;
    
    //当前实体圆颜色
    cycleScrollView.currentPageDotColor = FFFFFFColor;
    //小圆圈颜色
    cycleScrollView.pageDotColor = FFFFFFColor;
    //滚动视图的滚动方向
    cycleScrollView.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    //无网络读取的图片（占位图）;
    cycleScrollView.placeholderImage = LoadImage(@"morentu");
    //页码控件的位置
    cycleScrollView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
    //自定义轮播时间
    cycleScrollView.delegate = self;
//    cycleScrollView.autoScrollTimeInterval = 2;
    cycleScrollView.autoScroll = NO;
    cycleScrollView.pageControlStyle = SDCycleScrollViewPageContolStyleAnimated;
    [self addSubview:cycleScrollView];
    //加入一个按钮和标签
    [self addButtonAndLabel];
    self.cycle.progress = 1.0;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timeChange) userInfo:nil repeats:YES];
}
//得到图片链接数组
- (NSArray*)getURLSForImagesWithArr:(NSArray*)arr
{
    NSMutableArray* array = [NSMutableArray array];
    for (JSONFromConfig* images in arr)
    {
        [array addObject:images.imageUrlFirst];
    }
    return [array copy];
}

//得到文字数组
- (NSArray*)getTitlesForImagesWithArr:(NSArray*)arr
{
    NSMutableArray* array = [NSMutableArray array];
    for (JSONFromConfig* images in arr)
    {
        [array addObject:images.imageTitle];
    }
    return [array copy];
}

#pragma mark - SDCycleScrollViewDelegate
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index
{
    NSLog(@"---点击了第%ld张图片", (long)index);
    if (self.clickBlock)
    {
        self.clickBlock(index);
    }
}
/** 图片滚动回调 */
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didScrollToIndex:(NSInteger)index
{
    
}

//初始化
- (id)initWithImages:(NSArray*)images withRatio:(CGFloat)ratio
{
    if (self = [super init])
    {
        self.backgroundColor = FFFFFFColor;
        self.timeCount = 4;
        //加入系统的启动图
        [self addBackImageView];
        //创建滚动广告图
        [self createHeaderViewWithArray:images withRatio:ratio];
    }
    return self;
}

//倒计时
- (void)timeChange
{
    if (self.timeCount <= 1)
    {
        self.timeCount = 1;
        //进入首页
        [self pushMainVC];
    }
    else
    {
        self.timeCount--;
    }
    NSLog(@"倒计时：%.ld",self.timeCount);
//    self.loadProgress.progress = (CGFloat) fabs((double)self.timeCount/3);
    //这是一个处理一句文字不同字体的颜色和字体
//    NSString* str = [NSString stringWithFormat:@"立即进入%ldS",(long)self.timeCount];
//    NSMutableAttributedString *stra = [[NSMutableAttributedString alloc] initWithString:str];
//    [stra addAttribute:NSFontAttributeName value:SetFont(14.0) range:NSMakeRange(0,4)];
//    [stra addAttribute:NSFontAttributeName value:SetFont(20.0) range:NSMakeRange(4,str.length - 5)];
//    [stra addAttribute:NSFontAttributeName value:SetFont(14.0) range:NSMakeRange(str.length - 1,1)];
//    self.label.attributedText = stra;
}

- (void)dealloc
{
    [self removeTimer];
}

//移除计时器
- (void)removeTimer
{
    [self.timer invalidate];
    self.timer = nil;
    self.timeCount = 4;
}

@end
