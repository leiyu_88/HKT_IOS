//
//  GGView.h
//  华康通
//
//  Created by  雷雨 on 2017/7/26.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import <UIKit/UIKit.h>

//点击哪个图片回掉！
typedef void (^LBClick) (NSInteger tag);

@interface GGView : UIView

@property (nonatomic, strong) LBClick clickBlock;

- (id)initWithImages:(NSArray*)images withRatio:(CGFloat)ratio;

@end
