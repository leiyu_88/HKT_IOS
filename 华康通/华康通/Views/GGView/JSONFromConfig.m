//
//  JSONFromConfig.m
//  华康通
//
//  Created by  雷雨 on 2017/7/27.
//  Copyright © 2017年 com.huakang. All rights reserved.
//

#import "JSONFromConfig.h"

@implementation JSONFromConfig

-(id)initWithJSON:(NSDictionary*)json
{
    if (self = [super init])
    {
        if ([json[@"imageType"] isKindOfClass:[NSNull class]] || !json[@"imageType"])
        {
            self.imageType = @"";
        }
        else
        {
            self.imageType = json[@"imageType"];
        }
        if ([json[@"showDate"] isKindOfClass:[NSNull class]] || !json[@"showDate"])
        {
            self.showDate = @"";
        }
        else
        {
            self.showDate = json[@"showDate"];
        }
        if ([json[@"imageUrlFirst"] isKindOfClass:[NSNull class]] || !json[@"imageUrlFirst"])
        {
            self.imageUrlFirst = @"";
        }
        else
        {
            self.imageUrlFirst = json[@"imageUrlFirst"];
        }
        
        if ([json[@"imageUrlSecond"] isKindOfClass:[NSNull class]] || !json[@"imageUrlSecond"])
        {
            self.imageUrlSecond = @"";
        }
        else
        {
            self.imageUrlSecond = json[@"imageUrlSecond"];
        }
        
        if ([json[@"imageTitle"] isKindOfClass:[NSNull class]] || !json[@"imageTitle"])
        {
            self.imageTitle = @"";
        }
        else
        {
            self.imageTitle = json[@"imageTitle"];
        }
        
        if ([json[@"imageDetailtype"] isKindOfClass:[NSNull class]] || !json[@"imageDetailtype"])
        {
            self.imageDetailtype = @"";
        }
        else
        {
            self.imageDetailtype = json[@"imageDetailtype"];
        }
    }
    return self;
}

+(id)configWithJSON:(NSDictionary*)json
{
    return [[self alloc]initWithJSON:json];
}

@end
