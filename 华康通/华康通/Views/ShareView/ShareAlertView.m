//
//  ShareAlertView.m
//  华康通
//
//  Created by  雷雨 on 2018/10/19.
//  Copyright © 2018年 com.huakang. All rights reserved.
//

#import "ShareAlertView.h"

@implementation ShareAlertView

+ (id)view
{
    return [[[NSBundle mainBundle]loadNibNamed:@"ShareAlertView" owner:nil options:nil]lastObject];
}

- (IBAction)closeKeyboard:(UITextField*)sender
{
    [sender resignFirstResponder];
}
//移除弹出视图
- (IBAction)closeView:(id)sender
{
    NSLog(@"关闭视图");
    if (self.closeBlock)
    {
        self.closeBlock();
    }
}

//发送手机号码给服务器
- (IBAction)go:(id)sender
{
    NSLog(@"发送手机号码给服务器");
    if (self.postBlock)
    {
        if (self.textField.text)
        {
            self.postBlock(self.textField.text);
        }
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self endEditing:YES];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self=[super initWithCoder:aDecoder])
    {
        self.autoresizingMask = UIViewAutoresizingNone;
    }
    return self;
}

@end
