//
//  ShareAlertView.h
//  华康通
//
//  Created by  雷雨 on 2018/10/19.
//  Copyright © 2018年 com.huakang. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^closeViewBlock)();
typedef void(^goPostPhoneNumberBlock)(NSString* text);

@interface ShareAlertView : UIView
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UIButton *goButton;
@property (nonatomic, strong) closeViewBlock closeBlock;
@property (nonatomic, strong) goPostPhoneNumberBlock postBlock;


+ (id)view;

@end

NS_ASSUME_NONNULL_END
