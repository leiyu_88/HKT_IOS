//
//  ShareView.h
//  华康通
//
//  Created by 雷雨 on 16/11/21.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShareAlertView.h"

@interface ShareView : UIView

@property (nonatomic, strong) NSArray* buttons;
@property (nonatomic, strong) UILabel* topLabel;

@property (nonatomic, strong) NSArray* twoButtonImages;
@property (nonatomic, strong) NSArray* twoLabelTitles;

@property (nonatomic, strong) NSArray* threeButtonImages;
@property (nonatomic, strong) NSArray* threeLabelTitles;

@property (nonatomic, strong) NSArray* buttonImages;
@property (nonatomic, strong) NSArray* labelTitles;

@property (nonatomic, strong) NSString* url;
@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSString* content;
@property (nonatomic, strong) NSString* imageURL;
@property (strong, nonatomic) UIView* backView;
@property (nonatomic, unsafe_unretained) ShareAlertView* shareAlertView;
@property (nonatomic, strong) UIView* myBackView;
//得到由标题、内容、链接拼接的一个字符串
@property (nonatomic, strong) NSString* message;

- (id)initWithFrame:(CGRect)frame withIndex:(NSInteger)index;

@end
