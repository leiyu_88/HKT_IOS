//
//  CheckNewVersionView.m
//  华康通测试版
//
//  Created by leiyu on 16/12/14.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import "CheckNewVersionView.h"

@implementation CheckNewVersionView

+ (id)view
{
    return [[[NSBundle mainBundle]loadNibNamed:@"CheckNewVersionView" owner:nil options:nil]lastObject];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self=[super initWithCoder:aDecoder])
    {
        self.autoresizingMask = UIViewAutoresizingNone;
    }
    return self;
}

- (void)returnButtonWithBlock:(BlockReturnButton)block
{
    self.myBlock = block;
}

- (IBAction)goAppStore:(id)sender
{
    if (self.myBlock)
    {
        self.myBlock(1);
    }
}

- (IBAction)cancelDeleteView:(id)sender
{
    if (self.myBlock)
    {
        self.myBlock(0);
    }
}

@end
