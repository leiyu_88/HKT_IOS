//
//  CheckNewVersionView.h
//  华康通测试版
//
//  Created by leiyu on 16/12/14.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^BlockReturnButton)(NSInteger buttonTag);

@interface CheckNewVersionView : UIView

@property (strong, nonatomic) IBOutlet UITextView *textView;
@property (strong, nonatomic) IBOutlet UIImageView *myBackImageView;
@property (strong, nonatomic) IBOutlet UIButton *leftButton;
@property (strong, nonatomic) IBOutlet UIView *centerView;
@property (strong, nonatomic) IBOutlet UIButton *rightButton;
//是否隐藏取消按钮
@property (nonatomic, unsafe_unretained) BOOL isHidden;
@property (nonatomic, strong) BlockReturnButton myBlock;

- (void)returnButtonWithBlock:(BlockReturnButton)block;
+ (id)view;

@end
