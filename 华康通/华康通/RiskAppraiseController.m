//
//  RiskAppraiseController.m
//  华康通
//
//  Created by 雷雨 on 16/5/17.
//  Copyright © 2016年 华康集团. All rights reserved.
//

#import "RiskAppraiseController.h"
#import "RiskAppraiseNextController.h"
#import "UMMobClick/MobClick.h"


@interface RiskAppraiseController ()
@property (weak, nonatomic) IBOutlet UIButton *myButton;
@property (weak, nonatomic) IBOutlet UIImageView *myImageView;

@end

@implementation RiskAppraiseController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.myButton.layer.cornerRadius = 20.0;
    self.myButton.layer.masksToBounds = YES;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:LoadImage(@"back1") style:UIBarButtonItemStylePlain target:self action:@selector(popRootVC)];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"RiskAppraiseController"];//("PageOne"为页面名称，可自定义)
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"RiskAppraiseController"];
}

- (void)popRootVC
{
    [self.navigationController popViewControllerAnimated:YES];
}
//开始测评
- (IBAction)beginRisk:(UIButton *)sender
{
    RiskAppraiseNextController* riskVC = [[RiskAppraiseNextController alloc]initWithNibName:@"RiskAppraiseNextController" bundle:nil];
    riskVC.count = 0;
    [self.navigationController pushViewController:riskVC animated:YES];
}

@end
