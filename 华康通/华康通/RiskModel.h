//
//  RiskModel.h
//  华康通
//
//  Created by 雷雨 on 16/5/17.
//  Copyright © 2016年 华康集团. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RiskModel : NSObject

+ (NSDictionary*)riskQuestions;

@end
