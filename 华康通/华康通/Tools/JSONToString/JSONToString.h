//
//  JSONToString.h
//  华康通
//
//  Created by leiyu on 16/10/12.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSONToString : NSObject

//字典转换为字符串
+ (NSString*)dictionaryToJson:(NSDictionary *)dic;

@end
