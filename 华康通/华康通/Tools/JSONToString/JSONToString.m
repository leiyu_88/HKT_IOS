//
//  JSONToString.m
//  华康通
//
//  Created by leiyu on 16/10/12.
//  Copyright © 2016年 com.huakang. All rights reserved.
//

#import "JSONToString.h"

@implementation JSONToString

+ (NSString*)dictionaryToJson:(NSDictionary *)dic
{
    if (dic)
    {
        NSError *parseError = nil;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:&parseError];
//        NSLog(@"加密字符串+++++++++++++++++++%@++++++++++++++++++++++",[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]);
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    else
        return nil;
}

@end
