//
//  LYNetworkAlert.m
//  习本课堂
//
//  Created by 雷雨 on 16/11/5.
//  Copyright © 2016年 亮信科技. All rights reserved.
//

#import "LYNetworkAlert.h"

@interface LYNetworkAlert()

@property (nonatomic ,strong) UILabel* alertLabel;

@end
@implementation LYNetworkAlert

//弹出网络提示视图，并在3秒后动画移除视图
- (void)addAlertViewForScreenTopWithMessage:(NSString*)message
{
//    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleBlackTranslucent;
    self.alertLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, UISCREENWEITH, 20)];
    self.alertLabel.backgroundColor = YYColor;
    self.alertLabel.textColor = FFFFFFColor;
    self.alertLabel.text = message;
    self.alertLabel.textAlignment = NSTextAlignmentCenter;
    self.alertLabel.font = SetFont(14);
    [[UIApplication sharedApplication].keyWindow addSubview:self.alertLabel];
    dispatch_time_t delay = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5.0 * NSEC_PER_SEC));
    dispatch_after(delay, dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:1.0 animations:^{
            self.alertLabel.frame = CGRectMake(0, -20, UISCREENWEITH, 20);
        } completion:^(BOOL finished) {
            [self.alertLabel removeFromSuperview];
            self.alertLabel = nil;
//            [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
        }];
    });
}

@end
