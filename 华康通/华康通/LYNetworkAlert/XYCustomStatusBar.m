//
//  XYCustomStatusBar.m
//  习本课堂
//
//  Created by 雷雨 on 16/11/5.
//  Copyright © 2016年 亮信科技. All rights reserved.
//

#import "XYCustomStatusBar.h"

@implementation XYCustomStatusBar

- (void)dealloc
{
    _messageLabel = nil;
}

- (id)init
{
    if (self = [super init])
    {
        self.frame = [UIApplication sharedApplication].statusBarFrame;
        self.backgroundColor = YYColor;
        self.windowLevel = UIWindowLevelStatusBar + 1.0f;
        _messageLabel = [[UILabel alloc] initWithFrame:self.bounds];
        [_messageLabel setTextColor:FFFFFFColor];
        [_messageLabel setTextAlignment:NSTextAlignmentCenter];
        _messageLabel.font = SetFont(16.0);
        [_messageLabel setBackgroundColor:ClEARColor];
        [self addSubview:_messageLabel];
    }
    return self;
}

- (void)showStatusMessage:(NSString *)message
{
    self.hidden = NO;
    self.alpha = 1.0f;
    _messageLabel.text = @"";
    CGSize totalSize = self.frame.size;
    self.frame = (CGRect){ self.frame.origin, totalSize.width, 0 };
    [UIView animateWithDuration:0.5 animations:^{
        self.frame = (CGRect){self.frame.origin, totalSize };
    } completion:^(BOOL finished){
        self->_messageLabel.text = message;
        dispatch_time_t delay = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC));
        dispatch_after(delay, dispatch_get_main_queue(), ^{
            [self hide];
        });
    }];
}

- (void)hide{
    self.alpha = 1.0f;
    [UIView animateWithDuration:0.9f animations:^{
        self.alpha = 0.0f;
    } completion:^(BOOL finished){
        self->_messageLabel.text = @"";
        self.hidden = YES;
    }];
}
@end
