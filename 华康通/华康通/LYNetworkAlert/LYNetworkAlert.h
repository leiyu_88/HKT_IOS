//
//  LYNetworkAlert.h
//  习本课堂
//
//  Created by 雷雨 on 16/11/5.
//  Copyright © 2016年 亮信科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LYNetworkAlert : NSObject

//弹出网络提示视图，并在3秒后动画移除视图
- (void)addAlertViewForScreenTopWithMessage:(NSString*)message;

@end
