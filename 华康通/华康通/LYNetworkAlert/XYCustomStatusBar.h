//
//  XYCustomStatusBar.h
//  习本课堂
//
//  Created by 雷雨 on 16/11/5.
//  Copyright © 2016年 亮信科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XYCustomStatusBar : UIWindow
{
    UILabel *_messageLabel;
}
- (void)showStatusMessage:(NSString *)message;
- (void)hide;

@end
